import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import BackWithSearch from '../Elements/BackWithSearch.js';
import Ink from "react-ink";
import { Link } from "react-router-dom";

// fontawesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faQrcode } from '@fortawesome/free-solid-svg-icons';
import { faCamera } from '@fortawesome/free-solid-svg-icons';
import { faUniversity } from '@fortawesome/free-solid-svg-icons';
import { faArrowAltCircleRight } from '@fortawesome/free-solid-svg-icons';
import { faBiking } from '@fortawesome/free-solid-svg-icons';
import { faStore } from '@fortawesome/free-solid-svg-icons';
import { faAd } from '@fortawesome/free-solid-svg-icons';
import { faPeopleCarry } from '@fortawesome/free-solid-svg-icons';
import { faBtc } from '@fortawesome/free-brands-svg-icons';
import { faHotel } from '@fortawesome/free-solid-svg-icons';
import { faCreditCard } from '@fortawesome/free-regular-svg-icons';
import { faMobileAlt } from '@fortawesome/free-solid-svg-icons';
import { faHandHoldingHeart } from '@fortawesome/free-solid-svg-icons';
import { faTaxi } from '@fortawesome/free-solid-svg-icons';
import { faGrinHearts } from '@fortawesome/free-regular-svg-icons';
import { faWallet } from '@fortawesome/free-solid-svg-icons';
import { faBuromobelexperte } from '@fortawesome/free-brands-svg-icons';
import { faAlignJustify } from '@fortawesome/free-solid-svg-icons';
import { faThLarge } from '@fortawesome/free-solid-svg-icons';
import { faGripHorizontal } from '@fortawesome/free-solid-svg-icons';
import { faBarcode } from '@fortawesome/free-solid-svg-icons';
import { faUtensils } from '@fortawesome/free-solid-svg-icons';
import { faExchangeAlt } from '@fortawesome/free-solid-svg-icons';
import { faEuroSign } from '@fortawesome/free-solid-svg-icons';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { faGift } from '@fortawesome/free-solid-svg-icons';
import { faReceipt } from '@fortawesome/free-solid-svg-icons';

class FirstScreen extends Component {

    render() {
        if (window.innerWidth > 768) {
            return <Redirect to="/" />;
        }

        return (
            <React.Fragment>

                <BackWithSearch
                    boxshadow={true}
                    has_title={true}
                    // title={"Alle Apps"}
                    disbale_search={true}
                    back_to_home={false}
                    goto_orders_page={false}
                />

                {/* search apps */}
                <div style={{position:"absolute", top:"0px", right:"10px", width:"calc(100% - 80px)", zIndex:9}}>
                    <input placeholder="Alle Apps" type="text" style={{borderRadius:"5px", background:"#eee", width:"100%", marginTop:"12px", padding:"5px 10px", paddingLeft:"32px", border:"1px solid #ccc"}} />
                    <FontAwesomeIcon icon={faSearch} style={{position:"absolute", top:"21px", left:"10px", color:"#888"}} />
                </div>

                <div className="mb-200" style={{position: "relative", margin: "auto", overflow:"auto", marginTop:"-1px"}}>

                    <div style={{background:"#fff", padding:"10px 10px 15px"}}>

                        <div style={{textAlign:"center"}}><b>Meine Apps</b></div>

                        <ul className="homeAppsSmall">

                            <li onClick={() => window.location = "/transfer"}>
                                <FontAwesomeIcon icon={faExchangeAlt} className="withBg" style={{background:"#0F8CE9", color:"#fff", borderRadius:"5px"}} />
                                <div style={{display:"none"}}>Transfer</div>
                                <Ink duration="500" />
                            </li>

                            <li onClick={() => window.location = "https://lieferservice123.com"}>
                                <FontAwesomeIcon icon={faUtensils} className="withBg" style={{background:localStorage.getItem("storeColor"), color:"#fff", borderRadius:"5px"}} />
                                <div style={{display:"none"}}>Restaurants</div>
                                <Ink duration="500" />
                            </li>

                            <li onClick={() => window.location = ""}>
                                <FontAwesomeIcon icon={faStore} style={{color:"#8360c3"}} />
                                <div style={{display:"none"}}>Shops</div>
                                <Ink duration="500" />
                            </li>

                        </ul>

                        <div style={{border:"1px dashed #aaa", borderRadius:"2px", textAlign:"center", padding:"5px 10px 15px"}}>
                            <div style={{fontWeight:400, fontFamily:"open sans", fontSize:"40px", color:"#aaa", lineHeight:1}}>+</div>
                            <div style={{fontSize:"12px"}}>Hinzufügen</div>
                        </div>

                    </div>

                    <div style={{background:"#eee", height:"10px"}}></div>

                    <div style={{background:"#fff"}}>
                        <div className="mt-10" style={{textAlign:"center"}}><b>Alle Apps</b></div>

                        {/* recent */}
                        <div className="mt-10"><b style={{fontSize:"12px", padding:"10px"}}>Kürzlich</b></div>
                        <ul className="homeApps">
                            <li onClick={() => window.location = "/transfer"}>
                                <FontAwesomeIcon icon={faExchangeAlt} style={{background:"#0F8CE9", color:"#fff", padding:"0 5px", borderRadius:"5px", width:"30px", height:"30px"}} />
                                <div>Transfer</div>
                                <Ink duration="500" />
                            </li>

                            <li onClick={() => window.location = "https://lieferservice123.com"}>
                                <FontAwesomeIcon icon={faUtensils} style={{background:localStorage.getItem("storeColor"), color:"#fff", padding:"0 7px", borderRadius:"5px", width:"30px", height:"30px"}} />
                                <div>Restaurants</div>
                                <Ink duration="500" />
                            </li>

                            <li onClick={() => window.location = ""}>
                                <FontAwesomeIcon icon={faStore} style={{color:"#8360c3"}} />
                                <div>Shops</div>
                                <Ink duration="500" />
                            </li>

                            {/* <li>
                                <FontAwesomeIcon icon={faAd} style={{color:"#48A1DD"}} />
                                <div>Kleinanzeigen</div>
                                <Ink duration="500" />
                            </li>

                            <li>
                                <FontAwesomeIcon icon={faMobileAlt} style={{color:"#EF961D"}} />
                                <div>Guthaben</div>
                                <Ink duration="500" />
                            </li>

                            <li>
                                <FontAwesomeIcon icon={faPeopleCarry} style={{color:"#0AB4F3"}} />
                                <div>Dienstleistungen</div>
                                <Ink duration="500" />
                            </li>

                            <li>
                                <FontAwesomeIcon icon={faBtc} style={{color:"#F4B400"}} />
                                <div>Kryptomarkt</div>
                                <Ink duration="500" />
                            </li>

                            <li>
                                <FontAwesomeIcon icon={faHotel} style={{color:"#FF385C"}} />
                                <div>Unterkünfte</div>
                                <Ink duration="500" />
                            </li>

                            <li>
                                <FontAwesomeIcon icon={faTaxi} style={{color:"#E0AD02"}} />
                                <div>Taxi</div>
                                <Ink duration="500" />
                            </li>

                            <li>
                                <FontAwesomeIcon icon={faHandHoldingHeart} style={{color:"#F3016A"}} />
                                <div>Dating</div>
                                <Ink duration="500" />
                            </li> */}

                            <Link to="/apps">
                                <li>
                                    <FontAwesomeIcon icon={faGripHorizontal} style={{color:"#aaa"}} />
                                    <div>Mehr</div>
                                    <Ink duration="500" />
                                </li>
                            </Link>

                        </ul>

                        {/* fund transfer */}
                        <div className="mt-10"><b style={{fontSize:"12px", padding:"10px"}}>Geldtransfer</b></div>
                        <ul className="homeApps">

                            <li onClick={() => window.location = "/transfer"}>
                                <FontAwesomeIcon icon={faExchangeAlt} style={{background:"#0F8CE9", color:"#fff", padding:"0 5px", borderRadius:"5px", width:"30px", height:"30px"}} />
                                <div>Transfer</div>
                                <Ink duration="500" />
                            </li>

                            <li>
                                <FontAwesomeIcon icon={faGift} style={{color:"#FF385C"}} />
                                <div>Geschenkkarten</div>
                                <Ink duration="500" />
                            </li>

                            <Link to="/home/request"><li>
                                <FontAwesomeIcon icon={faEuroSign} style={{border:"2px solid #0F8CE9", color:"#0F8CE9", padding:"5px 0", borderRadius:"5px", width:"30px", height:"30px", borderBottomLeftRadius:"60%", borderBottomRightRadius:"60%"}}  />
                                <div>Anfordern</div>
                                <Ink duration="500" />
                            </li></Link>

                            <li>
                                <FontAwesomeIcon icon={faReceipt} style={{color:"#FF385C"}} />
                                <div>Go Dutch</div>
                                <Ink duration="500" />
                            </li>

                        </ul>

                        {/* third-party services */}
                        <div className="mt-10"><b style={{fontSize:"12px", padding:"10px"}}>Dienste von Drittanbietern</b></div>
                        <ul className="homeApps">
                            
                            <li onClick={() => window.location = "https://lieferservice123.com"}>
                                <FontAwesomeIcon icon={faUtensils} style={{background:localStorage.getItem("storeColor"), color:"#fff", padding:"0 7px", borderRadius:"5px", width:"30px", height:"30px"}} />
                                <div>Restaurants</div>
                                <Ink duration="500" />
                            </li>

                            <li onClick={() => window.location = ""}>
                                <FontAwesomeIcon icon={faStore} style={{color:"#8360c3"}} />
                                <div>Shops</div>
                                <Ink duration="500" />
                            </li>

                        </ul>
                    </div>
            
                </div>

            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
});

export default connect(
    mapStateToProps,
    {}
)(FirstScreen);
