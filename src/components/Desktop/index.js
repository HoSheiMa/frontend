import React, { Component } from "react";

import Footer from "./Footer";
import Hero from "./Hero";
import Meta from "../helpers/meta";
import StoreAchievements from "./StoreAchievements";
import { connect } from "react-redux";
import { getSettings } from "../../services/settings/actions";
import { getSingleLanguageData } from "../../services/translations/actions";

// fontawesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCaretUp } from '@fortawesome/free-solid-svg-icons';

//bootstrap
import ReactDOM from 'react-dom';
import { Modal } from 'react-bootstrap';

class Desktop extends Component {
    state = {
        showGdpr: false
    };
    componentDidMount() {
        if (!localStorage.getItem("storeColor")) {
            this.props.getSettings();
        }

        if (!localStorage.getItem("gdprAccepted")) {
            localStorage.setItem("gdprAccepted", "false");
            if (localStorage.getItem("showGdpr") === "true") {
                this.setState({ showGdpr: true });
            }
        }

        if (localStorage.getItem("showGdpr") === "true" && localStorage.getItem("gdprAccepted") === "false") {
            this.setState({ showGdpr: true });
        }
    }
    handleGdprClick = () => {
        localStorage.setItem("gdprAccepted", "true");
        this.setState({ showGdpr: false });
    };

    handleOnChange = event => {
        // console.log(event.target.value);
        this.props.getSingleLanguageData(event.target.value);
        localStorage.setItem("userPreferedLanguage", event.target.value);
    };

    componentWillReceiveProps(nextProps) {
        if (this.props.languages !== nextProps.languages) {
            if (localStorage.getItem("multiLanguageSelection") === "true") {
                if (localStorage.getItem("userPreferedLanguage")) {
                    this.props.getSingleLanguageData(localStorage.getItem("userPreferedLanguage"));
                    // console.log("Called 1");
                } else {
                    if (nextProps.languages.length) {
                        const id = nextProps.languages.filter(lang => lang.is_default === 1)[0].id;
                        this.props.getSingleLanguageData(id);
                    }
                }
            } else {
                // console.log("Called 2");
                if (nextProps.languages.length) {
                    const id = nextProps.languages.filter(lang => lang.is_default === 1)[0].id;
                    this.props.getSingleLanguageData(id);
                }
            }
        }
    }

    changeLang = (lang, langLong) => {
        localStorage.setItem("lang", lang);
        localStorage.setItem("langLong", langLong);
        window.location.reload();
    }

    showModalLang () {
        this.setState({
            langModal: true
        });
    }
    hideModalLang = () => {
        this.setState({
            langModal: false
        });
    }

    render() {

        return (
            <React.Fragment>
                {this.state.showGdpr && (
                    <div className="fixed-gdpr">
                        <span
                            dangerouslySetInnerHTML={{
                                __html: localStorage.getItem("gdprMessage")
                            }}
                        ></span>
                        <span>
                            <button
                                className="btn btn-sm ml-2"
                                style={{ backgroundColor: localStorage.getItem("storeColor") }}
                                onClick={this.handleGdprClick}
                            >
                                {localStorage.getItem("gdprConfirmButton")}
                            </button>
                        </span>
                    </div>
                )}
                <Meta
                    seotitle={"Download Paydizer"}
                    seodescription={"Verfügbar für alle Plattformen; Senden und Empfangen von Geld in Sekundenschnelle."}
                    ogtype="website"
                    ogtitle={"Paydizer"}
                    ogdescription={"Senden und Empfangen von Geld in Sekundenschnelle."}
                    ogurl={window.location.href}
                    twittertitle={"Paydizer"}
                    twitterdescription={"Senden und Empfangen von Geld in Sekundenschnelle."}
                />

                <Hero />

                {/* <StoreAchievements /> */}

                {/* <Footer /> */}

                {/* {localStorage.getItem("multiLanguageSelection") === "true" &&
                    (this.props.languages && this.props.languages.length > 0 && (
                        <div className="mt-4 d-flex align-items-center justify-content-center">
                            <div className="mr-2">{localStorage.getItem("changeLanguageText")}</div>
                            <select
                                onChange={this.handleOnChange}
                                defaultValue={
                                    localStorage.getItem("userPreferedLanguage")
                                        ? localStorage.getItem("userPreferedLanguage")
                                        : this.props.languages.filter(lang => lang.is_default === 1)[0].id
                                }
                                className="form-control language-select"
                            >
                                {this.props.languages.map(language => (
                                    <option value={language.id} key={language.id}>
                                        {language.language_name}
                                    </option>
                                ))}
                            </select>
                        </div>
                    ))} */}

                    <div style={{textAlign:"center", borderTop:"1px solid #eee", fontSize:"12px"}} className="p-20">

                        {/* <a href="#" className="m-2">Hilfe Center</a>
                        <a href="#" className="m-2">Über <FontAwesomeIcon icon={faCaretUp} /></a> */}
                        <a href="https://paydizer.com/store" target="_blank" className="m-2">Partner</a>
                        <a href="https://gitlab.com/paydizer" target="_blank" className="m-2">Developer</a>
                        <a href="#" className="m-2" onClick={() => this.showModalLang()}>{localStorage.getItem("langLong")?localStorage.getItem("langLong"):"Sprache"} <FontAwesomeIcon icon={faCaretUp} /></a>

                        <span className="ml-10 mr-10">Copyright © 2020 Paydizer All Rights Reserved.</span>

                        <a href="/pages/privacy" className="m-2">Datenschutz</a><span style={{padding:"2px"}}>|</span>
                        {/* <a href="/pages/cookies" className="m-2">Cookies</a><span style={{padding:"2px"}}>|</span> */}
                        <a href="/pages/agb" className="m-2">AGB</a><span style={{padding:"2px"}}>|</span>
                        <a href="/pages/impressum" className="m-2">Impressum</a>

                        <a
                            href={"https://twitter.com/paydizer"}
                            className="btn btn-sm btn-rounded btn-alt-secondary ml-10"
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            <i className="fa fa-fw fa-twitter" />
                        </a>

                    </div>

                    {/* select lang */}
                    <Modal show={this.state.langModal} onHide={this.hideModalLang} dialogClassName='modal-xl'>
                        
                        <Modal.Header closeButton>
                            <Modal.Title>Sprache auswählen</Modal.Title>
                        </Modal.Header>

                        <Modal.Body style={{padding:"0 15px 10px"}}>
                            <div className="selectLang">
                                <li onClick={() => this.changeLang("af", "Afrikaans")}>Afrikaans</li>
                                <li onClick={() => this.changeLang("sq", "Albanisch")}>Albanisch</li>
                                <li onClick={() => this.changeLang("am", "Amharisch")}>Amharisch</li>
                                <li onClick={() => this.changeLang("ar", "Arabisch")}>Arabisch</li>
                                <li onClick={() => this.changeLang("hy", "Armenisch")}>Armenisch</li>
                                <li onClick={() => this.changeLang("az", "Aserbaidschanisch")}>Aserbaidschanisch</li>
                                <li onClick={() => this.changeLang("eu", "Baskisch")}>Baskisch</li>
                                <li onClick={() => this.changeLang("bn", "Bengalisch")}>Bengalisch</li>
                                <li onClick={() => this.changeLang("my", "Birmanisch")}>Birmanisch</li>
                                <li onClick={() => this.changeLang("bs", "Bosnisch")}>Bosnisch</li>
                                <li onClick={() => this.changeLang("bg", "Bulgarisch")}>Bulgarisch</li>
                                <li onClick={() => this.changeLang("ceb", "Cebuano")}>Cebuano</li>
                                <li onClick={() => this.changeLang("ny", "Chichewa")}>Chichewa</li>
                                <li onClick={() => this.changeLang("zh-TW", "Chinesisch (traditionell)")}>Chinesisch (traditionell)</li>
                                <li onClick={() => this.changeLang("zh", "Chinesisch (vereinfacht)")}>Chinesisch (vereinfacht)</li>
                                <li onClick={() => this.changeLang("da", "Dänisch")}>Dänisch</li>
                                <li onClick={() => this.changeLang("de", "Deutsch")}>Deutsch</li>
                                <li onClick={() => this.changeLang("en", "Englisch")}>Englisch</li>
                                <li onClick={() => this.changeLang("eo", "Esperanto")}>Esperanto</li>
                                <li onClick={() => this.changeLang("et", "Estnisch")}>Estnisch</li>
                                <li onClick={() => this.changeLang("tl", "Filipino")}>Filipino</li>
                                <li onClick={() => this.changeLang("fr", "Französisch")}>Französisch</li>
                                <li onClick={() => this.changeLang("fy", "Friesisch")}>Friesisch</li>
                                <li onClick={() => this.changeLang("gl", "Galizisch")}>Galizisch</li>
                                <li onClick={() => this.changeLang("ka", "Georgisch")}>Georgisch</li>
                                <li onClick={() => this.changeLang("el", "Griechisch")}>Griechisch</li>
                                <li onClick={() => this.changeLang("gu", "Gujarati")}>Gujarati</li>
                                <li onClick={() => this.changeLang("ht", "Haitianisch")}>Haitianisch</li>
                                {/* <li>Hausa</li> */}
                                <li onClick={() => this.changeLang("haw", "Hawaiisch")}>Hawaiisch</li>
                                <li onClick={() => this.changeLang("he", "Hebräisch")}>Hebräisch</li>
                                <li onClick={() => this.changeLang("hi", "Hindi")}>Hindi</li>
                                <li onClick={() => this.changeLang("hmn", "Hmong")}>Hmong</li>
                                <li onClick={() => this.changeLang("ig", "Igbo")}>Igbo</li>
                                <li onClick={() => this.changeLang("id", "Indonesisch")}>Indonesisch</li>
                                <li onClick={() => this.changeLang("ga", "Irisch")}>Irisch</li>
                                <li onClick={() => this.changeLang("is", "Isländisch")}>Isländisch</li>
                                <li onClick={() => this.changeLang("it", "Italienisch")}>Italienisch</li>
                                <li onClick={() => this.changeLang("ja", "Japanisch")}>Japanisch</li>
                                <li onClick={() => this.changeLang("jv", "Javanisch")}>Javanisch</li>
                                <li onClick={() => this.changeLang("yi", "Jiddisch")}>Jiddisch</li>
                                <li onClick={() => this.changeLang("kn", "Kannada")}>Kannada</li>
                                <li onClick={() => this.changeLang("kk", "Kasachisch")}>Kasachisch</li>
                                <li onClick={() => this.changeLang("ca", "Katalanisch")}>Katalanisch</li>
                                <li onClick={() => this.changeLang("km", "Khmer")}>Khmer</li>
                                <li onClick={() => this.changeLang("rw", "Kinyarwanda")}>Kinyarwanda</li>
                                <li onClick={() => this.changeLang("ky", "Kirgisisch")}>Kirgisisch</li>
                                <li onClick={() => this.changeLang("ko", "Koreanisch")}>Koreanisch</li>
                                <li onClick={() => this.changeLang("co", "Korsisch")}>Korsisch</li>
                                <li onClick={() => this.changeLang("hr", "Kroatisch")}>Kroatisch</li>
                                <li onClick={() => this.changeLang("ku", "Kurmandschi")}>Kurdisch (Kurmandschi)</li>
                                <li onClick={() => this.changeLang("lo", "Lao")}>Lao</li>
                                <li onClick={() => this.changeLang("la", "Lateinisch")}>Lateinisch</li>
                                <li onClick={() => this.changeLang("lv", "Lettisch")}>Lettisch</li>
                                <li onClick={() => this.changeLang("lt", "Litauisch")}>Litauisch</li>
                                <li onClick={() => this.changeLang("lb", "Luxemburgisch")}>Luxemburgisch</li>
                                <li onClick={() => this.changeLang("mg", "Malagasy")}>Malagasy</li>
                                <li onClick={() => this.changeLang("ml", "Malayalam")}>Malayalam</li>
                                <li onClick={() => this.changeLang("ms", "Malaysisch")}>Malaysisch</li>
                                <li onClick={() => this.changeLang("mt", "Maltesisch")}>Maltesisch</li>
                                <li onClick={() => this.changeLang("mi", "Maori")}>Maori</li>
                                <li onClick={() => this.changeLang("mr", "Marathi")}>Marathi</li>
                                <li onClick={() => this.changeLang("mk", "Mazedonisch")}>Mazedonisch</li>
                                <li onClick={() => this.changeLang("mn", "Mongolisch")}>Mongolisch</li>
                                <li onClick={() => this.changeLang("ne", "Nepalesisch")}>Nepalesisch</li>
                                <li onClick={() => this.changeLang("nl", "Niederländisch")}>Niederländisch</li>
                                <li onClick={() => this.changeLang("no", "Norwegisch")}>Norwegisch</li>
                                <li onClick={() => this.changeLang("or", "Oriya")}>Odia (Oriya)</li>
                                <li onClick={() => this.changeLang("ps", "Paschtu")}>Paschtu</li>
                                <li onClick={() => this.changeLang("fa", "Persisch")}>Persisch</li>
                                <li onClick={() => this.changeLang("pl", "Polnisch")}>Polnisch</li>
                                <li onClick={() => this.changeLang("pt", "Portugiesisch")}>Portugiesisch</li>
                                <li onClick={() => this.changeLang("pa", "Punjabi")}>Punjabi</li>
                                <li onClick={() => this.changeLang("ro", "Rumänisch")}>Rumänisch</li>
                                <li onClick={() => this.changeLang("ru", "Russisch")}>Russisch</li>
                                <li onClick={() => this.changeLang("sm", "Samoanisch")}>Samoanisch</li>
                                <li onClick={() => this.changeLang("gd", "Schottisch-Gälisch")}>Schottisch-Gälisch</li>
                                <li onClick={() => this.changeLang("sv", "Schwedisch")}>Schwedisch</li>
                                <li onClick={() => this.changeLang("sr", "Serbisch")}>Serbisch</li>
                                <li onClick={() => this.changeLang("st", "Sesotho")}>Sesotho</li>
                                <li onClick={() => this.changeLang("sn", "Shona")}>Shona</li>
                                <li onClick={() => this.changeLang("sd", "Sindhi")}>Sindhi</li>
                                <li onClick={() => this.changeLang("si", "Singhalesisch")}>Singhalesisch</li>
                                <li onClick={() => this.changeLang("sk", "Slowakisch")}>Slowakisch</li>
                                <li onClick={() => this.changeLang("sl", "Slowenisch")}>Slowenisch</li>
                                <li onClick={() => this.changeLang("so", "Somali")}>Somali</li>
                                <li onClick={() => this.changeLang("es", "Spanisch")}>Spanisch</li>
                                <li onClick={() => this.changeLang("sw", "Suaheli")}>Suaheli</li>
                                <li onClick={() => this.changeLang("su", "Sundanesisch")}>Sundanesisch</li>
                                <li onClick={() => this.changeLang("tg", "Tadschikisch")}>Tadschikisch</li>
                                <li onClick={() => this.changeLang("ta", "Tamil")}>Tamil</li>
                                <li onClick={() => this.changeLang("tt", "Tatarisch")}>Tatarisch</li>
                                <li onClick={() => this.changeLang("te", "Telugu")}>Telugu</li>
                                <li onClick={() => this.changeLang("th", "Thailändisch")}>Thailändisch</li>
                                <li onClick={() => this.changeLang("cs", "Tschechisch")}>Tschechisch</li>
                                <li onClick={() => this.changeLang("tr", "Türkisch")}>Türkisch</li>
                                <li onClick={() => this.changeLang("tk", "Turkmenisch")}>Turkmenisch</li>
                                <li onClick={() => this.changeLang("ug", "Uigurisch")}>Uigurisch</li>
                                <li onClick={() => this.changeLang("uk", "Ukrainisch")}>Ukrainisch</li>
                                <li onClick={() => this.changeLang("hu", "Ungarisch")}>Ungarisch</li>
                                <li onClick={() => this.changeLang("ur", "Urdu")}>Urdu</li>
                                <li onClick={() => this.changeLang("uz", "Usbekisch")}>Usbekisch</li>
                                <li onClick={() => this.changeLang("vi", "Vietnamesisch")}>Vietnamesisch</li>
                                <li onClick={() => this.changeLang("cy", "Walisisch")}>Walisisch</li>
                                <li onClick={() => this.changeLang("be", "Weißrussisch")}>Weißrussisch</li>
                                <li onClick={() => this.changeLang("xh", "Xhosa")}>Xhosa</li>
                                <li onClick={() => this.changeLang("yo", "Yoruba")}>Yoruba</li>
                                <li onClick={() => this.changeLang("zu", "Zulu")}>Zulu</li>
                            </div>
                        </Modal.Body>
                    </Modal>

            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    settings: state.settings.settings,
    language: state.languages.language
});

export default connect(
    mapStateToProps,
    { getSettings, getSingleLanguageData }
)(Desktop);
