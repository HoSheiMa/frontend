import React, { Component } from "react";
import { getSinglePage, clearSinglePage } from "../../services/pages/actions";
import { connect } from "react-redux";
import Meta from "../helpers/meta";
// import Footer from "../Desktop/Footer";
import Footer from "../Takeaway/Items/Footer";
import BackWithSearch from "../Takeaway/Elements/BackWithSearch";
import ContentLoader from "react-content-loader";
import Nav from "../Takeaway/Nav";


class SinglePage extends Component {

    constructor() {
        super();

        this.state = { 
            bgWidth: window.innerWidth,
            bgHeight: (window.innerHeight / 4.2),
         };
    }

    static contextTypes = {
        router: () => null
    };

    componentDidMount() {
        this.props.clearSinglePage();
        var slug = window.location.href.split("/").pop();
        var store = window.location.href.replace("/"+slug, "").split("/").pop();
        if(store && store !== "pages"){
            // console.log("has store");
            this.props.getSinglePage(slug, this.props.restaurant_info.id);
        } else {
            // console.log("has not store");
            this.props.getSinglePage(slug, "");
        }

        this.timeout = setInterval(() => {
            this.setState({ 
                bgWidth: window.innerWidth,
                bgHeight: (window.innerHeight / 4.2)
            });
            
        }, 1000);
    }

    render() {
        const { single_page: page } = this.props;
        const { restaurant } = this.props;
        const {bgWidth, bgHeight} = this.state;


        return (
            <React.Fragment>
                {/* <BackWithSearch boxshadow={true} has_title={false} disbale_search={true} /> */}

                {JSON.parse(localStorage.getItem("state")).items.restaurant_info.name && <Nav 
                    shop={true} 
                    restaurant={this.props.restaurant_info} 
                    bgWidth={bgWidth} 
                    bgHeight={bgHeight} 
                    page={'SinglePage'} 
                />}
                

                {page.length === 0 ? (
                    <React.Fragment>
                        <div className="container-fluid p-0 pb-50">
                            <div className="container">
                                <div className="row">
                                    <div className="col-md-12 pt-80">
                                        <ContentLoader
                                            height={window.innerHeight}
                                            width={window.innerWidth}
                                            speed={1.2}
                                            primaryColor="#f3f3f3"
                                            secondaryColor="#ecebeb"
                                        >
                                            <rect x="20" y="25" rx="0" ry="0" width="155" height="22" />
                                            <rect x="20" y="120" rx="0" ry="0" width="180" height="18" />
                                            <rect x="20" y="145" rx="0" ry="0" width="380" height="18" />
                                            <rect x="20" y="170" rx="0" ry="0" width="300" height="18" />
                                            <rect x="20" y="195" rx="0" ry="0" width="150" height="18" />
                                            <rect x="20" y="220" rx="0" ry="0" width="350" height="18" />
                                            <rect x="20" y="245" rx="0" ry="0" width="150" height="18" />
                                        </ContentLoader>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </React.Fragment>
                ) : (
                    <React.Fragment>
                        <Meta
                            seotitle={page.name}
                            seodescription={localStorage.getItem("seoMetaDescription")}
                            ogtype="website"
                            ogtitle={page.name}
                            ogdescription={localStorage.getItem("seoOgDescription")}
                            ogurl={window.location.href}
                            twittertitle={page.name}
                            twitterdescription={localStorage.getItem("seoTwitterDescription")}
                        />

                        <div style={{background:"rgb(232, 233, 238)", overflow:"auto"}}>
                            <div style={{width:"100%", float:"left", fontSize:"18px", fontFamily:"open sans", fontWeight:300, margin:"15px 15px 0"}}>
                                {/* <span><a href={"/"+this.props.restaurant_info.slug}>Startseite</a></span>
                                <span> » {page.name}</span> */}
                            </div>
                        </div>

                        <div className="container-fluid pt-30 pb-50">
                            <div className="container">
                                <div className="row">
                                    <div className="col-md-12">

                                        {!JSON.parse(localStorage.getItem("state")).items.restaurant_info.name && <React.Fragment><h1 className="text-muted pt-80">{page.name}</h1><hr /></React.Fragment>}
                                        
                                        <div
                                            dangerouslySetInnerHTML={{
                                                __html: page.body
                                            }}
                                        ></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </React.Fragment>
                )}

                {/* {window.innerWidth >= 768 ? <Footer /> : null} */}
                {JSON.parse(localStorage.getItem("state")).items.restaurant_info.name && (
                    <Footer restaurant={JSON.parse(localStorage.getItem("state")).items.restaurant_info} />
                )}
                

            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    single_page: state.pages.single_page,
    restaurant_info: state.items.restaurant_info,

});

export default connect(
    mapStateToProps,
    { getSinglePage, clearSinglePage }
)(SinglePage);
