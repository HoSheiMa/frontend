import React, { Component } from "react";
import { Redirect } from "react-router";


import { Link } from "react-router-dom";

import { connect } from "react-redux";
import BackWithSearch from '../Elements/BackWithSearch.js';
import QrReader from 'react-qr-reader'
import Ink from "react-ink";

class Scan extends Component {
    state = {
        result: 'No result'
    };

    handleScan = data => {
        if (data) {
            this.setState({
                result: data
            })
            this.props.history.push("/home/scan/amount");

        }


    }
    handleError = err => {
        console.error(err)
    }

    render() {
        if (window.innerWidth > 768) {
            return <Redirect to="/" />;
        }

        if (localStorage.getItem("storeColor") === null) {
            return <Redirect to={"/"} />;
        }
        const { user } = this.props;

        if (!user.success) {
            return (
                //redirect to login page if not loggedin
                <Redirect to={"/login"} />
            );
        }

        return (
            <React.Fragment>

                <BackWithSearch
                    boxshadow={true}
                    has_title={true}
                    title={"Scan QR Code"}
                    disbale_search={true}
                    back_to_home={false}
                    goto_orders_page={false}
                    customBg={"#1D82D2"}
                />

                <div style={{background: "#1D82D2", minHeight:"800px", position: "relative", margin: "auto", overflow:"auto", color:"#fff"}}>

                    <div style={{textAlign:"center", background:"#fff", margin:"0 10px", borderRadius:"3px"}} className="mb-20">
                        <QrReader
                            delay={300}
                            onError={this.handleError}
                            onScan={this.handleScan}
                            style={{width:"100%", background:"#fff"}}
                        />
                        <p>{this.state.result}</p>
                    </div>



                    <div className="text-center mt-20" style={{float:"left", width:"50%"}}>
                        <Link to="/home/scan">
                            <div style={{cursor:"pointer",  position:"relative", maxWidth:"100px", margin:"auto", padding:"10px"}}>
                                <img src="https://lieferservice123.com/assets/img/home/scan-white.png" style={{width:"25px"}} />
                                <div className="mt-10">Scan & Pay</div>
                                <Ink duration="500" />
                            </div>
                        </Link>
                    </div>

                    <div className="text-center mt-20" style={{float:"left", width:"50%"}}>
                        <Link to="/home/pay">
                            <div style={{cursor:"pointer", opacity:0.5,position:"relative", maxWidth:"120px", margin:"auto", padding:"10px"}}>
                                <img src="https://lieferservice123.com/assets/img/home/qr-code-white3.png" style={{width:"25px"}} />
                                <div className="mt-10">Zahlungscode</div>
                                <Ink duration="500" />
                            </div>
                        </Link>
                    </div>

                </div>

            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user.user,
});

export default connect(
    mapStateToProps,
    {}
)(Scan);
