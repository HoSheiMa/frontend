import React, { Component } from "react";

import Jello from "react-reveal/Jello";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";

class Footer extends Component {
    state = {
        active_home: false,
        active_friends: false,
        active_account: false
    };

    componentDidMount() {
        if (this.props.active_home === true) {
            this.setState({ active_home: true });
        }
        if (this.props.active_friends === true) {
            this.setState({ active_friends: true });
        }
        if (this.props.active_account === true) {
            this.setState({ active_account: true });
        }
    }

    render() {
        const { cartTotal } = this.props;
        return (
            <React.Fragment>
                <p>{this.state.active}</p>
                <div className="content pt-10 py-5 font-size-xs clearfix footer-fixed">

                    <NavLink to={"/"} className={this.state.active_home ? "active-footer-tab-superapp col-4 footer-links" : "col-4 footer-links"}>
                        <i className="si si-home fa-2x" /> <br />
                        <span className={this.state.active_home ? "active-footer-tab-superapp" : ""}>
                            {this.state.active_home ? (
                                <Jello>Home</Jello>
                            ) : (
                                <span> Home</span>
                            )}
                        </span>
                    </NavLink>

                    <NavLink to="/home/friends" className={this.state.active_friends ? "active-footer-tab-superapp col-4 footer-links" : "col-4 footer-links"}>
                        <i className="si si-users fa-2x" /> <br />
                        <span className={this.state.active_friends ? "active-footer-tab-superapp" : ""}>
                            {this.state.active_friends ? (
                                <Jello>Freunde</Jello>
                            ) : (
                                <span> Freunde</span>
                            )}
                            {/* <span className="cart-quantity-badge" style={{ backgroundColor: localStorage.getItem("storeColor") }}>
                                {cartTotal.productQuantity}
                            </span> */}
                        </span>
                    </NavLink>

                    <NavLink to="/home/account" className={this.state.active_account ? "active-footer-tab-superapp col-4 footer-links" : "col-4 footer-links"}>
                        <i className="si si-user fa-2x" /> <br />
                        <span className={this.state.active_account ? "active-footer-tab-superapp" : ""}>
                            {this.state.active_account ? (
                                <Jello>{localStorage.getItem("footerAccount")}</Jello>
                            ) : (
                                <span> {localStorage.getItem("footerAccount")}</span>
                            )}
                        </span>
                    </NavLink>

                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    cartTotal: state.total.data
});

export default connect(
    mapStateToProps,
    {}
)(Footer);
