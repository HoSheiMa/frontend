    import React, { Component } from "react";
    import { connect } from "react-redux";
    import Ink from "react-ink";
    import { Link } from "react-router-dom";
    import { Redirect } from "react-router";
    import axios from "axios";
    
    //bootstrap
    import ReactDOM from 'react-dom';
    import { Modal, Button } from 'react-bootstrap';
    
    // fontawesome
    import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
    import { faCaretDown } from '@fortawesome/free-solid-svg-icons';
    
    class Login extends Component {
    
        constructor() {
            super();
    
            if(localStorage.getItem("country")){
                var country = localStorage.getItem("country");
            } else {
                var country = "";
            }
    
            if(localStorage.getItem("phoneCountryCode")){
                var phoneCountryCode = localStorage.getItem("phoneCountryCode");
            } else {
                var phoneCountryCode = "";
            }
    
            this.state = {
                country: country,
                phoneCountryCode: phoneCountryCode
            };
    
        }
    
        componentDidMount() {
    
            //set country if not set
            if (!localStorage.getItem("country")) {
                this.getCountry();
            }
            
        }
    
        //set country and phone prefix
        getCountry = () => {
            axios.get("https://lieferservice123.com/php/country.php", {}).then(response => {
                localStorage.setItem("country", response.data.country);
                localStorage.setItem("phoneCountryCode", response.data.phoneCountryCode);
                this.setState({
                    country: response.data.country,
                    phoneCountryCode: response.data.phoneCountryCode
                });
    
                if(response.data.country == "AT" || response.data.country == "DE" || response.data.country == "CH"){
                    localStorage.setItem("userPreferedLanguage", "1");
                } else {
                    localStorage.setItem("userPreferedLanguage", "2");
                }
            })
            .catch(function(error) {
                console.log(error);
            });
        }
    
        render() {
            if (window.innerWidth > 768) {
                return <Redirect to="/" />;
            }
    
            const { user } = this.props;
            if (user.success) {
                return (
                    <Redirect to={"/"} />
                );
            }
    
            return (
                <React.Fragment>
    
                    <div style={{padding:"18px", margin: "auto"}}>
    
                        {/* logo */}
                        <img src="https://paydizer.com/assets/img/pwa/paydizer/launchericons/android-launchericon-96-96.png" style={{width:"40px", borderRadius:"100%", float:"left"}} className="mr-10" />
                        <div style={{lineHeight:"1.2", float:"left", color:"#448AFF"}}>
                            <div style={{fontWeight:"bold", fontSize:"20px"}}>Paydizer</div>
                            <div style={{letterSpacing:"-0.5px", fontSize:"12px", textTransform:"lowercase", fontWeight:600, fontFamily:"open sans"}}>einfach bequem</div>
                        </div>
    
                        {/* language */}
                        <div style={{color:"#448AFF", float:"right", fontWeight:600, fontFamily:"open sans"}}>Sprache <FontAwesomeIcon icon={faCaretDown} /></div>
    
                        {/* brand name */}
                        <div style={{position:"relative", margin:"auto", fontStyle:"italic"}}>
                            <div className="mt-200 text-center" style={{color:"#448AFF", fontWeight:"bold", fontSize:"60px"}}>Paydizer</div>
                            <div style={{color:"#448AFF", fontWeight:"bold", margin:"auto", marginTop:"-20px", maxWidth:"250px", textAlign:"right"}}>Full-Service Plattform</div>
                        </div>
    
                        {/* enter phone number */}
                        <div className="mt-10" style={{position:"absolute", bottom:"18px", width:"calc(100% - 36px)"}}>
                            <Link to="/home/account/login/phoneLogin">
                                <div style={{ position:"relative"}} className="pb-10">
    
                                    <input type="tel" name="phone" style={{border:"1px solid #448AFF", borderRadius:"3px", fontSize:"16px", width:"100%", padding:"12px 10px", paddingLeft:"70px"}} autoComplete="off" placeholder="Telefonnummer eingeben" />
                                    
                                    <span style={{fontSize:"16px", padding:"10px", position:"absolute", top:"3px", left:"5px"}}>+{this.state.phoneCountryCode} <FontAwesomeIcon icon={faCaretDown} /></span> 
                                </div>
                            </Link>
    
                            <div className="mt-10"><Link to="/home/account/login/phoneLogin"><Button style={{fontSize:"18px", height:"50px", width:"100%", position:"relative"}}>Weiter<Ink duration="300" /></Button></Link></div>
                        </div>
    
    
                    </div>
    
                </React.Fragment>
            );
        }
    }
    
    const mapStateToProps = state => ({
        user: state.user.user,
    });
    
    export default connect(
        mapStateToProps,
        {}
    )(Login);
 