import React, { Component } from "react";
    import { connect } from "react-redux";
    import BackWithSearch from '../../Elements/BackWithSearch.js';
    import Ink from "react-ink";
    import { Redirect } from "react-router";
    import { sendSMSCode } from "../../../../services/user/actions";
    import axios from "axios";
    
    //bootstrap
    import ReactDOM from 'react-dom';
    import { Modal, Button } from 'react-bootstrap';
    
    // fontawesome
    import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
    import { faCaretDown } from '@fortawesome/free-solid-svg-icons';
    
    class PhoneLogin extends Component {
    
    
    
        constructor() {
            super();
    
            if(localStorage.getItem("fromTel")){
                var fromTel = localStorage.getItem("fromTel");
            } else {
                var fromTel = "";
            }
    
            this.state = {
                fromTel: fromTel
            };
           
            this.innerRef = React.createRef();
    
        }
    
        componentDidMount() {
    
            const { user } = this.props;
            if (user.success) {
                return (
                    <Redirect to={"/"} />
                );
            }
    
            //set country if not set
            if (!localStorage.getItem("country")) {
                this.getCountry();
            }
            
            setTimeout(() => {
                this.innerRef.current.focus();
            }, 100)
        }
    
        showModalCountryCode () {
            this.setState({
                CountryCodeModal: true
            });
        }
        hideModalCountryCode = () => {
            this.setState({
                CountryCodeModal: false
            });
        }
    
        //lang and country
        getCountry = () => {
            axios.get("https://paydizer.com/php/country.php", {}).then(response => {
                localStorage.setItem("country", response.data.country);
                localStorage.setItem("phoneCountryCode", response.data.phoneCountryCode);
    
                if(response.data == "AT"){
                    localStorage.setItem("userPreferedLanguage", "1");
                } 
                else if(response.data == "DE"){
                    localStorage.setItem("userPreferedLanguage", "1");
                } 
                else if(response.data == "CH"){
                    localStorage.setItem("userPreferedLanguage", "1");
                } else {
                    localStorage.setItem("userPreferedLanguage", "2");
                }
            })
            .catch(function(error) {
                console.log(error);
            });
        }
        changeCountry = (country, phoneCountryCode) => {
            if(country !== localStorage.getItem("country")){
                localStorage.removeItem("userSetAddress");
            }
    
            localStorage.setItem("country", country);
            localStorage.setItem("phoneCountryCode", phoneCountryCode);
            var url = window.location.href;
            window.location = url;
        }
    
        toogleMenu () {
            var x = document.getElementById("hamburgerMenu");
            if (x.style.display === "none") {
                x.style.display = "block";
            } else {
                x.style.display = "none";
            }
        }
    
        country_formatted_tel = (fromTel) => {        
            var fromTel = fromTel.trim();
            var first2 = fromTel.substr(0, 2);
            if(first2 == localStorage.getItem("phoneCountryCode")) {
                var fromTel = "+".fromTel;
                return fromTel;
            }
        
            var firstLetter = fromTel.substr(0, 1);
            if(firstLetter == "0") {
                var fromTel = "+"+localStorage.getItem("phoneCountryCode")+fromTel.substr(1);
                return fromTel;
            }
        
            return fromTel;
        }
    
        verifyPhone = () => {
    
            console.log("this.state.fromTel", this.state.fromTel);
            console.log("this.state.fromTel.length", this.state.fromTel.length);
    
            if(this.state.fromTel){
                this.props.sendSMSCode(this.country_formatted_tel(this.state.fromTel));
    
                if(this.state.fromTel == localStorage.getItem("fromTel")){
                } else {
                    localStorage.setItem("fromTel", this.state.fromTel);
                }
    
                setTimeout(() => {
                    window.location = "/home/account/login/phoneVerify"
    
                    // const { history } = this.props;
                    // this.props.history.push("/home/account/login/phoneVerify");
                }, 1500)
            }
        }
    
        handleTelChange = event => {
            this.setState({ fromTel: event.target.value });
        };
    
        render() {
            if (window.innerWidth > 768) {
                return <Redirect to="/" />;
            }
    
            const { user } = this.props;
            if (user.success) {
                return (
                    <Redirect to={"/"} />
                );
            }
    
            return (
                <React.Fragment>
    
                    <BackWithSearch
                        boxshadow={true}
                        has_title={true}
                        title={""}
                        disbale_search={true}
                        back_to_home={false}
                        goto_orders_page={false}
                    />
    
                    <div className="bg-white text-center" style={{maxWidth:"468px", margin: "auto", padding:"18px"}}>
    
                        <div style={{fontSize:"30px"}}>Login per Telefon</div>
    
                        {/* enter phone number */}
                        <div className="mt-20" style={{width:"100%"}}>
                            <div style={{ position:"relative"}} className="pb-10">
    
                                <input type="tel" name="phone" defaultValue={localStorage.getItem("fromTel")?localStorage.getItem("fromTel"):""} style={{border:"1px solid #448AFF", borderRadius:"3px", fontSize:"16px", width:"100%", padding:"12px 10px", paddingLeft:"70px"}} ref={this.innerRef} autoComplete="off" placeholder="Telefonnummer eingeben" onChange={this.handleTelChange} />
                                
                                <span onClick={() => this.showModalCountryCode()} style={{fontSize:"16px", padding:"10px", position:"absolute", top:"3px", left:"5px"}}>+{localStorage.getItem("phoneCountryCode")} <FontAwesomeIcon icon={faCaretDown} /></span> 
                            </div>
    
                            <div className="mt-10"><Button onClick={() => this.verifyPhone()} style={{fontSize:"18px", height:"50px", width:"100%", position:"relative"}}>Weiter<Ink duration="300" /></Button></div>
                        </div>
    
                    </div>
    
                {/* choose area code */}
                <Modal show={this.state.CountryCodeModal} onHide={this.hideModalCountryCode} size="sm">
                    
                    <Modal.Body style={{padding:"0"}}>
                        <div style={{color:"#8D8B8F", padding:"18px", fontWeight:300, fontFamily:"open sans"}}>Länder-Code</div>
                        <div style={{fontFamily:"open sans", fontWeight:600, fontSize:"15px", cursor:"pointer"}}>
    
                            <div onClick={() => { this.changeCountry("AT", 43); }} style={{borderTop:"1px solid #C4C1C3", padding:"18px", position:"relative"}}>Österreich<span style={{float:"right"}}>+43</span><Ink duration="300" /></div>
                            <div onClick={() => { this.changeCountry("DE", 49); }} style={{borderTop:"1px solid #C4C1C3", padding:"18px", position:"relative"}}>Deutschland<span style={{float:"right"}}>+49</span><Ink duration="300" /></div>
                            <div onClick={() => { this.changeCountry("CH", 41); }} style={{borderTop:"1px solid #C4C1C3", padding:"18px", position:"relative"}}>Schweiz<span style={{float:"right"}}>+41</span><Ink duration="300" /></div>
    
                            <div style={{color:localStorage.getItem("storeColor"), borderTop:"1px solid #C4C1C3", padding:"18px", position:"relative"}} onClick={() => this.hideModalCountryCode()}>Schließen<Ink duration="300" /></div>
                        </div>
                    </Modal.Body>
                </Modal>
    
                </React.Fragment>
            );
        }
    }
    
    const mapStateToProps = state => ({
        user: state.user.user,
    });
    
    export default connect(
        mapStateToProps,
        { sendSMSCode }
    )(PhoneLogin);