import React, { Component } from "react";
import { connect } from "react-redux";
import BackWithSearch from '../../../Elements/BackWithSearch.js';
import Ink from "react-ink";
import { Link } from "react-router-dom";
import { Redirect } from "react-router-dom";

// fontawesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUserAlt } from '@fortawesome/free-solid-svg-icons';
import { faCamera } from '@fortawesome/free-solid-svg-icons';
import { faEdit } from '@fortawesome/free-regular-svg-icons';
import { faPlusSquare } from '@fortawesome/free-regular-svg-icons';

class Homepage extends Component {

    toogleMenu () {
        var x = document.getElementById("hamburgerMenu");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }


    getAddress = (address, extract, long=true) => {
        if(extract == "str+nr"){
            var res = address;
            var split = res.split(",");
            if(long) var res = split.slice(0, split.length - 1).join(",");
            else var res = split[0];
            return res;
        } else if(extract == "str"){
            var res = address;
            var split = res.split(",");
            var res = split[0];
            return res;
        }  else if(extract == "plz+city"){
            var res = address;
            var split = res.split(",");
            var res = split[1];
            return res;
        } else if(extract == "plz"){
            var res = address;
            var split = res.split(",");
            var res = split[1];

            //only plz without city
            var split = res.split(" ");
            var res = split[1];
            return res;
        } else {
            var res = address.split(", ");
            return res[0];
        }
    }

    render() {
        if (window.innerWidth > 768) {
            return <Redirect to="/" />;
        }

        const { user, logoutDeliveryUser } = this.props;

        if (!user.success) {
            return (
                <Redirect to={"/home/account/login"} />
            );
        }

        return (
            <React.Fragment>

                <BackWithSearch
                    boxshadow={true}
                    has_title={true}
                    title={""}
                    disbale_search={true}
                    back_to_home={false}
                    goto_orders_page={false}
                />

                <div className="pb-200" style={{background:"#f6f6f6", minHeight:"800px", margin: "auto", overflow:"auto"}}>

                    {/* <div style={{opacity:"0.5", background:"#ccc", borderBottom:"1px solid #ccc"}}><FontAwesomeIcon icon={faUserAlt} style={{padding:"20px", height:"150px", width:"100%"}} /></div> */}

                    <div className="bg-white" style={{padding:"20px"}}>
                        <div style={{zIndex:9, position:"relative", background:"#eee", border:"3px solid #ccc", textAlign:"center", maxWidth:"120px", margin:"auto", padding:"10px 5px 10px"}}>
                            <FontAwesomeIcon icon={faCamera} style={{fontSize:"50px"}} />
                            <div className="mt-5" style={{fontSize:"12px"}}><b>Foto hochladen</b></div>
                        </div>
                    </div>

                    <div style={{padding:"20px"}} className="mt-10 bg-white">
                        <div className="text-center" style={{color:"#666", fontSize:"16px"}}>Username festlegen <FontAwesomeIcon icon={faEdit} style={{color:"blue"}} /></div>
                        <div className="mt-5 text-center" style={{color:"blue"}}>What's up</div>
                    </div>

                    <div style={{padding:"20px"}} className="mt-10 bg-white">

                        <div>
                            <span style={{color:"#999", width:"120px", display:"inline-block"}}>Paydizer Konto</span>
                            {user.data.phone}
                        </div>

                        <div className="mt-10">
                            <span style={{color:"#999", width:"120px", display:"inline-block"}}>Echter Name</span>
                            {user.data.name ? user.data.name : <span style={{color:"blue"}}>Nicht verifiziert</span>}
                        </div>

                        <div className="mt-10">
                            <span style={{color:"#999", width:"120px", display:"inline-block"}}>Region</span>
                            {localStorage.getItem("userSetAddress") ? this.getAddress(JSON.parse(localStorage.getItem("userSetAddress")).address, "plz+city") : <span style={{color:"blue"}}>Region festlegen</span>}
                        </div>

                        <div className="mt-10">
                            <span style={{color:"blue"}}><FontAwesomeIcon icon={faPlusSquare} className="mr-2" />Persönliche Informationen hinzufügen</span>
                        </div>

                    </div>

                    <div style={{padding:"5px", background:"#F6F6F6"}}></div>

                    <Link to="">
                        <div className="category-list-item bg-white" style={{borderBottom:"1px solid #eee", padding:"10px 18px 5px", position:"relative"}}>
                            <div className="display-flex py-2">
                                <div className="flex-auto border-0">Meine Momente aufnehmen</div>
                                <div className="flex-auto text-right"><i className="si si-camera" style={{fontSize:"18px", background:"#eee", padding:"8px", borderRadius:"100%", color:"#666"}} /></div>
                            </div>
                            <Ink duration="500" />
                        </div>
                    </Link>

 
                </div>

            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user.user,
});

export default connect(
    mapStateToProps,
    {  }
)(Homepage);
