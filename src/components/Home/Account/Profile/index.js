import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import BackWithSearch from '../../Elements/BackWithSearch.js';
import Ink from "react-ink";
import { Redirect } from "react-router";

//bootstrap
import ReactDOM from 'react-dom';
import { Modal, Button } from 'react-bootstrap';

// fontawesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import { faMinusCircle } from '@fortawesome/free-solid-svg-icons';
import { faQrcode } from '@fortawesome/free-solid-svg-icons';

class Activate extends Component {

    toogleMenu () {
        var x = document.getElementById("hamburgerMenu");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }

    
    getAddress = (address, extract, long=true) => {
        if(extract == "str+nr"){
            var res = address;
            var split = res.split(",");
            if(long) var res = split.slice(0, split.length - 1).join(",");
            else var res = split[0];
            return res;
        } else if(extract == "str"){
            var res = address;
            var split = res.split(",");
            var res = split[0];
            return res;
        }  else if(extract == "plz+city"){
            var res = address;
            var split = res.split(",");
            var res = split[1];
            return res;
        } else if(extract == "plz"){
            var res = address;
            var split = res.split(",");
            var res = split[1];

            //only plz without city
            var split = res.split(" ");
            var res = split[1];
            return res;
        } else {
            var res = address.split(", ");
            return res[0];
        }
    }

    render() {
        if (window.innerWidth > 768) {
            return <Redirect to="/" />;
        }

        if (localStorage.getItem("storeColor") === null) {
            return <Redirect to={"/"} />;
        }
        const { user } = this.props;

        if (!user.success) {
            return (
                //redirect to login page if not loggedin
                <Redirect to={"/login"} />
            );
        }

        const { cartTotal } = this.props;

        return (
            <React.Fragment>

                <BackWithSearch
                    boxshadow={true}
                    has_title={true}
                    title={"Mein Profil"}
                    disbale_search={true}
                    back_to_home={false}
                    goto_orders_page={false}
                    blueBg={true}
                />

                <div className="pb-200" style={{background:"#f6f6f6", minHeight:"800px", margin: "auto", overflow:"auto"}}>

                    <Link to="/home/account/profile/homepage">
                        <div className="category-list-item bg-white" style={{borderBottom:"1px solid #eee", padding:"10px 18px", position:"relative"}}>
                            <div className="display-flex py-2">
                                <div className="flex-auto border-0">Profil Homepage</div>

                                {/* avatar */}
                                <div style={{right:"40px", top:"10px", position:"absolute", width:"40px", background:"#eee", display:"inline-block", padding:"5px 10px", borderRadius:"5px", position:"absolute"}}>

                                    <FontAwesomeIcon icon={faUser} style={{color:"#666", fontSize:"20px"}} />

                                    <div style={{background:"rgba(0, 0, 0, 0.5)", position:"absolute", bottom:0, left:0, color:"#fff", display:"inline-block", borderBottomLeftRadius:"5px", borderBottomRightRadius:"5px", fontSize:"9px", textAlign:"center", width:"40px"}}>Avatar</div>

                                </div>
                                <div className="flex-auto text-right"><i className="si si-arrow-right" /></div>
                            </div>
                            <Ink duration="500" />
                        </div>
                    </Link>

                    <Link to="/home/account/identityVerification">
                        <div className="category-list-item bg-white" style={{borderBottom:"1px solid #eee", padding:"10px 18px", position:"relative"}}>
                            <div className="display-flex py-2">
                                <div className="flex-auto border-0">Echtnamen-Verifizierung</div>
                                <div style={{right:"40px", top:"16px", position:"absolute", display:"inline-block", padding:"2px 5px", borderRadius:"5px", border:"1px solid #ccc", position:"absolute", fontSize:"10px"}}>
                                    <span><FontAwesomeIcon icon={faMinusCircle} style={{color:"#999"}} /> Unverifiziert</span>
                                </div>
                                <div className="flex-auto text-right"><i className="si si-arrow-right" /></div>
                            </div>
                            <Ink duration="500" />
                        </div>
                    </Link>

                    <Link to="/home/account/profile/phoneNumber">
                        <div className="category-list-item bg-white" style={{borderBottom:"1px solid #eee", padding:"10px 18px", position:"relative"}}>
                            <div className="display-flex py-2">
                                <div className="flex-auto border-0">Telefonnummer</div>
                                <div style={{right:"40px", top:"12px", color:"#999", position:"absolute", display:"inline-block", padding:"5px", position:"absolute"}}>
                                    <span>{user.data.phone}</span>
                                </div>
                                <div className="flex-auto text-right"><i className="si si-arrow-right" /></div>
                            </div>
                            <Ink duration="500" />
                        </div>
                    </Link>

                    <Link to="/home/account/profile/qrCode">
                        <div className="category-list-item bg-white" style={{borderBottom:"1px solid #eee", padding:"10px 18px", position:"relative"}}>
                            <div className="display-flex py-2">
                                <div className="flex-auto border-0">Mein QR Code</div>
                                <div style={{right:"40px", top:"13px", fontSize:"18px", position:"absolute", display:"inline-block"}}>
                                    <span><FontAwesomeIcon icon={faQrcode} style={{color:"#999"}} /></span>
                                </div>
                                <div className="flex-auto text-right"><i className="si si-arrow-right" /></div>
                            </div>
                            <Ink duration="500" />
                        </div>
                    </Link>

                    <div style={{padding:"5px", background:"#F6F6F6"}}></div>

                    <Link to="/home/account/profile/address">
                        <div className="category-list-item bg-white" style={{borderBottom:"1px solid #eee", padding:"10px 18px", position:"relative"}}>
                            <div className="display-flex py-2">
                                <div className="flex-auto border-0">Lieferadresse</div>
                                <div style={{right:"40px", top:"16px", position:"absolute", display:"inline-block", padding:"2px 5px", borderRadius:"5px", border:"1px solid #ccc", position:"absolute", fontSize:"10px"}}>
                                    <span>{localStorage.getItem("userSetAddress")?this.getAddress(JSON.parse(localStorage.getItem("userSetAddress")).address, "str+nr"):"Keine Adresse gefunden"}</span>
                                </div>
                                <div className="flex-auto text-right"><i className="si si-arrow-right" /></div>
                            </div>
                            <Ink duration="500" />
                        </div>
                    </Link>

                    {/* <div style={{padding:"5px", background:"#F6F6F6"}}></div>

                    <Link to="">
                        <div className="category-list-item bg-white" style={{borderBottom:"1px solid #eee", padding:"10px 18px", position:"relative"}}>
                            <div className="display-flex py-2">
                                <div className="flex-auto border-0">Einladungen</div>
                                <div style={{right:"40px", top:"17px", position:"absolute", display:"inline-block", position:"absolute", fontSize:"12px", background:"#F3016A", borderRadius:"15px", padding:"0 5px", color:"#fff"}}>
                                    <span>Geschenkkarten</span>
                                </div>
                                <div className="flex-auto text-right"><i className="si si-arrow-right" /></div>
                            </div>
                            <Ink duration="500" />
                        </div>
                    </Link>

                    <Link to="">
                        <div className="category-list-item bg-white" style={{borderBottom:"1px solid #eee", padding:"10px 18px", position:"relative"}}>
                            <div className="display-flex py-2">
                                <div className="flex-auto border-0">Favoriten</div>
                                <div className="flex-auto text-right"><i className="si si-arrow-right" /></div>
                            </div>
                            <Ink duration="500" />
                        </div>
                    </Link> */}

                </div>

            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    cartTotal: state.total.data,
    user: state.user.user,

});

export default connect(
    mapStateToProps,
    {}
)(Activate);
