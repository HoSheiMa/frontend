import React, { Component } from "react";
import { connect } from "react-redux";
import BackWithSearch from '../../Elements/BackWithSearch.js';
import Ink from "react-ink";
import { Link } from "react-router-dom";
import { Redirect } from "react-router-dom";
import { logoutUser } from "../../../../services/user/actions";

// fontawesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMinusCircle } from '@fortawesome/free-solid-svg-icons';

class Notifications extends Component {

    toogleMenu () {
        var x = document.getElementById("hamburgerMenu");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }

    render() {
        if (window.innerWidth > 768) {
            return <Redirect to="/" />;
        }

        const { user, logoutDeliveryUser } = this.props;

        if (!user.success) {
            return (
                <Redirect to={"/home/account/login"} />
            );
        }

        return (
            <React.Fragment>

                <BackWithSearch
                    boxshadow={true}
                    has_title={true}
                    title={"Benachrichtigungen"}
                    disbale_search={true}
                    back_to_home={false}
                    goto_orders_page={false}
                />

                <div className="pb-200" style={{background:"#f6f6f6", minHeight:"800px", margin: "auto", overflow:"auto"}}>

                    <div style={{padding:"10px", background:"#F6F6F6"}}></div>

                    <div className="pl-20 mb-10" style={{color:"#999"}}>Wann möchten Sie benachrichtigt werden?</div>

                    <Link to="">
                        <div className="category-list-item bg-white" style={{borderBottom:"1px solid #eee", padding:"5px 18px", position:"relative"}}>
                            <div className="display-flex py-2">
                                <div className="flex-auto border-0">
                                    System-Benachrichtigungen
                                    <div style={{fontWeight:300, fontFamily:"open sans"}}>Systemservice, Guthabenänderungen, Rückerstattungen...</div>
                                </div>
                                <div className="flex-auto text-right"><i className="si si-arrow-right" /></div>
                            </div>
                            <Ink duration="500" />
                        </div>
                    </Link>

                    <Link to="">
                        <div className="category-list-item bg-white" style={{borderBottom:"1px solid #eee", padding:"5px 18px", position:"relative"}}>
                            <div className="display-flex py-2">
                                <div className="flex-auto border-0">
                                    Event-Benachrichtigungen
                                    <div style={{fontWeight:300, fontFamily:"open sans"}}>Beliebte Events und spezielle Angebote</div>
                                </div>
                                <div className="flex-auto text-right"><i className="si si-arrow-right" /></div>
                            </div>
                            <Ink duration="500" />
                        </div>
                    </Link>

                    <Link to="">
                        <div className="category-list-item bg-white" style={{borderBottom:"1px solid #eee", padding:"5px 18px", position:"relative"}}>
                            <div className="display-flex py-2">
                                <div className="flex-auto border-0">
                                    Erinnerung bei Nachrichten von Freunden
                                    <div style={{fontWeight:300, fontFamily:"open sans"}}>Nachrichten von Freunden</div>
                                </div>
                                <div className="flex-auto text-right"><i className="si si-arrow-right" /></div>
                            </div>
                            <Ink duration="500" />
                        </div>
                    </Link>

                    <div style={{padding:"5px", background:"#F6F6F6"}}></div>

                    <div className="pl-20 mb-10 mt-10" style={{color:"#999"}}>Beim Empfangen von Nachrichten</div>

                    <Link to="">
                        <div className="category-list-item bg-white" style={{borderBottom:"1px solid #eee", padding:"5px 18px", position:"relative"}}>
                            <div className="display-flex py-2">
                                <div className="flex-auto border-0">
                                    Sound
                                </div>
                                <div style={{right:"40px", top:"6px", color:"#999", position:"absolute", display:"inline-block", padding:"5px", position:"absolute"}}>
                                    <span>Einstellen</span>
                                </div>
                                <div className="flex-auto text-right"><i className="si si-arrow-right" /></div>
                            </div>
                            <Ink duration="500" />
                        </div>
                    </Link>

                    <Link to="">
                        <div className="category-list-item bg-white" style={{borderBottom:"1px solid #eee", padding:"5px 18px", position:"relative"}}>
                            <div className="display-flex py-2">
                                <div className="flex-auto border-0">
                                    Vibrieren
                                </div>
                                <div style={{right:"40px", top:"6px", color:"#999", position:"absolute", display:"inline-block", padding:"5px", position:"absolute"}}>
                                    <span>Einstellen</span>
                                </div>
                                <div className="flex-auto text-right"><i className="si si-arrow-right" /></div>
                            </div>
                            <Ink duration="500" />
                        </div>
                    </Link>

                    <Link to="">
                        <div className="category-list-item bg-white" style={{borderBottom:"1px solid #eee", padding:"5px 18px", position:"relative"}}>
                            <div className="display-flex py-2">
                                <div className="flex-auto border-0">
                                    Nachrichteninhalt anzeigen
                                </div>
                                <div className="flex-auto text-right"><i className="si si-arrow-right" /></div>
                            </div>
                            <Ink duration="500" />
                        </div>
                    </Link>

                    <div style={{padding:"5px", background:"#F6F6F6"}}></div>

                    <div className="pl-20 mb-10 mt-10" style={{color:"#999"}}>Beim Empfangen von QR Code Zahlungen</div>

                    <Link to="">
                        <div className="category-list-item bg-white" style={{borderBottom:"1px solid #eee", padding:"5px 18px", position:"relative"}}>
                            <div className="display-flex py-2">
                                <div className="flex-auto border-0">
                                    Klingelton für Qrcode-Zahlungen
                                </div>
                                <div className="flex-auto text-right"><i className="si si-arrow-right" /></div>
                            </div>
                            <Ink duration="500" />
                        </div>
                    </Link>

                </div>

            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user.user,
});

export default connect(
    mapStateToProps,
    { logoutUser }
)(Notifications);
