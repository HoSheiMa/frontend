import React, { Component } from "react";
import { connect } from "react-redux";
import BackWithSearch from '../../Elements/BackWithSearch.js';
import Ink from "react-ink";
import { Link } from "react-router-dom";
import { Redirect } from "react-router-dom";
import { logoutUser } from "../../../../services/user/actions";

// fontawesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMinusCircle, faUserFriends } from '@fortawesome/free-solid-svg-icons';
import { faUndo } from '@fortawesome/free-solid-svg-icons';
import { faAddressBook } from '@fortawesome/free-regular-svg-icons';
import { faUsers } from '@fortawesome/free-solid-svg-icons';

class FunctionalManagement extends Component {

    toogleMenu () {
        var x = document.getElementById("hamburgerMenu");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }

    render() {
        if (window.innerWidth > 768) {
            return <Redirect to="/" />;
        }

        const { user, logoutDeliveryUser } = this.props;

        if (!user.success) {
            return (
                <Redirect to={"/home/account/login"} />
            );
        }

        return (
            <React.Fragment>

                <BackWithSearch
                    boxshadow={true}
                    has_title={true}
                    title={"Funktionsmanagement"}
                    disbale_search={true}
                    back_to_home={false}
                    goto_orders_page={false}
                />

                <div className="pb-200" style={{background:"#f6f6f6", minHeight:"800px", margin: "auto", overflow:"auto"}}>

                    <div style={{padding:"10px", background:"#F6F6F6"}}></div>

                    <Link to="">
                        <div className="category-list-item bg-white" style={{borderBottom:"1px solid #eee", padding:"5px 18px", position:"relative"}}>
                            <div className="display-flex py-2">
                                <div className="flex-auto border-0">
                                    <FontAwesomeIcon icon={faUndo} style={{background:localStorage.getItem("storeColor"), color:"#fff", padding:"0 7px", borderRadius:"5px", width:"30px", height:"30px"}} />
                                    <div style={{marginTop:"5px", display:"inline-block", position:"absolute", marginLeft:"10px"}}>Shortcut-Einstellungen</div>
                                </div>
                                <div className="flex-auto text-right"><i className="si si-arrow-right" /></div>
                            </div>
                            <Ink duration="500" />
                        </div>
                    </Link>

                    <div style={{padding:"5px", background:"#F6F6F6"}}></div>

                    <Link to="">
                        <div className="category-list-item bg-white" style={{borderBottom:"1px solid #eee", padding:"5px 18px", position:"relative"}}>
                            <div className="display-flex py-2">
                                <div className="flex-auto border-0">
                                    <FontAwesomeIcon icon={faAddressBook} style={{background:localStorage.getItem("storeColor"), color:"#fff", padding:"0 7px", borderRadius:"5px", width:"30px", height:"30px"}} /> 
                                    <div style={{marginTop:"5px", display:"inline-block", position:"absolute", marginLeft:"10px"}}>Leute die du kennst</div>
                                </div>
                                <div className="flex-auto text-right"><i className="si si-arrow-right" /></div>
                            </div>
                            <Ink duration="500" />
                        </div>
                    </Link>


                    <Link to="">
                        <div className="category-list-item bg-white" style={{borderBottom:"1px solid #eee", padding:"5px 18px", position:"relative"}}>
                            <div className="display-flex py-2">
                                <div className="flex-auto border-0">
                                    <FontAwesomeIcon icon={faUserFriends} style={{background:localStorage.getItem("storeColor"), color:"#fff", padding:"0 7px", borderRadius:"5px", width:"30px", height:"30px"}} />
                                    <div style={{marginTop:"5px", display:"inline-block", position:"absolute", marginLeft:"10px"}}>Freund-Aktivitäten</div>
                                </div>
                                <div className="flex-auto text-right"><i className="si si-arrow-right" /></div>
                            </div>
                            <Ink duration="500" />
                        </div>
                    </Link>

                </div>

            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user.user,
});

export default connect(
    mapStateToProps,
    { logoutUser }
)(FunctionalManagement);
