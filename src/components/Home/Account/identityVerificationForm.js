import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import BackWithSearch from '../Elements/BackWithSearch.js';
import Ink from "react-ink";

//bootstrap
import ReactDOM from 'react-dom';
import { Modal, Button } from 'react-bootstrap';

// fontawesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShieldAlt } from '@fortawesome/free-solid-svg-icons';

class IdentityVerificationForm extends Component {

    toogleMenu () {
        var x = document.getElementById("hamburgerMenu");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }

    render() {
        if (window.innerWidth > 768) {
            return <Redirect to="/" />;
        }

        return (
            <React.Fragment>

                <BackWithSearch
                    boxshadow={true}
                    has_title={true}
                    title={"Identitätsprüfung"}
                    disbale_search={true}
                    back_to_home={false}
                    goto_orders_page={false}
                />

                <div className="mb-200" style={{margin: "auto", overflow:"auto"}}>

                        <div style={{padding:"10px 20px 5px"}}>
                            <div className="pt-10" style={{fontSize:"20px", fontWeight:600, fontFamily:"open sans"}}>Informationen hinzufügen</div>

                            <div className="mt-20">Vollständiger Name</div>
                            <div><input type="text" name="firstname" placeholder="Vorname" className="form-control mt-10" /></div>
                            <div><input type="text" name="lastname" placeholder="Nachname" className="form-control mt-10" /></div>

                            <div className="mt-20">Ausweisnummer</div>
                            <div><input type="text" name="passport_number" placeholder="Ausweisnummer eingeben" className="form-control mt-10" /></div>

                            <div className="mt-20">Geburtsdatum</div>
                            <div><input type="text" name="dob" placeholder="Datum auswählen" className="form-control mt-10" /></div>

                            <div className="mt-20">Ausweis-Ablaufdatum</div>
                            <div><input type="text" name="dob" placeholder="Datum auswählen" className="form-control mt-10" /></div>
                        </div>

                        <div className="m-20"><Button style={{fontSize:"18px", height:"50px", width:"100%", position:"relative"}}>Absenden<Ink duration="300" /></Button></div>

                        <div className="text-center" style={{padding:"20px", fontSize:"12px"}}><FontAwesomeIcon icon={faShieldAlt} /> Wir benötigen Informationen für eine Identitätsprüfung. Alle Daten werden auf der Cloud sicher verschlüsselt.</div>

            
                </div>

            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
});

export default connect(
    mapStateToProps,
    {}
)(IdentityVerificationForm);
