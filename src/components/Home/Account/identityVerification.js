import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import BackWithSearch from '../Elements/BackWithSearch.js';
import Ink from "react-ink";
import { Link } from "react-router-dom";

//bootstrap
import ReactDOM from 'react-dom';
import { Modal, Button } from 'react-bootstrap';

// fontawesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShieldAlt } from '@fortawesome/free-solid-svg-icons';

class IdentityVerification extends Component {


    toogleMenu () {
        var x = document.getElementById("hamburgerMenu");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }

    render() {
        if (window.innerWidth > 768) {
            return <Redirect to="/" />;
        }

        return (
            <React.Fragment>

                <BackWithSearch
                    boxshadow={true}
                    has_title={true}
                    title={""}
                    disbale_search={true}
                    back_to_home={false}
                    goto_orders_page={false}
                />

                <div className="mb-200" style={{margin: "auto", overflow:"auto"}}>

                        <div className="text-center mt-50">
                            <FontAwesomeIcon icon={faShieldAlt} style={{fontSize:"40px", color:"#0F8CE9"}} />
                            <div className="pt-10" style={{fontSize:"20px", fontWeight:600, fontFamily:"open sans"}}>Identitätsprüfung</div>
                            <div className="mt-10 mb-30" style={{padding:"0 20px"}}>Bevor Sie diesen Service nutzen können, müssen Sie Ihren Namen, Ihre Ausweisnummer und andere Informationen für die Identitätsprüfung verifizieren.</div>
                        </div>

                        <Link to="/home/account/identityVerificationForm">
                            <div className="m-20 mt-30"><Button style={{fontSize:"18px", height:"50px", width:"100%", position:"relative"}}>Verifizierung starten<Ink duration="300" /></Button></div>
                        </Link>

                        <div className="text-center mt-50" style={{padding:"20px", fontSize:"12px"}}><FontAwesomeIcon icon={faShieldAlt} /> Wir benötigen Informationen für eine Identitätsprüfung. Alle Daten werden auf der Cloud sicher verschlüsselt.</div>

            
                </div>

            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
});

export default connect(
    mapStateToProps,
    {}
)(IdentityVerification);
