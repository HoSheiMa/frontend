import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import BackWithSearch from '../Elements/BackWithSearch.js';
import Ink from "react-ink";
import { Redirect } from "react-router";

// fontawesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlusCircle } from '@fortawesome/free-solid-svg-icons';
import { faMinusCircle } from '@fortawesome/free-solid-svg-icons';

class Wallet extends Component {

    render() {
        if (window.innerWidth > 768) {
            return <Redirect to="/" />;
        }

        if (localStorage.getItem("storeColor") === null) {
            return <Redirect to={"/"} />;
        }
        const { user } = this.props;

        if (!user.success) {
            return (
                //redirect to login page if not loggedin
                <Redirect to={"/login"} />
            );
        }

        return (
            <React.Fragment>

                <BackWithSearch
                    boxshadow={true}
                    has_title={true}
                    title={"Guthaben"}
                    disbale_search={true}
                    back_to_home={false}
                    goto_orders_page={false}
                    customBg={"#1D82D2"}
                />

                <div style={{background: "#1D82D2", minHeight:"800px", position: "relative", margin: "auto", overflow:"auto"}}>

                    {/* add bank card */}
                    {/* <div style={{textAlign:"center"}}>
                        <div style={{fontSize: "12px", lineHeight: 1.5, marginBottom:"20px", color:"#555"}}> Um diese Funktion nutzen zu können, müssen Sie eine Bankkarte oder eine andere Zahlungsmethode hinzufügen.</div>
                        <Button>Hinzufügen</Button>
                    </div> */}

                    <div style={{textAlign:"center", background:"#fff", margin:"0 10px", padding:"30px 15px 30px", borderRadius:"3px"}} className="mb-10">
                        <div style={{fontSize: "12px"}} className="mb-10"> Geschätzter Gesamtbetrag aller Währungen</div>

                        <div className="text-center" style={{fontWeight:600, fontFamily:"open sans", fontSize:"30px"}}>
                            0,00 EUR
                        </div>
                    </div>

                    {/* transaction history */}
                    <div style={{fontWeight:600, fontFamily:"open sans", fontSize:"16px", background:"#fff", borderRadius:"3px", margin:"0 10px"}}>

                        <Link to={"/home/wallet/deposit"}>
                            <div className="category-list-item" style={{padding:"10px", position:"relative", borderBottom:"1px solid #eee"}}>
                                    <div className="display-flex py-2">
                                        <div className="mr-10 border-0">
                                            <FontAwesomeIcon icon={faPlusCircle} style={{color:"#428BC1", fontSize:"40px"}} className="mr-15" />
                                        </div>
                                        <div className="flex-auto border-0">
                                            <div style={{color:"#0070ba"}}>Guthaben Aufladen</div>
                                            <div style={{fontSize:"12px", fontWeight:"normal"}}>Schnelles Aufladen mit Kreditkarte, Klarna oder EPS.</div>
                                        </div>
                                        <div className="flex-auto text-right mt-20 ml-15">
                                            <i className="si si-arrow-right" />
                                        </div>
                                    </div>
                                <Ink duration="500" />
                            </div>
                        </Link>

                        <Link to={"/home/account/identityVerification"}>
                        <div className="category-list-item" style={{padding:"10px", position:"relative"}}>
                                <div className="display-flex py-2">
                                    <div className="mr-10 border-0">
                                        <FontAwesomeIcon icon={faMinusCircle} style={{color:"#F3016A", fontSize:"40px"}} className="mr-15" />
                                    </div>
                                    <div className="flex-auto border-0">
                                        <div style={{color:"#0070ba"}}>Geld Auszahlen</div>
                                        <div style={{fontSize:"12px", fontWeight:"normal"}}>Buchen Sie Geld von Ihrem Guthaben auf Ihr Bankkonto ab.</div>
                                    </div>
                                    <div className="flex-auto text-right mt-20 ml-15">
                                        <i className="si si-arrow-right" />
                                    </div>
                                </div>
                            <Ink duration="500" />
                        </div>
                        </Link>

                    </div>

                </div>

            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user.user,
});

export default connect(
    mapStateToProps,
    {}
)(Wallet);
