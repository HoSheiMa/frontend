import React, { Component } from "react";

import DelayLink from "../../../../helpers/delayLink";
import Ink from "react-ink";
import Moment from "react-moment";
import Popup from "reactjs-popup";
import { formatPrice } from "../../../../helpers/formatPrice";
import ItemsPic from '../../../../Takeaway/RunningOrder/RunOrd/itemsPic.js';
import axios from "axios";
import { GET_RESTAURANT_INFO_BY_ID_URL } from "../../../../../configs/index";

class OrderList extends Component {

    componentDidMount() {
        document.getElementsByTagName("body")[0].classList.add("bg-grey");
    }

    componentWillUnmount() {
        document.getElementsByTagName("body")[0].classList.remove("bg-grey");
    }

    __getOrderStatus = id => {
        // if (id === 1) {
        //     return localStorage.getItem("orderPlacedStatusText");
        // }
        // if (id === 2) {
        //     return localStorage.getItem("preparingOrderStatusText");
        // }
        // if (id === 3) {
        //     return localStorage.getItem("deliveryGuyAssignedStatusText");
        // }
        // if (id === 4) {
        //     return localStorage.getItem("orderPickedUpStatusText");
        // }
        // if (id === 5) {
        //     return localStorage.getItem("deliveredStatusText");
        // }
        if (id === 6) {
            return localStorage.getItem("canceledStatusText");
        }
        // if (id === 7) {
        //     return localStorage.getItem("readyForPickupStatusText");
        // }
        if(id === 1){
            return "Bestellung aufgegeben";
        }
        if(id === 2){
            return "Bestellung angenommen";
        }
        if(id === 3){
            return "Bestellung abgeschlossen";
        }
    }

    _getTotalItemCost = item => {
        let itemCost = parseFloat(item.price) * item.quantity;
        if (item.order_item_addons.length) {
            item.order_item_addons.map(addon => {
                itemCost += parseFloat(addon.addon_price) * item.quantity;
                return itemCost;
            });
        }
        return formatPrice(itemCost);
    }

    itemsPic = (supermarket_orders) =>{
        if(supermarket_orders.items == null || supermarket_orders.items.length === 0) return null;        
        return <ItemsPic supermarket_orders={supermarket_orders} />
    }

    elapsedTime = (time) => {
        switch (typeof time) {
            case 'number':
            break;
            case 'string':
            time = +new Date(time);
            break;
            case 'object':
            if (time.constructor === Date) time = time.getTime();
            break;
            default:
            time = +new Date();
        }
        var time_formats = [
            [60, 's', 1], // 60
            [120, 'vor 1 min', '1 minute from now'], // 60*2
            [3600, 'min', 60], // 60*60, 60
            [7200, 'vor 1 h', '1 hour from now'], // 60*60*2
            [86400, 'h', 3600], // 60*60*24, 60*60
            [172800, 'Gestern', 'Tomorrow'], // 60*60*24*2
            [604800, 'Tagen', 86400], // 60*60*24*7, 60*60*24
            [1209600, 'Letzte Woche', 'Next week'], // 60*60*24*7*4*2
            [2419200, 'Wochen', 604800], // 60*60*24*7*4, 60*60*24*7
            [4838400, 'Letzten Monat', 'Next month'], // 60*60*24*7*4*2
            [29030400, 'Monaten', 2419200], // 60*60*24*7*4*12, 60*60*24*7*4
            [58060800, 'Letztes Jahr', 'Next year'], // 60*60*24*7*4*12*2
            [2903040000, 'Jahren', 29030400], // 60*60*24*7*4*12*100, 60*60*24*7*4*12
        ];
        var seconds = (+new Date() - time) / 1000,
            token = 'vor',
            list_choice = 1;

        if (seconds == 0) {
            return 'Gerade jetzt'
        }
        if (seconds < 0) {
            seconds = Math.abs(seconds);
            token = 'ab jetzt';
            list_choice = 2;
        }
        var i = 0,
            format;
        while (format = time_formats[i++])
            if (seconds < format[0]) {
            if (typeof format[2] == 'string')
                return format[list_choice];
            else
                return token + ' ' + Math.floor(seconds / format[2]) + ' ' + format[1];
            }
        return time;
    }      
    toDate = (unix_timestamp) => {
        var d = new Date(unix_timestamp * 1000);
        return d.getFullYear() + '-' + (d.getMonth()+1) + '-' + d.getDate() + ' ' + d.getHours() + ':' + d.getMinutes();
    }

    addToCart = (order) => {
        axios.post(GET_RESTAURANT_INFO_BY_ID_URL + "/" + order.id).then(response => {

            const restaurant_info = response.data;

            // console.log("restaurant_info", restaurant_info);

            try {
                let localState = localStorage.getItem('state');
                if (localState) {
                    localState = JSON.parse(localState);
                    localState.cart = localState.cart || {};
                    localState.total = localState.total || {};
                    localState.total.data = localState.total.data || {};
                    localState.items = localState.items || {};
                    localState.items.restaurant_info = localState.items.restaurant_info || {};

                    localState.total.data.productQuantity = order.items.length;
                    localState.total.data.totalPrice = parseFloat(order.subtotal);

                    localState.cart.products = order.items || [];
                    localState.cart.products = localState.cart.products.map((ele) => ({...ele, price: ele.price}));

                    localState.items.restaurant_info = restaurant_info;
                    localState.items.restaurant_items = [];

                    localStorage.setItem('state', JSON.stringify(localState));
                    localStorage.setItem('activeRestaurant', order.shop_id);
                }
            } catch (err) {
                console.log(err);
            }
    
            window.location = '/cart';
        })
        .catch(function(error) {
            console.log(error);
        });
    }

    render() {
        const { order, user, cancelOrder } = this.props;
        return (
            <React.Fragment>

                    <div className="mb-20 bg-white" style={{ borderRadius: "0.275rem" }}>
                        
                        <DelayLink to={`/running-order-s/${order.id}`} delay={250} className="p-3" style={{ position: "relative", overflow:"auto", display:"block", borderBottom:"1px solid #eee" }}>

                            {/* order status, date, total */}
                            <div style={{width:"100%", float:"left"}}>
                            
                                {/* order status, date */}
                                <div className="pull-left">
                                    <div className="font-w700 flex-auto">{this.__getOrderStatus(order.orderstatus_id)}</div>
                                    <div className="text-muted" style={{fontSize:"0.9rem", fontWeight:300, fontFamily:"open sans"}}>{this.elapsedTime(this.toDate(order.added))}</div>
                                </div>

                                {/* total */}
                                <div className="pull-right text-right">
                                    <div className="font-w700 flex-auto">{localStorage.getItem("orderTextTotal").replace(":", "")}</div>
                                    <div className="text-muted" style={{fontSize:"0.9rem", fontWeight:300, fontFamily:"open sans"}}>{localStorage.getItem("currencyFormat")}{order.total}</div>
                                </div>
                            </div>

                            <Ink duration="500" />
                        </DelayLink>




                        <div className="pt-3 pl-3 pr-3 pb-1">

                            {/* shop img, shop name, items, delivery time */}
                            <div style={{width:"100%", float:"left"}} className="mb-10">
                                <div className="pull-left" style={{width:"30px"}}>
                                    <img src={'https://lieferservice123.com/'+order.shopImage} style={{width: "40px"}} /> 
                                </div>
                                <div className="pull-right" style={{width:"calc(100% - 50px)"}}>
                                    <div className="font-w700">{order.shopName}</div>
                                    <div className="text-muted" style={{fontSize:"0.9rem", fontWeight:300, fontFamily:"open sans"}}>{order.items.length} {order.items.length==1?"Produkt":"Produkte"}</div>
                                </div>
                            </div>
                            
                            <div style={{width:"100%", background:"#fff", overflow:"auto"}} className={order.cat == "grocery" ? "display-flex":""}>{this.itemsPic(order)}</div>

                            <div className="mt-5 mb-10"><span onClick={() => this.addToCart(order)} style={{color:localStorage.getItem("storeColor"), border:"1px solid orange", cursor:"pointer", borderRadius:"3px", padding:"3px 6px"}}>Zum Warenkorb hinzfügen</span></div>

                            <React.Fragment>
                                {order.coupon_name && (
                                    <div className="display-flex mt-10 font-w700">
                                        <React.Fragment>
                                            <div className="flex-auto">Coupon: </div>
                                            <div className="flex-auto text-right">{order.coupon_name}</div>
                                        </React.Fragment>
                                    </div>
                                )}

                                {order.tax && (
                                    <div className="display-flex mt-10 font-w700">
                                        <React.Fragment>
                                            <div className="flex-auto">{localStorage.getItem("taxText")}: </div>
                                            <div className="flex-auto text-right text-danger">
                                                <span>+</span>
                                                {order.tax}%
                                            </div>
                                        </React.Fragment>
                                    </div>
                                )}
                            </React.Fragment>

                            {order.orderstatus_id == 1 && (
                                <React.Fragment>
                                    <div className="pull-right">
                                        <Popup
                                            trigger={
                                                <button
                                                    className="btn btn-square btn-sm btn-outline-danger mb-0 mt-15"
                                                    style={{ position: "relative", fontSize: "0.8rem" }}
                                                >
                                                    {localStorage.getItem("cancelOrderMainButton")}
                                                    <Ink duration="500" />
                                                </button>
                                            }
                                            modal
                                            closeOnDocumentClick
                                        >
                                            {close => (
                                                <div className="pages-modal">
                                                    <div onClick={close} className="close-modal-header text-right">
                                                        <span className="close-modal-icon">&times;</span>
                                                    </div>
                                                    <div
                                                        className="text-center"
                                                        style={{
                                                            position: "absolute",
                                                            top: "40%",
                                                            left: "50%",
                                                            transform: "translate(-50%, -50%)",
                                                            width: "100%"
                                                        }}
                                                    >
                                                        <div style={{ fontSize: "1.2rem", fontWeight: "500" }}>
                                                            <i className="si si-info" style={{ fontSize: "4rem", opacity: "0.3", color: "#FF9800" }}></i>
                                                            <p>{order.unique_order_id}</p>
                                                            <p>{localStorage.getItem("orderCancellationConfirmationText")}</p>
                                                            {order.orderstatus_id === 1 ? (
                                                                <React.Fragment>
                                                                    {order.payment_mode !== "COD" && (
                                                                        <p className="text-muted font-w400">
                                                                            {localStorage.getItem("currencyFormat")} {order.total}{" "}
                                                                            {localStorage.getItem("willBeRefundedText")}
                                                                        </p>
                                                                    )}
                                                                    {order.payment_mode === "COD" && order.total - order.payable !== 0 && (
                                                                        <p className="text-muted font-w400">
                                                                            {localStorage.getItem("currencyFormat")}{" "}
                                                                            {formatPrice(order.total - order.payable)}{" "}
                                                                            {localStorage.getItem("willBeRefundedText")}
                                                                        </p>
                                                                    )}
                                                                </React.Fragment>
                                                            ) : (
                                                                <p className="text-muted font-w400">{localStorage.getItem("willNotBeRefundedText")}</p>
                                                            )}
                                                        </div>
                                                        <div>
                                                            <button
                                                                className="btn btn-lg btn-danger mr-3"
                                                                onClick={() => cancelOrder(user.data.auth_token, user.data.id, order.id)}
                                                                style={{
                                                                    border: "0",
                                                                    borderRadius: "0",
                                                                    backgroundColor: localStorage.getItem("storeColor")
                                                                }}
                                                            >
                                                                {localStorage.getItem("yesCancelOrderBtn")}
                                                            </button>
                                                            <button onClick={close} className="btn btn-lg" style={{ border: "0", borderRadius: "0" }}>
                                                                {localStorage.getItem("cancelGoBackBtn")}
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            )}
                                        </Popup>
                                    </div>
                                    <div className="clearfix"></div>
                                </React.Fragment>
                            )}

                        </div>

                    </div>

            </React.Fragment>
        );
    }
}

export default OrderList;
