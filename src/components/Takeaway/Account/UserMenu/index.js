import React, { Component } from "react";

import Collapsible from "react-collapsible";
import DelayLink from "../../../helpers/delayLink";
import Popup from "reactjs-popup";
import { logoutUser } from "../../../../services/user/actions";
import { connect } from "react-redux";
import Ink from "react-ink";
import axios from "axios";

// change lang/country
import { getSingleLanguageData } from "../../../../services/translations/actions";

//bootstrap
import ReactDOM from 'react-dom';
import { Modal, Button } from 'react-bootstrap';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheck } from '@fortawesome/free-solid-svg-icons';

import { isChrome, isChromium, isMobileSafari, isMobile, fullBrowserVersion, osName } from 'react-device-detect';

import MyOrders from '../Orders';
import MyWallet from '../Wallet';
import MyAddresses from '../Addresses';


declare var INSTALL_APP_POPUP;

class UserMenu extends Component {

    constructor() {
        super();
        
        this.state = { 
            pagesModal: false,
            berechtigungenModal: false,
        };
    }

    //my addresses
    showModalMyAddresses () {
        this.setState({
            myAddressesModal: true
        });
    }
    hideModalMyAddresses = () => {
        this.setState({
            myAddressesModal: false
        });
    }

    //my wallet
    showModalMyWallet () {
        this.setState({
            myWalletModal: true
        });
    }
    hideModalMyWallet = () => {
        this.setState({
            myWalletModal: false
        });
    }

    //my orders logged in
    showModalMyOrders () {
        this.setState({
            myOrdersModal: true
        });
    }
    hideModalMyOrders = () => {
        this.setState({
            myOrdersModal: false
        });
    }
    
    showModalPages (id) {
        this.setState({
            pagesModal: {
                [id]: true
            }
        });
    }
    hideModalPages = () => {
        this.setState({
            pagesModal: false
        });
    }

    //App install
    showModalInstall () {
        this.setState({
            installModal: true
        });
    }
    hideModalInstall = () => {
        this.setState({
            installModal: false
        });
    }
    showModalBerechtigungen () {
        this.setState({
            berechtigungenModal: true
        });
    }
    hideModalBerechtigungen = () => {
        this.setState({
            berechtigungenModal: false
        });
    }
    triggerInstallBtn = () => {
        var div = document.getElementById('download');
        div.innerHTML = 'Laden...';
        
        setTimeout(function () {

            if (INSTALL_APP_POPUP) {//INSTALL_APP_POPUP is declared globally
                console.log("INSTALL_APP_POPUP", INSTALL_APP_POPUP); 
                INSTALL_APP_POPUP.prompt(); //this will show the prompt from chrome
            } 

            div.innerHTML = 'Hinzufügen';
        }, 1500);
    }
    showPrompt = () => {
        console.log("button clicked");
        document.getElementById('showPwaprompt').style.display = "block";
    } 
    next = (install, pwaSupported) =>{
        if(pwaSupported == false){
            alert("Deine Browser Version wird nicht unterstützt. Bitte installiere Chrome 70 oder höher.");
        } else if(install == "voice"){
            this.hideModalBerechtigungen2();
            this.showModalInstall2();
        } else {
            this.hideModalBerechtigungen();
            this.showModalInstall();
        }
    }
    fullBrowserVersion = () =>{
        return fullBrowserVersion.replace( /^([^.]*\.)(.*)$/, function ( a, b, c ) { 
            return b + c.replace( /\./g, '' );
        });
    }

    // change country / lang
    showModalLang () {
        this.setState({
            LangModal: true
        });
    }
    hideModalLang = () => {
        this.setState({
            LangModal: false
        });
    }    
    getCountry = () => {
        axios
            .get("https://lieferservice123.com/php/country.php", {
            })
            .then(response => {
                localStorage.setItem("country", response.data);
                if(response.data == "AT"){
                    localStorage.setItem("phoneCountryCode", "43");
                    localStorage.setItem("userPreferedLanguage", "1");
                } 
                else if(response.data == "DE"){
                    localStorage.setItem("phoneCountryCode", "49");
                    localStorage.setItem("userPreferedLanguage", "1");
                } 
                else if(response.data == "CH"){
                    localStorage.setItem("phoneCountryCode", "41");
                    localStorage.setItem("userPreferedLanguage", "1");
                } else {
                    localStorage.setItem("userPreferedLanguage", "2");
                }
            })
            .catch(function(error) {
                console.log(error);
            });
    }
    changeCountry = (country) => {
        if(country !== localStorage.getItem("country")){
            localStorage.removeItem("userSetAddress");
        }

        localStorage.setItem("country", country);
        if(country == "AT"){
            localStorage.setItem("phoneCountryCode", "43");
            localStorage.setItem("shop", "Spar");
        } 
        else if(country == "DE"){
            localStorage.setItem("phoneCountryCode", "49");
            localStorage.setItem("shop", "REWE");
        } 
        else if(country == "CH"){
            localStorage.setItem("phoneCountryCode", "41");
        } 
        var url = window.location.href;
        window.location = url;
    }
    changeLang = (lang) => {
        // console.log("lang", lang);
        this.props.getSingleLanguageData(lang);
        localStorage.setItem("userPreferedLanguage", lang);
        var url = window.location.href;
        window.location = url;
    } 
    convertCodetoLang = (code)=>{
        if(code == "DE"){
            var country = "Deutschland"
        } else if(code == "AT"){
            var country = "Österreich"
        } else if(code == "CH"){
            var country = "Schweiz"
        } 

        return country;
    }

    render() {
        
        const { user, pages } = this.props;

        //check if chrome version is supported
        if(isChrome || isChromium){
            if(osName == "Windows" && this.fullBrowserVersion() >= 70){
                var pwaSupported = true;
            } else if(osName == "Android" && this.fullBrowserVersion() >= 31){
                var pwaSupported = true;
            } else if(osName == "Ubuntu" && this.fullBrowserVersion() >= 72){
                var pwaSupported = true;
            } else {
                var pwaSupported = false;
            }
        } else {
            var pwaSupported = false;
        }

        // change country / lang
        var langStyle = {
            marginBottom:"15px", cursor:"pointer", color:"#666"
        };
        var langStyleBold = {
            marginBottom:"15px", cursor:"pointer", fontWeight:"bold", color:"#666"
        };
        

        return (
            <React.Fragment>
                {/* <Collapsible trigger={localStorage.getItem("accountMyAccount")} transitionTime={200} open={true}> */}

                    {user.success ? (

                        <React.Fragment>

                            <div className="category-list-item" style={{cursor:"pointer", position:"relative", padding:"0 20px"}} onClick={() => this.showModalMyAddresses()}>
                                <div className="display-flex py-2">
                                    <div className="mr-10 border-0">
                                        <i className="si si-home" />
                                    </div>
                                    <div className="flex-auto border-0">{localStorage.getItem("accountManageAddress")}</div>
                                </div>
                                <Ink duration="500" />
                            </div>

                            <div className="category-list-item" style={{cursor:"pointer", position:"relative", padding:"0 20px"}} onClick={() => this.showModalMyOrders()}>
                                <div className="display-flex py-2">
                                    <div className="mr-10 border-0">
                                        <i className="si si-basket-loaded" />
                                    </div>
                                    <div className="flex-auto border-0">{localStorage.getItem("accountMyOrders")}</div>
                                </div>
                                <Ink duration="500" />
                            </div>

                            <div className="category-list-item" style={{cursor:"pointer", position:"relative", padding:"0 20px"}} onClick={() => this.showModalMyWallet()}>
                                <div className="display-flex py-2">
                                    <div className="mr-10 border-0">
                                        <i className="si si-wallet" />
                                    </div>
                                    <div className="flex-auto border-0">{localStorage.getItem("accountMyWallet")}</div>
                                </div>
                                <Ink duration="500" />
                            </div>
                        
                        </React.Fragment>

                    ) : (

                        <div className="category-list-item" style={{cursor:"pointer", position:"relative", padding:"0 20px"}} onClick={() => window.location = "/login"}>
                            <div className="display-flex py-2">
                                <div className="mr-10 border-0">
                                    <i className="si si-login" />
                                </div>
                                <div className="flex-auto border-0">{localStorage.getItem("firstScreenLoginBtn")}</div>
                            </div>
                            <Ink duration="500" />
                        </div>

                    )}

                    <hr style={{borderColor:"#e5e5e5"}} />

                    <div className="category-list-item" style={{cursor:"pointer", position:"relative", padding:"0 20px", cursor:"pointer"}} onClick={() => window.location = "/store"}>
                        <div className="display-flex py-2">
                            <div className="mr-10 mt-10 border-0">
                                <img src={"https://lieferservice.me/assets/img/store.jpg"} style={{width:"30px"}} />
                            </div>
                            <div className="flex-auto border-0">
                                Dein Geschäft jetzt anmelden
                                <div style={{fontFamily:"open sans", fontWeight:300, lineHeight:1.2, fontSize:"12px"}}>- Produkte in Echtzeit aktualisieren</div>
                                <div style={{fontFamily:"open sans", fontWeight:300, lineHeight:1.2, fontSize:"12px"}}>- Keine Provision, mehr Umsatz</div>
                            </div>
                        </div>
                        <Ink duration="500" />
                    </div>

                    <hr style={{borderColor:"#e5e5e5"}} />

                    <div className="category-list-item" style={{cursor:"pointer", position:"relative", padding:"0 20px", cursor:"pointer"}} onClick={() => window.location = "/shopper"}>
                        <div className="display-flex py-2">
                            <div className="mr-10 border-0">
                                <img src={"https://lieferservice.me/assets/img/shopper.jpg"} style={{width:"30px"}} />
                            </div>
                            <div className="flex-auto border-0">
                                Werde Shopper
                                <div style={{fontFamily:"open sans", fontWeight:300, lineHeight:0.8, fontSize:"12px"}}>und verdiene Geld</div>
                            </div>
                        </div>
                        <Ink duration="500" />
                    </div>

                    <hr style={{borderColor:"#e5e5e5"}} />

                    <div className="category-list-item" style={{cursor:"pointer", position:"relative", padding:"0 20px", cursor:"pointer"}} onClick={() => this.showModalBerechtigungen()}>
                        <div className="display-flex py-2">
                            <div className="mr-10 border-0">
                                <img src={"https://lieferservice.me/assets/img/phone.jpg"} style={{width:"30px"}} />
                            </div>
                            <div className="flex-auto border-0">
                                App installieren
                                <div style={{fontFamily:"open sans", fontWeight:300, lineHeight:1.2, fontSize:"12px"}}>- Lieferung auf der Karte verfolgen</div>
                                <div style={{fontFamily:"open sans", fontWeight:300, lineHeight:1.2, fontSize:"12px"}}>- Benutzerelebnis durch Schnelles Laden</div>
                            </div>
                        </div>
                        <Ink duration="500" />
                    </div>

                    <hr style={{borderColor:"#e5e5e5"}} />

                    <div className="category-list-item" style={{cursor:"pointer", position:"relative", padding:"0 20px", cursor:"pointer"}} onClick={() => this.showModalLang() }>
                        <div className="display-flex py-2">
                            <div className="mr-10 border-0">
                                <i className="si si-settings" />
                            </div>
                            <div className="flex-auto border-0">Land/Sprache ändern</div>
                        </div>
                        <Ink duration="500" />
                    </div>

                    {user.success && (

                        <React.Fragment>

                            <hr style={{borderColor:"#e5e5e5"}} />

                            <div className="category-list-item" style={{cursor:"pointer", position:"relative", padding:"0 20px", cursor:"pointer"}} onClick={() => { this.props.logoutUser(this.props.user); window.location = "/"; }}>
                                <div className="display-flex py-2">
                                    <div className="mr-10 border-0">
                                        <i className="si si-power logout-icon" />
                                    </div>
                                    <div className="flex-auto border-0">{localStorage.getItem("accountLogout")}</div>
                                </div>
                                <Ink duration="500" />
                            </div>

                        </React.Fragment>

                    )}

                {/* </Collapsible> */}

                {/* <Collapsible trigger={localStorage.getItem("accountHelpFaq")} transitionTime={200}>
                    {pages.map(page => (
                        <div key={page.id} className="category-list-item" style={{paddingLeft:"12px", cursor:"pointer"}}>
                            <div className="display-flex py-2" onClick={() => this.showModalPages(page.id)}>
                                <div className="flex-auto border-0">{page.name}</div>
                            </div>
                            <Modal show={this.state.pagesModal[page.id]} onHide={this.hideModalPages} size="lg" style={{background:"#333"}}>
                                <Modal.Header closeButton style={{padding:"16px 18px 0"}}><Modal.Title>{page.name}</Modal.Title></Modal.Header>
                                <Modal.Body>
                                    <div dangerouslySetInnerHTML={{ __html: page.body }} />
                                </Modal.Body>
                            </Modal>
                        </div>
                    ))}
                </Collapsible> */}

                {/* app advantages */}
                <Modal show={this.state.berechtigungenModal} onHide={this.hideModalBerechtigungen} style={{background:"#333"}}>
                
                    <Modal.Header closeButton>
                        <Modal.Title>App-Vorteile</Modal.Title>
                    </Modal.Header>

                    <Modal.Body style={{padding:"0 25px 10px"}}>
                        <div style={{marginTop:"12px"}}>
                            <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Lieferservice123 auf Desktop installieren.</div>

                            {/* <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Ohne Internetverbindung bei Lieferservice123 bestellen.</div> */}

                            {/* <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Bis zu 32% Mengenrabatt.</div> */}

                            <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Lieferung auf der Karte verfolgen.</div>

                            <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Benutzerelebnis durch Schnelles Laden.</div>

                            <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Bessere Kommunikation zwischen Shopper und Kunde durch Push-Nachrichten.</div>

                            <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Zuverlässig auch bei schlechter Internetverbindung.</div>

                            <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Installation über Google Chrome.</div>
                        </div>

                    </Modal.Body>

                    <Modal.Footer>
                        <Button variant="secondary" onClick={this.hideModalBerechtigungen}> 
                            Schließen
                        </Button> 
                        <Button variant="primary" onClick={() => this.next("", pwaSupported)}>
                            Weiter
                        </Button>
                    </Modal.Footer>
                </Modal>

                {/* app install */}
                <Modal show={this.state.installModal} onHide={this.hideModalInstall} style={{background:"#333"}}>
                    
                    <Modal.Header closeButton>
                        <Modal.Title>Installieren</Modal.Title>
                    </Modal.Header>

                    <Modal.Body style={{padding:"0 25px 10px"}}>
                        <div style={{marginTop:"12px"}}>
                            Klicke auf hinzufügen um Lieferservice123 auf deinen Bildschirm zu installieren.
                        </div>

                        <div style={{marginTop:"20px", fontSize:"11px", color:"#999"}}>Wenn dir Lieferservice123 gefällt, dann installiere bitte die  App.</div>
                    </Modal.Body>

                    <Modal.Footer>
                        <Button variant="secondary" onClick={this.hideModalInstall}> 
                            Schließen
                        </Button> 
                        <Button variant="primary" onClick={() => this.triggerInstallBtn()} id="download">
                            Hinzufügen
                        </Button>
                    </Modal.Footer>
                </Modal>

                {/* country/ lang */}
                <Modal show={this.state.LangModal} onHide={this.hideModalLang} style={{background:"#333"}}>
                    <Modal.Header closeButton>
                        {/* <Modal.Title>Einstellungen</Modal.Title> */}
                    </Modal.Header>
                    <Modal.Body style={{padding:"0 25px 10px"}}>

                        <div style={{float:"left", width:"188px", borderRight:"1px solid #f8f5f2", margin:"0 12px"}}>
                            <div style={{fontSize:"1.8rem", marginBottom:"15px", fontWeight:"bold"}}>Land</div>
                            <div style={localStorage.getItem("country")=="DE"?langStyleBold:langStyle}><a onClick={() => { this.changeCountry("DE"); }}>Deutschland</a></div>
                            <div style={localStorage.getItem("country")=="AT"?langStyleBold:langStyle}><a onClick={() => { this.changeCountry("AT"); }}>Österreich</a></div>
                            <div style={localStorage.getItem("country")=="CH"?langStyleBold:langStyle}><a onClick={() => { this.changeCountry("CH"); }}>Schweiz</a></div>
                        </div>

                        <div style={{float:"left", width:"188px", margin:"0 12px"}}>
                            <div style={{fontSize:"1.8rem", marginBottom:"15px", fontWeight:"bold"}}>{localStorage.getItem("footerLanguage")}</div>

                            <div style={localStorage.getItem("userPreferedLanguage")==1?langStyleBold:langStyle}><div className="flag-container"><span className="locale-flag locale-flag-de"></span></div> <a onClick={() => { this.changeLang("1"); }} style={{marginLeft:"10px"}}>Deutsch</a></div>

                            <div style={localStorage.getItem("userPreferedLanguage")==2?langStyleBold:langStyle}><div className="flag-container"><span className="locale-flag locale-flag-en"></span></div> <a onClick={() => { this.changeLang("2"); }} style={{marginLeft:"10px"}}>English</a></div>
                        </div>

                    </Modal.Body>
                </Modal>

                {/* my orders */}
                <Modal show={this.state.myOrdersModal} onHide={this.hideModalMyOrders} style={{background:"#333"}}>
                    <Modal.Header closeButton style={{background:"rgb(232, 233, 238)"}}>
                    </Modal.Header>
                    <Modal.Body style={{padding:0, background:"rgb(232, 233, 238)"}}>
                        <MyOrders />
                    </Modal.Body>
                </Modal>

                {/* my wallet */}
                <Modal show={this.state.myWalletModal} onHide={this.hideModalMyWallet} style={{background:"#333"}}>
                    <Modal.Header closeButton style={{background:"rgb(232, 233, 238)"}}>
                    </Modal.Header>
                    <Modal.Body style={{padding:0, background:"rgb(232, 233, 238)"}}>
                        <MyWallet />
                    </Modal.Body>
                </Modal>

                {/* my addresses */}
                <Modal show={this.state.myAddressesModal} onHide={this.hideModalMyAddresses} style={{background:"#333"}} size="sm">
                    <Modal.Header closeButton style={{background:"rgb(232, 233, 238)"}}>
                    </Modal.Header>
                    <Modal.Body style={{padding:0, background:"rgb(232, 233, 238)"}}>
                        <MyAddresses />
                    </Modal.Body>
                </Modal>

            </React.Fragment>
        );
    }
}


const mapStateToProps = state => ({
    user: state.user.user
});

export default connect(
    mapStateToProps,
    { UserMenu, logoutUser, getSingleLanguageData }
)(UserMenu);
