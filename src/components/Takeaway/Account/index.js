import React, { Component } from "react";

import Footer from "../Footer";
import Logout from "./Logout";
import Meta from "../../helpers/meta";
import Nav from "../Nav";
import { Redirect } from "react-router";
import UserInfo from "./UserInfo";
import UserMenu from "./UserMenu";
import { connect } from "react-redux";
import { getPages } from "../../../services/pages/actions";
import { getSingleLanguageData } from "../../../services/translations/actions";

class Account extends Component {
    componentDidMount() {
        const { user } = this.props;
        if (localStorage.getItem("storeColor") !== null) {
            if (user.success) {
                this.props.getPages();
            }
        }

        if (localStorage.getItem("state") !== null) {
            const languages = JSON.parse(localStorage.getItem("state")).languages;
            if (languages && languages.length > 0) {
                if (localStorage.getItem("multiLanguageSelection") === "true") {
                    if (localStorage.getItem("userPreferedLanguage")) {
                        this.props.getSingleLanguageData(localStorage.getItem("userPreferedLanguage"));
                        // console.log("Called 1");
                    } else {
                        if (languages.length) {
                            const id = languages.filter(lang => lang.is_default === 1)[0].id;
                            this.props.getSingleLanguageData(id);
                        }
                    }
                } else {
                    // console.log("Called 2");
                    if (languages.length) {
                        const id = languages.filter(lang => lang.is_default === 1)[0].id;
                        this.props.getSingleLanguageData(id);
                    }
                }
            }
        }
    }

    handleOnChange = event => {
        // console.log(event.target.value);
        this.props.getSingleLanguageData(event.target.value);
        localStorage.setItem("userPreferedLanguage", event.target.value);
    };

    render() {
        if (localStorage.getItem("storeColor") === null) {
            return <Redirect to={"/"} />;
        }
        const { user, pages } = this.props;

        if (!user.success) {
            return (
                //redirect to login page if not loggedin
                <Redirect to={"/login"} />
            );
        }
        const languages = JSON.parse(localStorage.getItem("state")).languages;
        console.log(languages);

        return (
            <React.Fragment>
                <Meta
                    seotitle="Account"
                    seodescription={localStorage.getItem("seoMetaDescription")}
                    ogtype="website"
                    ogtitle={localStorage.getItem("seoOgTitle")}
                    ogdescription={localStorage.getItem("seoOgDescription")}
                    ogurl={window.location.href}
                    twittertitle={localStorage.getItem("seoTwitterTitle")}
                    twitterdescription={localStorage.getItem("seoTwitterDescription")}
                />

                {/* <Nav logo={true} active_nearme={true} disable_back_button={true} loggedin={user.success} /> */}


                    {/* <UserInfo user_info={user.data} /> */}

                    <UserMenu pages={pages} />

                    {/* <Logout /> */}

                    {/* <Footer active_account={true} /> */}

                    {localStorage.getItem("multiLanguageSelection") === "true" && (
                        languages && languages.length > 0 && (
                            <div className="mt-4 d-flex align-items-center justify-content-center">
                                <div className="mr-2">{localStorage.getItem("changeLanguageText")}</div>
                                <select
                                    onChange={this.handleOnChange}
                                    defaultValue={
                                        localStorage.getItem("userPreferedLanguage")
                                            ? localStorage.getItem("userPreferedLanguage")
                                            : languages.filter(lang => lang.is_default === 1)[0].id
                                    }
                                    className="form-control language-select"
                                >
                                    {languages.map(language => (
                                        <option value={language.id} key={language.id}>
                                            {language.language_name}
                                        </option>
                                    ))}
                                </select>
                            </div>
                        )
                    )}
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user.user,
    pages: state.pages.pages,
    language: state.languages.language
});

export default connect(
    mapStateToProps,
    { getPages, getSingleLanguageData }
)(Account);
