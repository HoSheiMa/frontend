import React, { Component } from "react";

import Moment from "react-moment";

//bootstrap
import ReactDOM from 'react-dom';
import { Modal, Button, Dropdown } from 'react-bootstrap';

import Ink from "react-ink";
import ViewOrder from "../../../../Delivery/Dashboard/viewOrder.js";

class TransactionList extends Component {

    constructor() {
        super();
        
        this.state = { 
            invoiceSupermarketModal: false,
        };
    }

    static contextTypes = {
        router: () => null
    };

    //view transaction
    showModalInvoiceSupermarket (id) {
        this.setState({
            invoiceSupermarketModal: {
                [id]: true
            }
        });
    }
    hideModalInvoiceSupermarket = () => {
        this.setState({
            invoiceSupermarketModal: false
        })
    }

    render() {
        const { transaction } = this.props;
        return (
            <React.Fragment>
                {/* <div className="slider-wrapper transaction-wrapper">
                    <div className="d-flex">
                        <div className="mr-4">
                            {transaction.type === "deposit" && (
                                <span className="btn btn-square btn-sm btn-outline-success min-width-125">
                                    {localStorage.getItem("walletDepositText")}
                                </span>
                            )}
                            {transaction.type === "withdraw" && (
                                <span className="btn btn-square btn-sm btn-outline-danger min-width-125">
                                    {localStorage.getItem("walletWithdrawText")}
                                </span>
                            )}
                        </div>
                        <div className="mr-4 font-w700">
                            {localStorage.getItem("currencyFormat")}
                            {transaction.amount / 100}
                        </div>
                        <div className="mr-4">{transaction.meta["description"]}</div>
                        <div className="mr-4">
                            <Moment fromNow>{transaction.created_at}</Moment>
                        </div>
                    </div>
                </div> */}

                <div key={transaction.id} style={{cursor:"pointer", position:"relative", borderBottom:"1px solid #eee", overflow:"auto", borderRadius: "0.275rem"}} onClick={() => this.showModalInvoiceSupermarket(transaction.id)} className="mb-5 bg-white p-10">

                    <div style={{float:"left", width:"50%"}} title={transaction.created_at}>
                        {/* {this.showDate(transaction.created_at)} */}
                        <Moment fromNow>{transaction.created_at}</Moment>
                        <div style={{fontSize:"12px", fontWeight:300, fontFamily:"open sans"}}>{transaction.meta["description"]}</div>
                    </div>

                    <div style={{float:"right", width:"50%", fontWeight:"bold", textAlign:"right"}}>
                        {localStorage.getItem("currencyFormat")} {(transaction.amount / 100).toFixed(2)}     
                    </div>

                    <Ink duration="300" />
                </div>

                {/* batch summary */}
                <Modal show={this.state.invoiceSupermarketModal[transaction.id]} onHide={this.hideModalInvoiceSupermarket} style={{background:"rgba(0,0,0, 0.85)"}} size={transaction.meta["description"].indexOf("Payment for order: ") == -1 ?"sm":""}>

                    <Modal.Header closeButton>
                        {/* <Modal.Title style={{textAlign:"center"}}>Auftrags-Übersicht</Modal.Title> */}
                    </Modal.Header>

                    <Modal.Body style={{padding:"0 25px 25px"}}>
                        {transaction.meta["description"].indexOf("Payment for order: ") >= 0 ? (
                            <ViewOrder id={transaction.meta["description"].replace("Payment for order: ", "")} />
                        ) : (
                            <React.Fragment>

                                <div style={{margin:"5px 0"}}><b>Beschreibung:</b> {transaction.meta["description"]}</div>
                                <div style={{margin:"5px 0"}}><b>Betrag:</b> {localStorage.getItem("currencyFormat")} {(transaction.amount / 100).toFixed(2)}</div>
                                <div style={{margin:"5px 0"}}><b>Datum:</b> {transaction.created_at}</div>

                                <div style={{margin:"5px 0"}}>
                                    <b>Typ:</b> {transaction.type === "deposit" && (
                                        localStorage.getItem("walletDepositText")
                                    )}
                                    {transaction.type === "withdraw" && (
                                        localStorage.getItem("walletWithdrawText")
                                    )}
                                </div>
                                
                            </React.Fragment>
                        )}
                        
                    </Modal.Body>
                </Modal>

            </React.Fragment>
        );
    }
}

export default TransactionList;
