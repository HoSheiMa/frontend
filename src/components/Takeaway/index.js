import React, { Component } from "react";

import FirstScreen from "./FirstScreen";
import Shops from "./Shops";
import Popup from "reactjs-popup";

class Mobile extends Component {
    state = {
        showGdpr: false
    };
    componentDidMount() {
        setTimeout(() => {
            if (document.getElementsByClassName("popup-content")[0]) {
                document.getElementsByClassName("popup-content")[0].style.backgroundColor = "transparent";
            }
        }, 10);

        if (!localStorage.getItem("gdprAccepted")) {
            localStorage.setItem("gdprAccepted", "false");
            if (localStorage.getItem("showGdpr") === "true") {
                this.setState({ showGdpr: true });
            }
        }

        if (localStorage.getItem("showGdpr") === "true" && localStorage.getItem("gdprAccepted") === "false") {
            this.setState({ showGdpr: true });
        }
    }
    handleGdprClick = () => {
        localStorage.setItem("gdprAccepted", "true");
        this.setState({ showGdpr: false });
    };
    render() {
        return (
            <React.Fragment>
                <Popup open={this.state.showGdpr} closeOnDocumentClick={false} onClose={this.handleGdprClick}>
                    <div className="mobile-gdpr">
                        <button
                            className="close btn btn-sm ml-2"
                            style={{ backgroundColor: localStorage.getItem("storeColor") }}
                            onClick={this.handleGdprClick}
                        >
                            {localStorage.getItem("gdprConfirmButton")}
                        </button>
                        <span
                            dangerouslySetInnerHTML={{
                                __html: localStorage.getItem("gdprMessage")
                            }}
                        ></span>
                    </div>
                </Popup>
                {localStorage.getItem("userSetAddress") ? (
                    <div>
                        <Shops />
                    </div>
                ) : (
                    <FirstScreen languages={this.props.languages} />
                )}
            </React.Fragment>
        );
    }
}

export default Mobile;
