import React, { Component } from "react";

import ContentLoader from "react-content-loader";
import Flip from "react-reveal/Flip";
import Ink from "react-ink";

class PopularPlaces extends Component {
    render() {
        const { locations, handleGeoLocationClick, newClass } = this.props;

        if(newClass){
            var style = {
                position: "relative", cursor:"pointer", fontWeight:"normal", margin:"5px 5px 10px 0", lineHeight:"16px", padding:"4px 8px", border: "1px solid rgb(72, 98, 112)", borderRadius:"4px", display:"inline-block"
            };
        } else {
            var style = {
                position: "relative", cursor:"pointer", fontWeight:"normal"
            };
        }

        return (
            <React.Fragment>
                {/* <h1 className="text-muted h4">{localStorage.getItem("searchPopularPlaces")}</h1> */}
                {locations.length === 0 ? (null
                    // <ContentLoader height={160} width={400} speed={1.2} primaryColor="#f3f3f3" secondaryColor="#ecebeb">
                    //     <rect x="0" y="0" rx="15" ry="15" width="125" height="30" />
                    //     <rect x="135" y="0" rx="15" ry="15" width="100" height="30" />
                    //     <rect x="245" y="0" rx="15" ry="15" width="110" height="30" />
                    //     <rect x="0" y="40" rx="15" ry="15" width="85" height="30" />
                    //     <rect x="95" y="40" rx="15" ry="15" width="125" height="30" />
                    // </ContentLoader>
                ) : null}
                {locations.map((location, index) => (

                    (location.country == localStorage.getItem("country") ? 
                        <Flip top delay={index * 50} key={location.id}>
                            <a
                                style={style}
                                onClick={() => {
                                    const geoLocation = [
                                        {
                                            formatted_address: location.name,
                                            geometry: {
                                                location: {
                                                    lat: location.latitude,
                                                    lng: location.longitude
                                                }
                                            }
                                        }
                                    ];
                                    handleGeoLocationClick(geoLocation);
                                }}
                            >
                                <Ink duration="500" />
                                {location.name}
                            </a>
                        </Flip> 
                    : null)
                ))}
            </React.Fragment>
        );
    }
}

export default PopularPlaces;
