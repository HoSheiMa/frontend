import React, { Component } from "react";

import Flip from "react-reveal/Flip";
import Geocode from "react-geocode";
import GooglePlacesAutocomplete from "react-google-places-autocomplete";
import Meta from "../../helpers/meta";
import PopularPlaces from "./PopularPlaces";
import { Redirect } from "react-router";
import { connect } from "react-redux";
import { geocodeByPlaceId } from "react-google-places-autocomplete";
import { getPopularLocations } from "../../../services/popularLocations/actions";
import Ink from "react-ink";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';
import { faLocationArrow } from '@fortawesome/free-solid-svg-icons';
import { faTimes } from '@fortawesome/free-solid-svg-icons';



class Location extends Component {
    // referrer = document.referrer;


    constructor() {
        super();
        
        this.state = { 
            // google_script_loaded: false,
            google_script_loaded: true, //set it permanently to true for modal in shop details
            house: "",
            tag: "",
        };

        this.innerRef = React.createRef();

    }
    
    static contextTypes = {
        router: () => null
    };

    componentDidMount() {
        this.props.getPopularLocations();

        // if (this.searchInput) {
        //     this.searchInput.focus();
        // }

        const existingScript = document.getElementById("googleMaps");
        if (!existingScript) {
            const script = document.createElement("script");
            script.src = "https://maps.googleapis.com/maps/api/js?key=" + localStorage.getItem("googleApiKey") + "&libraries=places";
            script.id = "googleMaps";
            document.body.appendChild(script);
            script.onload = () => {
                this.setState({ google_script_loaded: true });
            };
        }

        setTimeout(() => {
            this.innerRef.current.focus();
        }, 100)

    }

    componentWillUnmount() {
        //remove script when component unmount
        const existingScript = document.getElementById("googleMaps");
        if (existingScript) {
            existingScript.parentNode.removeChild(existingScript);
        }
    }

    
    getUrlParameter(sParam) {
        var sPageURL =decodeURIComponent(window.location.search.substring(1));
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam) {
                return sParameterName[1];
            }
        }
    }

    handleGeoLocationClick = (results, address) => {
        //check if hausnummer in address
        if(address){
            var num = address.split(" ").pop();
            var first = num.charAt(0);
            var last = num.charAt(num.length-1);

            if(first.match(/^-{0,1}\d+$/) && isNaN(last)){
                console.log(num + " is a number");
                document.getElementById('errorNum').style.display = "none";
            } else if(isNaN(num)) {//if no number at end of string, return false
                console.log(num + " is not a number");
                this.searchInput.focus(); //sets focus to element
                var val = this.searchInput.value; //store the value of the element
                this.searchInput.value = ''; //clear the value of the element
                this.searchInput.value = address+' '; //set that value back. 
    
                document.getElementById('errorNum').style.display = "block";
                return false;
            } else {
                console.log(num + " is a number");
                document.getElementById('errorNum').style.display = "none";
            }
        }

        // set localstorage for userSetAddress
        // console.log(results);
        const userSetAddress = {
            lat: (typeof(results[0].geometry.location.lat) === "function") ? results[0].geometry.location.lat() : results[0].geometry.location.lat,
            lng: (typeof(results[0].geometry.location.lng) === "function") ? results[0].geometry.location.lng() : results[0].geometry.location.lng,
            address: results[0].formatted_address,
            house: this.state.house,
            tag: this.state.tag
        };
        localStorage.setItem("userSetAddress", JSON.stringify(userSetAddress));
        
        //get this result and store in a localstorage
        localStorage.setItem("geoLocation", JSON.stringify(results[0]));

        //redirect the user to map marker page
        // setTimeout(() => {
        //     if(document.URL.indexOf("?pwa=") >= 0) {
        //         this.context.router.history.push("/my-location?pwa="+this.getUrlParameter("pwa"));
        //     } else if(document.URL.indexOf("?redirect=") >= 0) {
        //         this.context.router.history.push("/my-location?redirect="+this.getUrlParameter("redirect"));
        //     } else {
        //         this.context.router.history.push("/my-location");
        //     }
        // }, 100);

        const { user } = this.props;

        if (user.success) {
             setTimeout(() => {
                //redirect the user to map marker page
                if(this.props.redirect) {
                    this.context.router.history.push("/my-location?redirect="+this.props.redirect);
                } else {
                    this.context.router.history.push("/my-location");
                }
            }, 100);
        } else {

            var time = 100; //change time to 3000 if you want to debug
            
            if(this.props.modal && this.props.redirect) {//this must be first becasue it already includes activeShop slug in url
                console.log("this.props.modal: "+this.props.redirect+this.props.modal);
                setTimeout(() => { window.location = this.props.redirect+this.props.modal; }, time);
            }
            else if(this.props.redirect) {//this must be first becasue it already includes activeShop slug in url
                console.log("this.props.modal: "+this.props.redirect);
                setTimeout(() => { window.location = this.props.redirect; }, time);
            }
            else if(document.URL.indexOf("?redirect=") >= 0) {
                console.log("redirect: /"+this.getUrlParameter("redirect"));
                setTimeout(() => { window.location = "/"+this.getUrlParameter("redirect"); }, time);
            }  
            else if(localStorage.getItem("activeShop")) {
                console.log("activeShop: "+localStorage.getItem("activeShop"));
                setTimeout(() => { window.location = "/"+localStorage.getItem("activeShop"); }, time);
            } 
            else if(document.URL.indexOf("?pwa=") >= 0) {
                console.log("pwa: /"+this.getUrlParameter("pwa")+"?pwa="+this.getUrlParameter("pwa"));
                setTimeout(() => { window.location = "/"+this.getUrlParameter("pwa")+"?pwa="+this.getUrlParameter("pwa"); }, time);
            } 
            else {
                console.log("window.location.href: "+window.location.href);
                setTimeout(() => { window.location = window.location.href; }, time);
            }
        }
    };

    getMyLocation = () => {
        document.getElementById("loadLocationTxt").innerHTML = 'Standortdaten werden geladen...';

        setTimeout(() => {
            const location = navigator && navigator.geolocation;

            if (location) {
                location.getCurrentPosition(
                    position => {
                        this.reverseLookup(position.coords.latitude, position.coords.longitude);
                    },
                    error => {
                        document.getElementById("loadLocationTxt").innerHTML = localStorage.getItem("gpsAccessNotGrantedMsg");
                    }
                );
            }
        }, 1000);
    };

    reverseLookup = (lat, lng) => {
        Geocode.setApiKey(localStorage.getItem("googleApiKey"));

        Geocode.fromLatLng(lat, lng).then(
            response => {
                // const address = response.results[0].formatted_address;
                // console.log(address);
                const myLocation = [
                    {
                        formatted_address: response.results[0].formatted_address,
                        geometry: {
                            location: {
                                lat: lat,
                                lng: lng
                            }
                        }
                    }
                ];
                console.log(myLocation);
                this.handleGeoLocationClick(myLocation);
            },
            error => {
                console.error(error);
            }
        );
    };

    hideErrorNum = () =>{
        document.getElementById('errorNum').style.display = "none";
    }

    closeLink = () => {
        if(document.URL.indexOf("?pwa=") >= 0) {
            this.context.router.history.push("/"+this.getUrlParameter("pwa")+"?pwa="+this.getUrlParameter("pwa"));
        } 
        else if(document.URL.indexOf("?redirect=") >= 0) {
            this.context.router.history.push("/"+this.getUrlParameter("redirect"));
        } 
        else {
            window.location = window.location.href;
        }
    }

    
    getAddress = (address, extract, long=true) => {
        if(extract == "str+nr"){
            var res = address;
            var split = res.split(",");
            if(long) var res = split.slice(0, split.length - 1).join(",");
            else var res = split[0];
            return res;
        } else if(extract == "str"){
            var res = address;
            var split = res.split(",");
            var res = split[0];
            return res;
        }  else if(extract == "plz+city"){
            var res = address;
            var split = res.split(",");
            var res = split[1];
            return res;
        } else if(extract == "plz"){
            var res = address;
            var split = res.split(",");
            var res = split[1];

            //only plz without city
            var split = res.split(" ");
            var res = split[1];
            return res;
        } else if(extract == "country"){
            var res = address.split(", ").pop();
            return res;
        } else {
            var res = address.split(", ");
            return res[0];
        }
    }

    render() {
        if (localStorage.getItem("storeColor") === null) {
            return <Redirect to={"/"} />;
        }
        return (
            <React.Fragment>
                <Meta
                    seotitle={localStorage.getItem("seoMetaTitle")}
                    seodescription={localStorage.getItem("seoMetaDescription")}
                    ogtype="website"
                    ogtitle={localStorage.getItem("seoOgTitle")}
                    ogdescription={localStorage.getItem("seoOgDescription")}
                    ogurl={window.location.href}
                    twittertitle={localStorage.getItem("seoTwitterTitle")}
                    twitterdescription={localStorage.getItem("seoTwitterDescription")}
                />

                <div className="col-12 p-0 pt-0">

                    {/* address or postcode */}
                    {this.state.google_script_loaded && (
                        <GooglePlacesAutocomplete
                            autocompletionRequest={{
                                componentRestrictions: {
                                    //country: ['at'],
                                    country: [localStorage.getItem("country")],
                                }
                            }}
                            loader={<img src="/assets/img/various/spinner.svg" className="location-loading-spinner" alt="loading" />}
                            renderInput={props => (
                                <div className="input-location-icon-field">
                                    <i className="si si-magnifier"></i>
                                    <input
                                        {...props}
                                        className="form-control search-input search-box"
                                        placeholder={localStorage.getItem("searchAreaPlaceholder")}
                                        // ref={input => {
                                        //     this.searchInput = input;
                                        // }}
                                        // autoFocus
                                        ref={this.innerRef}
                                        onFocus={ this.hideErrorNum } 
                                    />
                                    <div id="errorNum" style={{ display:"none", color: "#fff", background: "orange", width: "100%", padding: "10px 15px", zIndex: 1}}><FontAwesomeIcon icon={faExclamationTriangle} style={{marginRight:"5px"}} />Gib Deine Hausnummer an</div>
                                </div>
                            )}
                            renderSuggestions={(active, suggestions, onSelectSuggestion) => (
                                <div className="location-suggestions-container">
                                    {suggestions.map((suggestion, index) => (
                                        <Flip top delay={index * 50} key={suggestion.id}>
                                            <div
                                                className="location-suggestion"
                                                onClick={event => {
                                                    onSelectSuggestion(suggestion, event);
                                                    geocodeByPlaceId(suggestion.place_id)
                                                        .then(results => this.handleGeoLocationClick(results, suggestion.structured_formatting.main_text))
                                                        .catch(error => console.error(error));
                                                }}
                                            >
                                                <span className="location-main-name">{suggestion.structured_formatting.main_text}</span>
                                                <br />
                                                <span className="location-secondary-name">{suggestion.structured_formatting.secondary_text}</span>
                                            </div>
                                        </Flip>
                                    ))}
                                </div>
                            )}
                        />
                    )}

                    {/* current location */}
                    <div onClick={this.getMyLocation} style={{borderBottom: "1px solid #eee", position:"relative", background:"#fff", padding:"10px 15px", overflow:"auto", marginTop: "0.6rem", cursor:"pointer"}}>
                        <i className="si si-pointer" style={{float:"left", width:"20px", paddingTop:"5px"}}></i> 
                        <div style={{float:"left", width:"calc(100% - 20px)"}}>
                            <div style={{fontWeight:600, fontFamily:"open sans"}}>Current location</div>
                            <div style={{color:"#999"}} id="loadLocationTxt">{localStorage.getItem("useCurrentLocationText")}</div>
                        </div>
                        <Ink duration="500" />
                    </div>

                    {/* current address */}
                    {localStorage.getItem("userSetAddress") && (
                        <div onClick={() => this.closeLink()} style={{borderBottom: "1px solid #eee", position:"relative", background:"#fff", padding:"10px 15px", overflow:"auto", cursor:"pointer"}}>
                            <i className="si si-pointer" style={{float:"left", width:"20px", paddingTop:"5px"}}></i> 
                            <div style={{float:"left", width:"calc(100% - 20px)"}}>
                                <div style={{fontWeight:600, fontFamily:"open sans"}}>{this.getAddress(JSON.parse(localStorage.getItem("userSetAddress")).address, "str+nr")}</div>
                                <div style={{color:"#999"}} id="loadLocationTxt">{this.getAddress(JSON.parse(localStorage.getItem("userSetAddress")).address, "country")}</div>
                            </div>
                            <Ink duration="300" />
                        </div>
                    )}
                    
                </div>
                {/* <PopularPlaces handleGeoLocationClick={this.handleGeoLocationClick} locations={this.props.popular_locations} /> */}
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    popular_locations: state.popular_locations.popular_locations,
    user: state.user.user

});

export default connect(
    mapStateToProps,
    { getPopularLocations }
)(Location);
