import React, { Component } from "react";

import ContentLoader from "react-content-loader";
import DelayLink from "../../helpers/delayLink";
import Geocode from "react-geocode";
import GoogleMap from "./GoogleMap";
import Ink from "react-ink";
import { connect } from "react-redux";
import { saveAddress } from "../../../services/addresses/actions";

export class GeoLocationPage extends Component {
    state = {
        location: "",
        dragging: true,
        house: "",
        tag: "",
        error: false,
        loading: false
    };

    static contextTypes = {
        router: () => null
    };

    handleAddressInput = event => {
        const { name, value } = event.target;
        this.setState({ [name]: value });
    };

    reverseLookup = (lat, lng) => {
        Geocode.setApiKey(localStorage.getItem("googleApiKey"));

        Geocode.fromLatLng(lat, lng).then(
            response => {
                const address = response.results[0].formatted_address;
                console.log(address);
                this.setState({ location: address, dragging: false });
            },
            error => {
                console.error(error);
            }
        );
    };

    onMarkerDragEnd = map => {
        console.log(map);
        console.log({ lat: map.center.lat(), lng: map.center.lng() });
        localStorage.setItem("userLat", map.center.lat());
        localStorage.setItem("userLng", map.center.lng());
        this.reverseLookup(map.center.lat(), map.center.lng());
    };
    handleDragging = () => {
        this.setState({ dragging: true });
    };

    getUrlParameter(sParam) {
        var sPageURL =decodeURIComponent(window.location.search.substring(1));
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam) {
                return sParameterName[1];
            }
        }
    }

    handleSaveNewAddress = () => {
        const { user } = this.props;
        //if user loggedin, then save into db
        if (user.success) {
            this.setState({ loading: true });
            this.props.saveAddress(
                user.data.id,
                user.data.auth_token,
                localStorage.getItem("userLat"),
                localStorage.getItem("userLng"),
                this.state.location,
                this.state.house,
                this.state.tag
            );
        } 
            
        const userSetAddress = {
            lat: localStorage.getItem("userLat"),
            lng: localStorage.getItem("userLng"),
            address: this.state.location,
            house: this.state.house,
            tag: this.state.tag
        };
        //else save in localstorage for future use (Later when user loggsin or registers, and orders, send this address to db)
        localStorage.setItem("userSetAddress", JSON.stringify(userSetAddress));
       

        //then redirect user to the homepage
        var time = 100; //change time to 3000 if you want to debug
        if(document.URL.indexOf("?pwa=") >= 0) {
            setTimeout(() => { window.location = "/"+this.getUrlParameter("pwa")+"?pwa="+this.getUrlParameter("pwa"); }, time);
        } else if(document.URL.indexOf("?redirect=") >= 0) {
            setTimeout(() => { window.location = this.getUrlParameter("redirect"); }, time);
        } else {
            setTimeout(() => { window.location = "/" }, time);
        }
    };

    componentWillReceiveProps(nextProps) {
        console.log(nextProps);
    }

    render() {
        return (
            <div>
                {this.state.loading && (
                    <div className="height-100 overlay-loading">
                        <div>
                            <img src="/assets/img/loading-food.gif" alt={localStorage.getItem("pleaseWaitText")} />
                        </div>
                    </div>
                )}

                <GoogleMap reverseLookup={this.reverseLookup} onMarkerDragEnd={this.onMarkerDragEnd} handleDragging={this.handleDragging}></GoogleMap>
                <button
                    type="button"
                    className="btn search-navs-btns"
                    style={{ position: "relative", borderRadius: "0 50px 50px 0" }}
                    onClick={this.context.router.history.goBack}
                >
                    <i className="si si-arrow-left" />
                    <Ink duration="500" />
                </button>
                <div className="confirm-gps-location">
                    {this.state.dragging ? (
                        <ContentLoader height={345} width={window.innerWidth} speed={1.2} primaryColor="#f3f3f3" secondaryColor="#ecebeb">
                            <rect x="20" y="15" rx="0" ry="0" width="110" height="16" />
                            <rect x="20" y="45" rx="0" ry="0" width="280" height="20" />
                            <rect x="315" y="45" rx="0" ry="0" width="70" height="20" />

                            <rect x="20" y="100" rx="0" ry="0" width="110" height="16" />
                            <rect x="20" y="130" rx="0" ry="0" width="280" height="20" />

                            <rect x="20" y="180" rx="0" ry="0" width="110" height="16" />
                            <rect x="20" y="210" rx="0" ry="0" width="280" height="20" />

                            <rect x="0" y="280" rx="0" ry="0" width={window.innerWidth} height="60" />
                        </ContentLoader>
                    ) : (
                        <React.Fragment>
                            <div className="p-15">
                                <p className="mb-0 font-w600 text-muted">{localStorage.getItem("yourLocationText")}</p>
                                <div className="d-flex align-items-baseline">
                                    <p className="truncate-text mb-0" style={{ minWidth: "84%" }}>
                                        {this.state.location}
                                    </p>
                                    <DelayLink
                                        to={"/search-location"}
                                        delay={400}
                                        className="change-address-text m-0 p-5 pull-right"
                                        style={{ color: localStorage.getItem("storeColor"), position: "relative" }}
                                    >
                                        {localStorage.getItem("cartChangeLocation")}
                                        <Ink duration={400} />
                                    </DelayLink>
                                </div>
                                {/* <hr />
                                <div className="form-group m-0">
                                    <label className="col-12 edit-address-input-label p-0">{localStorage.getItem("editAddressAddress")}</label>
                                    <div className="col-md-9 p-0">
                                        <input
                                            type="text"
                                            name="house"
                                            onChange={this.handleAddressInput}
                                            className="form-control edit-address-input mb-2"
                                            value={this.state.house}
                                        />
                                    </div>
                                    <label className="col-12 edit-address-input-label p-0">{localStorage.getItem("editAddressTag")}</label>
                                    <div className="col-md-9  p-0">
                                        <input
                                            type="text"
                                            name="tag"
                                            onChange={this.handleAddressInput}
                                            className="form-control edit-address-input edit-address-tag mb-2"
                                            placeholder={localStorage.getItem("addressTagPlaceholder")}
                                            value={this.state.tag}
                                        />
                                    </div>
                                </div> */}
                            </div>
                            <button
                                type="button"
                                className="btn-save-address"
                                onClick={this.handleSaveNewAddress}
                                style={{ backgroundColor: localStorage.getItem("storeColor"), position: "fixed", bottom: "0" }}
                            >
                                {localStorage.getItem("buttonSaveAddress")}
                                <Ink duration={200} />
                            </button>
                        </React.Fragment>
                    )}
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user.user
});

export default connect(
    mapStateToProps,
    { saveAddress }
)(GeoLocationPage);
