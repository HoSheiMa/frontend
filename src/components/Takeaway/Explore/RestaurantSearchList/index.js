import React, { Component } from "react";

import DelayLink from "../../../helpers/delayLink";
import Ink from "react-ink";
import LazyLoad from "react-lazyload";
import ProgressiveImage from "react-progressive-image";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMotorcycle } from '@fortawesome/free-solid-svg-icons';
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons';

class RestaurantSearchList extends Component {
    

    getDayName = () => {
        switch (new Date().getDay()) {
            case 0:
                return "sonntag"
            case 1:
                return "montag"
            case 2:
                return "dienstag"
            case 3:
                return "mittwoch"
            case 4:
                return "donnerstag"
            case 5:
                return "freitag"
            case 6:
                return "samstag"
        }
    }

    openingHours = (openFrom="14:00", openTo="03:00") => {
        //now date
        var now = new Date(); 

        //given date
        var h = now.getHours();
        var day = now.getDate();
        var month = ('0' + (now.getMonth() + 1)).slice(-2)
        var year = now.getUTCFullYear();

        //function slice() removes last 3 char from openTo to only get h without min
        //check if after midnight because date comparison should not be like 2020/01/09 14:00 - 2020/01/09 03:00
        
        if (h < 8 && openTo.slice(0, -3) < 8){//if openTo is after midnight && current time is after midgnight
            var from = new Date(year + "/" + month + "/" + day + " " + openFrom);
            var from = this.addDays2(from, -1);
            var to = new Date(year + "/" + month + "/" + day + " " + openTo); 
        }else if (openTo.slice(0, -3) < 8){//if openTo is after midnight && current time is NOT after midgnight
            var from = new Date(year + "/" + month + "/" + day + " " + openFrom);
            var to = new Date(year + "/" + month + "/" + day + " " + openTo); 
            var to = this.addDays2(to, 1);
        }else{
            var from = new Date(year+"/"+month+"/"+day+" "+openFrom); //set current date and time from table of restaurant 
            var to   = new Date(year+"/"+month+"/"+day+" "+openTo); //set current date and time from table of restaurant
        }

        var today = now.getFullYear()+'/'+(now.getMonth()+1)+'/'+now.getDate()+' '+now.getHours()+":"+now.getMinutes(); 
        var check = new Date(today); //this must always be current date, never change

        //return from+" - "+to;
        if (check > from && check < to)
            return ""; 
        else 
            return "geschlossen"; 
            
    }

    addDays2 = (date, amount) => {
        var tzOff = date.getTimezoneOffset() * 60 * 1000,
            t = date.getTime(),
            d = new Date(),
            tzOff2;
      
        t += (1000 * 60 * 60 * 24) * amount;
        d.setTime(t);
      
        tzOff2 = d.getTimezoneOffset() * 60 * 1000;
        if (tzOff != tzOff2) {
          var diff = tzOff2 - tzOff;
          t += diff;
          d.setTime(t);
        }
      
        return d;
    }
    
    render() {
        const { restaurants } = this.props;
        console.log(restaurants);

        return (
            <React.Fragment>
                <div className="bg-white mb-50 mt-30">
                    <h5 className="px-15 mb-1 text-muted">{localStorage.getItem("exploreRestautantsText")}</h5>
                    {restaurants.map(restaurant => (
                    
                            <React.Fragment key={restaurant.id}>
                                
                                <div className="col-xs-12 col-sm-12 restaurant-block" style={{position:"relative"}}> 

                                    {/* deliveries */}
                                    {/* {restaurant.orders_accepted_online ? <span style={{position: "absolute", bottom: "15px", left: "5px", color:"#999", fontSize:"10px", fontWeight:"bold"}}>
                                        <span style={{margin:"0 8px", color:localStorage.getItem("storeColor")}}>
                                            {restaurant.orders_accepted_online} {localStorage.getItem("homePageDeliveries")}
                                        </span>
                                    </span> : ""} */}
                                                
                                    <DelayLink
                                        to={"../" + restaurant.slug}
                                        delay={200}
                                        className="block text-center"
                                        style={{marginBottom:0}}
                                        clickAction={() => {
                                            localStorage.getItem("userPreferredSelection") === "DELIVERY" &&
                                                restaurant.delivery_type === 1 &&
                                                localStorage.setItem("userSelected", "DELIVERY");
                                            localStorage.getItem("userPreferredSelection") === "SELFPICKUP" &&
                                                restaurant.delivery_type === 2 &&
                                                localStorage.setItem("userSelected", "SELFPICKUP");
                                            localStorage.getItem("userPreferredSelection") === "DELIVERY" &&
                                                restaurant.delivery_type === 3 &&
                                                localStorage.setItem("userSelected", "DELIVERY");
                                            localStorage.getItem("userPreferredSelection") === "SELFPICKUP" &&
                                                restaurant.delivery_type === 3 &&
                                                localStorage.setItem("userSelected", "SELFPICKUP");
                                        }}
                                    >
                                        <div
                                            className={`block-content block-content-full ${
                                                restaurant.is_featured ? "ribbon ribbon-bookmark ribbon-warning pt-2" : "pt-2"
                                            } `}
                                        >
                                            {restaurant.is_featured ? (
                                                <div className="ribbon-box">{localStorage.getItem("restaurantFeaturedText")}</div>
                                            ) : null}

                                            <LazyLoad>
                                                <ProgressiveImage delay={100} src={restaurant.image} placeholder={restaurant.placeholder_image}>
                                                    {(src, loading) => (
                                                        <img
                                                            src={src}
                                                            alt={restaurant.name}
                                                            className="restaurant-image"
                                                            style={{
                                                                filter: loading ? "blur(1.2px) brightness(0.9)" : "none"
                                                            }}
                                                        />
                                                    )}
                                                </ProgressiveImage>
                                            </LazyLoad>
                                        </div>


                                        <div className="block-content block-content-full restaurant-info">

                                            <div className="font-w600 mb-5 text-dark" style={{fontSize:"18px"}}>
                                                {restaurant.name} 
                                            </div>

                                            <div className="font-size-sm text-muted truncate-text">{restaurant.description}</div>
                                            {/* <div style={{fontSize:"10px", color:"#8a8b85"}}>{restaurant.address}</div> */}

                                            <hr className="my-10" />
                                            
                                            <div className="restaurant-meta mt-5 align-items-center justify-content-between text-muted" style={{ fontSize:"12px"}}>

                                                {/* rating */}
                                                {/* <div className="p-0 text-left">
                                                    <i className="fa fa-star pr-1" style={{ color: localStorage.getItem("storeColor") }} />{" "} {restaurant.rating}
                                                </div> */}

                                                {(restaurant.orders_accepted_online+restaurant.orders_accepted_tel) >= 5 ? 
                                                    <span style={{paddingRight:"8px"}}>
                                                        <i className="fa fa-star pr-1" style={{color: localStorage.getItem("storeColor")}} />{restaurant.orders_accepted_online+restaurant.orders_accepted_tel}
                                                    </span>
                                                : null}

                                                {/* delivery time */}
                                                {(restaurant[this.getDayName()]) ? 
                                                    ((restaurant[this.getDayName()] == "geschlossen" || this.openingHours(
                                                        restaurant[this.getDayName()].slice(0, 5),//from ex: 10:00
                                                        restaurant[this.getDayName()].slice(-5)//to ex: 23:00
                                                    ) == "geschlossen") ? 
                                                    <span style={{color:"#ff4246", paddingRight:"8px"}}>
                                                        <i className="si si-clock pr-1" />
                                                        {localStorage.getItem("homePageClosed")}
                                                    </span> : <span style={{color:"green", paddingRight:"8px"}}>
                                                        <i className="si si-clock pr-1" />
                                                        {localStorage.getItem("homePageOpen")}
                                                        {/* {restaurant[this.getDayName()]} */}
                                                    </span>) 
                                                : null}
                                                
                                                {/* delivery charges */}
                                                <FontAwesomeIcon icon={faMotorcycle} style={{fontSize:"12px", marginRight:"3px"}} /><span style={{paddingRight:"8px"}} >{restaurant.delivery_charges=='0.00'?localStorage.getItem("homePageFree"):restaurant.delivery_charges+" "+localStorage.getItem("currencyFormat")}</span>

                                                {/* min order */}
                                                <span style={{display:"inline-block"}}>
                                                    <FontAwesomeIcon icon={faShoppingCart} style={{fontSize:"12px", marginRight:"3px"}} />
                                                    {localStorage.getItem("homePageForTwoText")}{" "}
                                                    {restaurant.price_range}{" "}
                                                    {localStorage.getItem("currencyFormat")}
                                                </span>
                                            </div>
                                        </div>
                                        <Ink duration="500" hasTouch={false} />
                                    </DelayLink>
                                </div>
                            </React.Fragment>

                        

                    ))}
                </div>
            </React.Fragment>
        );
    }
}

export default RestaurantSearchList;
