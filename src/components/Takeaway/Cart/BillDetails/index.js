import React, { Component } from "react";

import { connect } from "react-redux";
import { formatPrice } from "../../../helpers/formatPrice";
import { cartDiscount } from "../../../../services/checkout/actions";//by freelancer on 13 Feb
import { ORDER_LIST_URL } from "../../../../configs/index";
import axios from "axios";

import ReactTooltip from 'react-tooltip';

// fontawesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';


//bootstrap
import Modal from 'react-bootstrap/Modal';
import ReactDOM from 'react-dom';
import { Button } from 'react-bootstrap';

import Checkbox from '../../Checkout/Checkbox';

class BillDetails extends Component {
    state = {
        delivery_charges: 0,
        distance: 0,
        vipChecked: false,
        selfPickupChecked: (localStorage.getItem("userSelected") === "SELFPICKUP" ? true : false),
        activatePremium: false
    };

    handleVipChange = event => {
        this.setState({ 
            vipChecked: false
        })

        setTimeout(() => {
            if(this.state.vipChecked === false){
                this.showModalCompare()
            }
        }, 100);
    }

    handleSelfPickupChange = event => {
        if(localStorage.getItem("userSelected") === "SELFPICKUP"){
            this.setState({ selfPickupChecked: false })
            this.setState({ delivery_charges: this.props.restaurant_info.delivery_charges });
            localStorage.setItem("userSelected", "DELIVERY");
            localStorage.setItem("userPreferredSelection", "DELIVERY");
        } else {
            this.setState({ selfPickupChecked: true })
            this.setState({ delivery_charges: 0 });
            localStorage.setItem("userSelected", "SELFPICKUP");
            localStorage.setItem("userPreferredSelection", "SELFPICKUP");
        }
    }

    showModalCompare () {
        this.setState({
            compareModal: true
        });
    }
    hideModalCompare = () => {
        this.setState({
            compareModal: false
        });
    }

    showModalInvite () {
        this.setState({
            inviteModal: true
        });
    }
    hideModalInvite = () => {
        this.setState({
            inviteModal: false
        });
    }

    showModalVipStatus () {
        this.setState({
            vipStatusModal: true
        });
    }
    hideModalVipStatus = () => {
        this.setState({
            vipStatusModal: false
        });
        console.log("reload page");
        window.location.reload(false);
    }

    componentDidMount() {
        if (localStorage.getItem("userSelected") === "SELFPICKUP") {
            this.setState({ delivery_charges: 0 });
        } else {
            this.setState({ delivery_charges: this.props.restaurant_info.delivery_charges });
        }

        if(localStorage.getItem("fromTel")){
            this.ordersCount();
        }
    }

    ordersCount = () => {
        var url = '//'+window.location.hostname+'/php/Cart_countOrders.php?fromTel='+btoa(localStorage.getItem("fromTel"));
        axios
        .get(url)
        .then(response => {
            this.setState({
                vipStatus: response.data.vipStatus,
            });

            console.log("vipStatus", this.state.vipStatus);
        })
        .catch(function(error) {
            console.log(error);
        });
    }

    openSharePopup = () => {

        const supported = ('share' in navigator);

        if (supported) {
            const shareOpts = {
                title: "Der neue Lieferservice mit unschlagbaren Preisen",
                text: "Keine Lieferkosten, Premium Mitglieder, bis zu 32% Mengenrabatt, Dauerauftrag und vieles mehr.",
                url: "https://lieferservice.vip"
            }
            navigator.share(shareOpts);
        } else {
            alert("Deine Browser Version wird nicht unterstützt. Bitte installiere Chrome 71 oder höher.");
        }
    }

    openContactPopup = async () => {
        try {
            // check if its supported
            const supported = ('contacts' in navigator && 'ContactsManager' in window);
            if (!supported) {
                alert("Deine Browser Version wird nicht unterstützt. Bitte installiere Chrome 77 oder höher.");
            } else {
                // properties to get from contact.
                const props = [
                    "name",
                    "tel",
                    // "email",
                    // "addresses",
                    // "icons"
                ];
                // multiple means you can select multiple contact at a time.
                const contacts = await navigator.contacts.select(props, { multiple: true });

                if(contacts.length > 0){
                    this.hideModalCompare();
                    this.showModalInvite();
                    this.setState({contacts})
                    // contacts.forEach((contact) => {
                    //     this.contactsInput.value +=  contact.name+", ";
                    // });
                } 
            }        
        } catch (error) {
            console.error(error);   
            alert("error: "+error)
        }
    }    
    
    inviteFriends = (fromName, fromTel) => {
        var div = document.getElementById('einladen');
        div.innerHTML = 'Laden...';

        if(fromName && fromTel){
            var result = document.getElementById('result');
            result.innerHTML = '<span style="color:green">Wird gesendet...</span>';

            if(fromName) localStorage.setItem("fromName", fromName);
            if(fromTel) localStorage.setItem("fromTel", this.country_formatted_tel(fromTel));
            let contacts = JSON.stringify(this.state.contacts);
        
            var url = '//'+window.location.hostname+'/php/twilio/FirstScreen_invite.php?fromName='+fromName+'&fromTel='+this.country_formatted_tel(fromTel)+'&contacts='+contacts;
            axios
            .get(url)
            .then(response => {
                this.hideModalInvite();
                this.showModalVipStatus();
                setTimeout(function () {
                    var div = document.getElementById('vipStatus');
                    div.innerHTML = response.data.text;
                }, 100);
            })
            .catch(function(error) {
                console.log(error);
            });
        } else {
            setTimeout(function () {
                var result = document.getElementById('result');
                result.innerHTML = '<span style="color:red">Bitte Name und Telefon eingeben.</span>';

                var div = document.getElementById('einladen');
                div.innerHTML = 'Einladen';
            }, 1000);
        }
    } 
    
    onNameChanged = (e) => {
        this.setState({
            fromName: e.currentTarget.value
        });
    }
    onTelChanged = (e) => {
        this.setState({
            fromTel: e.currentTarget.value
        });
    }

    checkMinOrder(minOrder, gesamt) {
        if (parseFloat(gesamt) < parseFloat(minOrder)) {
            return "error";
        }
    }

    needAmount(minOrder, total) {
        return (parseFloat(minOrder) - parseFloat(total)).toFixed(2);
    }

    componentWillReceiveProps(nextProps) {
        if (localStorage.getItem("userSelected") === "DELIVERY") {
            if (this.props.restaurant_info.delivery_charges !== nextProps.restaurant_info.delivery_charges) {
                this.setState({ delivery_charges: nextProps.restaurant_info.delivery_charges });
            }
        }

        if (nextProps.distance) {
            if (localStorage.getItem("userSelected") === "DELIVERY") {
                if (nextProps.restaurant_info.delivery_charge_type === "DYNAMIC") {
                    this.setState({ distance: nextProps.distance }, () => {
                        //check if restaurant has dynamic delivery charge..
                        this.calculateDynamicDeliveryCharge();
                    });
                }
            }
        }
    }

    calculateDynamicDeliveryCharge = () => {
        const { restaurant_info } = this.props;

        const distanceFromUserToRestaurant = this.state.distance;
        console.log("Distance from user to restaurant: " + distanceFromUserToRestaurant + " km");

        if (distanceFromUserToRestaurant > restaurant_info.base_delivery_distance) {
            const extraDistance = distanceFromUserToRestaurant - restaurant_info.base_delivery_distance;
            console.log("Extra Distance: " + extraDistance + " km");

            const extraCharge = (extraDistance / restaurant_info.extra_delivery_distance) * restaurant_info.extra_delivery_charge;
            console.log("Extra Charge: " + extraCharge);

            const dynamicDeliveryCharge = parseFloat(restaurant_info.base_delivery_charge) + parseFloat(extraCharge);
            console.log("Total Charge: " + dynamicDeliveryCharge);

            this.setState({ delivery_charges: dynamicDeliveryCharge });
        } else {
            this.setState({ delivery_charges: restaurant_info.base_delivery_charge });
        }
    };

    // Calculating total with/without coupon/tax
    getTotalAfterCalculation = () => {
        const { total, restaurant_info, coupon } = this.props;
        let calc = 0;
        var discount = 8;
        if (coupon.code) {
            if (coupon.discount_type === "PERCENTAGE") {
                calc = formatPrice(
                    formatPrice(
                        parseFloat(total) + parseFloat(restaurant_info.restaurant_charges || 0.0) + parseFloat(this.state.delivery_charges || 0.0)
                    ) -
                    formatPrice(
                        (coupon.discount / 100) * parseFloat(total) +
                        parseFloat(restaurant_info.restaurant_charges || 0.0) +
                        parseFloat(this.state.delivery_charges || 0.0)
                    )
                );
            } else {
                calc = formatPrice(
                    parseFloat(total) +
                    (parseFloat(restaurant_info.restaurant_charges) || 0.0) +
                    (parseFloat(this.state.delivery_charges) || 0.0) -
                    (parseFloat(coupon.discount) || 0.0)
                );
            }
        } else {

            if (formatPrice(total) > (parseFloat(restaurant_info.price_range) * 1.9) && this.state.vipStatus && this.state.activatePremium) {//lieferkosten sparen
                calc = formatPrice(
                    parseFloat(total) + parseFloat(restaurant_info.restaurant_charges || 0.0)
                );
            } else{
                calc = formatPrice(
                    parseFloat(total) + parseFloat(restaurant_info.restaurant_charges || 0.0) + parseFloat(this.state.delivery_charges || 0.0)
                );
            }

        }
        if (localStorage.getItem("taxApplicable") === "true") {
            calc = formatPrice(parseFloat(parseFloat(calc) + parseFloat(parseFloat(localStorage.getItem("taxPercentage")) / 100) * calc));
        } 
        else if (formatPrice(total) > (parseFloat(restaurant_info.price_range) * 4) && this.state.vipStatus && this.state.activatePremium) {//32% discount
            calc = formatPrice(calc - (total * ((discount * 4) / 100)));
        } 
        else if (formatPrice(total) > (parseFloat(restaurant_info.price_range) * 3) && this.state.vipStatus && this.state.activatePremium) {//24% discount
            calc = formatPrice(calc - (total * ((discount * 3) / 100)));
        } 
        else if (formatPrice(total) > (parseFloat(restaurant_info.price_range) * 2) && this.state.vipStatus && this.state.activatePremium) {//16% discount
            calc = formatPrice(calc - (total * ((discount * 2) / 100)));
        }

        return calc;
    };

    discount = (total, minOrder, percent) => {
        if(total){
            if(formatPrice(total) > (parseFloat(minOrder) * 4)){
                var xDiscount = (formatPrice(total) * (percent / 100) * 4).toFixed(2);
            }
            else if(formatPrice(total) > (parseFloat(minOrder) * 3)){
                var xDiscount = (formatPrice(total) * (percent / 100) * 3).toFixed(2);
            }
            else if(formatPrice(total) > (parseFloat(minOrder) * 2)){
                var xDiscount = (formatPrice(total) * (percent / 100) * 2).toFixed(2);
            }

            this.props.cartDiscount(xDiscount);
        }else{
            this.props.cartDiscount(0);
        }
        
        return xDiscount;
    }

    discountPercent = (total, minOrder, percent) => {
        if(formatPrice(total) > (parseFloat(minOrder) * 4)){
            var xDiscount = percent * 4;
        }
        else if(formatPrice(total) > (parseFloat(minOrder) * 3)){
            var xDiscount = percent * 3;
        }
        else if(formatPrice(total) > (parseFloat(minOrder) * 2)){
            var xDiscount = percent * 2;
        }

        return xDiscount+'%';
    }

    needAmountDiscount(minOrder, total, leverage) {
        if(leverage == 1.9){//lieferkosten sparen
            return (parseFloat(minOrder)*1.9 - parseFloat(total)).toFixed(2);
        }
        else if(leverage == 2){//16% discount
            return (parseFloat(minOrder)*2 - parseFloat(total)).toFixed(2);
        }
        else if(leverage == 3){//24% discount
            return (parseFloat(minOrder)*3 - parseFloat(total)).toFixed(2);
        }
        else if(leverage == 4){//32% discount
            return (parseFloat(minOrder)*4 - parseFloat(total)).toFixed(2);
        }
        else{//gratisgetränk
            return (15 - parseFloat(total)).toFixed(2);
        }
    }

    country_formatted_tel = (fromTel) => {        
        var fromTel = fromTel.trim();
        var first2 = fromTel.substr(0, 2);
        if(first2 == this.props.restaurant_info.prefix) {
            var fromTel = "+".fromTel;
            return fromTel;
        }
    
        var firstLetter = fromTel.substr(0, 1);
        if(firstLetter == "0") {
            var fromTel = "+"+this.props.restaurant_info.prefix+fromTel.substr(1);
            return fromTel;
        }
    
        return fromTel;
    }

    render() {
        const { total, restaurant_info, coupon } = this.props;
        var rabatt = 8; //in percent

        if (localStorage.getItem("userSelected") === "SELFPICKUP") {
            var delivery_charges = "";
        } else if(formatPrice(total) > (parseFloat(restaurant_info.price_range) * 1.9) && this.state.vipStatus && this.state.activatePremium) {
            var delivery_charges = <React.Fragment>
                <hr />
                <div className="display-flex" style={{ marginBottom: "8px" }}>
                    <div className="flex-auto coupon-text">Gratis Zustellung</div>
                    <div className="flex-auto text-right coupon-text">{restaurant_info.currencyFormat} 0.00</div>
                </div>
            </React.Fragment>
        } else {
            var delivery_charges = <React.Fragment>
                <div className="display-flex" style={{ marginBottom: "8px" }}>
                    <div className="flex-auto">{localStorage.getItem("cartDeliveryCharges")}</div>
                    <div className="flex-auto text-right">
                        {restaurant_info.currencyFormat} {formatPrice(this.state.delivery_charges)}
                    </div>
                </div>
            </React.Fragment>
        }


        return (
            <React.Fragment>
                <div className="bg-white bill-details mb-20">
                    <div className="p-15">
                        {/* <h2 className="bill-detail-text m-0">{localStorage.getItem("cartBillDetailsText")}</h2> */}

                        {/* sub total */}
                        {/* <div className="display-flex" style={{ marginBottom: "8px" }}>
                            <div className="flex-auto">{localStorage.getItem("cartItemTotalText")}</div>
                            <div className="flex-auto text-right">
                                {restaurant_info.currencyFormat} {formatPrice(total)}
                            </div>
                        </div> */}
                        
                        {/* delivery_charges */}
                        {/* {delivery_charges} */}

                        {/* service fee */}
                        {/* {localStorage.getItem("taxApplicable") === "true" && (
                            <React.Fragment>
                                <div className="display-flex" style={{ marginBottom: "8px" }}>
                                    <div className="flex-auto">Servicegebühr <span data-tip='' data-for='serviceFeeInfo'><FontAwesomeIcon icon={faInfoCircle} /></span></div>
                                    <div className="flex-auto text-right">
                                        {restaurant_info.currencyFormat} {(localStorage.getItem("taxPercentage") * formatPrice(total) / 100).toFixed(2)}
                                    </div>
                                </div>
                            </React.Fragment>
                        )} */}

                        {/* <ReactTooltip id='serviceFeeInfo' place="top"><div style={{fontSize:"10px", fontWeight:"300", fontFamily:"open sans"}}>Diese Gebühr unterstützt die Plattform und deckt<br/>eine breite Palette von Betriebskosten ab,<br/>einschließlich Versicherung und Kundenservice.</div></ReactTooltip> */}

                        {/* restaurant_charges */}
                        {/* {restaurant_info.restaurant_charges === "0.00" || restaurant_info.restaurant_charges === null ? null : (
                            <div className="display-flex" style={{ marginBottom: "8px" }}>
                                <div className="flex-auto">{localStorage.getItem("cartRestaurantCharges")}</div>
                                <div className="flex-auto text-right">
                                    {restaurant_info.currencyFormat} {restaurant_info.restaurant_charges}
                                </div>
                            </div>
                        )} */}

                        {/* <hr /> */}

                        {/* coupon */}
                        {/* {coupon.code && (
                            <React.Fragment>
                                <div className="display-flex" style={{ marginBottom: "8px" }}>
                                    <div className="flex-auto coupon-text">{localStorage.getItem("cartCouponText")}</div>
                                    <div className="flex-auto text-right coupon-text">
                                        <span>-</span>
                                        {coupon.discount_type === "PERCENTAGE"
                                            ? coupon.discount + "%"
                                            : restaurant_info.currencyFormat + coupon.discount}
                                    </div>
                                </div>
                                <hr />
                            </React.Fragment>
                        )} */}

                        {/* premium discount max. 32% */}
                        {formatPrice(total) > (parseFloat(restaurant_info.price_range) * 2) && this.state.vipStatus && this.state.activatePremium ? (
                            <React.Fragment>
                            <div className="display-flex" style={{ marginBottom: "8px" }}>
                                <div className="flex-auto coupon-text">{this.discountPercent(total, restaurant_info.price_range, rabatt)} Mengenrabatt</div>
                                <div className="flex-auto text-right coupon-text">
                                    <span>-</span>
                                    {restaurant_info.currencyFormat} {this.discount(formatPrice(total), restaurant_info.price_range, rabatt)}
                                </div>
                            </div>
                            <hr />
                            </React.Fragment>
                        ) : this.discount()}

                        {/* gratisgetränk */}
                        {formatPrice(total) >= 15 && this.state.vipStatus && this.state.activatePremium ? (
                            <React.Fragment>
                            <div className="display-flex" style={{ marginBottom: "8px" }}>
                                <div className="flex-auto coupon-text">Gratisgetränk (0,3L Cola)</div>
                                <div className="flex-auto text-right coupon-text">
                                    {restaurant_info.currencyFormat} 0.00
                                </div>
                            </div>
                            <hr />
                            </React.Fragment>
                        ) : (null)}

                        {/* total */}
                        {/* <div className="display-flex" style={{ marginBottom: "8px" }}>
                            <div className="flex-auto font-w700">{localStorage.getItem("cartToPayText")}</div>
                            <div className="flex-auto text-right font-w700">
                                {restaurant_info.currencyFormat} 
                                {this.getTotalAfterCalculation()}
                            </div>
                        </div> */}

                        {/* subtotal */}
                        <div className="display-flex" style={{ marginBottom: "8px" }}>
                            <div className="flex-auto font-w700">{localStorage.getItem("cartItemTotalText")}</div>
                            <div className="flex-auto text-right font-w700">
                                {restaurant_info.currencyFormat} {formatPrice(total)}
                            </div>
                        </div>

                        {/* min order */}
                        {this.checkMinOrder(restaurant_info.price_range, total) == "error" && (
                            <div className="display-flex" style={{ marginBottom: "8px", color:"#f44336", fontWeight:"700"}}>
                                <div className="flex-auto">Benötigter Betrag, um den Mindestbestellwert zu erreichen</div>
                                <div className="flex-auto text-right" style={{minWidth:"50px"}}>
                                    {restaurant_info.currencyFormat} {this.needAmount(restaurant_info.price_range, total)}
                                </div>
                            </div>
                        )}

                        {/* <hr /> */}







                        {/* Premium Vorteile aktivieren */}
                        {this.state.vipStatus ? null : (this.state.activatePremium?<label style={{width:"100%"}}>
                            <Checkbox
                                name="vipVorteile"
                                checked={this.state.vipChecked}
                                onChange={this.handleVipChange}
                            />
                            <span style={{marginLeft:"5px"}}>Premium Vorteile gratis freischalten</span>
                        </label>:null)}

                        {/* selbstabholung aktivieren */}
                        {/* <label style={{width:"100%"}}>
                            <Checkbox
                                name="selfPickup"
                                checked={this.state.selfPickupChecked}
                                onChange={this.handleSelfPickupChange}
                            />
                            <span style={{marginLeft:"5px"}}>{localStorage.getItem("selectedSelfPickupMessage")}</span>
                        </label> */}

                        {/* compare Premium vs guest */}
                        <Modal show={this.state.compareModal} onHide={this.hideModalCompare}>
                            
                            <Modal.Header closeButton>
                                <Modal.Title>Vorteile Vergleichen</Modal.Title>
                            </Modal.Header>

                            <Modal.Body style={{padding:"0 25px 10px"}}>

                                <div className="vorteile" style={{border:0}}>
                                    <div style={{width:"150px", float:"left"}}>&nbsp;</div>
                                    <div style={{width:"50px", float:"left"}}>Gast</div>
                                    <div style={{width:"50px", float:"left"}}>Premium</div>
                                </div>

                                <div className="vorteile">
                                    <div style={{width:"150px", float:"left"}}>32% Mengenrabatt</div>
                                    <div style={{width:"50px", float:"left"}}>x</div>
                                    <div style={{width:"50px", float:"left"}}><FontAwesomeIcon style={{marginRight:"5px"}} icon={faCheck} /></div>
                                </div>

                                <div className="vorteile">
                                    <div style={{width:"150px", float:"left"}}>Gratis Getränk</div>
                                    <div style={{width:"50px", float:"left"}}>x</div>
                                    <div style={{width:"50px", float:"left"}}><FontAwesomeIcon style={{marginRight:"5px"}} icon={faCheck} /></div>
                                </div>

                                <div className="vorteile">
                                    <div style={{width:"150px", float:"left"}}>Gratis Zustellung</div>
                                    <div style={{width:"50px", float:"left"}}>x</div>
                                    <div style={{width:"50px", float:"left"}}><FontAwesomeIcon style={{marginRight:"5px"}} icon={faCheck} /></div>
                                </div>

                                <div className="vorteile">
                                    <div style={{width:"150px", float:"left"}}>Dauerauftrag</div>
                                    <div style={{width:"50px", float:"left"}}><FontAwesomeIcon style={{marginRight:"5px"}} icon={faCheck} /></div>
                                    <div style={{width:"50px", float:"left"}}><FontAwesomeIcon style={{marginRight:"5px"}} icon={faCheck} /></div>
                                </div>

                                <div className="vorteile">
                                    <div style={{width:"150px", float:"left"}}>Google Assistant Bestellung</div>
                                    <div style={{width:"50px", float:"left"}}><FontAwesomeIcon style={{marginRight:"5px"}} icon={faCheck} /></div>
                                    <div style={{width:"50px", float:"left"}}><FontAwesomeIcon style={{marginRight:"5px"}} icon={faCheck} /></div>
                                </div>

                                <div className="vorteile">
                                    <div style={{width:"150px", float:"left"}}>Schnellere Lieferung</div>
                                    <div style={{width:"50px", float:"left"}}>x</div>
                                    <div style={{width:"50px", float:"left"}}><FontAwesomeIcon style={{marginRight:"5px"}} icon={faCheck} /></div>
                                </div>

                                <div className="vorteile">
                                    <div style={{width:"150px", float:"left"}}>Kostenlos</div>
                                    <div style={{width:"50px", float:"left"}}><FontAwesomeIcon style={{marginRight:"5px"}} icon={faCheck} /></div>
                                    <div style={{width:"50px", float:"left"}}><FontAwesomeIcon style={{marginRight:"5px"}} icon={faCheck} /></div>
                                </div>

                                <div style={{marginTop:"20px", fontSize:"11px"}}>Normalerweise werden Premium Vorteile nach 10 Bestellungen freigeschaltet. Um diesen Schritt zu überspringen, mindestens 10 Kontake bitte einladen.</div>
                            
                            </Modal.Body>

                            <Modal.Footer>
                                {/* <Button variant="secondary" onClick={this.hideModalCompare}> 
                                    Schließen
                                </Button>  */}
                                <Button variant="secondary" onClick={() => this.openSharePopup()}> 
                                    Teilen
                                </Button>
                                <Button variant="primary" onClick={() => this.openContactPopup()}>
                                    Kontakte auswählen
                                </Button>
                            </Modal.Footer>
                        </Modal>

                        {/* invite 10 friends */}
                        <Modal show={this.state.inviteModal} onHide={this.hideModalInvite}>
                    
                            <Modal.Header closeButton>
                                <Modal.Title>Premium Vorteile freischalten für...</Modal.Title>
                            </Modal.Header>

                            <Modal.Body style={{padding:"0 25px 10px"}}>
                                <div style={{marginTop:"12px"}}>

                                    {/* <label style={{fontWeight:"normal", display:"block"}}>
                                        Freunde *
                                        <textarea 
                                        readOnly
                                        name="contacts" 
                                        type="text" 
                                        style={{padding:"5px", width:"100%", height:"100px"}} 
                                        ref={input => {
                                            this.contactsInput = input;
                                        }} 
                                        />
                                    </label> */}

                                    <label style={{fontWeight:"normal", display:"block", marginTop:"10px"}}>
                                        Name *
                                        <input 
                                        name="name" 
                                        style={{padding:"5px", width:"100%"}} 
                                        defaultValue={this.state.fromName}
                                        onChange={this.onNameChanged} 
                                        // ref={input => {
                                        //     this.nameInput = input;
                                        // }}
                                        // autoFocus
                                        />
                                    </label>

                                    <label style={{fontWeight:"normal", display:"block", marginTop:"10px"}}>
                                        Telefonnummer *
                                        <input 
                                        name="tel" 
                                        type="tel" 
                                        maxLength="15" 
                                        style={{padding:"5px", width:"100%"}} 
                                        defaultValue={this.state.fromTel}
                                        onChange={this.onTelChanged} 
                                        />
                                    </label>

                                </div>

                                <div id="result" style={{marginTop:"10px"}}></div>

                                <div style={{marginTop:"20px", fontSize:"9px", color:"#999"}}>Bitte Deinen Namen und Tel eingeben und auf Freischalten klicken.</div>
                            </Modal.Body>

                            <Modal.Footer>
                                <Button variant="secondary" onClick={this.hideModalInvite}> 
                                    Schließen
                                </Button> 
                                <Button variant="primary" onClick={() => this.inviteFriends(this.state.fromName, this.state.fromTel)} id="einladen">
                                    Freischalten
                                </Button>
                            </Modal.Footer>
                        </Modal>

                        {/* Premium status after adding friends */}
                        <Modal show={this.state.vipStatusModal} onHide={this.hideModalVipStatus}>
                            
                            <Modal.Header closeButton>
                                <Modal.Title>Premium Status</Modal.Title>
                            </Modal.Header>

                            <Modal.Body style={{padding:"0 25px 10px"}}>

                                <div id="vipStatus"></div>

                                {/* <div style={{marginTop:"20px", fontSize:"9px", color:"#999"}}>Bitte Deinen Namen und Tel eingeben (für gratis Premium Status) und auf den Teilen Button klicken um die App kostenlos via SMS mit Deinen Freunden zu teilen.</div> */}
                            
                            </Modal.Body>

                            <Modal.Footer>
                                <Button variant="secondary" onClick={this.hideModalVipStatus}> 
                                    Schließen
                                </Button> 
                            </Modal.Footer>
                        </Modal>  
                        
                        {/* free drink  */}
                        {this.state.vipStatus && this.state.activatePremium && localStorage.getItem("userSelected") === "DELIVERY" && formatPrice(total) < 15 ? (
                            <div className="display-flex">
                                <div className="flex-auto" style={{fontSize:"11px", fontFamily:"open sans", fontWeight:300}}><FontAwesomeIcon style={{marginRight:"5px"}} icon={faCheck} />Gratisgetränk ab 15 {restaurant_info.currencyFormat}</div>
                                {/* <div className="flex-auto text-right" style={{minWidth:"50px"}}>
                                    {restaurant_info.currencyFormat} {this.needAmountDiscount("", total)}
                                </div> */}
                            </div>
                        ):""}

                        {/* no delivery charges  */}
                        {this.state.vipStatus && this.state.activatePremium && localStorage.getItem("userSelected") === "DELIVERY" && formatPrice(total) < (parseFloat(restaurant_info.price_range) * 1.9) ? (
                            <div className="display-flex">
                                <div className="flex-auto" style={{fontSize:"11px", fontFamily:"open sans", fontWeight:300}}><FontAwesomeIcon style={{marginRight:"5px"}} icon={faCheck} />Gratis Zustellung ab {(parseFloat(restaurant_info.price_range) * 1.9).toFixed(2)} {restaurant_info.currencyFormat}</div>
                                {/* <div className="flex-auto text-right" style={{minWidth:"50px"}}>
                                    {restaurant_info.currencyFormat} {this.needAmountDiscount(restaurant_info.price_range, total, 1.9)}
                                </div> */}
                            </div>
                        ):""}

                        {/* 16% discount */}
                        {this.state.vipStatus && this.state.activatePremium && formatPrice(total) < (parseFloat(restaurant_info.price_range) * 2) ? (
                            <div className="display-flex">
                                <div className="flex-auto" style={{fontSize:"11px", fontFamily:"open sans", fontWeight:300}}><FontAwesomeIcon style={{marginRight:"5px"}} icon={faCheck} />16% Rabatt ab {(parseFloat(restaurant_info.price_range) * 2).toFixed(2)} {restaurant_info.currencyFormat}</div>
                                {/* <div className="flex-auto text-right" style={{minWidth:"50px"}}>
                                    {restaurant_info.currencyFormat} {this.needAmountDiscount(restaurant_info.price_range, total, 2)}
                                </div> */}
                            </div>
                        ):""}

                        {/* 24% discount */}
                        {this.state.vipStatus && this.state.activatePremium && formatPrice(total) < (parseFloat(restaurant_info.price_range) * 3) ? (
                            <div className="display-flex">
                                <div className="flex-auto" style={{fontSize:"11px", fontFamily:"open sans", fontWeight:300}}><FontAwesomeIcon style={{marginRight:"5px"}} icon={faCheck} />24% Rabatt ab {(parseFloat(restaurant_info.price_range) * 3).toFixed(2)} {restaurant_info.currencyFormat}</div>
                                {/* <div className="flex-auto text-right" style={{minWidth:"50px"}}>
                                    {restaurant_info.currencyFormat} {this.needAmountDiscount(restaurant_info.price_range, total, 3)}
                                </div> */}
                            </div>
                        ):""}

                        {/* 32% discount */}
                        {this.state.vipStatus && this.state.activatePremium && formatPrice(total) < (parseFloat(restaurant_info.price_range) * 4) ? (
                            <div className="display-flex" style={{ marginBottom: "8px"}}>
                                <div className="flex-auto" style={{fontSize:"11px", fontFamily:"open sans", fontWeight:300}}><FontAwesomeIcon style={{marginRight:"5px"}} icon={faCheck} />32% Rabatt ab {(parseFloat(restaurant_info.price_range) * 4).toFixed(2)} {restaurant_info.currencyFormat}</div>
                                {/* <div className="flex-auto text-right" style={{minWidth:"50px"}}>
                                    {restaurant_info.currencyFormat} {this.needAmountDiscount(restaurant_info.price_range, total, 4)}
                                </div> */}
                            </div>
                        ):""}

                        {this.checkMinOrder(restaurant_info.price_range, this.getTotalAfterCalculation()) == "error" && (
                            <div className="minimumorderamount-reached">Leider kannst Du noch nicht bestellen. Unsere Shopper liefern erst ab einem Mindestbestellwert von {restaurant_info.price_range} {restaurant_info.currencyFormat} (exkl. Lieferkosten).</div>
                        )}



                    </div>
                </div>

            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    coupon: state.coupon.coupon,
    restaurant_info: state.items.restaurant_info,
});

export default connect(
    mapStateToProps,
    {cartDiscount}//by freelancer on 13 Feb
)(BillDetails);
