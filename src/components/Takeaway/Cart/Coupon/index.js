import React, { Component } from "react";

import Ink from "react-ink";
import { applyCoupon } from "../../../../services/coupon/actions";
import { connect } from "react-redux";

import Modal from 'react-bootstrap/Modal';
import { formatPrice } from "../../../helpers/formatPrice";
// import { getSettings } from "../../../../services/settings/actions";

class Coupon extends Component {
	
    constructor() {
        super();
        
        this.state = { 
            inputCoupon: "",
            couponFailed: false,
            couponFailedType: "",
            couponErrorMessage: "",
        };

        this.innerRef = React.createRef();
    }

	componentDidMount() {
		// automatically apply coupon if already exists in localstorage
		if (localStorage.getItem("appliedCoupon")) {
			this.setState({ inputCoupon: localStorage.getItem("appliedCoupon") }, () => {
				// this.refs.couponInput.defaultValue = localStorage.getItem("appliedCoupon");
				const { user } = this.props;
				const token = user.success ? this.props.user.data.auth_token : null;
				this.props
					.applyCoupon(
						token,
						localStorage.getItem("appliedCoupon"),
						this.props.restaurant_info.id,
						this.props.subtotal
					)
					.then((res) => {
						if (res) {
							if (res[0].payload.message === undefined) {
								this.setState({ couponFailed: false });
							}
						}
					});
			});
        }
        
        // this.props.getSettings();


	}
	componentWillReceiveProps(nextProps) {
		const { coupon } = this.props;
		//check if props changed after calling the server
		if (coupon !== nextProps.coupon) {
			//if nextProps.coupon is successful then
			if (nextProps.coupon.success) {
				console.log("SUCCESS COUPON");
				localStorage.setItem("appliedCoupon", nextProps.coupon.code);
				this.setState({ couponFailed: false, couponErrorMessage: "" });
			} else {
				console.log("COUPON Removed");
				// coupon is invalid
				console.log("FAILED COUPON");
				localStorage.removeItem("appliedCoupon");
				this.setState({
					couponFailed: !nextProps.coupon,
					couponFailedType: nextProps.coupon.type,
					couponErrorMessage: nextProps.coupon.message,
				});
			}
		}
	}
	handleInput = (event) => {
		this.setState({ inputCoupon: event.target.value });
	};

	handleSubmit = (event) => {
		event.preventDefault();
		const { user } = this.props;
		const token = user.success ? this.props.user.data.auth_token : null;
		this.props
			.applyCoupon(token, this.state.inputCoupon, this.props.restaurant_info.id, this.props.subtotal)
			.then((res) => {
				if (res) {
					if (res[0] === undefined || res[0].payload === undefined || res[0].payload.message === undefined) {
						this.setState({ couponFailed: false });
					}
				}
			});
    };
    
    showModalCoupon () {
        this.setState({
            CouponModal: true
        });

        setTimeout(() => {
            this.innerRef.current.focus();
        }, 100)
    }
    hideModalCoupon = () => {
        this.setState({
            CouponModal: false
        });
    }
    
    getSettings = (data, keyName)=>{
        var filteredData = data.filter(x=>x.key == keyName);
        var focusData = undefined;
        if(filteredData.length > 0){
            focusData = filteredData[0].value;
        }
        return focusData;
    }

	render() {
		const { coupon, user, coupon_error } = this.props;
		const { couponFailed, couponErrorMessage } = this.state;

		return (
			<React.Fragment>

                <b>{localStorage.getItem("cartCouponText")}</b>

                <span style={{textTransform:"uppercase", color:"rgb(252, 128, 25)", border:"1px solid rgb(252, 128, 25)", padding:"3px 5px", marginLeft:"10px", fontSize:"11px", borderRadius:"3px", cursor:"pointer"}} onClick={() => this.showModalCoupon()}>hinzufügen</span>

                {coupon.code && (
                    coupon.discount_type === "PERCENTAGE" ? (
                        <span style={{float:"right"}}>
                            - <span style={{color:"rgb(117, 117, 117)", fontWeight:300, fontFamily:"open sans", marginRight:"5px"}}>({coupon.discount}%)</span> 
                            {this.props.restaurant_info.currencyFormat}
                            {formatPrice(
                                (coupon.discount / 100) * (
                                    parseFloat(this.props.subtotal) +
                                    parseFloat(this.props.restaurant_info.restaurant_charges || 0.0) + //restaurant charges
                                    parseFloat(this.props.restaurant_info.delivery_charges || 0.0) + //delivery charges
                                    parseFloat( (parseFloat(this.props.subtotal) * parseFloat(this.getSettings(this.props.settings, "serviceFee") / 100)) || 0.0 ) + //service fee
                                    parseFloat( (parseFloat(this.props.subtotal) * parseFloat(this.props.delivery_tip / 100)) || 0.0 ) //delivery tip
                                )
                            )}
                        </span>
                    ) : (
                        <span style={{float:"right"}}>
                            - {this.props.restaurant_info.currencyFormat}{(parseFloat(coupon.discount).toFixed(2) || 0.0)}
                        </span>
                    )
                )}

                <Modal show={this.state.CouponModal} onHide={this.hideModalCoupon}>
                    <Modal.Header closeButton>
                        <Modal.Title>Gutscheincode hinzufügen</Modal.Title>
                    </Modal.Header>
                    <Modal.Body style={{padding:"15px"}}>
                        <form className="coupon-form" onSubmit={this.handleSubmit}>
                            <div className="input-group">
                                <div className="input-group-prepend">
                                    <button className="btn apply-coupon-btn">
                                        <i className="si si-tag" />
                                    </button>
                                </div>
                                <input
                                    type="text"
                                    className="form-control apply-coupon-input"
                                    placeholder={localStorage.getItem("cartCouponText")}
                                    onChange={this.handleInput}
                                    style={{ color: localStorage.getItem("storeColor") }}
                                    spellCheck="false"
                                    defaultValue={localStorage.getItem("appliedCoupon")?localStorage.getItem("appliedCoupon"):""}
                                    ref={this.innerRef}
                                />
                                <div className="input-group-append">
                                    <button type="submit" className="btn apply-coupon-btn" onClick={this.handleSubmit}>
                                        <i className="si si-arrow-right" />
                                        <Ink duration="500" />
                                    </button>
                                </div>
                            </div>
                        </form>

                        <div className="mt-20 mb-20 text-center" style={{fontFamily:"open sans", fontWeight:300}}>Es gelten die <a href="/pages/agb" target="_blank" style={{color:"blue"}}>Allgemeinen Geschäftsbedingungen</a></div>
                    </Modal.Body>
                </Modal>

                <div className="coupon-status mt-10">
                    {coupon.code && (
                        <div className="coupon-success pt-10 pb-10">
                            {localStorage.getItem("showCouponDescriptionOnSuccess") === "true" ? (
                                <React.Fragment>{coupon.description}</React.Fragment>
                            ) : (
                                <React.Fragment>
                                    {'"' + coupon.code + '"'} {localStorage.getItem("cartApplyCoupon")}{" "}
                                    {coupon.discount_type === "PERCENTAGE" ? (
                                        coupon.discount + "%"
                                    ) : (
                                        <React.Fragment>
                                            {localStorage.getItem("currencySymbolAlign") === "left" &&
                                                localStorage.getItem("currencyFormat") + coupon.discount}
                                            {localStorage.getItem("currencySymbolAlign") === "right" &&
                                                coupon.discount + localStorage.getItem("currencyFormat")}{" "}
                                            {/* {localStorage.getItem("cartCouponOffText")} */}
                                        </React.Fragment>
                                    )}
                                </React.Fragment>
                            )}
                        </div>
                    )}
                    {/* Coupon is not applied, then coupon state is true */}
                    {couponFailed && <div className="coupon-fail pt-10 pb-10">{couponErrorMessage}</div>}

                    {!couponFailed && couponErrorMessage !== "" && !coupon.hideMessage && (
                        <React.Fragment>
                            {user.success && (
                                <div className="coupon-fail pt-10 pb-10">
                                    {localStorage.getItem("cartInvalidCoupon")}
                                </div>
                            )}
                        </React.Fragment>
                    )}

                    {coupon_error === "NOTLOGGEDIN" && !user.success && (
                        <div className="coupon-fail pt-10 pb-10">{localStorage.getItem("couponNotLoggedin")}</div>
                    )}
                </div>

			</React.Fragment>
		);
	}
}

const mapStateToProps = (state) => ({
	user: state.user.user,
	coupon: state.coupon.coupon,
	restaurant_info: state.items.restaurant_info,
	coupon_error: state.coupon.coupon_error,
});

export default connect(
	mapStateToProps,
    { 
        applyCoupon, 
        // getSettings 
    }
)(Coupon);
