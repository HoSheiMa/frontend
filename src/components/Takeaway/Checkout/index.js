import React, { Component } from "react";

import BackWithSearch from "../../Takeaway/Elements/BackWithSearch";
import Meta from "../../helpers/meta";
import PaymentList from "./PaymentGuest";
import { Redirect } from "react-router";
import { checkConfirmCart } from "../../../services/confirmCart/actions";
import { connect } from "react-redux";
import { placeOrder } from "../../../services/checkout/actions";
import Ink from "react-ink";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUtensils } from '@fortawesome/free-solid-svg-icons';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import Nav from "../Nav";

// import Ink from "react-ink";




// import DelayLink from "../../helpers/delayLink";




class Checkout extends Component {


    static contextTypes = {
        router: () => null
    };

    componentDidMount() {
        if (this.props.cartProducts.length) {
            document.getElementsByTagName("body")[0].classList.add("bg-grey-light");
        }
    }

    __placeOrder = () => {
        const { user, cartProducts, coupon, cartTotal } = this.props;
        if (user.success) {
            this.props.placeOrder(user, cartProducts, coupon, localStorage.getItem("currentLocation"), cartTotal);
        }
    };

        
    getAddress = (address, country=false) => {
        if(country === false){
            var res = address;
            var split = res.split(",");
            var res = split.slice(0, split.length - 1).join(",");
            return res;
        }else{
            var res = address.split(", ");
            return res[0];
        }
    }

    componentWillUnmount() {
        document.getElementsByTagName("body")[0].classList.remove("bg-grey-light");
    }

    render() {
        //TODO:
        //check if the referrer is cart page, if not then redirect to cart
        // if (!this.props.confirmCart) {
        //     return <Redirect to={"/cart"} />;
        // }
        if (localStorage.getItem("storeColor") === null) {
            return <Redirect to={"/"} />;
        }
        if (localStorage.getItem("userSetAddress") === null) {
            return <Redirect to={"/search-location"} />;
        }
        return (
            <React.Fragment>
                <Meta
                    seotitle={localStorage.getItem("checkoutPageTitle")}
                    seodescription={localStorage.getItem("seoMetaDescription")}
                    ogtype="website"
                    ogtitle={localStorage.getItem("seoOgTitle")}
                    ogdescription={localStorage.getItem("seoOgDescription")}
                    ogurl={window.location.href}
                    twittertitle={localStorage.getItem("seoTwitterTitle")}
                    twitterdescription={localStorage.getItem("seoTwitterDescription")}
                />
                {/* <BackWithSearch boxshadow={true} has_title={true} title={localStorage.getItem("checkoutPageTitle")} disbale_search={true} /> */}
                
                {/* <Nav logo={true} active_nearme={true} disable_back_button={true} /> */}

                
                <PaymentList />
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    total: state.total.total,
    user: state.user.user,
    cartProducts: state.cart.products,
    cartTotal: state.total.data,
    coupon: state.coupon.coupon,
    confirmCart: state.confirmCart.confirmCart
});
export default connect(
    mapStateToProps,
    { checkConfirmCart, placeOrder }
)(Checkout);
