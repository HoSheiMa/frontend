const checkboxes = [
    {
      name: 'Montag',
      key: '1',
      label: '1',
    },
    {
      name: 'Dienstag',
      key: '2',
      label: '2',
    },
    {
      name: 'Mittwoch',
      key: '3',
      label: '3',
    },
    {
      name: 'Donnerstag',
      key: '4',
      label: '4',
    },
    {
      name: 'Freitag',
      key: '5',
      label: '5',
    },
    {
      name: 'Samstag',
      key: '6',
      label: '6',
    },
    {
      name: 'Sonntag',
      key: '7',
      label: '7',
    },
  ];
  
  export default checkboxes;