import * as firebase from "firebase/app";
import messaging from "../../../../init-fcm";
import { saveNotificationToken } from "../../../../services/notification/actions";

import React, { Component } from "react";
import { connect } from "react-redux";
import Ink from "react-ink";
import axios from "axios";
import { getSettings } from "../../../../services/settings/actions";
import Nav from "../../Nav";

import ContentLoader from "react-content-loader";
import { Helmet } from "react-helmet";

import PaypalExpressBtn from "react-paypal-express-checkout";
import PaystackButton from "react-paystack";
import { RAZORPAY_PAYMENT_URL } from "../../../../configs";
import StripeCheckout from "react-stripe-checkout";

import { formatPrice } from "../../../helpers/formatPrice";
import { placeOrder, placeOrderV2 } from "../../../../services/checkout/actions";
import { updateUserInfo } from "../../../../services/user/actions";
import { calculateDistance } from "../../../helpers/calculateDistance";

import Modal from 'react-bootstrap/Modal';
import ReactDOM from 'react-dom';
import { Button } from 'react-bootstrap';
import ReactTooltip from 'react-tooltip'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import setHours from "date-fns/setHours";
import setMinutes from "date-fns/setMinutes";

import PropTypes from 'prop-types';
import checkboxes from '../checkboxes';
import Checkbox from '../Checkbox';

import { loadStripe } from '@stripe/stripe-js';

import Coupon from "../../Cart/Coupon";

import { WEBSITE_URL } from "../../../../configs/website";

const stripePromise = loadStripe('pk_live_6SY1GFAnsvJEYaGtOCMZhbrf006iB8fEHP');

class PaymentGuest extends Component {

    constructor() {
        super();

        if(JSON.parse(localStorage.getItem("state")).user.user.success) {
            var fromName = JSON.parse(localStorage.getItem("state")).user.user.data.name;
            var fromTel = JSON.parse(localStorage.getItem("state")).user.user.data.phone;
        }else{
            var fromName = localStorage.getItem("fromName");
            var fromTel = localStorage.getItem("fromTel");
        }
        
        this.state = { 
            loading: true,
            stripe_opened: false,
            delivery_charges: 0.0,
            error: false,
            canPayPartialWithWallet: false,
            walletChecked: false,
            canPayFullWithWallet: false,
            distance: 0,

            bgWidth: window.innerWidth,
            bgHeight: (window.innerHeight / 4.2),
            fromName: fromName,
            fromTel: fromTel,
            pushChecked: false,
            //cronTime: new Date(),//no default time needed for now
            checkedItems: new Map(),
            delivery_tip: localStorage.getItem("deliveryTip"),//in %
            delivery_instructions: '',
            leaveAtDoorChecked: '',

        };

        this.innerRef = React.createRef();
        this.onDayChanged = this.onDayChanged.bind(this);
        // this.notificationPrompt = this.notificationPrompt.bind(this);

    }

    static contextTypes = {
        router: () => null
    };
    
    componentDidMount() {
        const { user } = this.props;

        if (user.success) {
            this.props.updateUserInfo(user.data.id, user.data.auth_token, null);
        }

        this.props.getSettings();

        if (localStorage.getItem("userSelected") === "SELFPICKUP") {
            this.setState({ delivery_charges: 0.0 });
        } else {
            this.setState({ delivery_charges: this.props.restaurant_info.delivery_charges });
        }
        
        this.timeout = setInterval(() => {
            this.setState({ 
                bgWidth: window.innerWidth,
                bgHeight: (window.innerHeight / 4.2)
            });
            
        }, 1000);

        // this.setState({ 
        //     delivery_tip: this.getSettings(this.props.settings, "deliveryTip")
        // })
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.restaurant_info.id) {
            this.__doesRestaurantOperatesAtThisLocation(nextProps.restaurant_info);
        }

        const { paymentgateways, user } = this.props;
        if (paymentgateways !== nextProps.paymentgateways) {
            this.setState({ loading: false });
        }
        // if (nextProps.checkout !== this.props.checkout) {
        //     //remove coupon
        //     localStorage.removeItem("appliedCoupon");
        //     localStorage.removeItem("refreshed_invoice");
        //     //redirect to running order page
        //     this.context.router.history.push("/running-order/" + nextProps.checkout.data.id);
        // }

        //when setdefaultaddress returns new addresses
        //if (JSON.parse(localStorage.getItem("state")).user.user.success) {
        if (user.success) {
            if (user.data.default_address !== null) {
                // console.log("userSetAddress");
                const userSetAddress = {
                    lat: user.data.default_address.latitude,
                    lng: user.data.default_address.longitude,
                    address: user.data.default_address.address,
                    house: user.data.default_address.house,
                    tag: user.data.default_address.tag
                };
                localStorage.setItem("userSetAddress", JSON.stringify(userSetAddress));
            }
        }

        // this.setState({ 
        //     delivery_tip: this.getSettings(this.props.settings, "deliveryTip")
        // })
        
        if (user.success) {
            // console.log("Wallet Balance: ", nextProps.user.data.wallet_balance);
            // console.log("Cart Amount: ", parseFloat(this.getTotalAfterCalculation()));

            //if  > 0 then user can pay with wallet (Amount will be deducted)
            if (nextProps.user.data.wallet_balance > 0) {
                console.log("Can pay partial with wallet");
                this.setState({ canPayPartialWithWallet: true, canPayFullWithWallet: false });
            }
    
            // if (nextProps.user.data.wallet_balance >= parseFloat(this.total(subtotal))) {
            if (nextProps.user.data.wallet_balance >= parseFloat(this.getTotalAfterCalculation())) {
                console.log("Can pay full with wallet");
                this.setState({ canPayFullWithWallet: true, canPayPartialWithWallet: false });
            }
        }
    }

    // Calculating total with/without coupon/tax
    getTotalAfterCalculation = () => {
        const { coupon, restaurant_info, user } = this.props;
        const total = this.props.cartTotal.totalPrice;
        let calc = 0;
        if (coupon.code) {
            if (coupon.discount_type === "PERCENTAGE") {//20.92 - (20.92 * 0.1)
                calc = formatPrice(
                    formatPrice(
                        parseFloat(total) + 
                        parseFloat(restaurant_info.restaurant_charges || 0.0) + //restaurant charges
                        parseFloat(this.props.restaurant_info.delivery_charges || 0.0) + //delivery charges
                        parseFloat( (parseFloat(total) * parseFloat(this.getSettings(this.props.settings, "serviceFee") / 100)) || 0.0 ) + //service fee
                        parseFloat( (parseFloat(total) * parseFloat(this.state.delivery_tip / 100)) || 0.0 ) //delivery tip
                    ) -
                        formatPrice(
                            (
                                parseFloat(total) +
                                parseFloat(restaurant_info.restaurant_charges || 0.0) + //restaurant charges
                                parseFloat(this.props.restaurant_info.delivery_charges || 0.0) + //delivery charges
                                parseFloat( (parseFloat(total) * parseFloat(this.getSettings(this.props.settings, "serviceFee") / 100)) || 0.0 ) + //service fee
                                parseFloat( (parseFloat(total) * parseFloat(this.state.delivery_tip / 100)) || 0.0 ) //delivery tip
                            ) * (coupon.discount / 100)
                        )
                );
            } else {
                calc = formatPrice(
                    parseFloat(total) +
                    (parseFloat(restaurant_info.restaurant_charges) || 0.0) + //restaurant charges
                    (parseFloat(this.props.restaurant_info.delivery_charges) || 0.0) + //delivery charges
                    parseFloat( (parseFloat(total) * parseFloat(this.getSettings(this.props.settings, "serviceFee") / 100)) || 0.0 ) + //service fee
                    parseFloat( (parseFloat(total) * parseFloat(this.state.delivery_tip / 100)) || 0.0 ) - //delivery tip
                    (parseFloat(coupon.discount) || 0.0)
                );
            }
        } else {
            calc = formatPrice(
                parseFloat(total) + 
                parseFloat(restaurant_info.restaurant_charges || 0.0) + //restaurant charges
                parseFloat(this.props.restaurant_info.delivery_charges || 0.0) + //delivery charges
                parseFloat( (parseFloat(total) * parseFloat(this.getSettings(this.props.settings, "serviceFee") / 100)) || 0.0 ) + //service fee
                parseFloat( (parseFloat(total) * parseFloat(this.state.delivery_tip / 100)) || 0.0 ) //delivery tip
            );
        }
        if (localStorage.getItem("taxApplicable") === "true") {
            calc = formatPrice(
                        parseFloat(
                            parseFloat(calc) + 
                            parseFloat(parseFloat(localStorage.getItem("taxPercentage")) / 100) * calc
                        )
            );
            if (this.state.walletChecked && user.data.wallet_balance < calc) {
                calc = calc - user.data.wallet_balance;
            }
            return calc;
        } else {
            if (this.state.walletChecked && user.data.wallet_balance < calc) {
                calc = calc - user.data.wallet_balance;
            }
            return calc;
        }
    }

    showModalError () {
        this.setState({
            errorModal: true
        });
    }
    hideModalError = () => {
        this.setState({
            errorModal: false
        });
    }

    showModalError2 () {
        this.setState({
            error2Modal: true
        });
    }
    hideModalError2 = () => {
        this.setState({
            error2Modal: false
        });
    }

    handleCheckboxChange = event => {
        
        this.setState({ 
            pushChecked: false
        })

        setTimeout(() => {
            console.log("event.target.checked ", this.state.pushChecked );

            if(this.state.pushChecked === false){
                this.showModalNotification()
            }
        }, 100);
    }    
    
    handleCheckboxChangeLeaveAtDoor = event => {
        this.setState({ 
            leaveAtDoorChecked: event.target.checked
        })

        // setTimeout(() => {
        //     console.log("event.target.checked", this.state.leaveAtDoorChecked);
        // }, 100);
    }

    getCheckedItems = (items) => {
        var result = []
        
        items.forEach((key, value) => {
            if(key){
                result.push(value)
            }
        });

        return result
    }

    showModalAGB () {
        this.setState({
            AGBModal: true
        });
    }
    hideModalAGB = () => {
        this.setState({
            AGBModal: false
        });
    }

    showModalCron () {
        this.setState({
            CronModal: true
        });
    }
    hideModalCron = () => {
        this.setState({
            CronModal: false
        });
    }

    showModalVerify () {
        this.setState({
            VerifyModal: true
        });
    }
    hideModalVerify = () => {
        this.setState({
            VerifyModal: false
        });
    }

    showModalNotification () {
        this.setState({
            NotificationModal: true
        });
    }
    hideModalNotification = () => {
        this.setState({
            NotificationModal: false
        });
    }

    showModalLoad () {
        this.setState({
            LoadModal: true
        });
    }
    hideModalLoad = () => {
        this.setState({
            LoadModal: false
        });
    }

    showModalTip () {
        this.setState({
            TipModal: true
        });
    }
    hideModalTip = () => {
        this.setState({
            TipModal: false
        });
    }

    __doesRestaurantOperatesAtThisLocation = restaurant_info => {
        //send user lat long to helper, check with the current restaurant lat long and setstate accordingly
        const { user } = this.props;
        if(!user.success){
            var distance = calculateDistance(
                restaurant_info.longitude,
                restaurant_info.latitude,
                JSON.parse(localStorage.getItem("userSetAddress")).lng,
                JSON.parse(localStorage.getItem("userSetAddress")).lat
            );
        }
        else if (user.success) {
            var distance = calculateDistance(
                restaurant_info.longitude,
                restaurant_info.latitude,
                user.data.default_address.longitude,
                user.data.default_address.latitude
            );
        }

        console.log("distance", distance);
        this.setState({ distance: distance });
        if (distance <= restaurant_info.delivery_radius) {
            this.setState({ is_operational: true, is_operational_loading: false });
        } else {
            this.setState({ is_operational: false, is_operational_loading: false });
        }
    };

    notificationPrompt = () => {
        const { user } = this.props;

        if (user.success && this.state.fromTel) {//we need just to check if user has tel in table
            if (firebase.messaging.isSupported()) {
                let handler = this.props.saveNotificationToken;
                messaging
                    .requestPermission()
                    .then(async () => {
                        setTimeout(() => {
                            this.setState({ 
                                pushChecked: true
                            })
                        }, 1000);

                        localStorage.setItem("pushChecked", true);
                        const push_token = await messaging.getToken();
                        handler(push_token, user.data.id, user.data.auth_token, this.state.fromTel);
                    })
                    .catch(function(err) {
                        console.log("Unable to get permission to notify.", err);
                    });
                navigator.serviceWorker.addEventListener("message", message => console.log(message));
            }
        }
    }

    getDayName = () => {
        switch (new Date().getDay()) {
            case 0:
                return "sonntag"
            case 1:
                return "montag"
            case 2:
                return "dienstag"
            case 3:
                return "mittwoch"
            case 4:
                return "donnerstag"
            case 5:
                return "freitag"
            case 6:
                return "samstag"
        }
    }

    randomString = (length, chars) => {
        var result = '';
        for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
        return result;
    }

    country_formatted_tel = (fromTel) => {        
        var fromTel = fromTel.trim();
        var first2 = fromTel.substr(0, 2);
        if(first2 == localStorage.getItem("prefix")) {
            var fromTel = "+".fromTel;
            return fromTel;
        }
    
        var firstLetter = fromTel.substr(0, 1);
        if(firstLetter == "0") {
            var fromTel = "+"+localStorage.getItem("prefix")+fromTel.substr(1);
            return fromTel;
        }
    
        return fromTel;
    }

    __placeOrder = (payment_token, method, fromName, fromTel, cronDay, cronTime, cronAction) => {

        console.log("shop", this.props.restaurant_info.cat);

        var div = document.getElementById('laden');
        div.innerHTML = '<span>Laden...</span>';

        this.isTelVerified();
        
        setTimeout(() => {
            console.log("tel_verified", this.state.tel_verified);
        
            if(this.state.tel_verified){

                this.showModalLoad();

                setTimeout(() => {
                    if(fromName) localStorage.setItem("fromName", fromName);
                    if(fromTel) localStorage.setItem("fromTel", this.country_formatted_tel(fromTel));
                    var code = this.randomString(10, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
                    
                    const { user, cartProducts, coupon, cartTotal, cartDiscount } = this.props;

                    //if(this.props.restaurant_info.cat == 'grocery'){
                        this.hideModalLoad();

                        var cat = this.props.restaurant_info.cat;
                        var shop_id = this.props.restaurant_info.id;

                        var items = cartProducts.map((ele) => ({ id: ele.id, quantity: ele.quantity }));
                        items = JSON.stringify(items);
                        
                        var subtotal = JSON.stringify(cartTotal.totalPrice);
                        var subtotal = parseFloat(subtotal).toFixed(2);
                        
                        var subtotalStore = JSON.stringify(cartTotal.totalPriceStore);
                        var subtotalStore = parseFloat(subtotalStore).toFixed(2);

                        var delivery_charge = this.props.restaurant_info.delivery_charges;
                        var delivery_tip = this.state.delivery_tip;
                        var service_fee = (subtotal*(this.getSettings(this.props.settings, "serviceFee")/100)).toFixed(2);
                        if(cat == "restaurant"){
                            var shopper_reward = this.getSettings(this.props.settings, "restaurantShopperReward")
                        } else {
                            var shopper_reward = this.getSettings(this.props.settings, "shopperReward")
                        }

                        var fromAddress = JSON.parse(localStorage.getItem("userSetAddress")).address;
                        var fromLat = JSON.parse(localStorage.getItem("userSetAddress")).lat;
                        var fromLng = JSON.parse(localStorage.getItem("userSetAddress")).lng;

                        this.props.placeOrderV2(
                            //old data
                            // user,
                            cartProducts,
                            coupon,
                            JSON.parse(localStorage.getItem("userSetAddress")),
                            localStorage.getItem("orderComment"),
                            // cartTotal,
                            // method,
                            payment_token,
                            (localStorage.getItem("userSelected") === "SELFPICKUP"?2:1),
                            this.state.walletChecked,
                            // fromName,
                            // this.country_formatted_tel(fromTel),
                            // code, //check if placeOrderV2 has this func
                            // cartDiscount,
                            cronDay, 
                            cronTime,
                            cronAction,

                            //new data
                            user.success?user:'',
                            cat,
                            subtotal,
                            subtotalStore,
                            this.getTotalAfterCalculation(),
                            fromName,
                            this.country_formatted_tel(fromTel),
                            localStorage.getItem("country"),
                            fromAddress,
                            fromLat,
                            fromLng,
                            items,
                            delivery_charge,
                            delivery_tip,
                            shopper_reward,
                            service_fee,
                            method,
                            this.state.delivery_instructions,
                            this.state.leaveAtDoorChecked
                        );

                    // } else {
                        
                    //     console.log("cartDiscount", cartDiscount);
                    //     this.props.placeOrder(
                    //         user,
                    //         cartProducts,
                    //         coupon,
                    //         JSON.parse(localStorage.getItem("userSetAddress")),
                    //         localStorage.getItem("orderComment"),
                    //         cartTotal,
                    //         method,
                    //         payment_token,
                    //         (localStorage.getItem("userSelected") === "SELFPICKUP"?2:1),
                    //         this.state.walletChecked,
                    //         fromName,
                    //         this.country_formatted_tel(fromTel),
                    //         code,
                    //         cartDiscount,
                    //         cronDay, 
                           
                    //         cronTime,
                    //         cronAction
                    //     );
                
                    //     this.sendCall(code); //FIXME: if development mode then disable
                    //     this.hideModalLoad();
                    // }
                    
                }, 1500);

            } else {

                div.innerHTML = "<span>"+localStorage.getItem("checkoutPlaceOrder")+"</span>";

                this.sendCode(fromTel, fromName, JSON.parse(localStorage.getItem("userSetAddress")).address);
            
                this.showModalVerify();

                setTimeout(() => {
                    this.innerRef.current.focus();
                }, 1000)
            }

        }, 1500);
    }

    isTelVerified = () =>{
        if(localStorage.getItem("codeCustomer")){
            axios
            .get(WEBSITE_URL+'/php/twilio/PaymentGuest_isTelVerified.php?fromTel='+btoa(this.country_formatted_tel(this.state.fromTel))+'&code='+localStorage.getItem("codeCustomer"))
            .then(response => {
                if(response.data.success == true){
                    this.setState({
                        tel_verified: true
                    });
                } else{
                    this.setState({
                        tel_verified: false
                    });
                }
            })
            .catch(function(error) {
                console.log(error);
            });

        } else {
            this.setState({
                tel_verified: false
            });
        }
    }

    sendCall = (code) => {
        //1. add to orders table with unique code that is not already in table
        //2. remove from warenkorb
        //3. show running order ersatz page

        console.log("lets make a call");

        fetch(WEBSITE_URL+'/php/twilio/PaymentGuest_call.php?code='+code, {
            method: 'get',
            // may be some code of fetching comes here
        }).then(function(response) {
                if (response.status >= 200 && response.status < 300) {
                    //return response.text()
                }
                //throw new Error(response.statusText)
            })
            .then(function(response) {
                //console.log(response);
            })
    }

    sendCode = (fromTel, fromName, address, resend=false) => {
        console.log("code will be sent");

        fetch(WEBSITE_URL+'/php/twilio/PaymentGuest_sendCode.php?fromTel='+btoa(this.country_formatted_tel(fromTel))+'&fromName='+fromName+'&address='+address+'&resend='+resend, {
            method: 'get',
            // may be some code of fetching comes here
        }).then(function(response) {
                if (response.status >= 200 && response.status < 300) {
                    //return response.text()
                    console.log("code was sent");

                    if(resend){
                        var div = document.getElementById('result');
                        div.innerHTML = '<span style="color:green">SMS erneut gesendet.</span>';
                    }
                }
            })
            .then(function(response) {
                //console.log(response);
            })
    }

    accept = (zahlungsmittel, fromName, fromTel, cronDay, cronTime, cronAction, code) => {
        console.log("checking code "+code);

        axios
        .get(WEBSITE_URL+'/php/twilio/PaymentGuest_checkCode.php?fromTel='+btoa(this.country_formatted_tel(fromTel))+'&code='+code)
        .then(response => {
            //console.log('response', response.data);
            var div = document.getElementById('result');

            if(response.data.success == true){
                div.innerHTML = '<span style="color:green">Erfolgreich bestätigt.</span>';

                var div2 = document.getElementById('confirm');
                div2.innerHTML = 'Laden...';

                localStorage.setItem("codeCustomer", code);

                setTimeout(() => {
                    this.hideModalVerify();
                    this.__placeOrder("", zahlungsmittel, fromName, fromTel, cronDay, cronTime, cronAction, true);
                }, 1000)
            }else{
                div.innerHTML = '<span style="color:red">Bestätigung fehlgeschlagen.</span>';
                //console.log("showModalVerify again");
            }
        })
        .catch(function(error) {
            console.log(error);
        });
    }
    
    onNameChanged = (e) => {
        this.setState({
            fromName: e.currentTarget.value
        });
    }
    onTelChanged = (e) => {
        this.setState({
            fromTel: e.currentTarget.value
        });
    }
    onDeliveryInstructionsChanged = (e) => {
        this.setState({
            delivery_instructions: e.currentTarget.value
        });
    }
    onPaymentChanged = (e) => {
        this.setState({
            zahlungsmittel: e.currentTarget.value
        });
    } 

    onDayChanged(e) {
        const item = e.target.name;
        const isChecked = e.target.checked;
        this.setState(
            prevState => ({ 
                checkedItems: prevState.checkedItems.set(item, isChecked)
            })
        );

        setTimeout(() => {
            let checkedValue = this.getCheckedItems(this.state.checkedItems)
            this.setState({
                cronDay: checkedValue.toString()
              });
        }, 1000);
        
    }
    onTimeChanged = date => {
        this.setState({
            cronTime: date
        });
    };
    handleDateChangeRaw = (e) => {//disable user input
        e.preventDefault();
    }
    onActionChanged = (e) => {//this is radio button
        this.setState({
            cronAction: e.currentTarget.value
        });
    }
    onCronNameChanged = (e) => {
        this.setState({
            cronName: e.currentTarget.value
        });
    }

    weekday = (string) =>{
        var string = string.replace(1, "Montag").replace(2, "Dienstag").replace(3, "Mittwoch").replace(4, "Donnerstag").replace(5, "Freitag").replace(6, "Samstag").replace(7, "Sonntag");
        return string;
    }

    onCodeChanged = (e) => {
        this.setState({
            verificationCode: e.currentTarget.value
        });
    }

    onTipChanged = (e) => {
        this.setState({
            delivery_tip: e.currentTarget.value
        });
    } 

    showDeliveryInstructions = () => {
        document.getElementById('showDeliveryInstructions').style.display = "none";
        document.getElementById('deliveryInstructions').style.display = "block";
    }

    getSettings = (data, keyName)=>{
        var filteredData = data.filter(x=>x.key == keyName);
        var focusData = undefined;
        if(filteredData.length > 0){
            focusData = filteredData[0].value;
        }
        return focusData;
    }

    render() {
        const {bgWidth, bgHeight} = this.state;
        const { user, cartDiscount, cartTotal, coupon, total } = this.props;

        
        // console.log("serviceFee", this.getSettings(this.props.settings, "serviceFee"));

        //from
        var fromHour = this.props.restaurant_info[this.getDayName()].slice(0, 2);
        var fromMin = this.props.restaurant_info[this.getDayName()].slice(3,5);
        if(fromMin == "00") var fromMin = 0;

        //to
        var openTo = this.props.restaurant_info[this.getDayName()].slice(-5);
        if(openTo.slice(0, -3) < 8) {
            var toHour = 23;
            var toMin = 59;
        }else{
            var toHour = this.props.restaurant_info[this.getDayName()].slice(8,-3);
            var toMin = this.props.restaurant_info[this.getDayName()].slice(-2);
        }
        if(toMin == "00") var toMin = 0;

        var cronTime = new Date(this.state.cronTime);
        var cronTime = cronTime.toLocaleTimeString('de').substr(0, 5);

        // console.log("from"+fromHour+":"+fromMin);
        // console.log("to"+toHour+":"+toMin);

        var datepicker = <DatePicker
            selected={this.state.cronTime}
            onChange={this.onTimeChanged}
            onChangeRaw={this.handleDateChangeRaw}

            showTimeSelect
            showTimeSelectOnly
            timeIntervals={15}
            timeCaption="Time"
            dateFormat="HH:mm"
            timeFormat="HH:mm"
            minTime={setHours(setMinutes(new Date(), fromMin), fromHour)}
            maxTime={setHours(setMinutes(new Date(), toMin), toHour)}
        />

        var cat = this.props.restaurant_info.description || '';
        if(cat.indexOf("Handy") >= 0){
            var bgID = "1556656793-08538906a9f8";
            var dauerauftrag = false;
        } else {
            var bgID = "1571939634097-f1829662cdfc";
            var dauerauftrag = true;
        }

        return (

            <React.Fragment>

            <Nav shop={true} restaurant={this.props.restaurant_info} bgWidth={bgWidth} bgHeight={bgHeight} page={'PaymentGuest'} />


            {/* pizza */}
            {/* <img src={"https://images.unsplash.com/photo-1571939634097-f1829662cdfc?ixlib=rb-1.2.1&auto=format&fit=crop&w="+bgWidth+"&h="+bgHeight+"&q=80"} /> */}
            {/* <img src={"https://images.unsplash.com/photo-1573821663912-6df460f9c684?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w="+bgWidth+"&h=200&q=80"} /> */}


            <div style={{background:"rgb(232, 233, 238)", padding:"10px 0", color:"#666"}}><div style={{maxWidth:"591px", margin:"auto"}}>

                {/* Contact */}
                <div style={{marginTop:"15px", background:"#fff", border: "1px solid rgba(0, 0, 0, 0.15)", boxShadow: "rgba(0, 0, 0, 0.06) 0px 2px 3px", borderRadius: "1px", padding:"10px"}}>
                    <div style={{marginBottom: "14px", fontSize: "14px", lineHeight: "35px", fontWeight:"bold", color:"#666"}}>{localStorage.getItem("checkoutContact")}</div>
                    {/* name */}
                    <div style={{marginTop:"12px", color:"#c2c5bc"}}>
                        <label style={{fontWeight:"300", display:"block", fontFamily:"open sans"}}>
                            {localStorage.getItem("loginLoginNameLabel")} *
                            <input
                                name="name"
                                className="form-control kundenDaten"
                                defaultValue={this.state.fromName}
                                onChange={this.onNameChanged} 
                            />
                        </label>
                    </div> 
                    {/* phone */}
                    <div style={{marginTop:"12px", color:"#c2c5bc"}}>
                        <label style={{fontWeight:"300", display:"block", fontFamily:"open sans"}}>
                            {localStorage.getItem("loginLoginPhoneLabel")} *
                            <input
                                name="tel"
                                className="form-control kundenDaten"
                                defaultValue={this.state.fromTel}
                                onChange={this.onTelChanged} 
                                type="tel"
                                placeholder={localStorage.getItem("prefix") && '+'+localStorage.getItem("prefix")}
                            />
                        </label>
                    </div>
                </div>

                {/* Payment */}
                <div style={{marginTop:"15px", background:"#fff", border: "1px solid rgba(0, 0, 0, 0.15)", boxShadow: "rgba(0, 0, 0, 0.06) 0px 2px 3px", borderRadius: "1px"}}>
                    <div style={{padding:"0 15px", marginBottom: "14px", fontSize: "14px", lineHeight: "35px", fontWeight:"bold", color:"#666"}}>{localStorage.getItem("checkoutSelectPayment")} *</div>
                    <div style={{marginTop:"12px", fontSize:"16px"}}>
                        {/* <label className="payment_func">
                            {localStorage.getItem("checkoutCodText")}
                            <input type="radio" name="payment" value="Barzahlung" onChange={this.onPaymentChanged} /> 
                            <span className="checkmark"></span>
                            <img src="/assets/img/cash.svg" style={{position: "absolute", right: "15px", height: "25px", width: "25px"}} />
                            <Ink duration="500" />
                        </label> */}
                        {/* <label className="payment_func">
                            {localStorage.getItem("checkoutCreditCard")}
                            <input type="radio" name="payment" value="Kreditkarte an der Tür" onChange={this.onPaymentChanged} /> 
                            <span className="checkmark"></span>
                            <img src="/assets/img/kreditkarte.svg" style={{position: "absolute", right: "15px", height: "25px", width: "25px"}} />
                            <Ink duration="500" />
                        </label> */}
                        <label className="payment_func">
                            {localStorage.getItem("checkoutCreditCard")}
                            <input type="radio" name="payment" value="cards" onChange={this.onPaymentChanged} /> 
                            <span className="checkmark"></span>
                            <img src="/assets/img/kreditkarte.svg" style={{position: "absolute", right: "15px", height: "25px", width: "25px"}} />
                            <Ink duration="500" />
                        </label>
                        <label className="payment_func">
                            Klarna
                            <input type="radio" name="payment" value="sofort" onChange={this.onPaymentChanged} /> 
                            <span className="checkmark"></span>
                            <img src="/assets/img/sofort.png" style={{position: "absolute", right: "5px", width: "50px", top: 0}} />
                            <Ink duration="500" />
                        </label>
                        <label className="payment_func">
                            EPS
                            <input type="radio" name="payment" value="eps" onChange={this.onPaymentChanged} /> 
                            <span className="checkmark"></span>
                            <img src="/assets/img/eps.png" style={{position: "absolute", right: "5px", width: "50px", top: "-5px"}} />
                            <Ink duration="500" />
                        </label>
                        {this.state.canPayFullWithWallet && (
                            <label className="payment_func">
                                Wallet Guthaben
                                <input type="radio" name="payment" value="wallet" onChange={this.onPaymentChanged} /> 
                                <span className="checkmark"></span>
                                <img src="/assets/img/various/wallet.png" style={{position: "absolute", right: "5px", width: "40px", top: "-5px"}} />
                                <Ink duration="500" />
                            </label>
                        )}
                    </div>
                </div>

                {/* Delivery instructions */}
                <div id="deliveryInstructions" style={{display:"none", marginTop:"15px", background:"#fff", border: "1px solid rgba(0, 0, 0, 0.15)", boxShadow: "rgba(0, 0, 0, 0.06) 0px 2px 3px", borderRadius: "1px", padding:"10px"}}>
                    {/* <div style={{marginBottom: "14px", fontSize: "14px", lineHeight: "35px", fontWeight:"bold", color:"#666"}}>Lieferanweisungen</div> */}
                    <div style={{marginTop:"12px", color:"#c2c5bc"}}>
                        <label style={{fontWeight:"300", display:"block", fontFamily:"open sans"}}>
                            <textarea
                                name="delivery-instructions"
                                className="form-control"
                                onChange={this.onDeliveryInstructionsChanged} 
                                placeholder={'Lieferanweisungen hinzufügen (optional)'}
                                style={{fontSize:"12px", border:"1px solid rgb(189, 189, 189)", padding:"10px 15px"}}></textarea>
                        </label>
                    </div>
                    <div>
                        <label style={{fontSize:"12px", margin:"15px 0 10px"}}>
                        <Checkbox
                            name="leaveAtDoor"
                            checked={this.state.leaveAtDoorChecked}
                            onChange={this.handleCheckboxChangeLeaveAtDoor}
                        />
                        <span style={{marginLeft:"5px"}}>An meiner Tür lassen, wenn ich nicht da bin</span>
                        </label>
                    </div>

                    <div style={{background:"rgb(247, 247, 247)", fontSize: "10px", borderRadius: "5px", padding:"10px"}}>
                        Wenn Sie diese Option auswählen, übernehmen Sie die volle Verantwortung für Ihre Bestellung, nachdem diese unbeaufsichtigt geliefert wurde, einschließlich etwaiger Verluste aufgrund von Diebstahl oder Schäden aufgrund von Temperaturempfindlichkeit.
                    </div>
                </div>

                {/* Checkbox - Leave at my door, if I am not around */}
                <div style={{margin:"25px 15px 0", fontSize:"12px"}}>
                    <label id="showDeliveryInstructions" onClick={() => this.showDeliveryInstructions()}>
                        <Checkbox
                            name="deliveryInstructions"
                            checked={this.state.leaveAtDoorChecked}
                            onChange={this.handleCheckboxChangeLeaveAtDoor}
                        />
                        <span style={{marginLeft:"5px"}}>An meiner Tür lassen, wenn ich nicht da bin</span>
                    </label>
                </div>

                {/* Wird bestellt, bitte warten... */}
                <Modal show={this.state.LoadModal} centered={true}>
                    <Modal.Body>
                        <div style={{fontSize:"1.6rem", fontFamily:"open sans", fontWeight:"400"}}>Wird bestellt, bitte warten...</div>

                        <div id="result"></div>
                    </Modal.Body>
                </Modal>

                {/* <div style={{margin:"50px 10px 19px", overflow:"auto"}}><img src="https://cdn2.hubspot.net/hubfs/2687833/belch.io/No%20Commission%20Icon.png" style={{marginRight:"20px", width:"50px", height:"50px", float:"left"}} />Deine Bestellung ist 100 % provisionsfrei für das Restaurant. Danke, dass du Restaurants in Deiner Nähe unterstützt!</div> */}

                {/* Dauerauftrag Link */}
                {/* {dauerauftrag ? <div style={{margin:"50px 15px 10px"}}>
                    <span style={{fontWeight:"bold"}}>Als Dauerauftrag speichern?</span> Klicke bitte auf <span style={{position:"relative", cursor:"pointer", color:"rgb(26, 115, 232)"}} onClick={() => { this.showModalCron(); }} data-tip='' data-place='top' data-for='repeatOrder'>regelmäßig wiederholen<Ink duration="500" /></span> um den Bestellvorgang in Zukunft zu überspringen. 
                    <ReactTooltip id='repeatOrder'><div style={{fontSize:"10px", fontWeight:"300", fontFamily:"open sans"}}>Du kannst den Bestellvorgang<br />in Zukunft überspringen. Klicke<br />bitte den Link an, um<br />Wochentag und Zeit einzustellen.</div></ReactTooltip>
                </div> : null} */}

                {/* Dauerauftrag Status */}
                {this.state.cronDay && this.state.cronTime && cronTime && this.state.cronAction ? <div style={{padding:"5px 15px 15px", color:"green"}}>
                    {this.state.cronAction == 1 ? <span>Wir senden jeden <b>{this.weekday(this.state.cronDay)}</b> (eine Stunde vor dem vereinbarten Zeitpunkt <b>{cronTime}</b>) eine Bestätigungs-SMS an deine Telefonnummer. </span> 
                    : <span>Dein Essen wird jeden <b>{this.weekday(this.state.cronDay)}</b> zum vereinbarten Zeitpunkt ({cronTime}) an deine Adresse (<b>{JSON.parse(localStorage.getItem("userSetAddress")).address}</b>) geliefert. </span>}
                    {/* Wir senden jeden <b>{this.weekday(this.state.cronDay)}</b> zum vereinbarten Zeitpunkt (<b>{cronTime}</b>) eine verbindliche Bestellung an das Restaurant.  */}
                    Diese Einstellung wird gespeichert, sobald Du diese Bestellung aufgegeben hast.
                </div>:""}
                
                {/* Dauerauftrag Einstellen */}
                <Modal show={this.state.CronModal} onHide={this.hideModalCron} size="lg">
                    <Modal.Header closeButton>
                        <Modal.Title>Regelmäßig Wiederholen</Modal.Title>
                    </Modal.Header>
                    <Modal.Body style={{padding:"0 25px 10px"}}>

                        <hr/>

                        {/* day picker */}
                        <div style={{fontWeight: 300, fontFamily: "open sans", marginBottom:"10px"}}>Wochentage *</div>  
                        {checkboxes.map(item => (
                            <label key={item.key} style={{margin:"5px 15px 5px 0", fontWeight:"normal"}}>
                                <Checkbox name={item.key} checked={this.state.checkedItems.get(item.key)} onChange={this.onDayChanged} /> {item.name}
                            </label>
                        ))}

                        <hr/>

                        {/* time picker */}
                        {this.state.cronDay ? <div><div style={{fontWeight: 300, fontFamily: "open sans", marginBottom:"10px"}}>Zeit *</div>{datepicker}<hr/></div>:null}


                        {/* action */}
                        {this.state.cronDay && this.state.cronTime ? <div><div style={{fontWeight: 300, fontFamily: "open sans", marginBottom:"10px"}}>Aktion *</div>
                        <label className="payment_func" style={{display:"inline-block", border:"none", marginBottom:0}}>
                            Zahlungspflichtig Bestellen 
                            {this.state.cronAction == 0 ? <input type="radio" name="cronAction" value="0" onChange={this.onActionChanged} defaultChecked />  : <input type="radio" name="cronAction" value="0" onChange={this.onActionChanged} /> }
                            <span className="checkmark"></span>
                            <Ink duration="500" />
                        </label>
                        <FontAwesomeIcon data-for='action0' data-tip='' data-place='left' style={{marginLeft:"10px", fontSize:"16px"}} icon={faInfoCircle} />
                        <ReactTooltip id='action0'><div style={{fontSize:"10px", fontWeight:"300", fontFamily:"open sans"}}>Wir senden zum vereinbarten<br/>Zeitpunkt eine verbindliche Bestellung<br/>an den Anbieter.</div></ReactTooltip>
                        <br/>
                        <label className="payment_func" style={{display:"inline-block", border:"none", marginBottom:0}}>
                            Bestätigungs-SMS senden 
                            {this.state.cronAction == 1 ? <input type="radio" name="cronAction" value="1" onChange={this.onActionChanged} defaultChecked />  : <input type="radio" name="cronAction" value="1" onChange={this.onActionChanged} /> }
                            <span className="checkmark"></span>
                            <Ink duration="500" />
                        </label>
                        <FontAwesomeIcon data-for='action1' data-tip='' data-place='left' style={{marginLeft:"10px", fontSize:"16px"}} icon={faInfoCircle} />
                        <ReactTooltip id='action1'><div style={{fontSize:"10px", fontWeight:"300", fontFamily:"open sans"}}>Wir senden nur dann eine verbindliche<br/>Bestellung an den Anbieter, wenn Sie<br/>den Link klicken, denn wir Ihnen<br/>eine Stunde vor dem vereinbarten<br/>Zeitpunkt, per SMS zusenden.</div></ReactTooltip>
                        {/* Es wird nur dann eine verbindliche<br/>Bestellung aufgegeben, wenn Sie den<br/>Link in der SMS klicken. */}
                        {/* Wir senden nur dann eine verbindliche Bestellung an das Restaurant, wenn Sie den Link klicken,denn wir Ihnen eine Stunde vor dem vereinbarten Zeitpunkt, per SMS zusenden. */}

                        <hr/></div>:null}


                        {/* save as */}
                        {/* <div style={{fontWeight: 300, fontFamily: "open sans", marginBottom:"10px"}}>Speichern als...</div>
                        <label style={{fontWeight:"300", display:"inline-block", fontFamily:"open sans"}}>
                            <input
                                name="cronName"
                                style={{width:"212px"}}
                                onChange={this.onCronNameChanged} 
                                defaultValue={"robot_bestellung_tops_kitchen"}

                            />
                        </label>
                        <FontAwesomeIcon data-tip='' data-place='top' data-for='cronName' style={{marginLeft:"10px", fontSize:"16px"}} icon={faInfoCircle} />
                        <ReactTooltip id='cronName'><div style={{fontSize:"10px", fontWeight:"300", fontFamily:"open sans"}}>Wie möchtest du diese Bestellung benennen.</div></ReactTooltip> */}

                        {/* info */}
                        {/* <div style={{marginTop:"20px", fontSize:"9px", color:"#999"}}>
                            Zeitgesteuerte Bestellungen helfen Dir den Bestellvorgang zu überspringen, um Zeit zu sparen. Du kannst zeitgesteuerte Bestellungen auf unserer App aktivieren, um wiederkehrende Bestellungen automatisch zu wiederholen. Einmal einstellen und Dein Essen wird täglich zum vereinbarten Zeitpunkt zugestellt. Eine kosteneffiziente Lösung für Kleinbetriebe, Büros, Kanzleien, Ordinationen, etc.
                        </div> */}

                        <div style={{marginTop:"20px", fontSize:"12px", color:"#999"}}>
                            Einmalig einstellen und Dein Essen wird täglich zum vereinbarten Zeitpunkt zugestellt.
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        {!this.state.cronDay || !this.state.cronTime || !cronTime || !this.state.cronAction ? <div style={{fontSize:"11px", fontWeight:"300", fontFamily:"open sans"}}>Bitte Wochentag, Zeit und Aktion auswählen, um diese Bestellung als Dauerauftrag zu speichern.</div> : <div style={{fontSize:"11px", fontWeight:"400", fontFamily:"open sans", color:"green"}}>Dauerauftrag wurde eingestellt.</div>}
                        
                        <Button variant="secondary" onClick={this.hideModalCron}> 
                            Schließen
                        </Button> 
                        
                        {/* {this.state.cronDay && this.state.cronTime && cronTime && this.state.cronAction ? <Button variant="primary" onClick={this.hideModalCron}> 
                            Speichern
                        </Button> : ""} */}
                    </Modal.Footer>
                </Modal>

                {/* Bestellung Freigeben */}
                <Modal show={this.state.VerifyModal} onHide={this.hideModalVerify} size="sm">
                    <Modal.Header closeButton>
                        <Modal.Title>Bestätigen</Modal.Title>
                    </Modal.Header>
                    <Modal.Body style={{padding:"0 25px 10px"}}>

                        <div style={{color:"#999", fontFamily:"open sans", fontWeight:"300"}}>Aktion</div>
                        <div style={{marginBottom:"15px"}}>Bestellung freigeben</div>

                        <div style={{color:"#999", fontFamily:"open sans", fontWeight:"300"}}>Anbieter</div>
                        <div style={{marginBottom:"15px"}}>{this.props.restaurant_info.name}<br/>{this.props.restaurant_info.address}</div>

                        <div style={{color:"#999", fontFamily:"open sans", fontWeight:"300", marginBottom:"15px"}}>Klicke auf <span onClick={() => this.sendCode(this.state.fromTel, "", "", true)} style={{color:"#0872BA", cursor:"pointer"}}>Code senden</span> um den Bestätigungscode erneut an {this.state.fromTel} zu senden.</div>

                        <input maxLength="4" type="tel" placeholder="Code eingeben" name="code" onChange={this.onCodeChanged} style={{padding:"8px", marginBottom:"5px", border:"1px solid #ccc"}} ref={this.innerRef} />

                        <div id="result"></div>

                        <div style={{marginTop:"20px", fontSize:"9px", color:"#999"}}>
                            {/* Bitte den 4-stelligen Code eingeben, um den Dauerauftrag zu bestätigen.  */}
                            Bitte überprüfen Sie die SMS-Daten und geben Sie mit dem Code Ihr Go.
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={this.hideModalVerify}> 
                            Schließen
                        </Button> 
                        <Button id="confirm" variant="primary" onClick={() => this.accept(this.state.zahlungsmittel, this.state.fromName, this.state.fromTel, this.state.cronDay, cronTime, this.state.cronAction, this.state.verificationCode)}> 
                            Bestätigen
                        </Button> 
                    </Modal.Footer>
                </Modal>

                {/* Checkbox - Benachrichtigungen Link */}
                <div style={{margin:"0 15px 10px", fontSize:"12px"}}>
                    {/* Ja, ich möchte eine einmalige Bestellbestätigung per Push-Nachricht erhalten. */}
                    {this.state.fromTel ? <div>
                        <label>
                        <Checkbox
                            name="notification"
                            checked={this.state.pushChecked}
                            onChange={this.handleCheckboxChange}
                        />
                        {/* <span style={{marginLeft:"5px"}}>Ja, ich möchte zur Sicherheit eine Bestellbestätigung erhalten.</span> */}
                        <span style={{marginLeft:"5px"}}>Ich möchte eine Bestellbestätigung erhalten.</span>
                        </label>
                    </div> : null}
                </div>

                {/* Benachrichtigungen Zulassen */}
                <Modal show={this.state.NotificationModal} onHide={this.hideModalNotification} size="sm">
                    <Modal.Header closeButton>
                        <Modal.Title>Benachrichtigung</Modal.Title>
                    </Modal.Header>
                    <Modal.Body style={{padding:"0 25px 10px"}}>
                        <div style={{fontFamily:"open sans", fontWeight:"300", marginBottom:"15px"}}>Ich möchte eine Bestellbestätigung via Push Nachricht zulassen.</div>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={this.hideModalNotification}> 
                            Schließen
                        </Button> 
                        <Button id="confirm" variant="primary" onClick={() => this.notificationPrompt()}> 
                            Zulassen
                        </Button> 
                    </Modal.Footer>
                </Modal>

                {/* Order Button */}
                {this.state.is_operational ? (
                    (this.state.zahlungsmittel && this.state.fromName && this.state.fromTel) ? (
                        <div>
                            <button
                                onClick={() => this.__placeOrder("", this.state.zahlungsmittel, this.state.fromName, this.state.fromTel, this.state.cronDay, cronTime, this.state.cronAction)}
                                type="button"
                                className="btn-save-address"
                                id="laden"
                                style={{ backgroundColor: localStorage.getItem("storeColor")}}
                            >
                                {localStorage.getItem("checkoutPlaceOrder")}
                                <Ink duration="500" />
                            </button>
                        </div>
                    ) : (
                        <div>
                            <button
                                onClick={() => this.showModalError()}
                                type="button"
                                className="btn-save-address"
                                style={{ backgroundColor: localStorage.getItem("storeColor")}}
                            >
                                {localStorage.getItem("checkoutPlaceOrder")}
                                <Ink duration="500" />
                            </button>

                            <Modal show={this.state.errorModal} onHide={this.hideModalError} size="sm">
                                <Modal.Header closeButton>
                                    {/* <Modal.Title>Fehlermeldung</Modal.Title> */}
                                </Modal.Header>
                                <Modal.Body style={{padding:"0 25px 10px"}}>
                                    <div style={{fontFamily:"open sans", fontWeight:600, textAlign:"center"}}>
                                        <div style={{marginBottom:"20px"}}><FontAwesomeIcon icon={faExclamationTriangle} style={{color:"red", fontSize:"60px"}} /></div>
                                        Bitte Name, Telefon und Zahlungsart eingeben.
                                    </div>
                                </Modal.Body>
                            </Modal>
                        </div>
                    )
                ) : (
                    <div>
                        <button
                            onClick={() => this.showModalError2()}
                            type="button"
                            className="btn-save-address"
                            style={{ backgroundColor: localStorage.getItem("storeColor")}}
                        >
                            {localStorage.getItem("checkoutPlaceOrder")}
                            <Ink duration="500" />
                        </button>
                        <Modal show={this.state.error2Modal} onHide={this.hideModalError2} size="sm">
                            <Modal.Header closeButton>
                                {/* <Modal.Title>Fehlermeldung</Modal.Title> */}
                            </Modal.Header>
                            <Modal.Body style={{padding:"0 25px 10px"}}>
                                    <div style={{fontFamily:"open sans", fontWeight:600, textAlign:"center"}}>
                                        <div style={{marginBottom:"20px"}}><FontAwesomeIcon icon={faExclamationTriangle} style={{color:"red", fontSize:"60px"}} /></div>
                                        {localStorage.getItem("cartRestaurantNotOperational")}
                                    </div>
                                </Modal.Body>
                        </Modal>
                    </div>
                )}

                {/* invoice */}
                <div style={{marginTop:"15px", background:"#fff", border: "1px solid rgba(0, 0, 0, 0.15)", boxShadow: "rgba(0, 0, 0, 0.06) 0px 2px 3px", borderRadius: "1px", padding:"10px"}}>

                    {/* Subtotal */}
                    <div style={{marginBottom: "4px", fontSize: "14px", color:"#666"}}>
                        Zwischensumme
                        <span style={{float:"right"}}>
                            {this.props.restaurant_info.currencyFormat}{cartTotal.totalPrice.toFixed(2)}
                        </span>
                    </div>

                    {/* Delivery */}
                    <div style={{marginBottom: "4px", fontSize: "14px", color:"#666"}}>
                        Lieferkosten
                        <span style={{float:"right"}}>
                            {this.props.restaurant_info.currencyFormat}{this.props.restaurant_info.delivery_charges}
                        </span>
                    </div>

                    {/* Service fee */}
                    <div style={{marginBottom: "4px", fontSize: "14px", color:"#666"}}>
                        Servicegebühr <span data-tip='' data-for='serviceFeeInfo'><FontAwesomeIcon icon={faInfoCircle} /></span>
                        <span style={{float:"right"}}>
                            {this.props.restaurant_info.currencyFormat}{(cartTotal.totalPrice*(this.getSettings(this.props.settings, "serviceFee")/100)).toFixed(2)}
                        </span>
                    </div>

                    <ReactTooltip id='serviceFeeInfo' place="top"><div style={{fontSize:"10px", fontWeight:"300", fontFamily:"open sans"}}>Diese Gebühr unterstützt die Plattform und deckt<br/>eine breite Palette von Betriebskosten ab,<br/>einschließlich Versicherung und Kundenservice.</div></ReactTooltip>

                    <hr/>

                    {/* Delivery Tip */}
                    <div style={{marginBottom: "4px", fontSize: "14px", color:"#666"}}>
                        <b>Trinkgeld</b>
                        <span style={{textTransform:"uppercase", color:"rgb(252, 128, 25)", border:"1px solid rgb(252, 128, 25)", padding:"3px 5px", marginLeft:"10px", fontSize:"11px", borderRadius:"3px", cursor:"pointer"}} onClick={() => this.showModalTip()}>ändern</span>
                        <span style={{float:"right"}}>
                            <span style={{color:"rgb(117, 117, 117)", fontWeight:300, fontFamily:"open sans", marginRight:"5px"}}>({this.state.delivery_tip}%)</span> 
                            {this.props.restaurant_info.currencyFormat}{(cartTotal.totalPrice*(this.state.delivery_tip/100)).toFixed(2)}
                        </span>
                    </div>
                    <div style={{color:"rgb(117, 117, 117)", fontSize:"12px", marginTop:"10px"}}>
                        Möchten Sie die Bemühungen Ihres Zustellers anerkennen? Betrachten Sie ein größeres Trinkgeld als Dankeschön - 100% des Trinkgeldes gehen an Zusteller.
                    </div>
                    <Modal show={this.state.TipModal} onHide={this.hideModalTip} style={{background:"#fff"}}>
                        <Modal.Header closeButton>
                            <Modal.Title>Kontaktloses Trinkgeld</Modal.Title>
                        </Modal.Header>
                        <Modal.Body style={{padding:"15px"}}>

                            <div style={{marginTop:"20px", marginLeft:"15px", fontSize:"12px"}}>
                                <li style={{margin:"3px 0"}}>100% der Trinkgelder gehen an selbstständige Zusteller.</li>
                                <li style={{margin:"3px 0"}}>Trinkgelder sind immer optional.</li>
                                <li style={{margin:"3px 0"}}>Sie können ihr Trinkgeld nach der Lieferung ändern.</li>
                            </div>

                            <div style={{marginTop:"30px", fontSize:"16px"}}>
                                <label className="payment_func" style={{border:"none"}}>
                                    0% ({this.props.restaurant_info.currencyFormat}{(cartTotal.totalPrice*0).toFixed(2)})
                                    <input type="radio" name="tip" value="0" onChange={this.onTipChanged} checked={this.state.delivery_tip == 0?true:false} /> 
                                    <span className="checkmark" style={{left:"5px"}}></span>
                                    <img src="/assets/img/cash.svg" style={{position: "absolute", right: "15px", height: "25px", width: "25px"}} />
                                    <Ink duration="500" />
                                </label>

                                <label className="payment_func">
                                    5% ({this.props.restaurant_info.currencyFormat}{(cartTotal.totalPrice*0.05).toFixed(2)})
                                    <input type="radio" name="tip" value="5" onChange={this.onTipChanged} checked={this.state.delivery_tip == 5?true:false} /> 
                                    <span className="checkmark" style={{left:"5px"}}></span>
                                    <img src="/assets/img/cash.svg" style={{position: "absolute", right: "15px", height: "25px", width: "25px"}} />
                                    <Ink duration="500" />
                                </label>

                                <label className="payment_func">
                                    10% ({this.props.restaurant_info.currencyFormat}{(cartTotal.totalPrice*0.10).toFixed(2)})
                                    <input type="radio" name="tip" value="10" onChange={this.onTipChanged} checked={this.state.delivery_tip == 10?true:false} /> 
                                    <span className="checkmark" style={{left:"5px"}}></span>
                                    <img src="/assets/img/cash.svg" style={{position: "absolute", right: "15px", height: "25px", width: "25px"}} />
                                    <Ink duration="500" />
                                </label>

                                <label className="payment_func">
                                    15% ({this.props.restaurant_info.currencyFormat}{(cartTotal.totalPrice*0.15).toFixed(2)})
                                    <input type="radio" name="tip" value="15" onChange={this.onTipChanged} checked={this.state.delivery_tip == 15?true:false} /> 
                                    <span className="checkmark" style={{left:"5px"}}></span>
                                    <img src="/assets/img/cash.svg" style={{position: "absolute", right: "15px", height: "25px", width: "25px"}} />
                                    <Ink duration="500" />
                                </label>

                                <label className="payment_func" style={{border:"none"}}>
                                    20% ({this.props.restaurant_info.currencyFormat}{(cartTotal.totalPrice*0.20).toFixed(2)})
                                    <input type="radio" name="tip" value="20" onChange={this.onTipChanged} checked={this.state.delivery_tip == 20?true:false} /> 
                                    <span className="checkmark" style={{left:"5px"}}></span>
                                    <img src="/assets/img/cash.svg" style={{position: "absolute", right: "15px", height: "25px", width: "25px"}} />
                                    <Ink duration="500" />
                                </label>
                            </div>

                            <div style={{marginTop:"20px"}}>
                                <button
                                    onClick={() => this.hideModalTip()}
                                    type="button"
                                    className="btn-save-address"
                                    style={{textTransform:"unset", backgroundColor: localStorage.getItem("storeColor")}}
                                >
                                    Trinkgeld speichern
                                    <Ink duration="500" />
                                </button>
                            </div>
                        </Modal.Body>
                    </Modal>

                    <hr/>

                    <div style={{marginBottom: "4px", fontSize: "14px", color:"#666"}}>

                        {user.success ? <Coupon subtotal={cartTotal.totalPrice} delivery_tip={this.state.delivery_tip} /> : (
                            <React.Fragment>
                                <b>Gutschein</b>
                                <span style={{textTransform:"uppercase", color:"rgb(252, 128, 25)", border:"1px solid rgb(252, 128, 25)", padding:"3px 5px", marginLeft:"10px", fontSize:"11px", borderRadius:"3px", cursor:"pointer"}} onClick={() => alert("Um einen Gutschein einzulösen, musst du angemeldet sein.")}>hinzufügen</span>
                            </React.Fragment>
                        )}

                    </div>
                    
                    <hr/>

                    {/* Total */}
                    <div style={{marginBottom: "4px", fontSize: "14px", color:"#666"}}>
                        <b>Gesamt</b>
                        <span style={{float:"right"}}>
                            <b>{this.props.restaurant_info.currencyFormat}{this.getTotalAfterCalculation()}</b>
                        </span>
                    </div>

                </div>

                {/* terms of use */}
                <div className="mb-200" style={{color:"#999", fontSize:"10px", padding:"10px 15px", cursor:"pointer", textAlign:"center"}} onClick={() => { this.showModalAGB(); }}>
                    {localStorage.getItem("checkoutAGB")} 
                </div>
                <Modal show={this.state.AGBModal} onHide={this.hideModalAGB} size="lg">
                    <Modal.Header closeButton>
                        {/* <Modal.Title>AGB</Modal.Title> */}
                    </Modal.Header>
                    <Modal.Body style={{padding:0}}>
                        <iframe src={'//'+window.location.hostname+'/pages/agb'} style={{width:"100%", height:(window.innerHeight / 1.5)}} frameBorder="0" />
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={this.hideModalAGB}> 
                            Schließen
                        </Button> 
                    </Modal.Footer>
                </Modal>


            </div></div>

            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user.user,
    addresses: state.addresses.addresses,
    cartProducts: state.cart.products,
    cartTotal: state.total.data, //total without discount
    //cartTotal : state.checkout.cartTotal, //total with discount
    cartDiscount : state.checkout.cartDiscount, //only discount amount
    coupon: state.coupon.coupon,
    checkout: state.checkout.checkout,
    paymentgateways: state.paymentgateways.paymentgateways,
    restaurant_info: state.items.restaurant_info,
    settings: state.settings.settings,

});
export default connect(
    mapStateToProps,
    { getSettings, saveNotificationToken, placeOrder, updateUserInfo, placeOrderV2 }
)(PaymentGuest);
