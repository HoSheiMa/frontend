import React, { Component } from 'react'
import Ink from "react-ink";
import ReactDOM from 'react-dom';
import axios from "axios";
import MainPageItem from "./mainPageItem";

//bootstrap
import { Modal, Button } from 'react-bootstrap';


class MainPageCategories extends Component {

    state = {
        categoryList: []
    };

    constructor(props) {
        super(props);
        this.getSearchInfoCatRequestToServer();

    }



    getSearchInfoCatRequestToServer = (showProductsOnInit) => {
        var url = '//'+window.location.hostname+'/php/twilio/FirstScreen_einkaufSearch.php?q=&act=cat&country='+localStorage.getItem("country")+'&shop='+this.props.selectedShop+'';
        axios.get(url)
          .then(( res ) => {
            this.setState({
                categoryList: res.data
            });
          })
    }

    categoryHtml = (categoryName, index) => {
        return (
            <div key={index} style={{background:"#fff", marginTop:"10px", width:"100%", float:"left"}}>
                <div style={{width:"100%", float:"left", fontSize:"18px", fontFamily:"open sans", fontWeight:400, margin:"10px 15px"}}><a href={"/?cat=grocery&act=category&catType="+encodeURIComponent(categoryName)}>{categoryName}</a></div>
                <MainPageItem modal_input_results={this.props.modal_input_results} selectedShop={this.props.selectedShop} suggestion2OptionClicked={this.props.suggestion2OptionClicked} category={categoryName}></MainPageItem>
            </div>
        );
    }

  render() {
    
    

      let categoryHtml = '';
      categoryHtml = this.state.categoryList.map((ele, i) => this.categoryHtml(ele.category, i));

      
    return (<div>{categoryHtml}</div>)

  }
}

export default MainPageCategories