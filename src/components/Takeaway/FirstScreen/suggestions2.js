import React, { Component } from 'react'
import Ink from "react-ink";
import ReactDOM from 'react-dom';

//bootstrap
import { Modal, Button } from 'react-bootstrap';


class Suggestions2 extends Component {

  state = {
    ZoomModal: false
  };

  showModalZoom (id) {
    this.setState({
      ZoomModal: {
        [id]: true
      }
    });
  }
  hideModalZoom = () => {
      this.setState({
          ZoomModal: false
      });
  }

  componentDidMount() {
    // document.addEventListener('click', this.handleClickOutside, true);
  }

  componentWillUnmount() {
    // document.removeEventListener('click', this.handleClickOutside, true);
  }

  handleClickOutside = event => {
    const domNode = ReactDOM.findDOMNode(this);

    if (!domNode || !domNode.contains(event.target) && !this.state.ZoomModal) {
      console.log("handle click");
      // document.getElementById('suggestionsBox').style.display = "none";
      // document.getElementById('closeSuggestionsIcon').style.display = "none";
      // this.props.resetSearchInputStates();
    }
  }



  breadcrumbHtml  = (currentSearchType) => {
    if (this.props.selectedCategory || this.props.subCategoryList.length) {// 
    return <div style={{background:"rgb(231, 238, 248)", borderBottom:"1px solid #d7d7d7", borderTopLeftRadius:"3px", borderTopRightRadius:"3px"}}><div style={{display:"inline-block", fontSize:"12px", margin:"2px 5px", position:"relative"}}>
       <span onClick={()=>this.props.getSearchInfoCat(false, true)} style={{color:"rgb(79, 101, 135)", padding:"5px 10px", cursor:"pointer"}}>
          Home
        </span>
      { (localStorage.getItem("selectedCategory")) ? (<span>
          <span>&#187;</span>
          <span onClick={()=>this.props.suggestion2OptionClicked({category: localStorage.getItem("selectedCategory")}, 'category')} style={{color:"rgb(79, 101, 135)", padding:"5px 10px", cursor:"pointer"}}>
            {localStorage.getItem("selectedCategory")}
          </span>
        </span>
      ) : '' } 
      { (localStorage.getItem("sub_category")) ? (<span>
          <span>&#187;</span>
          <span style={{padding:"5px 10px"}}>
            {localStorage.getItem("sub_category")}
          </span>
        </span>
      ) : '' } 
      {/* { (localStorage.getItem("sub_category2")) ? ` -> ${localStorage.getItem("sub_category2")}` : '' }  */}
      
      </div>
    <br /></div>
    } else {
      return '';
    }
  }

  categoryHtml  = (r, props) => (
    <div key={r.id} style={{display:"inline-block", textDecoration:"underline", fontSize:"12px", margin:"2px 5px", position:"relative", cursor:"pointer", color: (props.selectedCategory === r.category) ? "blue" : "rgb(79, 101, 135)"}} onClick={()=>props.suggestion2OptionClicked(r, 'category')}>{r.category} <Ink duration="500" /></div>
  );
    
  productItemHtml = (r, props) => (
    <li className={"suggestion-word-style"+((r.quantity >= 0) ? ' borderAdd' : '')} key={r.id} style={{textAlign:"left", background:"#fff"}}>

      <img src={"https://lieferservice123.com"+(r.placeholder_image ? r.placeholder_image : r.image)} style={{width:"160px", marginRight:"10px", float:"left"}} onClick={this.showModalZoom.bind(this, r.id)} /> 

      {r.quantity >= 0 ? <div className="react-reveal quantity-badge-list moreCss">{r.quantity}</div> : ''}

      <div style={{float:"left", width:"calc(100% - 170px)"}} onClick={()=>props.suggestion2OptionClicked(r, 'product')}>

        <div style={{color:"#333", fontSize:"12px"}}>{r.name}</div>

        <div style={{color:"#888", fontSize:"10px"}}>{r.category} {r.sub_category ? " » "+r.sub_category : null} {r.sub_category2 ? " » "+r.sub_category2 : null}</div>

        <div style={{color:"#bf0b1c", fontWeight:"bold", fontSize:"16px", marginTop:"15px"}}>{r.price} € <span style={{color:"#888", fontSize:"10px", fontWeight:"normal"}}>{r.grammageBadge}</span></div>

      </div>

      <Modal show={this.state.ZoomModal[r.id]} onHide={this.hideModalZoom}>
          <Modal.Header closeButton>
          </Modal.Header>
          <Modal.Body style={{padding:"0 25px 10px"}}>
              <div style={{fontSize:"12px"}}>{r.marke}</div>
              <div style={{fontSize:"12px"}}>{r.name}</div>

              <br/>

              <img src={"https://lieferservice123.com"+r.image} style={{width:"100%"}} alt="Bild wird geladen..." />

              <div style={{margin:"15px 0"}}>
                <span style={{color:"#555"}}>Preis: </span>
                <span style={{color:"#bf0b1c", fontSize:"16px"}}>{r.price} € </span>
                {r.grammageBadge ? <span style={{color:"#bf0b1c", fontSize:"14px"}}>({r.grammageBadge})</span> : null}
              </div>

              <button onClick={()=>props.suggestion2OptionClicked(r, 'product')} type="button" class="btn-save-address" style={{backgroundColor: "rgb(252, 128, 25)"}}>In den Einkaufswagen</button>
          </Modal.Body>
      </Modal>

      {/* <Ink duration="500" /> */}

    </li>
  );

  subCategoryHtml = (r, props, currentSearchType) => (
    <div key={r.id} style={{display:"inline-block", textDecoration:"underline", fontSize:"12px", margin:"2px 5px", position:"relative", cursor:"pointer", color: (props.selectedSubCategory === r[currentSearchType]) ? "blue" : "rgb(79, 101, 135)"}} onClick={()=>props.suggestion2OptionClicked(r, 'subCategory', currentSearchType)}>{r[currentSearchType]} <Ink duration="500" /></div>
  );

  errorMessageHtml = () => {
    console.log("in error message");
    return (this.props.no_sub_cat_results ? <h2 style={{padding:"50px 15px", textAlign:"center", fontFamily:"open sans", fontWeight:300, float:"left", width:"100%"}}>Keine Ergebnisse gefunden</h2> : <h2 style={{padding:"50px 15px", textAlign:"center", fontFamily:"open sans", fontWeight:300, float:"left", width:"100%"}}>Laden...</h2>)
  };

  render() {

    let { results, catSearchResults, modal_input_results, selectedCategory, subCategoryList, currentSearchType } = this.props;

    results = results || [];
    modal_input_results = modal_input_results || [];

    results = results.map((productItem) => {
      const itemFoundIndex =  modal_input_results.findIndex(ele => (ele && productItem && ele.id === productItem.id));
      if (itemFoundIndex >= 0) {
        // console.log(modal_input_results[itemFoundIndex]);
        productItem.quantity = modal_input_results[itemFoundIndex].quantity;
      }
      return productItem;
    });
    switch (currentSearchType) {
      case 'cat':
        currentSearchType = 'sub_category';
        break;
      case 'sub_category':
        currentSearchType = 'sub_category2';
        break;
      default:
        break;
    }
    const firstSelectInput = localStorage.getItem("firstSelectInput");
    catSearchResults = catSearchResults || [];
    let options = '';
    selectedCategory = localStorage.getItem("selectedCategory");
    if (selectedCategory || (subCategoryList.length)) {
      if (subCategoryList.length || results.length) {
        options = [this.breadcrumbHtml(currentSearchType)].concat(subCategoryList.map(r => this.subCategoryHtml(r, this.props, currentSearchType)).concat(results.map(r => this.productItemHtml(r, this.props))));
      } else if (firstSelectInput) {
        options = this.errorMessageHtml();
      }
    } else if (catSearchResults.length || results.length) {
      options = [this.breadcrumbHtml(currentSearchType)].concat(catSearchResults.map(r => this.categoryHtml(r, this.props)).concat(results.map(r => this.productItemHtml(r, this.props))));
    } else if (firstSelectInput) {
      options = this.errorMessageHtml();
    }

    // options = `${JSON.stringify(this.breadcrumbHtml(currentSearchType))} ${JSON.stringify(options)}`

    return (<div>
    
      <ul id="suggestionsBox" style={{textAlign:"center", zIndex:2, borderRadius:"3px", boxShadow: "0 0 5px rgba(0,0,0, .65)", position:"absolute", listStyle:"none", padding:0, margin:0, marginTop:"5px", background:"#fff", width:"95%"}}> {options}</ul>

    </div>)

  }
}

export default Suggestions2