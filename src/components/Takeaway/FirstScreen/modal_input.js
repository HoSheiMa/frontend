import React, { Component } from "react";

class ModalInput extends Component{

    onChangeText = (text) =>{
        const { myNo, onChangeTextItem } = this.props;
        onChangeTextItem(myNo, text);
    }

    onSelectText = () =>{
        const {onSelectTextItem, text, myNo} = this.props;
        onSelectTextItem(myNo, text);
    }

    // update the count of the cart in localstorage.
    updateCountInCart = (val) => {
        let countInCart = (localStorage.getItem("countInCart") && !isNaN(localStorage.getItem("countInCart"))) ? (parseFloat(localStorage.getItem("countInCart"))) : 0;
        countInCart += parseInt(val);
        countInCart = (countInCart < 0) ? 0 : countInCart;
        // console.log("cart item", countInCart);
        localStorage.setItem("countInCart", countInCart);
    };

    onChangeQuantity = (val) => {
        let {quantity, onChangeQuantityItem, myNo} = this.props;

        this.updateCountInCart(val);
        quantity += parseInt(val);
        if(quantity < 0){
            quantity = 0;
        }
        onChangeQuantityItem(myNo, quantity);
    }

    render(){

        const { text, placeholder, onSelectTextItem, quantity, order_id, myinfo, myNo, price, isFirst } = this.props;
        return(
            // <div onClick={this.onSelectText} style={{borderBottom: "1px solid #ccc"}}> //issue with quant < 1
            <div style={{borderBottom: "1px solid #ccc", fontSize:"12px"}}>

                <input 
                    placeholder={placeholder} 
                    value={text} 
                    onClick = {() => { this.onSelectText();}}
                    onChange={(e)=>this.onChangeText(e.currentTarget.value)} 
                    style={{padding:"5px", width:"100%"}} 
                    className="bequemEinkaufen" 
                    id={'input'+myNo}
                    // ref={input => {
                    //     if(input && isFirst){
                    //         setTimeout(function(){
                    //             console.log(input);
                    //             input.focus();
                    //         },500)
                            
                    //     }
                       
                    // }}
                    autoFocus={true}
                /> 
            

                {/* add display:"inline-block" when suggestion is clicked */}
                <span id={'span'+myNo} style={{padding:"5px 0px", width:"calc(100% - 110px)", display:"none"}}>{text}</span>

                {order_id === null ? null:
                    <span style={{padding:"5px 0", float:"right"}}>
                        <span style={{cursor:"pointer", margin:"5px", padding:"0 5px", border:"1px solid #ccc"}} onClick={()=>this.onChangeQuantity(-1)} >-</span>
                        <span>{quantity}</span>
                        <span style={{cursor:"pointer", margin:"5px", padding:"0 5px", border:"1px solid #ccc"}} onClick={()=>this.onChangeQuantity(1)}>+</span>
                        <span>€ {(price * quantity).toFixed(2)}</span>
                    </span>
                }
            </div>
        );
    };
}

export default ModalInput