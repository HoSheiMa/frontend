import React, { Component } from 'react'
import Ink from "react-ink";
import ReactDOM from 'react-dom';
import axios from "axios";

//bootstrap
import { Modal, Button } from 'react-bootstrap';


class MainPageSub2CategoryItems extends Component {

    state = {
        itemList: [],
        ZoomModal: false,

    };

    constructor(props) {
        super(props);
        this.getItemsInCategory(this.props.category);
    }

    showModalZoom (id) {
        this.setState({
          ZoomModal: {
            [id]: true
          }
        });
      }
      hideModalZoom = () => {
          this.setState({
              ZoomModal: false
          });
      }
    
    checkQtyInCart = () => {
      const modal_input_results = this.props.modal_input_results || [];
      const results = this.state.itemList.map((productItem) => {
        const itemFoundIndex =  modal_input_results.findIndex(ele => (ele && productItem && ele.id === productItem.id));
        if (itemFoundIndex >= 0) {
          // console.log(modal_input_results[itemFoundIndex]);
          productItem.qty = modal_input_results[itemFoundIndex].quantity;
        }
        return productItem;
      });
      this.setState({
        itemList: results
      });
    }

      getItemsInCategory = async (categoryName) => {
        try {
            var url = `//${window.location.hostname}/php/twilio/FirstScreen_einkaufSearch.php?act=&q=&country=${localStorage.getItem("country")}&cat=${encodeURIComponent(categoryName)}&sub_category2=${encodeURIComponent(this.props.sub2Cat)}&limit=50&shop=${this.props.selectedShop}`;
            const data = await axios.get(url);
            this.setState({
                itemList: data.data
            });
            this.checkQtyInCart();
        } catch (error) {
            throw error;
        }
    };

    itemHtml = (r) => (
        <li className={"suggestion-word-style mainPageItem"+((r.qty >= 0) ? ' borderAdd' : '')} key={r.id}>

        <img src={"https://lieferservice123.com"+(r.placeholder_image ? r.placeholder_image : r.image)} style={{marginRight:"10px"}} onClick={this.showModalZoom.bind(this, r.id)} /> 
  
        {r.qty >= 0 ? <div className="react-reveal quantity-badge-list moreCss">{r.qty}</div> : ''}
  
        <div style={{width:"100%"}} onClick={()=>{this.addItemToCart(r, 'product')}}>
  
          <div style={{color:"#333", fontSize:"12px"}}>{r.name}</div>
    
          <div style={{color:"#bf0b1c", fontWeight:"bold", fontSize:"16px"}}>{r.price} € <span style={{color:"#888", fontSize:"10px", fontWeight:"normal"}}>{r.grammageBadge}</span></div>
  
        </div>
  
        <Modal show={this.state.ZoomModal[r.id]} onHide={this.hideModalZoom}>
            <Modal.Header closeButton>
            </Modal.Header>
            <Modal.Body style={{padding:"0 25px 10px"}}>
                <div style={{fontSize:"12px"}}>{r.marke}</div>
                <div style={{fontSize:"12px"}}>{r.name}</div>
  
                <br/>
  
                <img src={"https://lieferservice123.com"+r.image} style={{width:"100%"}} alt="Bild wird geladen..." />
  
                <div style={{margin:"15px 0"}}>
                  <span style={{color:"#555"}}>Preis: </span>
                  <span style={{color:"#bf0b1c", fontSize:"16px"}}>{r.price} € </span>
                  {r.grammageBadge ? <span style={{color:"#bf0b1c", fontSize:"14px"}}>({r.grammageBadge})</span> : null}
                </div>
  
                <button onClick={()=>this.addItemToCart(r, 'product')} type="button" class="btn-save-address" style={{backgroundColor: "rgb(252, 128, 25)"}}>In den Einkaufswagen</button>
            </Modal.Body>
        </Modal>
  
        {/* <Ink duration="500" /> */}
  
      </li>
    )

    addItemToCart = (r, type) => {
      this.props.suggestion2OptionClicked(r, 'product');
      setTimeout(() => {
        this.checkQtyInCart();
      }, 1000);
    };

  render() {
    //   console.log(this.state.itemList);
    
      let itemHtml = '';
      itemHtml = this.state.itemList.map((ele) => this.itemHtml(ele));
   return (<div className="mainPageScroll" style={{width:"100%", overflow:"scroll"}}>
      <div style={{width:"max-content"}}>{itemHtml}</div>
    </div>)

  }
}

export default MainPageSub2CategoryItems