import React from 'react'
import Ink from "react-ink";

const Suggestions = (props) => {
  const options = props.results.map(r => (
    <li className={"suggestion-word-style"+((r.qty >= 0) ? ' borderAdd' : '')} key={r.id} onClick={()=>props.onClickSuggestionWord(props.myNo, r.name, r.id, r.price, r)} style={{textAlign:"left", background:"#fff"}}>

      <img src={"https://lieferservice123.com"+(r.placeholder_image ? r.placeholder_image : r.image)} style={{width:"160px", marginRight:"10px", float:"left"}} /> 

      {r.qty >= 0 ? <div className="react-reveal quantity-badge-list moreCss">{r.qty}</div> : ''}

      <div style={{float:"left", width:"calc(100% - 170px)"}}>

        <div style={{color:"#333", fontSize:"12px"}}>{r.name}</div>

        <div style={{color:"#888", fontSize:"10px"}}>{r.category} {r.sub_category ? " » "+r.sub_category : null} {r.sub_category2 ? " » "+r.sub_category2 : null}</div>

        <div style={{color:"#bf0b1c", fontWeight:"bold", fontSize:"16px", marginTop:"15px"}}>{r.price} € <span style={{color:"#888", fontSize:"10px", fontWeight:"normal"}}>{r.grammageBadge}</span></div>

      </div>

      {/* <Ink duration="500" /> */}

    </li>
  ))
  return <ul id="suggestionsBox" style={{borderRadius:"3px", boxShadow: "0 0 5px rgba(0,0,0, .65)", position:"absolute", listStyle:"none", padding:0, margin:0, background:"#fafafa", width:"90%"}}>{options}</ul>
}

export default Suggestions