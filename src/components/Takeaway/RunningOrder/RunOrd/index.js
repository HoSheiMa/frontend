import React, { Component } from "react";

import BackWithSearch from "../../../Takeaway/Elements/BackWithSearch";
import Map from "./../Map";
import Meta from "../../../helpers/meta";
import { Redirect } from "react-router";
import { connect } from "react-redux";

import Ink from "react-ink";

import { Modal, Button } from 'react-bootstrap';
import ReactTooltip from 'react-tooltip';

import Items from './items.js';
import ItemsPic from './itemsPic.js';

// fontawesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { faClock } from '@fortawesome/fontawesome-free-regular';
import { faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons';
import { faShoppingBag } from '@fortawesome/free-solid-svg-icons';
import { faCreditCard } from '@fortawesome/free-solid-svg-icons';
import { faPhoneAlt } from '@fortawesome/free-solid-svg-icons';
import { faComment } from '@fortawesome/fontawesome-free-regular'
import { faUserCircle } from '@fortawesome/fontawesome-free-regular'
import { faCircle } from '@fortawesome/free-solid-svg-icons';
import { faEdit } from '@fortawesome/free-solid-svg-icons';
import { faQuestionCircle } from '@fortawesome/free-solid-svg-icons';

import { SUPERMARKET_ORDER_URL } from "../../../../configs/index";
import axios from "axios";

import RewviewChanges from './reviewChanges.js';
import ViewReceipt from './viewReceipt.js';

import { GET_RESTAURANT_INFO_BY_ID_URL } from "../../../../configs/index";



class RunOrd extends Component {

    constructor() {
        super();

        if(localStorage.getItem("fromName") && localStorage.getItem("fromTel")) {
            console.log("already set fromName and fromTel");
            var fromName = localStorage.getItem("fromName");
            var fromTel = localStorage.getItem("fromTel");
        } else {
            console.log("set fromName and fromTel");
            var fromName = this.getUrlParameter('fromName');
            var fromTel = this.getUrlParameter('fromTel');
        }
        
        this.state = {
            supermarket_orders: [],
            fromTel: fromTel,
            fromName: fromName,
            paid: false
        };
    }

    static contextTypes = {
        router: () => null
    };

    showModalReviewChanges () {
        this.setState({
            ReviewChangesModal: true
        });
    }
    hideModalReviewChanges = () => {
        this.setState({
            ReviewChangesModal: false
        });
    }

    showModalViewReceipt () {
        this.setState({
            ViewReceiptModal: true
        });
    }
    hideModalViewReceipt = () => {
        this.setState({
            ViewReceiptModal: false
        });
    }

    showModalTip () {
        this.setState({
            TipModal: true
        });
    }
    hideModalTip = () => {
        this.setState({
            TipModal: false
        });
    }

    __refreshOrderStatus = () => {
        this.supermarketOrders();
        // if (this.refs.refreshButton) {
        //     this.refs.refreshButton.setAttribute("disabled", "disabled");
        //     this.supermarketOrders();
        //     this.refs.btnSpinner.classList.remove("hidden");
        //     setTimeout(() => {
        //         this.refs.btnSpinner.classList.add("hidden");
        //     }, 2 * 1000);
        //     setTimeout(() => {
        //         if (this.refs.refreshButton.hasAttribute("disabled")) {
        //             this.refs.refreshButton.removeAttribute("disabled");
        //         }
        //     }, 2 * 1000);
        // }
    };

    onCodeChanged = (e) => {
        this.setState({
            verificationCode: e.currentTarget.value
        });
    }
    
    componentDidMount() {
        this.supermarketOrders();

        localStorage.setItem("showMap", false)

        this.refreshSetInterval = setInterval(() => {
            this.__refreshOrderStatus();
        }, 15 * 1000);

        console.log("this.state.paid", this.state.paid);

        setTimeout(() => {
            this.setState({
                delivery_tip: this.state.supermarket_orders.delivery_tip
            });
        }, 2 * 1000);

        console.log("check tel");
        this.isTelVerified();
    }

    isTelVerified = () =>{
        if(localStorage.getItem("codeCustomer")){
            axios
            .get('//'+window.location.hostname+'/php/twilio/PaymentGuest_isTelVerified.php?fromTel='+btoa(this.country_formatted_tel(this.state.fromTel))+'&code='+localStorage.getItem("codeCustomer"))
            .then(response => {
                if(response.data.success == true){
                    this.setState({
                        tel_verified: true
                    });
                } else{
                    this.setState({
                        tel_verified: false
                    });
                }

                localStorage.setItem("fromName", response.data.name);
            })
            .catch(function(error) {
                console.log(error);
            });

        } else {
            this.setState({
                tel_verified: false
            });
        }
    } 

    supermarketOrders = () =>{
        axios
        .post(SUPERMARKET_ORDER_URL, {
            id: window.location.href.split("/").pop()
        })
        .then (newArray => {
            // console.log("newArray.data", newArray.data);

            //check status of payment and if necessary update status
            if(newArray.data.payment_status == "consumed" || newArray.data.payment_method == "wallet") {
                this.setState({
                    paid: true
                });
            }
            if(newArray.data.payment_method == "cards"){
                this.checkPaymentStatusCards(newArray.data);
            } else if(newArray.data.payment_method == "sofort" || newArray.data.payment_method == "eps"){
                this.checkPaymentStatus(newArray.data);
            }

            this.setState({
                supermarket_orders: newArray.data
            });
        })
        .catch(function(error) {
            console.log(error);
        });
    }

    toDate = (unix_timestamp) => {
        var d = new Date(unix_timestamp * 1000);
        return d.getFullYear() + '-' + (d.getMonth()+1) + '-' + d.getDate() + ' ' + d.getHours() + ':' + d.getMinutes();
    }

    getAddress = (address, extract, long=true) => {
        if(extract == "str+nr"){
            var res = address;
            var split = res.split(",");
            if(long) var res = split.slice(0, split.length - 1).join(",");
            else var res = split[0];
            return res;
        } else if(extract == "str"){
            var res = address;
            var split = res.split(",");
            var res = split[0];
            return res;
        }  else if(extract == "plz"){
            var res = address;
            var split = res.split(",");
            var res = split[1];

            //only plz without city
            // var split = res.split(" ");
            // var res = split[1];
            return res;
        } else {
            var res = address.split(", ");
            return res[0];
        }
    }

    navigation = (lat, long) =>{
        // If it's an iPhone..
        if( (navigator.platform.indexOf("iPhone") != -1) 
            || (navigator.platform.indexOf("iPod") != -1)
            || (navigator.platform.indexOf("iPad") != -1))
             window.open("maps://maps.google.com/maps?daddr="+lat+","+long+"&amp;ll=");
        else
             window.open("http://maps.google.com/maps?daddr="+lat+","+long+"&amp;ll=");
    }

    items = (supermarket_orders, url) =>{
        if(supermarket_orders.items == null || supermarket_orders.items.length === 0) return null;        
        return <Items supermarket_orders={supermarket_orders} url={url} />;
    }

    itemsPic = (supermarket_orders) =>{
        if(supermarket_orders.items == null || supermarket_orders.items.length === 0) return null;        
        return <ItemsPic supermarket_orders={supermarket_orders} />
    }

    getUrlParameter(sParam) {
        var sPageURL =decodeURIComponent(window.location.search.substring(1));
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam) {
                return sParameterName[1];
            }
        }
    }

    checkPaymentStatusCards = (supermarket_orders) =>{
        var url = '//'+window.location.hostname+'/php/stripe/retrieveSession.php?session='+supermarket_orders.payment_session;
        axios
        .get(url)
        .then(response => {

            if(response.data.payment_status == "paid"){
                this.setState({
                    paid: true
                });

                if(supermarket_orders.payment_status == null){//update mysql
                    this.updatePaymentStatus(supermarket_orders.id, "consumed");
                }
            }
        })
        .catch(function(error) {
            console.log(error);
        });
    }
    checkPaymentStatus = (supermarket_orders) =>{
        var url = '//'+window.location.hostname+'/php/stripe/retrieveSource.php?source='+supermarket_orders.payment_source;
        axios
        .get(url)
        .then(response => {

            if(response.data.status == "consumed"){
                this.setState({
                    paid: true
                });

                if(supermarket_orders.payment_status == null){//update mysql
                    this.updatePaymentStatus(supermarket_orders.id, "consumed");
                }
            }

            this.setState({
                redirectUrl: response.data.redirect.url
            });
            this.setState({
                iban_last4: response.data.sofort.iban_last4
            });
        })
        .catch(function(error) {
            console.log(error);
        });
    }

    updatePaymentStatus = (id, payment_status) =>{
        var url = '//'+window.location.hostname+'/php/stripe/updatePaymentStatus.php?id='+id+'&fromTel='+btoa(this.country_formatted_tel(localStorage.getItem("fromTel")))+'&code='+localStorage.getItem("codeCustomer")+'&payment_status='+payment_status;
        axios
        .get(url)
        .then(response => {
            
        })
        .catch(function(error) {
            console.log(error);
        });
    }

    country_formatted_tel = (fromTel) => {
        if(fromTel){
            var fromTel = fromTel.trim();
            var first2 = fromTel.substr(0, 2);
            if(first2 == localStorage.getItem('phoneCountryCode')) {
                var fromTel = "+".fromTel;
                return fromTel;
            }
        
            var firstLetter = fromTel.substr(0, 1);
            if(firstLetter == 0) {
                var fromTel = "+"+localStorage.getItem('phoneCountryCode')+fromTel.substr(1);
                return fromTel;
            }
        
            return fromTel;
        }
    }

    showTime = (unix_timestamp) => {
        var d = new Date(unix_timestamp * 1000);
        return  d.getHours() + ':' + d.getMinutes();
    }

    uppercase(str) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }

    sendSMS = (tel) => {
        var ua = navigator.userAgent.toLowerCase();
        var href;

        ////stackoverflow.com/questions/6480462/how-to-pre-populate-the-sms-body-text-via-an-html-link
        // if (ua.indexOf("iphone") > -1 || ua.indexOf("ipad") > -1){
        //     href = 'sms:/' + tel;
        // } else{
        //     href = 'sms://' + tel;
        // }

        href = 'sms:' + tel;

        window.open(href);
    }

    showDate = (unix_timestamp) => {
        var days = ['So','Mo','Di','Mi','Do','Fr','Sa'];
        const monthNames = ["Jän", "Feb", "März", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dez"];
        var d = new Date(unix_timestamp * 1000);
        return  days[d.getDay()]+', '+d.getDate()+ '. ' + (monthNames[d.getMonth()]);
    }

    makeTotalQuantity = (data = [], tab) => {
        let sum = 0;
        data.forEach((row) => {

            if(tab == "todo" && row.found == null){
                sum = sum + 1
            } 
            else if(tab == "review" && row.found == 0){
                sum = sum + 1
            } 
            else if(tab == "replacements" && row.found == 0 && row.replacement && row.approved == null){
                sum = sum + 1
            } 
            else if(tab == "done" && row.found > 0){
                sum = sum + 1
            }
        });
        return sum;
    }

    onTipChanged = (e) => {
        this.setState({
            delivery_tip: e.currentTarget.value
        });
    } 

    changeTip = (id) => {
        var url = '//'+window.location.hostname+'/php/shopperPwa_changeTip.php';
        var fd = new FormData();
        fd.append('fromTel', btoa(this.country_formatted_tel(localStorage.getItem("fromTel"))));
        fd.append('code', localStorage.getItem("codeCustomer"));
        fd.append('id', id);
        fd.append('delivery_tip', this.state.delivery_tip);
        axios
        .post(url, fd)
        .then(res => {
            if(res.data.error === false){
                var url = window.location.href;
                window.location = url;
            } else {
                alert("Ein Fehler ist aufgetreten.");
            }   
        })
        .catch(function(error) {
            console.log(error);
        });
    }

    addToCart = (id) => {
        axios.post(GET_RESTAURANT_INFO_BY_ID_URL + "/" + id).then(response => {

            const restaurant_info = response.data;

            // console.log("restaurant_info", restaurant_info);

            try {
                let localState = localStorage.getItem('state');
                if (localState) {
                    localState = JSON.parse(localState);
                    localState.cart = localState.cart || {};
                    localState.total = localState.total || {};
                    localState.total.data = localState.total.data || {};
                    localState.items = localState.items || {};
                    localState.items.restaurant_info = localState.items.restaurant_info || {};

                    localState.total.data.productQuantity = this.state.supermarket_orders.items.length;
                    localState.total.data.totalPrice = parseFloat(this.state.supermarket_orders.subtotal);

                    localState.cart.products = this.state.supermarket_orders.items || [];
                    localState.cart.products = localState.cart.products.map((ele) => ({...ele, price: ele.price}));

                    localState.items.restaurant_info = restaurant_info;
                    localState.items.restaurant_items = [];

                    localStorage.setItem('state', JSON.stringify(localState));
                    localStorage.setItem('activeRestaurant', id);
                }
            } catch (err) {
                console.log(err);
            }
    
            window.location = '/cart';
        })
        .catch(function(error) {
            console.log(error);
        });
    }

    render() {
        if (localStorage.getItem("storeColor") === null) {
            return <Redirect to={"/"} />;
        }

        // if (this.state.tel_verified === false) {
        //     return <Redirect to={"/"} />;
        // }

        const { user } = this.props;
        const { supermarket_orders } = this.state;

        console.log("supermarket_orders", supermarket_orders);


        //for debugging use http://172.105.79.167:3000/running-order/OD-03-26-4KBZK6
        //and then it will give error for location, then you can debug

        return (
            <React.Fragment>
                <Meta
                    seotitle={localStorage.getItem("seoMetaTitle")}
                    seodescription={localStorage.getItem("seoMetaDescription")}
                    ogtype="website"
                    ogtitle={localStorage.getItem("seoOgTitle")}
                    ogdescription={localStorage.getItem("seoOgDescription")}
                    ogurl={window.location.href}
                    twittertitle={localStorage.getItem("seoTwitterTitle")}
                    twitterdescription={localStorage.getItem("seoTwitterDescription")}
                />

                <BackWithSearch
                    boxshadow={true}
                    has_title={true}
                    title={supermarket_orders.shopName+" Bestellung"}
                    disbale_search={true}
                    back_to_home={true}
                    goto_orders_page={false}
                />

                <React.Fragment>

                    {/* {localStorage.getItem("showMap") === "true" && (
                        <Map
                            restaurant_latitude={''}
                            restaurant_longitude={''}
                            order_id={supermarket_orders.id}
                            orderstatus_id={supermarket_orders.orderstatus_id}
                            deliveryLocation={'{"lat":"48.276848","lng":"14.3133127","address":"Turmstra\u00dfe 3, 4020 Linz, Austria","house":null,"tag":null}'}
                        />
                    )} */}

                    <div className="bg-white mb-200" style={{ position: "relative", top: localStorage.getItem("showMap") === "true" ? "24.5rem" : "0", maxWidth: "468px", margin: "auto"}}>

                        {supermarket_orders.length == null ? (

                            <div>

                                {/* Bezahlstatus */}
                                {(supermarket_orders.orderstatus_id === 1 && this.state.paid == false) && (
                                    <React.Fragment>
                                        <div className="row" style={{textAlign:"center"}}>
                                            <div className="col-md-12">
                                                <div className="block block-link-shadow">
                                                    <div className="block-content block-content-full clearfix py-0">
                                                        <div className="mt-20">
                                                            <div className="font-w600 font-size-h1 mb-5">
                                                                Bezahlstatus
                                                            </div>
                                                            <div style={{fontWeight:"bold"}}>Bezahle Deine Bestellung mit {supermarket_orders.payment_method=="sofort"?"Klarna":"Kreditkarte"}</div>
                                                            <hr/>
                                                            <div className="font-size-sm text-muted">
                                                                Die {supermarket_orders.payment_method=="sofort"?"Klarnazahlung":"Kreditkartenzahlung"} wurde von Dir abgebrochen. Die Bestellung wird nicht ausgeführt. Klicke bitte <a href={this.state.redirectUrl} style={{color:"blue"}}>hier</a> um erneut zu versuchen, Deine Bestellung abzuschicken. 
                                                                {/* Hier kannst Du auch eine alternative Bezahlmethode wählen. */}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </React.Fragment>
                                )}
                                                    
                                {(supermarket_orders.orderstatus_id === 1) && (
                                    <React.Fragment>
                                        <div className="row" style={{textAlign:"center"}}>
                                            <div className="col-md-12">
                                                <div className="block-link-shadow" style={{background:localStorage.getItem("storeColor"), color:"#fff"}}>
                                                    <div className="block-content block-content-full clearfix py-0">
                                                        <div>
                                                            <img
                                                                src="/assets/img/order-placed.gif"
                                                                className="img-fluid img-avatar"
                                                                alt={localStorage.getItem("runningOrderPlacedTitle")}
                                                            />
                                                        </div>
                                                        <div style={{padding:"10px"}}>
                                                            <div className="font-w600 font-size-h4 mb-5">
                                                                Danke - Bestellung {this.state.paid?"gezahlt":"erhalten"}!
                                                            </div>
                                                            <div className="font-size-sm" style={{color:"#fff", fontWeight:300, fontFamily:"open sans"}}>
                                                                Sie haben noch Zeit, Artikel hinzuzufügen oder Ihre Bestellung zu bearbeiten, bevor der Einkauf beginnt.
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </React.Fragment>
                                )}
                                                    
                                {(supermarket_orders.orderstatus_id === 2 && this.state.paid && supermarket_orders.startShoppingDate == null) && (
                                    <React.Fragment>
                                        <div className="row" style={{textAlign:"center"}}>
                                            <div className="col-md-12">
                                                <div className="block-link-shadow" style={{background:localStorage.getItem("storeColor"), color:"#fff"}}>
                                                    <div className="block-content block-content-full clearfix py-0">
                                                        <div>
                                                            <img
                                                                src="/assets/img/order-placed.gif"
                                                                className="img-fluid img-avatar"
                                                                alt={localStorage.getItem("runningOrderPlacedTitle")}
                                                            />
                                                        </div>
                                                        <div style={{padding:"10px"}}>
                                                            <div className="font-w600 font-size-h4 mb-5">
                                                                Bestellung angenommen!
                                                            </div>
                                                            <div className="font-size-sm" style={{color:"#fff", fontWeight:300, fontFamily:"open sans"}}>
                                                                {supermarket_orders.acceptedName ? (
                                                                    <span>{this.uppercase(supermarket_orders.acceptedName)} hat Ihre Bestellung angenommen.</span>
                                                                ) : (
                                                                    <span>Ein Zusteller hat Ihre Bestellung angenommen.</span>
                                                                )}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </React.Fragment>
                                )}

                                {/* shopping */}
                                {(supermarket_orders.orderstatus_id === 2 && this.state.paid && supermarket_orders.startShoppingDate && supermarket_orders.startCheckoutDate == null && supermarket_orders.startNavigateDate == null) && (
                                    <React.Fragment>
                                        <div className="row">
                                            <div className="col-md-12">
                                                <div className="block-link-shadow" style={{background:localStorage.getItem("storeColor"), color:"#fff"}}>

                                                    <div style={{borderLeft: "2px solid orange", marginLeft: "18px", height: "211px", position: "absolute"}}></div>

                                                    <div className="block-content block-content-full clearfix py-0">
                                                        <div style={{textAlign:"center"}}>
                                                            <img
                                                                src="https://lieferservice123.com/assets/img/startShopping.gif"
                                                                className="img-fluid img-avatar"
                                                            />
                                                        </div>
                                                        <div style={{padding:"10px", paddingTop:0, marginLeft:"10px"}}>

                                                            <div className="font-w600 font-size-h4 mb-5">
                                                                <FontAwesomeIcon icon={faCircle} style={{fontSize:"18px", marginLeft:"-25px", marginTop:"5px", position:"absolute", color:"orange"}} />Shopping <span style={{float:"right", fontSize:"80%", fontWeight:"normal"}}>{this.makeTotalQuantity(supermarket_orders.items, "done")+this.makeTotalQuantity(supermarket_orders.items, "review")} von {supermarket_orders.items.length}</span>
                                                            </div>

                                                            {this.makeTotalQuantity(supermarket_orders.items, "replacements") > 0 ? (
                                                                <div>
                                                                    <div className="font-size-sm" style={{marginTop:"15px", color:"#fff"}}>
                                                                        {/* <FontAwesomeIcon icon={faUserCircle} style={{float:"left", fontSize:"40px", height:"40px", marginRight:"10px"}} /> */}
                                                                        <div style={{float:"left", width:"40px", height:"40px", background:"#E7003D", color:"#fff", textAlign:"center", padding:"8px 15px", borderRadius: "24px", boxShadow: "0 1px 2px #8d8d8d", color: "#fff", fontSize: "16px", fontWeight: "bold", marginRight:"10px"}}>{this.makeTotalQuantity(supermarket_orders.items, "replacements")}</div> 
                                                                        
                                                                        <span style={{fontSize:"12px"}}>{this.makeTotalQuantity(supermarket_orders.items, "replacements")} Artikel {this.makeTotalQuantity(supermarket_orders.items, "replacements")==1?"war":"waren"} nicht vorrätig, daher hat {supermarket_orders.acceptedName ? this.uppercase(supermarket_orders.acceptedName) : "ihr Shopper"} einige Änderungen an Ihrer Bestellung vorgenommen.</span>
                                                                    </div>

                                                                    <Button onClick={() => this.showModalReviewChanges()} variant="secondary" style={{background:"#fff", position:"relative", width:"100%", color:localStorage.getItem("storeColor"), marginTop:"20px", marginBottom:"5px", textTransform:"uppercase"}}><a href={""}>Änderungen Jetzt Überprüfen</a><Ink duration="500" /></Button>
                                                                </div>
                                                            ) : (
                                                                <div>
                                                                    <div className="font-size-sm" style={{marginTop:"15px", color:"#fff"}}>
                                                                        <FontAwesomeIcon icon={faUserCircle} style={{float:"left", fontSize:"40px", height:"40px", marginRight:"10px"}} /> Wir werden Sie über Änderungen Ihrer Bestellung informieren
                                                                    </div>

                                                                    <Button onClick={() => this.showModalReviewChanges()} variant="secondary" style={{background:"#fff", position:"relative", width:"100%", color:localStorage.getItem("storeColor"), marginTop:"20px", marginBottom:"5px", textTransform:"uppercase"}}><a href={""}>Gefundene Artikel Anzeigen</a><Ink duration="500" /></Button>
                                                                </div>
                                                            )}

                                                            {/* review changes */}
                                                            <Modal show={this.state.ReviewChangesModal} onHide={this.hideModalReviewChanges} style={{}}>
                                                                <Modal.Header closeButton style={{paddingBottom:0, background:"#F7F7F7"}}>
                                                                    <Modal.Title style={{width:"100%"}}>
                                                                        <div style={{fontFamily:"open sans", fontWeight:600, textAlign:"center", color:localStorage.getItem("storeColor")}}>Meine Artikel</div>
                                                                        <div style={{fontFamily:"open sans", fontWeight:600, textAlign:"center", fontSize:"12px", color:localStorage.getItem("storeColor")}}>{supermarket_orders.shopName} &middot; Lieferung @ {this.showTime(supermarket_orders.added)} - {this.showTime(supermarket_orders.added+3600)}</div>
                                                                    </Modal.Title>
                                                                </Modal.Header>
                                                                <Modal.Body style={{padding:0, background:"#F7F7F7"}}>

                                                                    <div style={{width:"100%", float:"left", padding:"15px 10px", background:"#fff", margin:"7.5px 0", borderTop:"1px solid #E8E8E8", borderBottom:"1px solid #E8E8E8"}}>

                                                                        {this.makeTotalQuantity(supermarket_orders.items, "replacements") > 0 ? (
                                                                            <div>
                                                                                {/* <div style={{float:"left", width:"50px"}}><FontAwesomeIcon icon={faQuestionCircle} style={{fontSize:"40px", verticalAlign:"bottom"}} /></div> */}
                                                                                <div style={{float:"left", width:"40px", height:"40px", background:"#E7003D", color:"#fff", textAlign:"center", padding:"8px 15px", borderRadius: "24px", boxShadow: "0 1px 2px #8d8d8d", color: "#fff", fontSize: "16px", fontWeight: "bold", marginRight:"10px"}}>{this.makeTotalQuantity(supermarket_orders.items, "replacements")}</div> 

                                                                                <div style={{float:"right", width:"calc(100% - 60px)"}}>
                                                                                    <div style={{fontWeight:"bold", fontFamily:"open sans", fontSize:"16px", color:"#333"}}>Überprüfe {this.makeTotalQuantity(supermarket_orders.items, "replacements")} {this.makeTotalQuantity(supermarket_orders.items, "replacements")==1?"Änderung":"Änderungen"}</div>
                                                                                    
                                                                                    <div style={{fontWeight:600, fontFamily:"open sans", color:"#ABABAB"}}>{this.makeTotalQuantity(supermarket_orders.items, "done")+this.makeTotalQuantity(supermarket_orders.items, "review")} von {supermarket_orders.items.length} Artikeln eingekauft</div>
                                                                                </div>
                                                                            </div>
                                                                        ) : (
                                                                            <div>
                                                                                <div style={{float:"left", width:"50px"}}><img src={'https://lieferservice123.com/assets/img/loading.gif'} style={{width:"40px", verticalAlign:"bottom"}} /></div>

                                                                                <div style={{float:"left", width:"calc(100% - 50px)"}}>
                                                                                    <div style={{fontWeight:"bold", fontFamily:"open sans", fontSize:"16px", color:"#333"}}>{supermarket_orders.acceptedName ? this.uppercase(supermarket_orders.acceptedName) : "Ihr Shopper"} kauft gerade ein</div>
                                                                                    <div style={{fontWeight:600, fontFamily:"open sans", color:"#ABABAB"}}>{this.makeTotalQuantity(supermarket_orders.items, "done")+this.makeTotalQuantity(supermarket_orders.items, "review")} von {supermarket_orders.items.length} Artikeln eingekauft</div>
                                                                                </div>

                                                                            </div>
                                                                        )}

                                                                    </div>

                                                                    <RewviewChanges supermarket_orders={supermarket_orders} />

                                                                    {supermarket_orders.acceptedName && (
                                                                        <div style={{width:"100%", float:"left", padding:"10px", textAlign:"center", borderTop:"1px solid #E8E8E8"}}>

                                                                            <div style={{padding:"25px 18px 8px", width:"100%", float:"left", fontFamily:"open sans", fontWeight:600, margin:"auto", color:"#ABABAB"}}>Wollen Sie Ihren Shopper sprechen?</div>

                                                                            <a href={'tel:'+supermarket_orders.fromTel} style={{overflow:"auto", display:"block", border:"1px solid orange", color:localStorage.getItem("storeColor"), padding:"10px 10px", fontWeight:"bold", fontFamily:"open sans", borderRadius:"5px", fontSize:"13px"}}><FontAwesomeIcon icon={faComment} style={{marginRight:"5px"}} />{this.uppercase(supermarket_orders.acceptedName)} eine Nachricht senden</a>
                                                                        </div>
                                                                    )}
                                                                </Modal.Body>
                                                            </Modal>
                                                            
                                                        </div>
                                                    </div>
                                                </div>

                                                <div style={{borderLeft: "2px solid #E0E0E0", marginLeft: "18px", height: "170px", position: "absolute"}}></div>

                                                <div className="font-w600 font-size-h4" style={{color:"#DFDFDF", marginLeft:"25px"}}>
                                                    <div style={{padding:"20px 10px"}}><FontAwesomeIcon icon={faCircle} style={{fontSize:"9px", marginLeft:"-21px", marginTop:"10px", position:"absolute"}} />Shopping Abgeschlossen</div>
                                                    <div style={{padding:"20px 10px"}}><FontAwesomeIcon icon={faCircle} style={{fontSize:"9px", marginLeft:"-21px", marginTop:"10px", position:"absolute"}} />Lieferung</div>
                                                    <div style={{padding:"20px 10px"}}><FontAwesomeIcon icon={faCircle} style={{fontSize:"9px", marginLeft:"-21px", marginTop:"10px", position:"absolute"}} />Fertig</div>
                                                </div>

                                                <div style={{background:"#F7F7F7", height:"20px"}}></div>
                                            </div>
                                        </div>
                                    </React.Fragment>
                                )}

                                {/* shopping completed */}
                                {(supermarket_orders.orderstatus_id === 2 && this.state.paid && supermarket_orders.startShoppingDate &&  this.makeTotalQuantity(supermarket_orders.items, "todo") == 0 && supermarket_orders.startCheckoutDate && supermarket_orders.startNavigateDate == null) && (
                                    <React.Fragment>
                                        <div className="row">
                                            <div className="col-md-12">


                                                <div className="block-link-shadow" style={{color:localStorage.getItem("storeColor")}}>

                                                    <div style={{borderLeft: "2px solid rgb(252, 128, 25)", marginLeft: "18px", height: "42px", position: "absolute", marginTop:"10px"}}></div>

                                                    <div className="block-content block-content-full clearfix py-0">
                                                        <div style={{padding:"10px", paddingTop:0, marginLeft:"10px"}}>
                                                            <div className="font-w600 font-size-h4 mb-5">
                                                                <FontAwesomeIcon icon={faCircle} style={{fontSize:"18px", marginLeft:"-25px", marginTop:"5px", position:"absolute"}} />Shopping
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="block-link-shadow" style={{background:localStorage.getItem("storeColor"), color:"#fff"}}>

                                                    <div style={{borderLeft: "2px solid orange", marginLeft: "18px", height: "172px", position: "absolute"}}></div>

                                                    <div className="block-content block-content-full clearfix py-0">
                                                        <div style={{textAlign:"center"}}>
                                                            <img
                                                                src="https://lieferservice123.com/assets/img/temperature-icon.jpg"
                                                                className="img-fluid img-avatar"
                                                            />
                                                        </div>
                                                        <div style={{padding:"10px 10px 15px", marginLeft:"10px"}}>
                                                            <div className="font-w600 font-size-h4 mb-5">
                                                                <FontAwesomeIcon icon={faCircle} style={{fontSize:"18px", marginLeft:"-25px", marginTop:"5px", position:"absolute", color:"orange"}} />Shopping Abgeschlossen
                                                            </div>
                                                            <div className="font-size-sm" style={{color:"#fff"}}>
                                                                Ihre Artikel sind verpackt und temperaturgesteuert bis zur Auslieferung.
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div style={{borderLeft: "2px solid #E0E0E0", marginLeft: "18px", height: "100px", position: "absolute"}}></div>


                                                <div className="font-w600 font-size-h4" style={{color:"#DFDFDF", marginLeft:"25px"}}>
                                                    <div style={{padding:"20px 10px"}}><FontAwesomeIcon icon={faCircle} style={{fontSize:"9px", marginLeft:"-21px", marginTop:"10px", position:"absolute"}} />Lieferung</div>
                                                    <div style={{padding:"20px 10px"}}><FontAwesomeIcon icon={faCircle} style={{fontSize:"9px", marginLeft:"-21px", marginTop:"10px", position:"absolute"}} />Fertig</div>
                                                </div>

                                                <div style={{background:"#F7F7F7", height:"20px"}}></div>
                                            </div>
                                        </div>
                                    </React.Fragment>
                                )}

                                {/* delivery */}
                                {(supermarket_orders.orderstatus_id === 2 && this.state.paid && supermarket_orders.startShoppingDate &&  this.makeTotalQuantity(supermarket_orders.items, "todo") == 0 && supermarket_orders.startNavigateDate) && (
                                    <React.Fragment>
                                        <div className="row">
                                            <div className="col-md-12">

                                                <div className="block-link-shadow" style={{color:localStorage.getItem("storeColor")}}>

                                                    <div style={{borderLeft: "2px solid rgb(252, 128, 25)", marginLeft: "18px", height: "74px", position: "absolute", marginTop:"10px"}}></div>

                                                    <div className="block-content block-content-full clearfix py-0">
                                                        <div style={{padding:"10px", paddingTop:0, marginLeft:"10px"}}>
                                                            <div className="font-w600 font-size-h4 mb-5">
                                                                <FontAwesomeIcon icon={faCircle} style={{fontSize:"18px", marginLeft:"-25px", marginTop:"5px", position:"absolute"}} />Shopping
                                                            </div>
                                                        </div>
                                                        <div style={{padding:"10px", paddingTop:0, marginLeft:"10px"}}>
                                                            <div className="font-w600 font-size-h4 mb-5">
                                                                <FontAwesomeIcon icon={faCircle} style={{fontSize:"18px", marginLeft:"-25px", marginTop:"5px", position:"absolute"}} />Shopping Abgeschlossen
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="block-link-shadow" style={{background:localStorage.getItem("storeColor"), color:"#fff"}}>

                                                    <div style={{borderLeft: "2px solid orange", marginLeft: "18px", height: "140px", position: "absolute"}}></div>

                                                    <div className="block-content block-content-full clearfix py-0">
                                                        <div style={{textAlign:"center"}}>
                                                            <img
                                                                src="https://lieferservice123.com/assets/img/order-onway.gif"
                                                                className="img-fluid img-avatar"
                                                            />
                                                        </div>
                                                        <div style={{padding:"10px 10px 15px", marginLeft:"10px"}}>
                                                            <div className="font-w600 font-size-h4 mb-5">
                                                                <FontAwesomeIcon icon={faCircle} style={{fontSize:"18px", marginLeft:"-25px", marginTop:"5px", position:"absolute", color:"orange"}} />Lieferung
                                                            </div>
                                                            <div className="font-size-sm" style={{color:"#fff"}}>
                                                                Ihr Shopper {supermarket_orders.acceptedName && this.uppercase(supermarket_orders.acceptedName)} ist unterwegs.
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div style={{borderLeft: "2px solid #E0E0E0", marginLeft: "18px", height: "35px", position: "absolute"}}></div>

                                                <div className="font-w600 font-size-h4" style={{color:"#DFDFDF", marginLeft:"25px"}}>
                                                    <div style={{padding:"20px 10px"}}><FontAwesomeIcon icon={faCircle} style={{fontSize:"9px", marginLeft:"-21px", marginTop:"10px", position:"absolute"}} />Fertig</div>
                                                </div>

                                                <div style={{background:"#F7F7F7", height:"20px"}}></div>
                                            </div>
                                        </div>
                                    </React.Fragment>
                                )}

                                {/* complete */}
                                {(supermarket_orders.orderstatus_id === 3 && this.state.paid && supermarket_orders.startShoppingDate &&  this.makeTotalQuantity(supermarket_orders.items, "todo") == 0 && supermarket_orders.startNavigateDate && supermarket_orders.markDeliveredDate) && (
                                    <React.Fragment>
                                        <div className="row">
                                            <div className="col-md-12">

                                                <div className="block-link-shadow" style={{background:"#62C94B", color:"#fff"}}>

                                                    <div style={{width:"100%", background:"#000", textAlign:"center"}}>
                                                        {supermarket_orders.leftAtDoor ? (
                                                            <img
                                                                src={'https://lieferservice123.com/'+supermarket_orders.leftAtDoor.replace("/var/www/", "")}
                                                                className="img-fluid" 
                                                                // style={{maxHeight:"250px"}}
                                                            />
                                                        ) : null}
                                                    </div>

                                                    <div className="block-content block-content-full clearfix py-0" style={{textAlign:"center"}}>
                                                        {supermarket_orders.leftAtDoor == null ? (
                                                            <img
                                                            src="https://lieferservice123.com/assets/img/order-complete.jpg"
                                                            className="img-fluid img-avatar" style={{width:"100px"}}
                                                        />
                                                        ) : null}
                                                        <div style={{padding:"10px 10px 15px", marginLeft:"10px"}}>
                                                            <div className="font-w600 font-size-h4 mb-5">
                                                                Bestellung Abgeschlossen
                                                            </div>
                                                            <div className="font-size-sm" style={{color:"#fff"}}>
                                                                Danke fürs Shoppen bei Lieferservice123!
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div style={{background:"#F7F7F7", height:"20px"}}></div>
                                            </div>
                                        </div>
                                    </React.Fragment>
                                )}
                                
                                {/* delivery time */}
                                {supermarket_orders.markDeliveredDate ? (
                                    <div style={{borderBottom:"1px solid #F5F5F5", padding:"15px 12px", fontWeight:600, fontFamily:"open sans", overflow:"auto"}}>
                                        <div style={{float:"left", width:"30px"}}><FontAwesomeIcon icon={faClock} style={{height: "18px", marginRight:"15px", float:"left", fontSize:"20px", color:"#757575"}} /></div>
                                        <div style={{float:"left", width:"calc(100% - 30px)"}}>Geliefert: {this.showDate(supermarket_orders.markDeliveredDate)}  {this.showTime(supermarket_orders.markDeliveredDate)}</div>
                                    </div>
                                ) : (
                                    <div style={{borderBottom:"1px solid #F5F5F5", padding:"15px 12px", fontWeight:600, fontFamily:"open sans", overflow:"auto"}}>
                                        <div style={{float:"left", width:"30px"}}><FontAwesomeIcon icon={faClock} style={{height: "18px", marginRight:"15px", float:"left", fontSize:"20px", color:"#757575"}} /></div>
                                        <div style={{float:"left", width:"calc(100% - 30px)"}}>Lieferung heute um {this.showTime(supermarket_orders.added)} - {this.showTime(supermarket_orders.added+3600)}</div>
                                    </div>
                                )}
                                
                                <div style={{borderBottom:"1px solid #F5F5F5", padding:"5px 12px", fontWeight:600, fontFamily:"open sans", overflow:"auto"}}>
                                    
                                    {/* delivery location */}
                                    <div style={{float:"left", width:"30px"}}><FontAwesomeIcon icon={faMapMarkerAlt} style={{height:"30px", margin:"0 17px 0 2px", fontSize:"20px", color:"#757575"}} /></div>
                                    
                                    <div style={{float:"left", width:"calc(100% - 30px)"}}>
                                        {this.getAddress(supermarket_orders.fromAddress)}<br/>
                                        
                                        <div style={{color:"#A5A5A5", fontSize:"12px"}}>{this.getAddress(supermarket_orders.fromAddress, "plz")}</div>
                                    
                                        {/* delivery instructions */}
                                        {supermarket_orders.delivery_instructions ? (
                                            <div style={{color:"#A5A5A5", fontSize:"12px"}}><span style={{}}>Anweisungen:</span> <i>{supermarket_orders.delivery_instructions}</i></div>
                                        ) : supermarket_orders.leaveAtDoorChecked ? (
                                            <div style={{color:"#A5A5A5", fontSize:"12px"}}><span style={{}}>Anweisungen:</span> <i>An meiner Tür lassen, falls ich nicht da bin</i></div>
                                        ) : null}
                                    </div>

                                </div>

                                <div style={{background:"#F7F7F7", height:"20px"}}></div>

                                {/* items */}
                                <div style={{borderTop:"1px solid #F5F5F5", borderBottom:"1px solid #F5F5F5", padding:"15px 12px", fontWeight:600, fontFamily:"open sans", overflow:"auto"}}>
                                    <div style={{float:"left", width:"30px"}}><FontAwesomeIcon icon={faShoppingBag} style={{margin:"0 5px", float:"left", fontSize:"16px", color:"#757575"}} /></div>
                                    <div style={{float:"left", width:"calc(100% - 30px)"}}>{supermarket_orders.items.length} {supermarket_orders.items.length==1?"Produkt":"Produkte"}</div>
                                    <div style={{marginTop:"10px", width:"100%", float:"left"}}>{this.itemsPic(supermarket_orders)}</div>
                                </div>

                                <div style={{background:"#F7F7F7", height:"20px"}}></div>

                                {/* payment method, subtotal, trinkgeld, service fee, total */}
                                <div style={{borderBottom:"1px solid #F5F5F5", padding:"15px 12px", fontWeight:600, fontFamily:"open sans", overflow:"auto"}}>
                                    <div style={{overflow:"auto"}}>
                                        <div style={{float:"left", width:"30px"}}><FontAwesomeIcon icon={faCreditCard} style={{height:"20px", float:"left", fontSize:"16px", color:"#757575"}} /> </div>
                                        <div style={{float:"left", width:"calc(100% - 30px)"}}>Kontonummer endet in {this.state.iban_last4}</div>
                                    </div>
                                    {/* <hr/> */}
                                    <ul style={{listStyle:"none", margin:0, padding:0, marginTop:"10px", width:"100%", float:"left"}}>
                                        {this.items(supermarket_orders, "RunOrd")}
                                    </ul>
                                </div>

                                {(supermarket_orders.orderstatus_id === 2 && this.state.paid && supermarket_orders.acceptedName) ? (
                                    <div style={{background:"#F7F7F7", padding:"18px", textAlign:"center"}}>
                                        
                                        <div onClick={() => this.sendSMS(supermarket_orders.fromTel)} style={{display:"block", border:"1px solid orange", color:localStorage.getItem("storeColor"), padding:"10px", fontWeight:"bold", fontFamily:"open sans", borderRadius:"5px", marginBottom:"20px", cursor:"pointer"}}><FontAwesomeIcon icon={faPhoneAlt} style={{marginRight:"5px"}} /> {this.uppercase(supermarket_orders.acceptedName)} anrufen</div>
                                        
                                        <a href={'tel:'+supermarket_orders.fromTel} style={{display:"block", border:"1px solid orange", color:localStorage.getItem("storeColor"), padding:"10px", fontWeight:"bold", fontFamily:"open sans", borderRadius:"5px"}}><FontAwesomeIcon icon={faComment} style={{marginRight:"5px"}} /> Nachricht senden</a>

                                    </div>  
                                ) : supermarket_orders.orderstatus_id === 3 ? (
                                    <div style={{background:"#F7F7F7", padding:"18px", textAlign:"center"}}>

                                        <div onClick={() => this.showModalViewReceipt()} style={{cursor:"pointer", position:"relative", display:"block", border:"1px solid orange", color:localStorage.getItem("storeColor"), padding:"10px", fontWeight:"bold", fontFamily:"open sans", borderRadius:"5px", marginBottom:"20px"}}>Rechnung anzeigen<Ink duration="500" /></div>

                                        {/* view receipt */}
                                        <Modal show={this.state.ViewReceiptModal} onHide={this.hideModalViewReceipt} style={{background:"#333"}}>
                                            <Modal.Header closeButton style={{paddingBottom:0}}>
                                                <Modal.Title style={{width:"100%", height:"50px"}}>
                                                    <div style={{fontFamily:"open sans", fontWeight:600, textAlign:"center", color:localStorage.getItem("storeColor")}}>Deine Rechnung</div>
                                                </Modal.Title>
                                            </Modal.Header>
                                            <Modal.Body style={{padding:0}}>

                                                <div style={{fontSize:"26px", padding:"0 10px", textAlign:"center"}}>{supermarket_orders.acceptedName ? this.uppercase(supermarket_orders.acceptedName) : "Ihr Shopper"} hat Ihre Bestellung geliefert</div>

                                                <div style={{marginTop:"20px", padding:"0 10px", textAlign:"center"}}>Ihre Bestellung wurde am {this.showDate(supermarket_orders.markDeliveredDate)} aufgegeben und am {this.showDate(supermarket_orders.markDeliveredDate)} um {this.showTime(supermarket_orders.markDeliveredDate)} Uhr von {supermarket_orders.shopName} geliefert</div>

                                                {/* <a style={{display:"block", cursor:"pointer", position:"relative", padding:"10px", textAlign:"center", color:localStorage.getItem("storeColor"), fontWeight:"bold", fontFamily:"open sans"}} onClick={() => this.showModalTip()}>Trinkgeld ändern<Ink duration="500" /></a> */}

                                                <ViewReceipt supermarket_orders={supermarket_orders} />

                                            </Modal.Body>
                                        </Modal>
                                        
                                        {/* show only if not older then 3 days */}
                                        {((Date.now()/1000) - supermarket_orders.markDeliveredDate) > 60 * 60 * 24 * 3 ? (
                                            <a style={{cursor:"pointer", position:"relative", display:"block", border:"1px solid orange", color:localStorage.getItem("storeColor"), padding:"10px", fontWeight:"bold", fontFamily:"open sans", borderRadius:"5px", marginBottom:"20px"}} onClick={() => alert("Sie können das Trinkgeld nur innerhalb von 3 Tagen nach der Lieferung ändern. Die Frist ist leider abgelaufen.")}>Trinkgeld ändern<Ink duration="500" /></a>
                                        ) : (
                                            <a style={{cursor:"pointer", position:"relative", display:"block", border:"1px solid orange", color:localStorage.getItem("storeColor"), padding:"10px", fontWeight:"bold", fontFamily:"open sans", borderRadius:"5px", marginBottom:"20px"}} onClick={() => this.showModalTip()}>Trinkgeld ändern<Ink duration="500" /></a>
                                        )}

                                        {/* change tip */}
                                        <Modal show={this.state.TipModal} onHide={this.hideModalTip} style={{background:"#fff"}}>
                                            <Modal.Header closeButton>
                                                <Modal.Title>Kontaktloses Trinkgeld<div style={{fontWeight:300, fontFamily:"open sans", fontSize:"12px"}}>Geben Sie Ihrem Zusteller ein Trinkgeld</div></Modal.Title>
                                            </Modal.Header>
                                            <Modal.Body style={{padding:"15px"}}>
                                                <div style={{marginTop:"20px", marginLeft:"15px", fontSize:"12px"}}>
                                                    <li style={{margin:"3px 0"}}>100% der Trinkgelder gehen an selbstständige Zusteller.</li>
                                                    <li style={{margin:"3px 0"}}>Trinkgelder sind immer optional.</li>
                                                    <li style={{margin:"3px 0"}}>Sie können ihr Trinkgeld nach der Lieferung ändern.</li>
                                                </div>

                                                <div style={{marginTop:"30px", fontSize:"16px"}}>
                                                    <label className="payment_func" style={{border:"none"}}>
                                                        0% ({supermarket_orders.currencyFormat}{(supermarket_orders.subtotal*0).toFixed(2)})
                                                        <input type="radio" name="tip" value="0" onChange={this.onTipChanged} checked={this.state.delivery_tip == 0?true:false} /> 
                                                        <span className="checkmark" style={{left:"5px"}}></span>
                                                        <img src="/assets/img/cash.svg" style={{position: "absolute", right: "15px", height: "25px", width: "25px"}} />
                                                        <Ink duration="500" />
                                                    </label>

                                                    <label className="payment_func">
                                                        5% ({supermarket_orders.currencyFormat}{(supermarket_orders.subtotal*0.05).toFixed(2)})
                                                        <input type="radio" name="tip" value="5" onChange={this.onTipChanged} checked={this.state.delivery_tip == 5?true:false} /> 
                                                        <span className="checkmark" style={{left:"5px"}}></span>
                                                        <img src="/assets/img/cash.svg" style={{position: "absolute", right: "15px", height: "25px", width: "25px"}} />
                                                        <Ink duration="500" />
                                                    </label>

                                                    <label className="payment_func">
                                                        10% ({supermarket_orders.currencyFormat}{(supermarket_orders.subtotal*0.10).toFixed(2)})
                                                        <input type="radio" name="tip" value="10" onChange={this.onTipChanged} checked={this.state.delivery_tip == 10?true:false} /> 
                                                        <span className="checkmark" style={{left:"5px"}}></span>
                                                        <img src="/assets/img/cash.svg" style={{position: "absolute", right: "15px", height: "25px", width: "25px"}} />
                                                        <Ink duration="500" />
                                                    </label>

                                                    <label className="payment_func">
                                                        15% ({supermarket_orders.currencyFormat}{(supermarket_orders.subtotal*0.15).toFixed(2)})
                                                        <input type="radio" name="tip" value="15" onChange={this.onTipChanged} checked={this.state.delivery_tip == 15?true:false} /> 
                                                        <span className="checkmark" style={{left:"5px"}}></span>
                                                        <img src="/assets/img/cash.svg" style={{position: "absolute", right: "15px", height: "25px", width: "25px"}} />
                                                        <Ink duration="500" />
                                                    </label>

                                                    <label className="payment_func" style={{border:"none"}}>
                                                        20% ({supermarket_orders.currencyFormat}{(supermarket_orders.subtotal*0.20).toFixed(2)})
                                                        <input type="radio" name="tip" value="20" onChange={this.onTipChanged} checked={this.state.delivery_tip == 20?true:false} /> 
                                                        <span className="checkmark" style={{left:"5px"}}></span>
                                                        <img src="/assets/img/cash.svg" style={{position: "absolute", right: "15px", height: "25px", width: "25px"}} />
                                                        <Ink duration="500" />
                                                    </label>
                                                </div>

                                                <div onClick={() => this.showModalViewReceipt()} style={{cursor:"pointer", position:"relative", display:"block", color:localStorage.getItem("storeColor"), padding:"10px", fontWeight:"bold", fontFamily:"open sans", marginBottom:"20px", textAlign:"center"}}>Rechnung anzeigen<Ink duration="500" /></div>

                                                <div style={{marginTop:"20px"}}>
                                                    <button
                                                        onClick={() => {
                                                            this.hideModalTip();
                                                            this.changeTip(supermarket_orders.id);
                                                        }}
                                                        type="button"
                                                        className="btn-save-address"
                                                        style={{textTransform:"unset", backgroundColor: localStorage.getItem("storeColor")}}
                                                    >
                                                        Trinkgeld speichern
                                                        <Ink duration="500" />
                                                    </button>
                                                </div>
                                            </Modal.Body>
                                        </Modal>

                                        <div onClick={() => this.addToCart(supermarket_orders.shop_id)} style={{cursor:"pointer", position:"relative", display:"block", border:"1px solid orange", color:localStorage.getItem("storeColor"), padding:"10px", fontWeight:"bold", fontFamily:"open sans", borderRadius:"5px"}}>Erneut bestellen<Ink duration="500" /></div>

                                    </div>  
                                ) : (
                                    <div style={{background:"#F7F7F7", height:"20px"}}></div>
                                )}
                
                                {/* speichern als 1-Klick Bestellung */}
                                {/* <div style={{padding:"0 15px", textAlign:"center"}}>
                                    <Button variant="primary">
                                        <a href={"https://app.lieferservice123.com/?install-voice=1&id="+supermarket_orders.id+"&code="+supermarket_orders.code+"&name=Bestellung "+supermarket_orders.id+""}>Speichern als 1-Klick Bestellung</a>
                                    </Button>
                                </div> */}

                            </div>
                        
                        ) : (
                        
                            <h2 style={{textAlign:"center", fontFamily:"open sans", fontWeight:300}}>Bestätigung wird geladen...</h2>
                        )}

                    </div>

                    {/* refresh status */}
                    {/* <div>
                        <button
                            className="btn btn-lg btn-refresh-status"
                            ref="refreshButton"
                            onClick={this.__refreshOrderStatus}
                            style={{
                                backgroundColor: localStorage.getItem("cartColorBg"),
                                color: localStorage.getItem("cartColorText")
                            }}
                        >
                            {localStorage.getItem("runningOrderRefreshButton")}{" "}
                            <span ref="btnSpinner" className="hidden">
                                <i className="fa fa-refresh fa-spin" />
                            </span>
                        </button>
                    </div> */}

                </React.Fragment>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user.user,
    cartProducts: state.cart.products,
});

export default connect(
    mapStateToProps,
)(RunOrd);
