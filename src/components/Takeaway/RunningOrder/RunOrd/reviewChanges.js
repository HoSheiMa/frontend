import { SUPERMARKET_ITEMS_URL } from "../../../../configs/index";

import React, { Component } from "react";
import { connect } from "react-redux";
import Ink from "react-ink";
import axios from "axios";

import { Modal, Button } from 'react-bootstrap';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faRedo } from '@fortawesome/free-solid-svg-icons';
import { faCaretRight } from '@fortawesome/free-solid-svg-icons';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { faBarcode } from '@fortawesome/free-solid-svg-icons';
import { faWindowClose } from '@fortawesome/free-solid-svg-icons';
import { faSync } from '@fortawesome/free-solid-svg-icons';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons';
import { faNotEqual } from '@fortawesome/free-solid-svg-icons';

import ReactTooltip from 'react-tooltip'
import ReactDOM from 'react-dom';

// import FoundItem from './foundItem';
// import FoundReplacement from './foundReplacement';

// import BarcodeReader from 'react-barcode-reader'
import BarcodeScannerComponent from "react-webcam-barcode-scanner";


// search shop
import Suggestions from '../../Items/ItemList/Suggestions'


class ReviewChanges extends Component {

    constructor(props){
        super(props);

        this.state = {
            test: [],
            ZoomModal: false,
            FoundItemModal: false,
            AnotherOptionsModal: false,
            RefundItemModal: false,
            ScanItemModal: false,
            ScanReplacementModal: false,
            SearchReplacementModal: false,
            FoundReplacementModal: false,
            barcode: 'No result',

            
            // search shop
            searchQuery: '',
            serchResults: [],
            selectedCategory: '',
            subCategoryList: [],
            // shopName: 'Billa',
        }
    }

    showModalZoom (id) {
        this.setState({
          ZoomModal: {
            [id]: true
          }
        });
    }
    hideModalZoom = () => {
        this.setState({
            ZoomModal: false
        });
    }


    showModalFoundItem (id) {
        this.setState({
            FoundItemModal: {
            [id]: true
          }
        });
    }
    hideModalFoundItem = () => {
        this.setState({
            FoundItemModal: false
        });
    }

    showModalAnotherOptions (id) {
        this.setState({
            AnotherOptionsModal: {
            [id]: true
          }
        });
    }
    hideModalAnotherOptions = () => {
        this.setState({
            AnotherOptionsModal: false
        });
    }

    showModalRefundItem (id) {
        this.setState({
            RefundItemModal: {
            [id]: true
          }
        });
    }
    hideModalRefundItem = () => {
        this.setState({
            RefundItemModal: false
        });
    }

    showModalScanItem (id) {
        this.setState({
            ScanItemModal: {
            [id]: true
          }
        });
    }
    hideModalScanItem = () => {
        this.setState({
            ScanItemModal: false
        });
    }

    showModalScanReplacement (id) {
        this.setState({
            ScanReplacementModal: {
            [id]: true
          }
        });
    }
    hideModalScanReplacement = () => {
        this.setState({
            ScanReplacementModal: false
        });
    }

    showModalSearchReplacement (id) {
        this.setState({
            SearchReplacementModal: {
            [id]: true
          }
        });
    }
    hideModalSearchReplacement = () => {
        this.setState({
            SearchReplacementModal: false
        });
    }

    showModalFoundReplacement (id, replaceWith) {
        this.setState({
            FoundReplacementModal: {
            [id]: true
          }
        });

        this.setState({
          replaceWith: replaceWith
        });
    }
    hideModalFoundReplacement = () => {
        this.setState({
            FoundReplacementModal: false
        });
    }


    async getName(id){
        const name = await axios
            .post(SUPERMARKET_ITEMS_URL, {
                id: id
            })
            .then(response => {
                return response.data.name;
            })
            .catch(function(error) {
                console.log(error);
                return "";
            });
        console.log('name', name);
        return name;
    }

    refundItem = (supermarket_orders_id, id, quantity) => {

        var url = '//'+window.location.hostname+'/php/shopperPwa_foundItem.php?supermarket_orders_id='+supermarket_orders_id+'&id='+id+'&quantity='+quantity+'&found=0';
        axios
        .get(url)
        .then(response => {
        })
        .catch(function(error) {
            console.log(error);
        });

        setTimeout(function () {
            var url = window.location.href;
            window.location = url;
        }, 100);
    
    }
  
    getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1));
        // console.log(sPageURL);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam) {
                return sParameterName[1];
            }
        }
    }


    makeTotalQuantity = (data = []) => {
        let sum = 0;
        data.forEach((row) => {

            if(row.found != null){
                sum = sum + 1
            }
        });
        return sum;
    }

    // scan product
    handleScan(data){
        this.setState({
            barcode: data,
        })
    }
    handleError(err){
        console.error(err)
    }

    country_formatted_tel = (fromTel) => {
        var fromTel = fromTel.trim();
        var first2 = fromTel.substr(0, 2);
        if(first2 == localStorage.getItem('phoneCountryCode')) {
            var fromTel = "+".fromTel;
            return fromTel;
        }
    
        var firstLetter = fromTel.substr(0, 1);
        if(firstLetter == 0) {
            var fromTel = "+"+localStorage.getItem('phoneCountryCode')+fromTel.substr(1);
            return fromTel;
        }
    
        return fromTel;
    }
      
    addBarcode = (id, barcode, shop) => {
        var url = '//'+window.location.hostname+'/php/shopperPwa_addBarcode.php?id='+id+'&fromTel='+btoa(this.country_formatted_tel(localStorage.getItem("fromTel")))+'&code='+localStorage.getItem("codeCustomer")+'&barcode='+barcode+'&shop='+shop;
        // var redirect = '/viewBatch/'+id+'?act=startShopping&tab=todo';  

        axios
        .get(url)
        .then(response => {
        })
        .catch(function(error) {
            console.log(error);
        });

        // setTimeout(function () {
        //     window.location = redirect;
        // }, 100);
    }
      
    scanReplacement = (id, barcode) => {
        var url = '//'+window.location.hostname+'/php/shopperPwa_scanReplacement.php?id='+id+'&fromTel='+btoa(this.country_formatted_tel(localStorage.getItem("fromTel")))+'&code='+localStorage.getItem("codeCustomer")+'&barcode='+barcode;
        // var redirect = '/viewBatch/'+id+'?act=startShopping&tab=todo';  

        axios
        .get(url)
        .then(response => {   
            // console.log("id", id);
            // console.log("response.data", response.data);
            this.showModalFoundReplacement(id, response.data);
            
        })
        .catch(function(error) {
            console.log(error);
        });

        // setTimeout(function () {
        //     window.location = redirect;
        // }, 100);
    }

    foundItem = (supermarket_orders_id, id) => {

        var url = '//'+window.location.hostname+'/php/shopperPwa_foundItem.php?supermarket_orders_id='+supermarket_orders_id+'&id='+id+'&quantity=1&found=1';
        axios
        .get(url)
        .then(response => {
        })
        .catch(function(error) {
            console.log(error);
        });

        setTimeout(function () {
            var url = window.location.href;
            window.location = url;
        }, 100);
    
    }

    showReplacementOptions = () =>{
        document.getElementById('replacement_options').style.display = "block";
    }

    //search shop
    onSearchChange = () => {
        document.getElementById('suggestionsBox').style.display = "block";
        document.getElementById('closeSuggestionsIcon').style.display = "block";
       
        this.setState({
            searchQuery: this.searchAjax.value,
            // remove this to not reset the category on search product.
            selectedCategory: '',
        }, () => {
          if (this.state.searchQuery && this.state.searchQuery.length > 1) {
                // if (this.state.searchQuery.length % 2 === 0) {
                    console.log(this.state.currentSearchType);
                    const selectedCategory = localStorage.getItem("selectedCategory");
                    const sub_category = localStorage.getItem("sub_category");
                    const sub_category2 = localStorage.getItem("sub_category2");

                    if (sub_category2) {
                        this.getSubCategoryInfo('sub_category2', `&sub_category2=${encodeURIComponent(sub_category2) || ''}&sub_cat=${encodeURIComponent(sub_category) || ''}`);
                    } else if (sub_category) {
                        this.getSubCategoryInfo('sub_category2', `&sub_cat=${encodeURIComponent(sub_category) || ''}`);
                    } else if (selectedCategory) {
                        this.getSubCategoryInfo('subcat', '');
                    } else {
                        this.getSearchInfoCatRequestToServer(false);
                    }
                    this.getSearchInfo();
                // }
          } else if (!this.state.searchQuery) {
              console.log("resetSearchInputStates");
              this.setState({
                selectedCategory: '',
                searchResults: [],
                subCategoryList: [],
                searchQuery: '',
                currentSearchType: 'cat'
            });
    
            localStorage.removeItem("selectedCategory");
            localStorage.removeItem("sub_category");
            localStorage.removeItem("sub_category2");
          }
        })
    }
    getSearchInfo = () => {
        var url = `//${window.location.hostname}/php/twilio/${this.state.firstScreen_einkaufSearch}.php?act=&q=${encodeURIComponent(this.state.searchQuery)}&country=${localStorage.getItem("country")}&shop=${this.state.shopName}&restaurant_id=${this.state.restaurant_id}`;
        const selectedCat = localStorage.getItem("selectedCategory");
        if (selectedCat) {

            if (this.state.currentSearchType === 'cat') {

                // act=&cat=${encodeURIComponent(selectedCat)}&sub_cat=${encodeURIComponent(this.state.selectedSubCategory)}&
                url = `${url}&cat=${encodeURIComponent(selectedCat)}`;
            } else {

                url = `${url}&cat=${encodeURIComponent(selectedCat)}&${this.state.currentSearchType}=${encodeURIComponent(this.state.selectedSubCategory)}`;
            }
        }
        axios.get(url)
          .then(( res ) => {
            this.setState({
                searchResults: res.data
            })
          })
    }
    getSearchInfoCatRequestToServer = (showProductsOnInit) => {
        //home cat
        var url = '//'+window.location.hostname+'/php/twilio/'+this.state.firstScreen_einkaufSearch+'.php?q='+encodeURIComponent(this.state.searchQuery || "")+'&act=cat&country='+localStorage.getItem("country")+'&shop='+this.state.shopName+'&restaurant_id='+this.state.restaurant_id;
        axios.get(url)
          .then(( res ) => {
            this.setState({
                catSearchResults: res.data,
                no_sub_cat_results: false
            });
            if (showProductsOnInit) {
                this.getSearchInfo();
            }

            if (!res.data.length) {
                this.setState({
                    no_sub_cat_results: true
                });
            }
          })
    }
    getSearchInfoCat = (firstClicked, showProductsOnInit) => {
        console.log("firstSelectInput", firstClicked);

        if(firstClicked === false){

            if (document.getElementById('suggestionsBox')) {
                document.getElementById('suggestionsBox').style.display = "block";
                document.getElementById('closeSuggestionsIcon').style.display = "block";
            }
    
            localStorage.removeItem("selectedCategory");
            localStorage.removeItem("sub_category");
            localStorage.removeItem("sub_category2");
            localStorage.setItem("firstSelectInput", true);
    
            this.setState({
                subCategoryList: [],
                selectedCategory: '',
                selectedSubCategory: '',
                currentSearchType: 'cat'
            });
            this.getSearchInfoCatRequestToServer(showProductsOnInit);
        } else {
            //nothing
            console.log("dont hide suggestions box");
        }
    }
    getSubCategoryInfo = (actType, extraParams = '') => {
        // get sub scteogry..
        const selectedCategory = localStorage.getItem("selectedCategory");
        var url = url = `//${window.location.hostname}/php/twilio/${this.state.firstScreen_einkaufSearch}.php?q=${encodeURIComponent(this.state.searchQuery)}&act=${actType}${extraParams}&cat=${encodeURIComponent(selectedCategory)}&country=${localStorage.getItem("country")}&shop=${this.state.shopName}&restaurant_id=${this.state.restaurant_id}`;
        
        axios.get(url)
          .then(( res ) => {
            if (res.data.length) {
                this.setState({
                    subCategoryList: res.data,
                    no_sub_cat_results: false
                });
            } else {
                this.setState({
                    no_sub_cat_results: true,
                    subCategoryList: []
                });
            }
          }).catch(err => {
              console.error(err);
          });
    }
    suggestion2OptionClicked = (item, type, keyType) => {
        // console.log(type);
        if (type === 'category') {// when category item is clicked
            localStorage.removeItem('sub_category');
            this.setState({
                currentSearchType: 'cat',
                selectedSubCategory: '',
                selectedCategory: item.category
            });
            setTimeout(() => {
                this.getSearchInfo();
                this.getSubCategoryInfo('subcat');
            }, 200);
            localStorage.setItem("selectedCategory", item.category);
            localStorage.setItem("firstSelectInput", true);
            
        } else if (type === 'subCategory') {
            
            localStorage.setItem(keyType, item[keyType]);

            // subCategoryList
            this.setState({
                selectedSubCategory: item[keyType],
                currentSearchType: keyType
            });
            setTimeout(() => {
                this.getSearchInfo();
                if (keyType === 'sub_category') {
                    this.getSubCategoryInfo('sub_category2', `&sub_category2=${encodeURIComponent(item.sub_category) || ''}&sub_cat=${encodeURIComponent(this.state.selectedSubCategory) || ''}`);
                }
            }, 200)
        } else {// when product item is clicked
            this.addButtonClicked(item);
        }
    };
    resetSearchInputStates = () => {
        this.setState({
            selectedCategory: '',
            catSearchResults: [],
            searchResults: [],
            subCategoryList: [],
            searchQuery: '',
            currentSearchType: 'cat',
            no_sub_cat_results: false

        });

        localStorage.removeItem("selectedCategory");
        localStorage.removeItem("sub_category");
        localStorage.removeItem("sub_category2");
    
    };
    closeSuggestionsBox = () =>{
        console.log("we resettning closeSuggestionsBox");

        document.getElementById('suggestionsBox').style.display = "none";
        document.getElementById('closeSuggestionsIcon').style.display = "none";
        this.searchAjax.value = ''; //clear the value of the element
        this.searchAjax.focus(); //sets focus to element

        this.resetSearchInputStates();

        localStorage.removeItem("firstSelectInput");
    }
    addButtonClicked = (item)=>{
        if (item.addon_categories && item.addon_categories.length) {
            this.setState({ popupOpenItem: item.id });
        } else {
            this.props.addProduct(item);
            this.forceStateUpdate();
        }
        
    };
    forceStateUpdate = () => {
        setTimeout(() => {
            this.forceUpdate();
            if (this.state.update) {
                this.setState({ update: false });
            } else {
                this.setState({ update: true });
            }
        }, 100);
    };
    addReplacement = (replaceWith, id) => {
        // console.log("replaceWith", replaceWith);
        this.showModalFoundReplacement(id, replaceWith);
    };
    setShopName = (shopName, restaurant_id, cat) => {
        this.setState({
            shopName: shopName
        });
        this.setState({
            restaurant_id: restaurant_id
        });
        if(cat == "restaurant"){
            this.setState({
                firstScreen_einkaufSearch: 'FirstScreen_einkaufSearchRestaurant'
            });
        } else {
            this.setState({
                firstScreen_einkaufSearch: 'FirstScreen_einkaufSearch'
            });
        }
    }

    request = (supermarket_orders_id, id, quantity, replacement, found_replacement, act) => {
        document.getElementById('approve'+id).style.display = "none";
        document.getElementById('approved'+id).style.display = "block";

        console.log('replace {"id":"'+id+'","quantity":'+quantity+',"found":0,"replacement":'+replacement+',"found_replacement":'+found_replacement+'} with {"id":"'+id+'","quantity":'+quantity+',"found":0,"replacement":'+replacement+',"found_replacement":'+found_replacement+',"approved":1}');

        var url = '//'+window.location.hostname+'/php/shopperPwa_confirmReplacement.php?supermarket_orders_id='+supermarket_orders_id+'&id='+id+'&quantity='+quantity+'&replacement='+replacement+'&found_replacement='+found_replacement+'&act='+act;
        axios
        .get(url)
        .then(response => {
        })
        .catch(function(error) {
            console.log(error);
        });

        setTimeout(function () {
            var url = window.location.href;
            window.location = url;
        }, 100);
    }

    render() {
        const { supermarket_orders } = this.props;
        let sub_total = 0;

        console.log("items", supermarket_orders.items);

        const styleReplaceWithImg = {
            width:"calc(100% - 115px)", float:"left", marginLeft:"15px"
        }
        const styleReplaceWithoutImg = {
            width:"calc(100% - 50px)", float:"left"
        }

        const styleWithImg = {
            width:"calc(100% - 115px)", float:"left", marginLeft:"15px"
        }
        const styleWithoutImg = {
            width:"calc(100% - 50px)", float:"left"
        }

        var newOrdersDataElements = (tab) => {

            var isAvailable = false;

            const ordersComponent = supermarket_orders.items.map((row, index) => {
                                
                sub_total += row.price*row.quantity;

                if(tab == null || tab == undefined)
                return;

                if(tab == 'adjustment' && row.found > 0 && row.found !== row.quantity){
                    isAvailable = true;
                    return (
                        <div>
                                
                            <div key={row.id} style={{width:"100%", float:"left", margin:"7.5px 0", padding:"15px 10px", borderTop:"1px solid #E8E8E8", background:"#fff"}}>

                                <div style={{marginBottom:"10px", color:"#51A344", fontSize:"11px", fontWeight:"bold"}}>
                                    <span style={{fontSize:"18px", fontFamily:"open sans", fontWeight:600}}><img src={'https://lieferservice123.com/assets/img/adjustment.png'} style={{marginRight:"5px", width:"25px"}} /> Anpassung</span>
                                </div>

                                {row.placeholder_image && <img onClick={this.showModalZoom.bind(this, row.id)} title={row.name} src={'https://lieferservice123.com'+row.placeholder_image} style={{float:"left", width:"40px", height:"40px"}} />}

                                <span onClick={this.showModalZoom.bind(this, row.id)} style={row.placeholder_image&&styleWithImg}>
                                    <div style={{color:"#A04362"}}>
                                        {row.found < row.quantity ? <span style={{marginRight:"5px"}}>⇣</span>:<span style={{marginRight:"5px"}}>⇡</span>} 
                                        {row.found}x (ursprünglich {row.quantity}x)
                                    </div>
                                    
                                    <div style={{fontWeight:600, fontFamily:"open sans"}}><b>{row.found}x</b> {row.name}</div>
                                
                                    {row.grammageBadge && <div style={{fontWeight:600, fontFamily:"open sans", color:"#ABABAB"}}>{row.grammageBadge}</div>}

                                </span> 

                                <span style={{float:"right", width:"50px", fontWeight:"bold", textAlign:"right"}}>
                                    <s style={{fontFamily:"open sans", fontWeight:600, fontSize:"13px"}}>€{(parseFloat(row.price)*parseFloat(row.quantity)).toFixed(2)}</s><br/>
                                    €{(parseFloat(row.price)*parseFloat(row.found)).toFixed(2)}
                                </span>
                            </div>

                            <Modal show={this.state.ZoomModal[row.id]} onHide={this.hideModalZoom} style={{background:"#333"}}>
                                <Modal.Header closeButton>
                                </Modal.Header>
                                <Modal.Body style={{padding:"0 25px 10px"}}>
                                    
                                    {row.image && <img src={"https://lieferservice123.com"+row.image} style={{width:"100%"}} alt="Bild wird geladen..." />}

                                    <div style={{width:"100%", float:"left", marginTop:"5px"}}>{row.marke}</div>
                                    <div style={{fontSize:"16px"}}><b>{row.quantity}x</b> {row.name}</div>
                    
                                    {row.grammageBadge && <div><span style={{fontSize:"12px"}}>({row.grammageBadge})</span></div>}

                                    <div style={{float:"left", width:"100%", marginTop:"15px", marginBottom:"20px"}}></div>

                                    {row.category && <div style={{width:"100%", float:"left", fontWeight:600, fontFamily:"open sans"}}>Kategorie<span style={{float:"right"}}>{row.category}</span></div>}

                                    <div style={{width:"100%", float:"left", fontWeight:600, fontFamily:"open sans"}}>Preis<span style={{float:"right"}}>€{row.price.toFixed(2)}</span></div>

                    
                                </Modal.Body>
                            </Modal>
                        </div>
                    )
                }  
                
                if(tab == 'review'){
                    isAvailable = true;
                    return (
                        <div>
                            {row.found == 0 && row.found_replacement != null ? (//replaced
                                
                                <div key={row.id} style={{width:"100%", float:"left", margin:"7.5px 0", borderTop:"1px solid #E8E8E8", borderBottom:"1px solid #E8E8E8", background:"#fff"}}>

                                    <div style={{padding:"15px 10px", overflow:"auto"}}>
                                        <div style={{marginBottom:"10px", color:"#C19E3B", fontSize:"11px", fontWeight:"bold"}}>
                                            <span style={{fontSize:"18px", fontFamily:"open sans", fontWeight:600}}><FontAwesomeIcon icon={faSync} style={{marginRight:"5px"}} /> Ersatz</span>
                                        </div>
                                    
                                        <div style={{width:"100%", float:"left"}}>
                                            {row.placeholder_image && <img onClick={this.showModalZoom.bind(this, row.id)} title={row.name} src={'https://lieferservice123.com'+row.placeholder_image} style={{float:"left", width:"40px", height:"40px"}} />}
                                            <span onClick={this.showModalZoom.bind(this, row.id)} style={row.placeholder_image ? styleReplaceWithImg : styleReplaceWithoutImg}>
                                                <div style={{color:"#A04362"}}>Ausverkauft</div>
                                                <div style={{fontWeight:600, fontFamily:"open sans"}}><b>{row.quantity}x</b> {row.name}</div>
                                                {row.grammageBadge && <div style={{fontWeight:600, fontFamily:"open sans", color:"#ABABAB"}}>{row.grammageBadge}</div>}
                                            </span> 
                                            <s style={{float:"right", width:"50px", fontWeight:"bold", textAlign:"right"}}>€{(parseFloat(row.price)*parseFloat(row.quantity)).toFixed(2)}</s>
                                        </div>

                                        <div style={{width:"100%", float:"left"}}>
                                            <div style={{padding:"15px 0", width:"100%", float:"left", fontWeight:600, fontFamily:"open sans", color:"#ABABAB"}}><FontAwesomeIcon icon={faSync} style={{marginRight:"5px"}} /> Ersetzt mit...</div>
                                            {row.replacement_placeholder_image && <img onClick={this.showModalZoom.bind(this, row.replacement_id)} title={row.replacement_name} src={'https://lieferservice123.com'+row.replacement_placeholder_image} style={{float:"left", width:"40px", height:"40px"}} />}
                                            <span onClick={this.showModalZoom.bind(this, row.replacement_id)} style={row.replacement_placeholder_image ? styleWithImg : styleWithoutImg}>
                                                <div style={{fontWeight:600, fontFamily:"open sans"}}><b>{row.found_replacement}x</b> {row.replacement_name}</div>
                                                {row.replacement_grammageBadge && <div style={{fontWeight:600, fontFamily:"open sans", color:"#ABABAB"}}>{row.replacement_grammageBadge}</div>}
                                            </span> 
                                            <span style={{float:"right", width:"50px", fontWeight:"bold", textAlign:"right"}}>€{(parseFloat(row.replacement_price)*parseFloat(row.found_replacement)).toFixed(2)}</span>
                                        </div>
                                    </div>

                                    {/* approved button */}
                                    {row.approved == 1 ? (
                                        <div style={{width:"100%", float:"left", color:"#53A235", fontWeight:"bold", padding:"10px 10px 15px", textAlign:"center"}}><FontAwesomeIcon icon={faCheck} style={{marginRight:"5px"}} /> Bestätigt</div>
                                    ) : (
                                        /* another options, approve buttons */
                                        <div id={'approve'+row.id} style={{width:"100%", float:"left"}}>
                                            <span onClick={this.showModalAnotherOptions.bind(this, row.id)} style={{width:"50%", float:"left", color:"#1A919D", padding:"10px 10px 15px", textAlign:"center", cursor:"pointer"}}>Andere Optionen...</span>
                                            <span onClick={() => this.request(supermarket_orders.id, row.id, row.quantity, row.replacement_id, row.found_replacement, "confirmReplacement")} style={{width:"50%", float:"right", color:"#1A919D", fontWeight:"bold", padding:"10px 10px 15px", textAlign:"center", cursor:"pointer"}}>Bestätigen</span>
                                        </div>
                                    )}

                                    <div id={'approved'+row.id} style={{display:"none", width:"100%", float:"left", color:"#53A235", fontWeight:"bold", padding:"10px 10px 15px", textAlign:"center"}}><FontAwesomeIcon icon={faCheck} style={{marginRight:"5px"}} /> Bestätigt</div>

                                </div>

                            ) : row.found == 0 && row.found_replacement == null ? (//refunded

                                <div key={row.id} style={{width:"100%", float:"left", margin:"7.5px 0", padding:"15px 10px", borderTop:"1px solid #E8E8E8", borderBottom:"1px solid #E8E8E8", background:"#fff"}}>

                                    <div style={{marginBottom:"10px", color:"#D55214", fontSize:"11px", fontWeight:"bold"}}>
                                        <span style={{fontSize:"18px", fontFamily:"open sans", fontWeight:600}}><FontAwesomeIcon icon={faRedo} style={{marginRight:"5px"}} /> Rückerstattet</span>
                                    </div>
                                    <div style={{width:"100%", float:"left"}}>
                                        {row.placeholder_image && <img onClick={this.showModalZoom.bind(this, row.id)} title={row.name} src={'https://lieferservice123.com'+row.placeholder_image} style={{float:"left", width:"40px", height:"40px"}} />}
                                        <span onClick={this.showModalZoom.bind(this, row.id)} style={row.placeholder_image ? styleWithImg : styleWithoutImg}>
                                            <div style={{fontWeight:600, fontFamily:"open sans"}}><b>{row.quantity}x</b> {row.name}</div>
                                            {row.grammageBadge && <div style={{fontWeight:600, fontFamily:"open sans", color:"#ABABAB"}}>{row.grammageBadge}</div>}
                                        </span> 
                                        <span style={{float:"right", width:"50px", fontWeight:"bold", textAlign:"right"}}>€{(parseFloat(row.price)*parseFloat(row.quantity)).toFixed(2)}</span>
                                    </div>

                                </div>

                            ) : null}

                            {/* show product */}
                            <Modal show={this.state.ZoomModal[row.id]} onHide={this.hideModalZoom} style={{background:"#333"}}>
                                <Modal.Header closeButton>
                                </Modal.Header>
                                <Modal.Body style={{padding:"0 25px 10px"}}>
                                    
                                    {row.image && <img src={"https://lieferservice123.com"+row.image} style={{width:"100%"}} alt="Bild wird geladen..." />}

                                    <div style={{width:"100%", float:"left", marginTop:"5px"}}>{row.marke}</div>
                                    <div style={{fontSize:"16px"}}><b>{row.quantity}x</b> {row.name}</div>
                    
                                    {row.grammageBadge && <div><span style={{fontSize:"12px"}}>({row.grammageBadge})</span></div>}

                                    <div style={{float:"left", width:"100%", marginTop:"15px", marginBottom:"20px"}}></div>

                                    {row.category && <div style={{width:"100%", float:"left", fontWeight:600, fontFamily:"open sans"}}>Kategorie<span style={{float:"right"}}>{row.category}</span></div>}

                                    <div style={{width:"100%", float:"left", fontWeight:600, fontFamily:"open sans"}}>Preis<span style={{float:"right"}}>€{row.price.toFixed(2)}</span></div>

                    
                                </Modal.Body>
                            </Modal>

                            {/* show replacement */}
                            <Modal show={this.state.ZoomModal[row.replacement_id]} onHide={this.hideModalZoom} style={{background:"#333"}}>
                                <Modal.Header closeButton>
                                </Modal.Header>
                                <Modal.Body style={{padding:"0 25px 10px"}}>
                                    
                                    {row.replacement_image && <img src={"https://lieferservice123.com"+row.replacement_image} style={{width:"100%"}} alt="Bild wird geladen..." />}

                                    <div style={{width:"100%", float:"left", marginTop:"5px"}}>{row.replacement_marke}</div>
                                    <div style={{fontSize:"16px"}}><b>{row.replacement_quantity}x</b> {row.replacement_name}</div>
                    
                                    {row.replacement_grammageBadge && <div><span style={{fontSize:"12px"}}>({row.replacement_grammageBadge})</span></div>}

                                    <div style={{float:"left", width:"100%", marginTop:"15px", marginBottom:"20px"}}></div>

                                    {row.replacement_category && <div style={{width:"100%", float:"left", fontWeight:600, fontFamily:"open sans"}}>Kategorie<span style={{float:"right"}}>{row.replacement_category}</span></div>}

                                    <div style={{width:"100%", float:"left", fontWeight:600, fontFamily:"open sans"}}>Preis<span style={{float:"right"}}>€{row.replacement_price && row.replacement_price.toFixed(2)}</span></div>

                                </Modal.Body>
                            </Modal>

                            {/* another options */}
                            <Modal show={this.state.AnotherOptionsModal[row.id]} onHide={this.hideModalAnotherOptions} style={{background:"#333"}}>
                                <Modal.Header closeButton style={{background:"#F7F7F7"}}>
                                    <div style={{fontWeight:"bold"}}>Andere Optionen für...</div>
                                </Modal.Header>
                                <Modal.Body style={{padding:0, borderTop:"1px solid #E8E8E8", background:"#F7F7F7"}}>
                                    
                                    <div style={{width:"100%", float:"left", padding:"10px 25px 10px", background:"#fff", borderBottom:"1px solid #E8E8E8"}}>
                                        {row.placeholder_image && <img onClick={this.showModalZoom.bind(this, row.id)} title={row.name} src={'https://lieferservice123.com'+row.placeholder_image} style={{float:"left", width:"40px", height:"40px"}} />}
                                        <span onClick={this.showModalZoom.bind(this, row.id)} style={row.placeholder_image ? styleReplaceWithImg : styleReplaceWithoutImg}>
                                            <div style={{color:"#A04362"}}>Ausverkauft</div>
                                            <div style={{fontWeight:600, fontFamily:"open sans"}}><b>{row.quantity}x</b> {row.name}</div>
                                            {row.grammageBadge && <div style={{fontWeight:600, fontFamily:"open sans", color:"#ABABAB"}}>{row.grammageBadge}</div>}
                                        </span> 
                                        <span style={{float:"right", width:"50px", fontWeight:"bold", textAlign:"right"}}>€{(parseFloat(row.price)*parseFloat(row.quantity)).toFixed(2)}</span>
                                    </div>

                                    <div style={{padding:"15px 25px 15px", width:"100%", float:"left", margin:"auto", fontWeight:600, fontFamily:"open sans", color:"#ABABAB"}}>Wähle andere Optionen</div>

                                    <div style={{width:"100%", float:"left", background:"#fff", borderTop:"1px solid #E8E8E8"}}>
                                        <div style={{cursor:"pointer", position:"relative", borderTop:"1px solid #E8E8E8", width:"100%", float:"left", padding:"10px 25px", fontWeigt:"bold", color:localStorage.getItem("storeColor")}}><FontAwesomeIcon icon={faSearch} style={{marginRight:"5px"}} /> Ersatz suchen<Ink duration="500" /></div>
                                        <div onClick={() => this.request(supermarket_orders.id, row.id, row.quantity, row.replacement_id, row.found_replacement, "refundItem")} style={{cursor:"pointer", position:"relative", borderTop:"1px solid #E8E8E8", width:"100%", float:"left", padding:"10px 25px", fontWeigt:"bold", color:localStorage.getItem("storeColor")}}><FontAwesomeIcon icon={faRedo} style={{marginRight:"5px"}} /> Rückerstattung<Ink duration="500" /></div>
                                    </div>

                                </Modal.Body>
                            </Modal>
                        </div>
                    )
                }
             
                if(tab == 'todo' && row.found == null){
                    isAvailable = true;
                    return (
                        <div>
                            <div key={row.id} style={{width:"100%", float:"left", padding:"15px 10px", borderTop:"1px solid #E8E8E8", background:"#fff"}}>

                                {row.placeholder_image && <img onClick={this.showModalZoom.bind(this, row.id)} title={row.name} src={'https://lieferservice123.com'+row.placeholder_image} style={{float:"left", width:"40px", height:"40px"}} />}

                                <span onClick={this.showModalZoom.bind(this, row.id)} style={row.placeholder_image&&styleWithImg}>
                                    <div style={{fontWeight:600, fontFamily:"open sans"}}><b>{row.quantity}x</b> {row.name}</div>
                                    
                                    {row.grammageBadge && <div style={{fontWeight:600, fontFamily:"open sans", color:"#ABABAB"}}>{row.grammageBadge}</div>}
                                </span> 

                                <span style={{float:"right", width:"50px", fontWeight:"bold", textAlign:"right"}}>€{(parseFloat(row.price)*parseFloat(row.quantity)).toFixed(2)}</span>

                            </div>

                            <Modal show={this.state.ZoomModal[row.id]} onHide={this.hideModalZoom} style={{background:"#333"}}>
                                <Modal.Header closeButton>
                                </Modal.Header>
                                <Modal.Body style={{padding:"0 25px 10px"}}>
                                    
                                    {row.image && <img src={"https://lieferservice123.com"+row.image} style={{width:"100%"}} alt="Bild wird geladen..." />}

                                    <div style={{width:"100%", float:"left", marginTop:"5px"}}>{row.marke}</div>
                                    <div style={{fontSize:"16px"}}><b>{row.quantity}x</b> {row.name}</div>
                    
                                    {row.grammageBadge && <div><span style={{fontSize:"12px"}}>({row.grammageBadge})</span></div>}

                                    <div style={{float:"left", width:"100%", marginTop:"15px", marginBottom:"20px"}}></div>

                                    {row.category && <div style={{width:"100%", float:"left", fontWeight:600, fontFamily:"open sans"}}>Kategorie<span style={{float:"right"}}>{row.category}</span></div>}

                                    <div style={{width:"100%", float:"left", fontWeight:600, fontFamily:"open sans"}}>Preis<span style={{float:"right"}}>€{row.price.toFixed(2)}</span></div>

                    
                                </Modal.Body>
                            </Modal>
                        </div>
                    )
                }

                if(tab == 'done' && row.found > 0 && row.quantity == row.found){
                    isAvailable = true;
                    return (
                        <div>
                                
                            <div key={row.id} style={{width:"100%", float:"left", padding:"15px 10px", borderTop:"1px solid #E8E8E8", background:"#fff"}}>

                                {row.placeholder_image && <img onClick={this.showModalZoom.bind(this, row.id)} title={row.name} src={'https://lieferservice123.com'+row.placeholder_image} style={{float:"left", width:"40px", height:"40px"}} />}

                                <span onClick={this.showModalZoom.bind(this, row.id)} style={row.placeholder_image&&styleWithImg}>
                                    <div style={{color:"#64B455", position:"absolute", marginLeft:"-17px", marginTop:"-10px"}}><FontAwesomeIcon icon={faCheckCircle} /></div>
                                    
                                    <div style={{fontWeight:600, fontFamily:"open sans"}}><b>{row.quantity}x</b> {row.name}</div>
                                    
                                    {row.grammageBadge && <div style={{fontWeight:600, fontFamily:"open sans", color:"#ABABAB"}}>{row.grammageBadge}</div>}

                                </span> 

                                <span style={{float:"right", width:"50px", fontWeight:"bold", textAlign:"right"}}>€{(parseFloat(row.price)*parseFloat(row.found)).toFixed(2)}</span>

                            </div>

                            <Modal show={this.state.ZoomModal[row.id]} onHide={this.hideModalZoom} style={{background:"#333"}}>
                                <Modal.Header closeButton>
                                </Modal.Header>
                                <Modal.Body style={{padding:"0 25px 10px"}}>
                                    
                                    {row.image && <img src={"https://lieferservice123.com"+row.image} style={{width:"100%"}} alt="Bild wird geladen..." />}

                                    <div style={{width:"100%", float:"left", marginTop:"5px"}}>{row.marke}</div>
                                    <div style={{fontSize:"16px"}}><b>{row.quantity}x</b> {row.name}</div>
                    
                                    {row.grammageBadge && <div><span style={{fontSize:"12px"}}>({row.grammageBadge})</span></div>}

                                    <div style={{float:"left", width:"100%", marginTop:"15px", marginBottom:"20px"}}></div>

                                    {row.category && <div style={{width:"100%", float:"left", fontWeight:600, fontFamily:"open sans"}}>Kategorie<span style={{float:"right"}}>{row.category}</span></div>}

                                    <div style={{width:"100%", float:"left", fontWeight:600, fontFamily:"open sans"}}>Preis<span style={{float:"right"}}>€{row.price.toFixed(2)}</span></div>

                    
                                </Modal.Body>
                            </Modal>
                        </div>
                    )
                }  

            });

            return isAvailable?ordersComponent:false; 

        }

        const fee = supermarket_orders.trinkgeld;

        const styleHeader = {
            padding:"25px 18px 3px", width:"100%", float:"left", margin:"auto", fontWeight:600, fontFamily:"open sans", color:"#ABABAB"
        }

        return (
            <React.Fragment>

                {newOrdersDataElements("review")!==false ? newOrdersDataElements("review"):null} 

                {newOrdersDataElements("adjustment")!==false ? newOrdersDataElements("adjustment"):null} 

                {newOrdersDataElements("todo")!==false ? <div><div style={styleHeader}>Todo</div>{newOrdersDataElements("todo")}</div>:null} 

                {newOrdersDataElements("done")!==false ? <div><div style={styleHeader}>Fertig</div>{newOrdersDataElements("done")}</div>:null} 

            </React.Fragment>
        );
    }
}



const mapDispatchToProps = (dispatch, props) => ({
    dispatch: dispatch
})

export default connect(
    mapDispatchToProps
)(ReviewChanges);

