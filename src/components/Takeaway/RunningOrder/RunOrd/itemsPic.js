import { SUPERMARKET_ITEMS_URL } from "../../../../configs/index";

import React, { Component } from "react";
import { connect } from "react-redux";
import axios from "axios";

import { Modal, Button } from 'react-bootstrap';


class Items extends Component {

    state = {
        test: [],
        ZoomModal: false,
    };

    showModalZoom (id) {
        this.setState({
          ZoomModal: {
            [id]: true
          }
        });
    }
    hideModalZoom = () => {
        this.setState({
            ZoomModal: false
        });
    }

    render() {
        const { supermarket_orders } = this.props;
        let sub_total = 0;
        const ordersComponent = supermarket_orders.items.map((row, index) => {
                                
            sub_total += row.price*row.quantity;

            return (

                supermarket_orders.cat == "restaurant" ? (
                    <div key={row.id} style={{float:"left", width:"100%", margin:"5px 0"}}>
                        <span style={{color:"#757575", float:"left"}}><span style={{color:"#999", fontWeight:"bold", marginRight:"10px"}}>{row.quantity}x</span> {row.name}</span> 
                    </div>
                ) : (
                    <div key={row.id} style={{float:"left", margin:"5px 15px"}}>
                        <span style={{width:"calc(100% - 80px)", float:"left"}}>
                            <span style={{fontSize:"12px", width:"20px", height:"20px", color:"#fff", textAlign:"center", float:"left", background:"rgb(96, 178, 70)", borderRadius: "20px"}}>{row.quantity}</span> 
                            <img onClick={this.showModalZoom.bind(this, row.id)} title={row.name} src={'https://lieferservice123.com'+row.placeholder_image} style={{width:"40px", height:"40px", cursor:"pointer"}} />
                        </span>

                        <Modal show={this.state.ZoomModal[row.id]} onHide={this.hideModalZoom} style={{background:"#333"}}>
                            <Modal.Header closeButton>
                            </Modal.Header>
                            <Modal.Body style={{padding:"0 25px 10px"}}>
                                <div style={{fontSize:"12px"}}>{row.marke}</div>
                                <div style={{fontSize:"12px"}}>{row.quantity}x {row.name}</div>
                
                                <br/>
                
                                <img src={"https://lieferservice123.com"+row.image} style={{width:"100%"}} alt="Bild wird geladen..." />
                
                                <div style={{margin:"15px 0"}}>
                                    {/* <span style={{color:"#555"}}>Preis: </span> */}
                                    {/* <span style={{color:"#bf0b1c", fontSize:"16px"}}>{row.price} € </span> */}
                                    {row.grammageBadge ? <span style={{color:"#bf0b1c", fontSize:"14px"}}>({row.grammageBadge})</span> : null}
                                </div>
                
                                {/* <button onClick={()=>this.props.addProduct(row)} type="button" class="btn-save-address" style={{backgroundColor: "rgb(252, 128, 25)"}}>In den Einkaufswagen</button> */}
                            </Modal.Body>
                        </Modal>
                    </div>
                )
            )
        });

        const fee = supermarket_orders.trinkgeld;

        return (
            <React.Fragment>

                {/* <div style={{width:"100%", float:"left", padding:"30px 0", marginTop:"30px", borderTop:"1px solid #eaecee"}}>
                    <b>{supermarket_orders.items.length} PRODUKTE</b><br/> */}
                    {ordersComponent}
                {/* </div> */}

                
            </React.Fragment>
        );
    }
}



const mapDispatchToProps = (dispatch, props) => ({
    dispatch: dispatch
})

export default connect(
    mapDispatchToProps
)(Items);

