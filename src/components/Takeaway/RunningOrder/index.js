import React, { Component } from "react";

import BackWithSearch from "../../Takeaway/Elements/BackWithSearch";
import Map from "./Map";
import Meta from "../../helpers/meta";
import { Redirect } from "react-router";
import { connect } from "react-redux";
import { updateUserInfo } from "../../../services/user/actions";

import { Button } from 'react-bootstrap';
import ReactTooltip from 'react-tooltip';

import GuestCheckout from '../Items/OrderListView/index.js';

// fontawesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';

class RunningOrder extends Component {
    state = {
        updatedUserInfo: false,
        show_delivery_details: false,
    };
    
    static contextTypes = {
        router: () => null
    };

    __refreshOrderStatus = () => {
        const { user } = this.props;
        if (user.success) {
            this.refs.refreshButton.setAttribute("disabled", "disabled");
            this.props.updateUserInfo(user.data.id, user.data.auth_token, this.props.match.params.unique_order_id);
            this.setState({ updatedUserInfo: true });
            this.refs.btnSpinner.classList.remove("hidden");
            setTimeout(() => {
                if (this.refs.refreshButton) {
                    this.refs.btnSpinner.classList.add("hidden");
                }
            }, 2 * 1000);
            setTimeout(() => {
                if (this.refs.refreshButton) {
                    if (this.refs.refreshButton.hasAttribute("disabled")) {
                        this.refs.refreshButton.removeAttribute("disabled");
                    }
                }
            }, 2 * 1000);
        }
    };

    componentDidMount() {
        const { user } = this.props;

        if (user.success) {
            this.props.updateUserInfo(user.data.id, user.data.auth_token, this.props.match.params.unique_order_id);
        }else{// FIXME: done by lieferservice
            this.props.updateUserInfo(15, "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbGllZmVyc2VydmljZS52aXAvcHVibGljL2FwaS9sb2dpbiIsImlhdCI6MTU4Mzk0NjcxNCwiZXhwIjoyMjA2MDI2NzE0LCJuYmYiOjE1ODM5NDY3MTQsImp0aSI6IldKaUQ2bWtMamxBOEVGQ3AiLCJzdWIiOjE1LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.uESElPUF4aUajY5DPu2Xbx_HQ7uCENIeFuxWO3nqcTI", this.props.match.params.unique_order_id);
        }

        this.refreshSetInterval = setInterval(() => {
            this.__refreshOrderStatus();
        }, 15 * 1000);

        //workaround - invoice css error on first request
        if(localStorage.getItem("refreshed_invoice")){
            console.log("workaround invoice: already refreshed")
        } else {
            localStorage.setItem("refreshed_invoice", true)
            console.log("workaround invoice: refresh in 1.5 s")
            
            setTimeout(function () {
                var url = window.location.href;
                window.location = url;
            }, 1500);
        }
    }


    componentWillReceiveProps(nextProps) {
        if (nextProps.user.running_order === null) {
            this.context.router.history.push("/");
        }
        if (nextProps.user.delivery_details !== null) {
            this.setState({ show_delivery_details: true });
        }
    }

    __getDirectionToRestaurant = (restaurant_latitude, restaurant_longitude) => {
        // http://maps.google.com/maps?q=24.197611,120.780512
        const directionUrl = "http://maps.google.com/maps?q=" + restaurant_latitude + "," + restaurant_longitude;
        window.open(directionUrl, "_blank");
    };

    componentWillUnmount() {
        clearInterval(this.refreshSetInterval);
    }

    //countdown
    toDate = (unix_timestamp) => {
        var d = new Date(unix_timestamp * 1000);
        return d.getFullYear() + '-' + (d.getMonth()+1) + '-' + d.getDate() + ' ' + d.getHours() + ':' + d.getMinutes();
    }
    timer = (countDownDate) =>{
        // Set the date we're counting down to
        //var countDownDate = new Date("Feb 7, 2020 01:55:00").getTime();
        var countDownDate = new Date(this.toDate(countDownDate)).getTime();

            // Get today's date and time
            var now = new Date().getTime();
            
            // Find the distance between now and the count down date
            var distance = countDownDate - now;
            
            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);
            
            // If the count down is finished, write some text
            if (distance < 0) {
                return "0";
            }else{
                //return days + "d " + hours + "h " + minutes + "m " + seconds + "s ";
                return minutes;
            }
    }   
    getCircleStyle = ()=>{
        const { user } = this.props;

        var distance = this.getDistance(JSON.parse(user.running_order.location).lat, JSON.parse(user.running_order.location).lng, user.running_order.restaurant.latitude, user.running_order.restaurant.longitude);

        var percent = distance * 100 / user.running_order.restaurant.delivery_radius;

        if(percent >= 90){
            var min = 60;
        } else if(percent >= 80){
            var min = 55;
        } else if(percent >= 70){
            var min = 50;
        } else if(percent >= 60){
            var min = 45;
        } else {
            var min = 40;
        }

        var min = this.timer(user.running_order.added+60*min);
        var circleStyle = {
            strokeDasharray: min*4+", 251.327"
        };

        return circleStyle;
    }
    getMin = ()=>{
        const { user } = this.props;

        var distance = this.getDistance(JSON.parse(user.running_order.location).lat, JSON.parse(user.running_order.location).lng, user.running_order.restaurant.latitude, user.running_order.restaurant.longitude);

        var percent = distance * 100 / user.running_order.restaurant.delivery_radius;

        if(percent >= 90){
            var min = 60;
        } else if(percent >= 80){
            var min = 55;
        } else if(percent >= 70){
            var min = 50;
        } else if(percent >= 60){
            var min = 45;
        } else {
            var min = 40;
        }

        var min = this.timer(user.running_order.added+60*min);

        return min;
    }

    
    getMinOnly = ()=>{
        const { user } = this.props;

        var distance = this.getDistance(JSON.parse(user.running_order.location).lat, JSON.parse(user.running_order.location).lng, user.running_order.restaurant.latitude, user.running_order.restaurant.longitude);

        var percent = distance * 100 / user.running_order.restaurant.delivery_radius;

        if(percent >= 90){
            var min = 60;
        } else if(percent >= 80){
            var min = 55;
        } else if(percent >= 70){
            var min = 50;
        } else if(percent >= 60){
            var min = 45;
        } else {
            var min = 40;
        }

        return min;
    }

    //check deliver radius
    degrees_to_radians = (degrees) =>{
        var pi = Math.PI;
        return degrees * (pi/180);
    }
    getDistance = (latitudeFrom, longitudeFrom, latitudeTo, longitudeTo) =>{
        var latFrom = this.degrees_to_radians(latitudeFrom);
        var lonFrom = this.degrees_to_radians(longitudeFrom);
        var latTo = this.degrees_to_radians(latitudeTo);
        var lonTo = this.degrees_to_radians(longitudeTo);

        var latDelta = latTo - latFrom;
        var lonDelta = lonTo - lonFrom;

        var angle = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(latDelta / 2), 2) +
            Math.cos(latFrom) * Math.cos(latTo) * Math.pow(Math.sin(lonDelta / 2), 2)));
        return (angle * 6371);
    }

    getAddress = (address, country=false, long=true) => {
        if(country === false){
            var res = address;
            var split = res.split(",");
            if(long) var res = split.slice(0, split.length - 1).join(",");
            else var res = split[0];
            return res;
        } else {
            var res = address.split(", ");
            return res[0];
        }
    }
    navigation = (lat, long) =>{
        // If it's an iPhone..
        if( (navigator.platform.indexOf("iPhone") != -1) 
            || (navigator.platform.indexOf("iPod") != -1)
            || (navigator.platform.indexOf("iPad") != -1))
             window.open("maps://maps.google.com/maps?daddr="+lat+","+long+"&amp;ll=");
        else
             window.open("http://maps.google.com/maps?daddr="+lat+","+long+"&amp;ll=");
    }

    render() {
        if (localStorage.getItem("storeColor") === null) {
            return <Redirect to={"/"} />;
        }
        const { user } = this.props;

        // if (!user.success) {
        //     return <Redirect to={"/"} />;
        // }

        //for debugging use http://172.105.79.167:3000/running-order/OD-03-26-4KBZK6
        //and then it will give error for location, then you can debug

        return (
            <React.Fragment>
                <Meta
                    seotitle={localStorage.getItem("seoMetaTitle")}
                    seodescription={localStorage.getItem("seoMetaDescription")}
                    ogtype="website"
                    ogtitle={localStorage.getItem("seoOgTitle")}
                    ogdescription={localStorage.getItem("seoOgDescription")}
                    ogurl={window.location.href}
                    twittertitle={localStorage.getItem("seoTwitterTitle")}
                    twitterdescription={localStorage.getItem("seoTwitterDescription")}
                />

                <BackWithSearch
                    boxshadow={true}
                    has_title={true}
                    title={user.running_order && user.running_order.unique_order_id}
                    disbale_search={true}
                    back_to_home={false}
                    goto_orders_page={true}
                />

                {user.running_order && (
                    <React.Fragment>

                        {localStorage.getItem("showMap") === "true" && (
                            <Map
                                restaurant_latitude={user.running_order.restaurant.latitude}
                                restaurant_longitude={user.running_order.restaurant.longitude}
                                order_id={user.running_order.id}
                                orderstatus_id={user.running_order.orderstatus_id}
                                deliveryLocation={user.running_order.location}
                            />
                        )}

                        {/* <div className="mt-15 mb-200" style={{maxWidth:"468px", margin:"auto"}}> */}
                        <div className="bg-white mb-200" style={{ position: "relative", top: localStorage.getItem("showMap") === "true" ? "24.5rem" : "4rem", maxWidth: "468px", margin: "auto"}}>

                            {this.state.show_delivery_details && (
                                <div className="block block-link-shadow pb-2 m-0 delivery-assigned-block">
                                    <div className="block-content block-content-full clearfix py-0">
                                        <div className="float-right">
                                            <img
                                                src={"/assets/img/delivery/" + user.delivery_details.photo}
                                                className="img-fluid img-avatar"
                                                alt={user.delivery_details.name}
                                            />
                                        </div>
                                        <div className="float-left mt-20" style={{ width: "75%" }}>
                                            <div className="font-w600 font-size-h5 mb-5">
                                                {user.delivery_details.name} {localStorage.getItem("deliveryGuyAfterName")}
                                            </div>
                                            <div className="font-size-sm text-muted">
                                                {user.delivery_details.description} <br />
                                                <span>
                                                    {localStorage.getItem("vehicleText")} {user.delivery_details.vehicle_number}
                                                </span>
                                            </div>
                                            <div className="">
                                                <a className="btn btn-get-direction mt-2" href={"tel:" + user.delivery_details.phone}>
                                                    {localStorage.getItem("callNowButton")} <i className="si si-call-out"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            )}

                            {/* countdown */}
                            <div className="css-countdown">
                                <div style={{position: "relative", transitionTimingFunction: "linear", marginLeft: "auto", marginRight: "auto", width: "80%", display: "flex", justifyContent: "center"}}>
                                    <svg height="90" width="80">

                                        <circle cx="40" cy="45" r="40" fill="#E0E0E0"></circle>
                                        <circle cx="40" cy="45" r="36" fill="#FFFFFF"></circle>
                                        <circle cx="40" cy="45" r="38" fill="none" strokeWidth="4" strokeMiterlimit="20" stroke="#f78004" transform="rotate(-90 40 45)" className="css-countdown-circle" style={this.getCircleStyle()}></circle>

                                        <text textAnchor="middle" x="40" y="44">
                                            <tspan className="css-countdown-min">{this.getMin()}</tspan>
                                            <tspan x="40" dy="1.2em" fill="#A5A6A9" style={{fontSize: "16px"}}>Min</tspan>
                                        </text>
                                    </svg>
                                </div>
                            </div>

                            <br/>

                            {/* <div dir="auto">
                                <h1>Lieferzeitpunkt bestätigt</h1>
                                <p>City Kebap Pizza hat bestätigt, dass Deine Bestellung in ungefähr 19 Minuten bei Dir ankommen wird. </p>
                            </div> */}

                            {/* Bestellung erfolgreich aufgegeben, Warten auf das Restaurant, um Ihre Bestellung zu bestätigen */}
                            {user.running_order.orderstatus_id === 1 && (
                                <React.Fragment>
                                    <div className="row" style={{textAlign:"center"}}>
                                        <div className="col-md-12">
                                            <div className="block block-link-shadow">
                                                <div className="block-content block-content-full clearfix py-0">
                                                    <div className="mt-20">
                                                        <div className="font-w600 font-size-h4 mb-5">
                                                            {localStorage.getItem("runningOrderPlacedTitle")}
                                                        </div>
                                                        <div className="font-size-sm text-muted">
                                                            {localStorage.getItem("runningOrderPlacedSub")}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </React.Fragment>
                            )}

                            {/* Wird gekocht!! Das Restaurant bereitet Ihre Bestellung vor */}
                            {user.running_order.orderstatus_id === 2 && (
                                <React.Fragment>
                                    <div className="row" style={{textAlign:"center"}}>
                                        <div className="col-md-12">
                                            <div className="block block-link-shadow">
                                                <div className="block-content block-content-full clearfix py-0">
                                                    <div className="mt-20">
                                                        <div className="font-w600 font-size-h4 mb-5">
                                                            {localStorage.getItem("runningOrderPreparingTitle")}
                                                        </div>
                                                        <div className="font-size-sm text-muted">
                                                            {localStorage.getItem("runningOrderPreparingSub")}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </React.Fragment>
                            )}

                            {/* Zusteller zugewiesen, Bestellung wird abgeholt.*/}
                            {user.running_order.orderstatus_id === 3 && (
                                <React.Fragment>
                                    <div className="row" style={{textAlign:"center"}}>
                                        <div className="col-md-12">
                                            <div className="block block-link-shadow">
                                                <div className="block-content block-content-full clearfix py-0">
                                                    <div className="mt-20">
                                                        <div className="font-w600 font-size-h4 mb-5">
                                                            {localStorage.getItem("runningOrderDeliveryAssignedTitle")}
                                                        </div>
                                                        <div className="font-size-sm text-muted">
                                                            {localStorage.getItem("runningOrderDeliveryAssignedSub")}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </React.Fragment>
                            )}

                            {/* Vroom Vroom!! Bestellung wurde abgeholt und ist unterwegs*/}
                            {user.running_order.orderstatus_id === 4 && (
                                <React.Fragment>
                                    <div className="row" style={{textAlign:"center"}}>
                                        <div className="col-md-12">
                                            {localStorage.getItem("enableDeliveryPin") === "true" && (
                                                <React.Fragment>
                                                    <div className="font-size-h4 mb-5 px-15 text-center">
                                                        <div className="font-w600 btn-deliverypin">
                                                            <span className="text-muted">
                                                                {localStorage.getItem("runningOrderDeliveryPin")}{" "}
                                                            </span>
                                                            {this.props.user.data.delivery_pin}
                                                        </div>
                                                    </div>
                                                    <hr />
                                                </React.Fragment>
                                            )}

                                            <div className="block block-link-shadow">
                                                <div className="block-content block-content-full clearfix py-0">
                                                    <div className="mt-20">
                                                        <div className="font-w600 font-size-h4 mb-5">
                                                            {localStorage.getItem("runningOrderOnwayTitle")}
                                                        </div>
                                                        <div className="font-size-sm text-muted">
                                                            {localStorage.getItem("runningOrderOnwaySub")}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </React.Fragment>
                            )}

                            {/* Bestellung storniert, Es tut uns leid. Ihre Bestellung wurde storniert. */}
                            {user.running_order.orderstatus_id === 6 && (
                                <div className="row" style={{textAlign:"center"}}>
                                    <div className="col-md-12">
                                        <div className="block block-link-shadow">
                                            <div className="block-content block-content-full clearfix py-0">
                                                <div className="mt-20">
                                                    <div className="font-w600 font-size-h4 mb-5">
                                                        {localStorage.getItem("runningOrderCanceledTitle")}
                                                    </div>
                                                    <div className="font-size-sm text-muted">
                                                        {localStorage.getItem("runningOrderCanceledSub")}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            )}

                            {/* Essen ist fertig, Ihre Bestellung ist zur Selbstabholung bereit. */}
                            {user.running_order.orderstatus_id === 7 && (
                                <React.Fragment>
                                    <div className="row" style={{textAlign:"center"}}>
                                        <div className="col-md-12">
                                            <div className="block block-link-shadow">
                                                <div className="block-content block-content-full clearfix py-0">
                                                    <div className="mt-20">
                                                        <div className="font-w600 font-size-h4 mb-5">
                                                            {localStorage.getItem("runningOrderReadyForPickup")}
                                                        </div>
                                                        <div className="font-size-sm text-muted">
                                                            {localStorage.getItem("runningOrderReadyForPickupSub")}
                                                        </div>
                                                    </div>

                                                    <button
                                                        className="btn btn-get-direction mt-2"
                                                        onClick={() =>
                                                            this.__getDirectionToRestaurant(
                                                                user.running_order.restaurant.latitude,
                                                                user.running_order.restaurant.longitude
                                                            )
                                                        }
                                                    >
                                                        <i className="si si-action-redo mr-5" />
                                                        {localStorage.getItem("deliveryGetDirectionButton")}
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </React.Fragment>
                            )}

                            {/* speichern als 1-Klick Bestellung */}
                            <div style={{padding:"0 15px", textAlign:"center"}}>
                                <Button variant="primary">
                                    <a href={"https://app.lieferservice123.com/?install-voice=1&id="+user.running_order.id+"&code="+user.running_order.code+"&name=Bestellung "+user.running_order.id+""}>Speichern als 1-Klick Bestellung</a>
                                </Button>
                            </div>

                            <br/><br/><br/>

                            {/* restaurant logo */}
                            <div style={{maxWidth:"768px", marginTop:"40px"}} className="restaurant-logo">
                                <div className="restaurant-logo__container" style={{zIndex: 0}}>
                                    <div className="restaurant-logo__inner" style={{border: "1px solid #d7d7d7"}}>
                                        <img src={user.running_order.restaurant.image} alt={user.running_order.restaurant.name} />
                                    </div>
                                </div>
                            </div>

                            {/* rechnung */}
                            <div style={{fontSize:"1rem", fontFamily:"open sans", fontWeight:"300", padding:"30px 15px 20px", overflow:"auto", border:"1px solid #d7d7d7", borderRadius:"15px", margin:"10px"}}>

                                <div style={{marginTop:"40px", padding:"10px 0", marginBottom:"10px", borderBottom:"1px solid #eaecee"}}>

                                    <b>{user.running_order.restaurant.name}</b><br/>
                                    {user.running_order.restaurant.address}<br/>
                                    <a href={'tel:' + user.running_order.restaurant.tel} style={{color: "rgb(26, 115, 232)"}}>{user.running_order.restaurant.tel}</a><br/>

                                    <br/>

                                    Bestelldatum: {this.toDate(user.running_order.added)}<br/>
                                    Bestell Nr: {user.running_order.id}<br/>

                                    <br/>

                                    <b>KUNDE</b><br/>
                                    {user.running_order.fromName} {user.running_order.vip > 0 ? <img src="/assets/img/vip4.png" style={{width:"26px", height:"26px"}} /> : "" }<br/>
                                    <span style={{color:"rgb(26, 115, 232)"}} onClick={() => this.navigation(JSON.parse(user.running_order.location).lat, JSON.parse(user.running_order.location).lng)}>{this.getAddress(user.running_order.address)}</span><br/>
                                    TEL <a style={{color:"rgb(26, 115, 232)"}} href={'tel:' + user.running_order.fromTel}>{user.running_order.fromTel}</a><br/>

                                    <br/>

                                    <b>WICHTIG</b><br/>
                                    Bezahlen mit {user.running_order.payment_mode === 'Barzahlung'?"":"💳"} {user.running_order.payment_mode}<br/>
                                    {user.running_order.delivery_type == 1 ? "Lieferung":"Abholung"} um: {this.toDate(user.running_order.added+60*this.getMinOnly())}<br/>
                                    
                                </div>

                                <ul style={{listStyle:"none", margin:0, marginLeft:"15px", padding:0}}>

                                    <GuestCheckout currencyFormat={user.running_order.restaurant.currencyFormat} id={user.running_order.id} />

                                    {(user.running_order.vip > 0 && (user.running_order.total - user.running_order.delivery_charge + parseFloat(user.running_order.discount)).toFixed(2) >= 15) ? 
                                    <li style={{width:"100%", overflow:"auto", margin:"5px 0"}}>
                                        <span>
                                            <span style={{color:"#757575", width:"20px", float:"left"}}>1x</span> 
                                            Gratisgetränk <FontAwesomeIcon icon={faInfoCircle} data-tip='' data-for='freeDrink' />
                                        </span>
                                        <span style={{padding:"0 4px", width:"80px", float:"right", textAlign:"right"}}>0.00 {user.running_order.restaurant.currencyFormat}</span> 
                                    </li> : ""}
                                    <li style={{fontWeight:"bold"}}>
                                        Zwischensumme
                                        <span style={{padding:"0 4px", width:"80px", float:"right", textAlign:"right"}}>{(user.running_order.total - user.running_order.delivery_charge + parseFloat(user.running_order.discount)).toFixed(2)} {user.running_order.restaurant.currencyFormat}</span> 
                                    </li>
                                    <li style={{fontWeight:"bold"}}>
                                        Lieferkosten
                                        <span style={{padding:"0 4px", width:"80px", float:"right", textAlign:"right"}}>{user.running_order.delivery_charge} {user.running_order.restaurant.currencyFormat}</span> 
                                    </li>
                                    {(user.running_order.discount > 0) ? 
                                    <li style={{fontWeight:"bold"}}>
                                        Mengenrabatt ({(user.running_order.discount * 100 / (user.running_order.total - user.running_order.delivery_charge + parseFloat(user.running_order.discount)).toFixed(2)).toFixed()}%)
                                        <span style={{padding:"0 4px", width:"80px", float:"right", textAlign:"right"}}>-{user.running_order.discount} {user.running_order.restaurant.currencyFormat}</span> 
                                    </li> : ""}
                                    <li style={{fontWeight:"bold"}}>
                                        Gesamt
                                        <span style={{padding:"0 4px", width:"80px", float:"right", textAlign:"right"}}>{user.running_order.total} {user.running_order.restaurant.currencyFormat}</span> 
                                    </li>

                                </ul>

                                <ReactTooltip id='freeDrink' place="top"><div style={{fontSize:"10px", fontWeight:"300", fontFamily:"open sans"}}>0,3L Cola, Fanta oder Sprite<br />gratis bei jeder Bestellung ab 15 {user.running_order.restaurant.currencyFormat}</div></ReactTooltip>


                            </div>

                        </div>

                        {/* refresh status */}
                        <div>
                            <button
                                className="btn btn-lg btn-refresh-status"
                                ref="refreshButton"
                                onClick={this.__refreshOrderStatus}
                                style={{
                                    backgroundColor: localStorage.getItem("cartColorBg"),
                                    color: localStorage.getItem("cartColorText")
                                }}
                            >
                                {localStorage.getItem("runningOrderRefreshButton")}{" "}
                                <span ref="btnSpinner" className="hidden">
                                    <i className="fa fa-refresh fa-spin" />
                                </span>
                            </button>
                        </div>
                    </React.Fragment>
                )}
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user.user
});

export default connect(
    mapStateToProps,
    { updateUserInfo }
)(RunningOrder);
