import React, { Component } from 'react'
import Ink from "react-ink";
import ReactDOM from 'react-dom';
import axios from "axios";


class Categories extends Component {
    
    constructor() {
        
        super();
        
        this.state = { 

            catList: [],
            
        };

    }

    componentDidMount() {
        this.loadCat();
    }
    

    loadCat = () => {
       var url = `//${window.location.hostname}/php/twilio/FirstScreen_einkaufSearch.php?q=&act=cat&country=${localStorage.getItem("country")}&shop=${this.props.shop}`;
        axios.get(url).then(( res ) => {

            if (res.data.length) {
                this.setState({
                    catList: [...this.state.catList, ...res.data],
                    no_categories: false
                });
            } else {
                this.setState({
                    no_categories: true
                });
            }
        })
    }

    render() {

        const { catList } = this.state;

        return (

            catList.length ? (
                catList.map((row, index) => {
                    return (
                        <a href={"/"+this.props.slug+"?act=category&catType="+btoa(row.category)} onClick={this.props.hideModalCat} key={row.category} style={{display:"block", cursor:"pointer", marginBottom:"20px", fontWeight:600, fontFamily:"open sans", fontSize:"1.15rem", color:"#3d4152"}}>
                            {row.category}
                        </a>
                    )
                })
            ) : (
                this.state.no_categories ? <h2 style={{padding:"50px 15px", textAlign:"center", fontFamily:"open sans", fontWeight:300, float:"left", width:"100%"}}>Keine Kategorien gefunden.</h2> : <h2 style={{padding:"50px 15px", textAlign:"center", fontFamily:"open sans", fontWeight:300, float:"left", width:"100%"}}>Laden...</h2>
            )

        )
    }
}

export default Categories