import React, { Component } from 'react'
import Ink from "react-ink";
import ReactDOM from 'react-dom';
import axios from "axios";

//bootstrap
import { Modal, Button } from 'react-bootstrap';

// fontawesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { faMinus } from '@fortawesome/free-solid-svg-icons';

import { addProduct, removeProduct } from "../../../../../services/cart/actions";
import { connect } from "react-redux";

class MainPageSubCatItem extends Component {

	state = {
		itemList: [],
		ZoomModal: false,

	};

	constructor(props) {
		super(props);
		this.getItemsInCategory(this.props.category);
	}	
	
	static contextTypes = {
        router: () => null
    };

    forceStateUpdate = () => {
        setTimeout(() => {
            this.forceUpdate();
            this.props.update();
        }, 100);
    };

	showModalZoom (id) {
		this.setState({
		ZoomModal: {
			[id]: true
		}
	});
	}
	hideModalZoom = () => {
		this.setState({
			ZoomModal: false
		});
	}

	getItemsInCategory = async (categoryName) => {
		try {
			var url = `//${window.location.hostname}/php/twilio/FirstScreen_einkaufSearch.php?act=&q=&country=${localStorage.getItem("country")}&cat=${encodeURIComponent(decodeURIComponent(categoryName))}&sub_category=${encodeURIComponent(decodeURIComponent(this.props.subCat))}&limit=10&shop=${this.props.selectedShop}`;
			const data = await axios.get(url);
			this.setState({
				itemList: data.data
			});
		} catch (error) {
			throw error;
		}
	};

	render() {

		let results = this.state.itemList;

		results = results.map((productItem) => {
			const itemFoundIndex =  this.props.modal_input_results.findIndex(ele => (ele && productItem && ele.id === productItem.id));
			if (itemFoundIndex >= 0) {
				// console.log(this.props.modal_input_results[itemFoundIndex]);
				productItem.quantity = this.props.modal_input_results[itemFoundIndex].quantity;
			}
			return productItem;
		});

		const { modal_input_results, addProduct, removeProduct } = this.props;


		return (

			<div className="mainPageScroll" style={{width:"100%", overflow:"scroll"}}>
				<div style={{width:"max-content"}}>
				
					{results.map((r) => (

						<li className={"suggestion-word-style mainPageItem"+((r.quantity >= 0) ? ' borderAdd' : '')} key={r.id}>
			
							{modal_input_results.find(cp => cp.id === r.id) !== undefined && (
								<div className="addToOrderQuickAdd" id={"addToOrderQuickAdd"+r.id} onMouseLeave={() => document.getElementById("addToOrderQuickAdd"+r.id).style.display = "none"} style={{display:"none", position:"absolute", height:"42px", background:"#fff", background:"#fff", zIndex:1, overflow:"auto", lineHeight:3.5, borderRadius:"4px", boxShadow:"rgba(0, 0, 0, 0.25) 0px 3px 10px"}}>

									<span style={{height:"42px", float:"left", textAlign:"center"}}><FontAwesomeIcon onClick={()=>{ r.quantity = 1; removeProduct(r); this.forceStateUpdate(); }} icon={faMinus} style={{fontSize:"24px", color:localStorage.getItem("cartColorBg"), borderRadius: "20px", width: "24px", height: "24px", padding: "3px"}} /></span>

									<span className="quantityQuick" style={{height:"42px", float:"left", textAlign:"center", fontSize:"16px", lineHeight:2.6}}>{modal_input_results.find(cp => cp.id === r.id).quantity}</span>

									<span style={{height:"42px", float:"left", textAlign:"center"}}><FontAwesomeIcon onClick={()=>{ addProduct(r); this.forceStateUpdate(); }} icon={faPlus} style={{fontSize:"24px", color:localStorage.getItem("cartColorBg"), borderRadius: "20px", width: "24px", height: "24px", padding: "3px"}} /></span>
								</div>
							)}

							{modal_input_results.find(cp => cp.id === r.id) !== undefined ? (
								<span onClick={() => document.getElementById("addToOrderQuickAdd"+r.id).style.display = "block"} icon={faPlus} style={{float:"right", fontSize:"12px", background:localStorage.getItem("cartColorBg"), color:"#fff", borderRadius: "20px", width: "24px", height: "24px", padding: "3px", position:"absolute", right:"10px", textAlign:"center"}}>{r.quantity}</span>
							) : (
								<FontAwesomeIcon className="addProd" onClick={()=>{ addProduct(r); this.forceStateUpdate(); setTimeout(() => { document.getElementById("addToOrderQuickAdd"+r.id).style.display = "block"; }, 100); }} icon={faPlus} style={{float:"right", fontSize:"24px", color:localStorage.getItem("cartColorBg"), borderRadius: "20px", width: "24px", height: "24px", padding: "3px", position:"absolute", right:"10px"}} />
							)}

							<img src={"https://lieferservice123.com"+(r.placeholder_image ? r.placeholder_image : r.image)} style={{marginRight:"10px"}} onClick={this.showModalZoom.bind(this, r.id)} /> 
			
							<div style={{width:"100%"}} onClick={this.showModalZoom.bind(this, r.id)}>
			
							<div style={{color:"#333", fontSize:"12px"}}>{r.name}</div>
			
							<div style={{color:"#bf0b1c", fontWeight:"bold", fontSize:"16px"}}>{r.price.toFixed(2)} € <span style={{color:"#888", fontSize:"10px", fontWeight:"normal"}}>{r.grammageBadge}</span></div>
			
							</div>
			
							<Modal show={this.state.ZoomModal[r.id]} onHide={this.hideModalZoom}>
								<Modal.Header closeButton>
								</Modal.Header>
								<Modal.Body style={{padding:"0 25px 10px"}}>
									<div style={{fontSize:"12px"}}>{r.marke}</div>
									<div style={{fontSize:"12px"}}>{r.name}</div>
					
									<br/>
					
									<img src={"https://lieferservice123.com"+r.image} style={{width:"100%"}} alt="Bild wird geladen..." />
					
									<div style={{margin:"15px 0"}}>
									<span style={{color:"#555"}}>Preis: </span>
									<span style={{color:"#bf0b1c", fontSize:"16px"}}>{r.price.toFixed(2)} € </span>
									{r.grammageBadge ? <span style={{color:"#bf0b1c", fontSize:"14px"}}>({r.grammageBadge})</span> : null}
									</div>
					
									<button onClick={()=>{ addProduct(r); this.forceStateUpdate(); }} type="button" class="btn-save-address" style={{backgroundColor: "rgb(252, 128, 25)"}}>In den Einkaufswagen</button>
								</Modal.Body>
							</Modal>
			
							{/* <Ink duration="500" /> */}
			
						</li>
					))}

				</div>
			</div>

		)

	}
}


const mapStateToProps = state => ({
    cartProducts: state.cart.products
});

export default connect(
    mapStateToProps,
    { addProduct, removeProduct }
)(MainPageSubCatItem);
