import React, { Component } from 'react'
import Ink from "react-ink";
import ReactDOM from 'react-dom';
import axios from "axios";
import MainPageSub2CategoryItems from "./mainPageSub2CategoryItems";

//bootstrap
import { Modal, Button } from 'react-bootstrap';


class MainPageSub2Categories extends Component {

    state = {
        subCategory2List: []
    };

    constructor(props) {
        super(props);
        this.getSubCat2FromSubCategory();
    }

    getSubCat2FromSubCategory = (showProductsOnInit) => {
        console.log(this.props.category);
        console.log(this.props.subCat);
        var url = `//${window.location.hostname}/php/twilio/FirstScreen_einkaufSearch.php?q=&act=sub_category2&cat=${encodeURIComponent(this.props.category)}&country=${localStorage.getItem("country")}&cat=${encodeURIComponent(this.props.category)}&sub_cat=${encodeURIComponent(this.props.subCat)}&limit=50&shop=${this.props.selectedShop}`;
        axios.get(url)
          .then(( res ) => {
            this.setState({
                subCategory2List: res.data
            });
          })
    }

    subCategory2Html = (subCategory2Name, index) => {
        return (
            <div key={index} style={{background:"#fff", marginTop:"10px", width:"100%", float:"left"}}>
                <div style={{width:"100%", float:"left", fontSize:"18px", fontFamily:"open sans", fontWeight:300, margin:"10px 15px"}}><a href={this.props.slug+"?cat=grocery&act=sub_category2&catType="+this.props.category+"&subCat="+this.props.subCat+"&subCat2="+subCategory2Name}>{subCategory2Name}</a></div>
                
                <MainPageSub2CategoryItems 
                modal_input_results={this.props.modal_input_results} 
                selectedShop={this.props.selectedShop} 
                addProduct={this.props.addProduct} 
                sub2Cat={subCategory2Name} 
                subCat={this.props.subCat} 
                category={this.props.category}
                
                shouldUpdate={this.props.shouldUpdate}
                update={this.props.update}
                addProduct={this.props.addProduct}
                removeProduct={this.props.removeProduct}></MainPageSub2CategoryItems>
            </div>
        );
    }

  render() {

      let subCategory2Html = '';
      subCategory2Html = this.state.subCategory2List.map((ele, i) => this.subCategory2Html(ele.sub_category2, i));

      
    return (<div>

        <div style={{width:"100%", float:"left", margin:"0px 18px"}}>
            <span style={{fontSize:"11px", fontFamily:"open sans", fontWeight:400}}><a href={this.props.slug}>Startseite</a></span>

            <span style={{fontSize:"11px", fontFamily:"open sans", fontWeight:400}}> » <a href={this.props.slug+"?cat=grocery&act=category&catType="+this.props.category}>{this.props.category}</a></span>

            <span style={{fontSize:"11px", fontFamily:"open sans", fontWeight:400}}> » {this.props.subCat}</span>
        </div>

        {subCategory2Html}

    </div>)

  }
}

export default MainPageSub2Categories