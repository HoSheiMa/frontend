import React, { Component } from 'react'
import Ink from "react-ink";
import ReactDOM from 'react-dom';
import axios from "axios";
import MainPageSubCatItem from "./mainPageSubCatItem";

//bootstrap
import { Modal, Button } from 'react-bootstrap';


class MainPageSubCategories extends Component {

    state = {
        subCategoryList: []
    };

    constructor(props) {
        super(props);
        this.getSubCatFromCategory();
    }

    getSubCatFromCategory = (showProductsOnInit) => {
        var url = '//'+window.location.hostname+'/php/twilio/FirstScreen_einkaufSearch.php?q=&act=subcat&cat='+encodeURIComponent(this.props.category)+'&country='+localStorage.getItem("country")+'&shop='+this.props.selectedShop+'';
        axios.get(url)
          .then(( res ) => {
            this.setState({
                subCategoryList: res.data
            });
          })
    }

    subCategoryHtml = (subCategoryName, index) => {
        return (
            <div key={index} style={{background:"#fff", marginTop:"10px", width:"100%", float:"left"}}>
                <div style={{width:"100%", float:"left", fontSize:"18px", fontFamily:"open sans", fontWeight:400, margin:"10px 15px"}}><a href={"?cat=grocery&act=sub_category&catType="+this.props.category+"&subCat="+subCategoryName}>{subCategoryName}</a></div>
                
                <MainPageSubCatItem 
                    modal_input_results={this.props.modal_input_results} 
                    selectedShop={this.props.selectedShop} 
                    subCat={subCategoryName} 
                    category={this.props.category}

                    shouldUpdate={this.props.shouldUpdate}
                    update={this.props.update}
                    addProduct={this.props.addProduct}
                    removeProduct={this.props.removeProduct}
                ></MainPageSubCatItem>
            </div>
        );
    }

  render() {

      let subCategoryHtml = '';
      subCategoryHtml = this.state.subCategoryList.map((ele, i) => this.subCategoryHtml(ele.sub_category, i));

      
        return (
        
            <div>
                <div style={{width:"100%", float:"left", margin:"0px 18px"}}>
                    <span style={{fontSize:"11px", fontFamily:"open sans", fontWeight:400}}><a href={this.props.slug}>Startseite</a></span>

                    <span style={{fontSize:"11px", fontFamily:"open sans", fontWeight:400}}> » {this.props.category}</span>
                </div>

                {subCategoryHtml}
            </div>
        
        )

  }
}

export default MainPageSubCategories