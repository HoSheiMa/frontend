
import React, { Component } from "react";
import Ink from "react-ink";
import { connect } from "react-redux";
import axios from "axios";

// fontawesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons';
import { faPhone } from '@fortawesome/free-solid-svg-icons';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons';
import { faLink } from '@fortawesome/free-solid-svg-icons';
import { faCheck } from '@fortawesome/free-solid-svg-icons';

import PWAPrompt from 'react-ios-pwa-prompt'
import { isChrome, isChromium, isMobileSafari } from 'react-device-detect';

import Modal from 'react-bootstrap/Modal';
import ReactDOM from 'react-dom';
import { Button } from 'react-bootstrap';

declare var INSTALL_APP_POPUP;//here it is again

class Footer extends Component {

    constructor() {
        super();
        this.state = { 
            installModal: false,
            berechtigungenModal: (document.URL.indexOf("?install=1") >= 0 ? true:false),
         };
    }

    static contextTypes = {
        router: () => null
    };

    componentDidMount() {
        axios.get('/php/manifest.php')
            .then(response => {
                if (response && response.status === 200) {   
                    console.log("start url from manifest if app is running", (response.data && response.data.start_url));
                } else {
                    console.log("error while fetching the manifest file", response);
                }
            })
            .catch(function(error) {
                console.log(error);
            });

        if(document.URL.indexOf("?install=1") >= 0){
            //install app for restaurant
            // var name = window.location.href.replace(/-/g, ' ').replace("?install=1", "").split("/").pop();
            // var slug = window.location.href.replace("?install=1", "").split("/").pop();

            // var myDynamicManifest = {
            //     "short_name": this.titleCase(name),
            //     "name": this.titleCase(name),
            //     "scope": "https://"+window.location.hostname+"/"+slug,
            //     "start_url": "https://"+window.location.hostname+"/"+slug,
            //     "display": "standalone",
            //     "theme_color": "#ffffff",
            //     "background_color": "#ffffff",
            //     "icons": [
            //         {
            //             "src": "https://"+window.location.hostname+"/icon-get.php?slug="+slug+"&w=36&h=36",
            //             "sizes": "36x36",
            //             "type": "image/png",
            //             "density": "0.75"
            //         },
            //         {
            //             "src": "https://"+window.location.hostname+"/icon-get.php?slug="+slug+"&w=48&h=48",
            //             "sizes": "48x48",
            //             "type": "image/png",
            //             "density": "1.0"
            //         },
            //         {
            //             "src": "https://"+window.location.hostname+"/icon-get.php?slug="+slug+"&w=96&h=96",
            //             "sizes": "96x96",
            //             "type": "image/png",
            //             "density": "2.0"
            //         },
            //         {
            //             "src": "https://"+window.location.hostname+"/icon-get.php?slug="+slug+"&w=128&h=128",
            //             "sizes": "128x128",
            //             "type": "image/png",
            //             "density": "2.0"
            //         },
            //         {
            //             "src": "https://"+window.location.hostname+"/icon-get.php?slug="+slug+"&w=144&h=144",
            //             "sizes": "144x144",
            //             "type": "image/png",
            //             "density": "3.0"
            //         },
            //         {
            //             "src": "https://"+window.location.hostname+"/icon-get.php?slug="+slug+"&w=192&h=192",
            //             "sizes": "192x192",
            //             "type": "image/png",
            //             "density": "4.0"
            //         },
            //         {
            //             "src": "https://"+window.location.hostname+"/icon-get.php?slug="+slug+"&w=512&h=512",
            //             "sizes": "512x512",
            //             "type": "image/png",
            //             "density": "6.0"
            //         }
            //     ],
            //     "gcm_sender_id": "103953800507"
            // }
            // const stringManifest = JSON.stringify(myDynamicManifest);
            // const blob = new Blob([stringManifest], {type: 'application/json'});
            // const manifestURL = URL.createObjectURL(blob);
            if (window.location.pathname !== '/') {
                // const temp = document.querySelector('#mainManifest');
                // temp.setAttribute("href", manifestURL);
                document.querySelector("#mainManifest").setAttribute("href", "/php/manifest.php");
            } else {
                // const temp = document.querySelector('#mainManifest');
                // temp.setAttribute("href", '/manifest.json');
                document.querySelector("#mainManifest").setAttribute("href", "/manifest.json");
            }

            setTimeout(function () {
                if (INSTALL_APP_POPUP) {//INSTALL_APP_POPUP is declared globally
                    console.log(INSTALL_APP_POPUP); 
                    INSTALL_APP_POPUP.prompt(); //this will show the prompt from chrome
                }
            }, 3000);
        }

    }

    showModalInstall () {
        this.setState({
           installModal: true
        });
    }
    hideModalInstall = () => {
        this.setState({
            installModal: false
        });
    }
    showModalBerechtigungen () {
        this.setState({
            berechtigungenModal: true
        });
    }
    hideModalBerechtigungen = () => {
        this.setState({
            berechtigungenModal: false
        });
    }
    triggerInstallBtn = () => {
        var div = document.getElementById('download');
        div.innerHTML = 'Laden...';
        
        setTimeout(function () {

            if (INSTALL_APP_POPUP) {//INSTALL_APP_POPUP is declared globally
                console.log(INSTALL_APP_POPUP); 
                INSTALL_APP_POPUP.prompt(); //this will show the prompt from chrome
            } 

            div.innerHTML = 'Hinzufügen';
        }, 1500);
    }
    triggerInstallRedirect = () => {
        if(document.URL.indexOf("//app.") >= 0){
            var url = "https://"+window.location.host+window.location.pathname+"?install=1";
            window.location = url;
        } else {
            var url = "https://app."+window.location.host+window.location.pathname+"?install=1";
            window.location = url;
        }
    }
    showPrompt = () => {
        console.log("button clicked");
        document.getElementById('showPwaprompt').style.display = "block";
    } 
    next = () =>{
        this.hideModalBerechtigungen();
        this.showModalInstall();
    }

    categories = (cat) => {
        try {
            let localState = localStorage.getItem('state');
            if (localState) {
                localState = JSON.parse(localState).items.restaurant_items.items;

                return Object.keys(localState).map((category, index) => (
                    <li key={category}><a href={"#"+category}>{category}</a></li>
                ))
            }
        } catch (err) {
            console.log(err);
        }
    }

    navigation = (lat, long) =>{
        // If it's an iPhone..
        if( (navigator.platform.indexOf("iPhone") != -1) 
            || (navigator.platform.indexOf("iPod") != -1)
            || (navigator.platform.indexOf("iPad") != -1))
             window.open("maps://maps.google.com/maps?daddr="+lat+","+long+"&amp;ll=");
        else
             window.open("http://maps.google.com/maps?daddr="+lat+","+long+"&amp;ll=");
    }

    streetView = (address, lat, long) => {
        window.open("https://www.google.at/maps/place/"+address+"/@"+lat+","+long+",17z/");
    }

    render() {
        const { restaurant, pages } = this.props;

        if(document.URL.indexOf("?install=1") >= 0) {
            if(isChrome || isChromium){
                var install = <div onClick={() => { this.showModalBerechtigungen() }} className="installApp">Zum Bildschirm Hinzufügen<Ink duration="500" /></div>
            } 
            else if(isMobileSafari){
                var install = <div onClick={() => { this.showPrompt() }} className="installApp">Zum Bildschirm Hinzufügen<Ink duration="500" /></div>
            }
        } else {
            var install = <div onClick={() => { this.triggerInstallRedirect() }} className="installApp">Zum Bildschirm Hinzufügen<Ink duration="500" /></div>
        }

        //show if main app active 
        if(matchMedia('(display-mode: standalone)').matches == true) {
            var install = <div onClick={() => { this.openChrome() }} className="installApp">Zum Bildschirm Hinzufügen<Ink duration="500" /></div>
        }

        //hide if demo restaurant active
        if(document.URL.indexOf("?pwa=") >= 0) {
            var install = "";
        }

        return (
            <React.Fragment>

                <div className="footer-items pb-100">
                    
                    {pages.length > 0 ? (
                        <ul>
                            <h1>Quick Links</h1>
                            {pages.map(page => (
                                <li key={page.id}>
                                    <a onClick={() => window.location = "/pages/"+restaurant.name+"/"+page.slug}>{page.name}</a>
                                </li>
                            ))}
                        </ul>
                    ) : (
                        <ul>
                            <h1>Kategorien</h1>
                            {this.categories()}
                        </ul>
                    )}

                    <ul style={{listStyle:"none"}}>
                        <h1>Kontakt</h1>
                        {restaurant.address && <li style={{marginLeft:0}}><a onClick={() => this.streetView(restaurant.address, restaurant.latitude, restaurant.longitude)}><FontAwesomeIcon icon={faMapMarkerAlt} style={{marginRight:"5px", width:"15px"}} /> {restaurant.address}</a></li>}
                        
                        {restaurant.tel && <li style={{marginLeft:0}}><a href={"tel:"+restaurant.tel}><FontAwesomeIcon icon={faPhone} style={{marginRight:"5px", width:"15px"}} /> {restaurant.tel}</a></li>}
                        
                        {restaurant.email && <li style={{marginLeft:0}}><a href={"mailto:"+restaurant.email}><FontAwesomeIcon icon={faEnvelope} style={{marginRight:"5px", width:"15px"}} /> {restaurant.email}</a></li>}
                        
                        {restaurant.website && <li style={{marginLeft:0}}><a href={restaurant.website} target="_blank"><FontAwesomeIcon icon={faLink} style={{marginRight:"5px", width:"15px"}} /> {restaurant.website.replace("https://www.", "")}</a></li>}
                    </ul>

                    <ul>
                        <h1>{localStorage.getItem("itemsPagePaymentMethods")}</h1>
                        <div>
                            <i><img src="https://lieferservice123.com/assets/img/cash.svg" style={{height: "25px", width: "25px"}} /></i>
                            <i><img src="https://lieferservice123.com/assets/img/kreditkarte.svg" style={{height: "25px", width: "25px"}} /></i>
                            <i><img src="https://lieferservice123.com/assets/img/klarna.png" style={{height: "25px", width: "25px"}} /></i>
                            <i><img src="https://lieferservice123.com/assets/img/eps.png" style={{height: "25px", width: "25px"}} /></i>
                        </div>

                        {/* 
                            redirect to subdomain app.lieferservice123.com/storename....
                            this is better to use because you cant install store app and main app at the same time 
                        */}
                        {/* <div className="pt-50">{(isChrome || isChromium || isMobileSafari) && install}</div> */}

                        {/* install app */}
                        {matchMedia('(display-mode: standalone)').matches == false && (

                            <div style={{width:"100%", float:"left", margin:"30px 0"}}>
                                <div style={{textShadow: "0 1px 4px rgba(0,0,0,0.1)"}}>

                                    {/* <div className='faqHead'>{localStorage.getItem("firstScreenDownloadApp")}</div> */}

                                    {/* <div> */}
                                        {/* <div style={{fontSize:"10px", margin:"10px 0"}}>{localStorage.getItem("firstScreenDownloadAppSub")}</div>  */}
                                        {/* <div style={{fontSize:"10px", margin:"10px 0"}}>{localStorage.getItem("firstScreenDownloadAppWin")}</div> */}
                                    {/* </div> */}
            
                                    <a onClick={() => { this.showModalBerechtigungen(); }} style={{cursor:"pointer", margin:"4px", marginLeft:0, display: "inline-block"}}><img width="156px" alt="Google Play" src="https://lieferservice123.com/assets/img/playstore.png" /></a>
                                
                                    <a onClick={() => { this.showPrompt() }} style={{cursor:"pointer", margin:"4px", display: "inline-block"}}><img width="156px" alt="App Store" src="https://lieferservice123.com/assets/img/appstore.png" /></a>

                                    <div id="showPwaprompt" style={{display:"none"}}>
                                        <PWAPrompt debug={1} 
                                        copyTitle={"Add to Home Screen"} 
                                        copyBody={"This website has app functionality. Add it to your home screen to use it in fullscreen and while offline."}
                                        copyShareButtonLabel={"1) Press the 'Share' button"}
                                        copyAddHomeButtonLabel={"2) Press 'Add to Home Screen'"}
                                        copyClosePrompt={"Cancel"} 
                                        />
                                    </div>

                                    {/* app-berechtigungen */}
                                    <Modal show={this.state.berechtigungenModal} onHide={this.hideModalBerechtigungen}>
                                        
                                        <Modal.Header closeButton>
                                            <Modal.Title>{restaurant.name} App</Modal.Title>
                                        </Modal.Header>
                    
                                        <Modal.Body style={{padding:"0 25px 10px"}}>
                                            <div style={{marginTop:"12px"}}>
                                                <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />{restaurant.name} auf Desktop installieren.</div>

                                                <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Lieferung auf der Karte verfolgen.</div>
                                                
                                                <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Benutzerelebnis durch Schnelles Laden.</div>

                                                <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Push-Nachrichten erhalten.</div>

                                                <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Zuverlässig auch bei schlechter Internetverbindung.</div>

                                                <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Installation über Google Chrome.</div>
                                            </div>

                                        </Modal.Body>
                    
                                        <Modal.Footer>
                                            <Button variant="secondary" onClick={this.hideModalBerechtigungen}> 
                                                Schließen
                                            </Button> 
                                            <Button variant="primary" onClick={() => this.next()}>
                                                Weiter
                                            </Button>
                                        </Modal.Footer>
                                    </Modal>

                                    {/* app-installieren */}
                                    <Modal show={this.state.installModal} onHide={this.hideModalInstall}>
                                        
                                        <Modal.Header closeButton>
                                            <Modal.Title>Installieren</Modal.Title>
                                        </Modal.Header>
                    
                                        <Modal.Body style={{padding:"0 25px 10px"}}>
                                            <div style={{marginTop:"12px"}}>
                                                Klicke auf hinzufügen um {restaurant.name} auf deinen Bildschirm zu installieren.
                                            </div>

                                            <div style={{marginTop:"20px", fontSize:"11px", color:"#999"}}>Wenn dir {restaurant.name} gefällt, dann installiere bitte die {restaurant.name} App.</div>
                                        </Modal.Body>
                    
                                        <Modal.Footer>
                                            <Button variant="secondary" onClick={this.hideModalInstall}> 
                                                Schließen
                                            </Button> 
                                            <Button variant="primary" onClick={() => this.triggerInstallBtn()} id="download">
                                                Hinzufügen
                                            </Button>
                                        </Modal.Footer>
                                    </Modal>

                                </div>
                            </div>

                        ) }

                    </ul>



                    
                </div>

            </React.Fragment>
        );
    }
}


const mapStateToProps = state => ({
    pages: state.pages.pages,

});
const mapDispatchToProps = (dispatch, props) => ({
    dispatch: dispatch
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
    //{ placeOrder, getRestaurantInfo }
)(Footer);


