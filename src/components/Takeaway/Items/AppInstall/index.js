
import React, { Component } from "react";
import Ink from "react-ink";
import { connect } from "react-redux";
import BackWithSearch from "../../Elements/BackWithSearch";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUtensils } from '@fortawesome/free-solid-svg-icons';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { faPhoneAlt } from '@fortawesome/free-solid-svg-icons';
import { faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import { faStar } from '@fortawesome/free-solid-svg-icons';
import { faComment } from '@fortawesome/free-solid-svg-icons';


import { PLACE_ORDER } from "../../../../services/checkout/actionTypes";
import { PLACE_ORDER_URL } from "../../../../configs";
import { updateCart } from "../../../../services/total/actions";
import { placeOrder } from "../../../../services/checkout/actions";

import { getRestaurantInfo2 } from "../../../../services/items/actions";

declare var INSTALL_APP_POPUP;



class AppInstall extends Component {

    static contextTypes = {
        router: () => null
    };

    componentDidMount() {
        getRestaurantInfo2(window.location.href.split("/").pop(), this.props.dispatch, res=>{})
    }

    triggerInstallBtn = () => {
        if (INSTALL_APP_POPUP) {
            console.log(INSTALL_APP_POPUP); 
            INSTALL_APP_POPUP.prompt(); 
        }
    }

    getAddress = (address, country=false) => {
        if(country === false){
            var res = address;
            var split = res.split(",");
            var res = split.slice(0, split.length - 1).join(",");
            return res;
        }else{
            var res = address.split(", ");
            return res[0];
        }
    }    
    
    getCityPlz = (address) => {
        var res = address.split(", ");
        return res[1];
    }

    render() {
        const { cartTotal, total, restaurant_info, cartProducts} = this.props;

        return (
            <React.Fragment>

                <div className="header" style={{margin:"auto", position:"relative", background:(localStorage.getItem("storeColor")?localStorage.getItem("storeColor"):"#ff8000")}}>

                    {!this.props.back_to_home && !this.props.goto_orders_page && !this.props.goto_accounts_page && (
                        <a
                            style={{ color: "#fff", top: 20, left: 15, position: "absolute" }}
                            onClick={this.context.router.history.goBack}
                        >
                            <i className="si si-arrow-left" />
                            <Ink duration="500" />
                        </a>
                    )}

                    <a href={'/restaurants'}><FontAwesomeIcon style={{ color: "#fff", fontSize: "26px", top: 15, left: 35, position: "absolute" }} icon={faUtensils} /></a>

                    <center><a href={'/search-location'} style={{fontSize: "12px", fontWeight: "700", color: "#fff", borderBottom: "2px solid #fff", paddingBottom: "3px" }}>{localStorage.getItem("userSetAddress")?this.getAddress(JSON.parse(localStorage.getItem("userSetAddress")).address):"Wo möchtest Du Essen bestellen?"}</a></center>
                    

                    <FontAwesomeIcon onClick={() => {
                        this.props.loggedin
                            ? this.context.router.history.push("/my-account")
                            : this.context.router.history.push("/login");
                    }} style={{ color: "#fff", fontSize: "26px", top: 15, right: 15, position: "absolute" }} icon={faBars} />

                </div>


                {/* <div className={"js-random-class subheader__image subheader__image--pizza"}></div>

                <div className="restaurant-logo">
                    <div className="restaurant-logo__container">
                        <div className="restaurant-logo__inner">
                            <img src={restaurant_info.image} alt={restaurant_info.name} />
                        </div>
                    </div>
                </div> */}

                <div style={{padding:"15px", margin: "auto", maxWidth: "768px"}}>

                    <div style={{width:"100%", marginBottom: "16px", overflow:"auto"}}>
                        <div style={{float:"left", width:"110px"}}><FontAwesomeIcon style={{background:"#FF8800", color:"#fff", padding:"20px", fontSize:"20px", width:"100px", height:"100px"}} icon={faUtensils} /></div>

                        <div style={{float:"right", width:"calc(100% - 110px)"}}>

                            <div style={{fontSize:"22px", lineHeight:"24px", marginBottom:"10px", color:"#212121", fontFamily:"Open Sans", fontWeight:"300"}}>Lieferservice.ag - Order food</div>
                            
                            <div>
                                <span style={{color:"#33691e", fontWeight:"700", fontSize: "13px", marginRight:"15px"}}>Lieferservice GmbH</span> <span style={{color:"#33691e", fontWeight:"700", fontSize: "13px"}}>Food & Drink</span>
                            </div>
                            
                            <div style={{color: "#616161", fontWeight: 400, margin: "2px 0"}}><img src="/assets/img/pegi3.png" style={{width:"14px", marginRight:"8px"}} />PEGI 3</div>

                            <div>
                                <FontAwesomeIcon style={{color:"#737373", margin: "0 2px", height:"10px", width:"10px"}} icon={faStar} />
                                <FontAwesomeIcon style={{color:"#737373", margin: "0 2px", height:"10px", width:"10px"}} icon={faStar} />
                                <FontAwesomeIcon style={{color:"#737373", margin: "0 2px", height:"10px", width:"10px"}} icon={faStar} />
                                <FontAwesomeIcon style={{color:"#737373", margin: "0 2px", height:"10px", width:"10px"}} icon={faStar} />
                                <FontAwesomeIcon style={{color:"#737373", margin: "0 2px", height:"10px", width:"10px"}} icon={faStar} />
                                <span style={{marginLeft: "3px", color: "#616161", fontWeight: 400, fontSize: "13px"}}>14,451</span> <FontAwesomeIcon style={{marginLeft: "3px", fontSize: "10px", color: "#616161"}} icon={faUser} />
                            </div>
                        </div>
                    </div>

                    {/* install btn */}
                    <a href={"#"} style={{background:"#689f38", borderRadius:"4px", fontSize: "14px", lineHeight: "34px", padding: "12px 20px", border: "1px", color: "#fff", fontFamily:"'Roboto',Arial,sans-serif", fontWeight:"bold", cursor:"pointer"}} 
                    onClick={() => { this.triggerInstallBtn(); }}>Install</a>

                    {/* screenshot */}
                    <div><button style={{border:"1px solid #333", background:"#000", textAlign:"center", marginTop:"30px"}}><img src="/assets/img/app1.gif" style={{width:"293px", height:"220px"}} /></button></div>

                    {/* description */}
                    <div style={{fontWeight:"bold", margin:"30px 0 10px"}}>Lieferservice.at - Order food delivery</div>
                    <div style={{lineHeight: 1.4, color:"#333", fontFamily:"Open Sans", fontWeight:"300"}}>Simply order your takeaway via smartphone and choose from more than 1.500 delivery services in Austria. You enjoy pizza, sushi, burger or Asian food? With lieferservice you will always find the right takeaway. Enter your postcode or use your current location, create your favourite menu and order easily. Try it now and get the lieferservice App for your smartphone or tablet.</div>

                    {/* reviews */}
                    <div style={{marginTop:"30px", color:"#333", fontFamily:"Open Sans", fontWeight:"300", fontSize:"22px"}}>Reviews</div>

                    <div style={{marginTop:"10px", color:"#333", fontFamily:"Open Sans", fontWeight:"300", fontSize:"14px"}}>
                        
                        <div style={{marginBottom:"20px", float:"left", width:"100%"}}>
                            <div style={{float:"left", width:"48px", height:"48px", background:"#7B1FA2", color:"#fff", textAlign:"center", padding:"10px 20px", borderRadius: "24px", boxShadow: "0 1px 2px #8d8d8d", color: "#fff", fontSize: "18px", fontWeight: 500}}>K</div>
                            <div style={{float:"right", width:"calc(100% - 58px)"}}>
                                <div style={{fontWeight: 500}}>Kevin</div>
                                <div>Easy to use... Intuitive!</div>
                            </div>
                        </div>

                        <div style={{marginBottom:"20px", float:"left", width:"100%"}}>
                            <div style={{float:"left", width:"48px", height:"48px", background:"#7B1FA2", color:"#fff", textAlign:"center", padding:"10px 20px", borderRadius: "24px", boxShadow: "0 1px 2px #8d8d8d", color: "#fff", fontSize: "18px", fontWeight: 500}}>J</div>
                            <div style={{float:"right", width:"calc(100% - 58px)"}}>
                                <div style={{fontWeight: 500}}>Josef</div>
                                <div>Reliable app with good discounts</div>
                            </div>
                        </div>

                        <div style={{marginBottom:"20px", float:"left", width:"100%"}}>
                            <div style={{float:"left", width:"48px", height:"48px", background:"#7B1FA2", color:"#fff", textAlign:"center", padding:"10px 20px", borderRadius: "24px", boxShadow: "0 1px 2px #8d8d8d", color: "#fff", fontSize: "18px", fontWeight: 500}}>R</div>
                            <div style={{float:"right", width:"calc(100% - 58px)"}}>
                                <div style={{fontWeight: 500}}>Rene</div>
                                <div>As an english-only speaking expat in Austria, I was hoping this app would provide a way to order food without having to resolve to goohle-translate every sentence into the phone.... the app does of course have an option to change language, so an entire order was made in english</div>
                            </div>
                        </div>

                        <div style={{marginBottom:"20px", float:"left", width:"100%"}}>
                            <div style={{float:"left", width:"48px", height:"48px", background:"#7B1FA2", color:"#fff", textAlign:"center", padding:"10px 20px", borderRadius: "24px", boxShadow: "0 1px 2px #8d8d8d", color: "#fff", fontSize: "18px", fontWeight: 500}}>P</div>
                            <div style={{float:"right", width:"calc(100% - 58px)"}}>
                                <div style={{fontWeight: 500}}>Peter</div>
                                <div>As an english-only speaking expat in Austria, I was hoping this app would provide a way to order food without having to resolve to goohle-translate every sentence into the phone.... the app does of course have an option to change language, so an entire order was made in english</div>
                            </div>
                        </div>

                        <div style={{marginBottom:"20px", float:"left", width:"100%"}}>
                            <div style={{float:"left", width:"48px", height:"48px", background:"#7B1FA2", color:"#fff", textAlign:"center", padding:"10px 20px", borderRadius: "24px", boxShadow: "0 1px 2px #8d8d8d", color: "#fff", fontSize: "18px", fontWeight: 500}}>F</div>
                            <div style={{float:"right", width:"calc(100% - 58px)"}}>
                                <div style={{fontWeight: 500}}>Fabian</div>
                                <div>Great app! No issues so far. Works well and the interface is really pleasant.</div>
                            </div>
                        </div>

                        <div style={{marginBottom:"20px", float:"left", width:"100%"}}>
                            <div style={{float:"left", width:"48px", height:"48px", background:"#7B1FA2", color:"#fff", textAlign:"center", padding:"10px 20px", borderRadius: "24px", boxShadow: "0 1px 2px #8d8d8d", color: "#fff", fontSize: "18px", fontWeight: 500}}>J</div>
                            <div style={{float:"right", width:"calc(100% - 58px)"}}>
                                <div style={{fontWeight: 500}}>Jack</div>
                                <div>Simple and perfect</div>
                            </div>
                        </div>

                        <div style={{marginBottom:"20px", float:"left", width:"100%"}}>
                            <div style={{float:"left", width:"48px", height:"48px", background:"#7B1FA2", color:"#fff", textAlign:"center", padding:"10px 20px", borderRadius: "24px", boxShadow: "0 1px 2px #8d8d8d", color: "#fff", fontSize: "18px", fontWeight: 500}}>B</div>
                            <div style={{float:"right", width:"calc(100% - 58px)"}}>
                                <div style={{fontWeight: 500}}>Benjamin</div>
                                <div>very good offers in terms of choice and good coupons</div>
                            </div>
                        </div>

                        <div style={{marginBottom:"20px", float:"left", width:"100%"}}>
                            <div style={{float:"left", width:"48px", height:"48px", background:"#7B1FA2", color:"#fff", textAlign:"center", padding:"10px 20px", borderRadius: "24px", boxShadow: "0 1px 2px #8d8d8d", color: "#fff", fontSize: "18px", fontWeight: 500}}>S</div>
                            <div style={{float:"right", width:"calc(100% - 58px)"}}>
                                <div style={{fontWeight: 500}}>Steve</div>
                                <div>I order only from here whenever i order, because from my experience this is the only good place that has cash payment and overall it works good)) even if something goes wrong and you contact them by chat they immediately answer and help you.</div>
                            </div>
                        </div>

                    </div>
                
                </div>


            </React.Fragment>
        );
    }
}


const mapStateToProps = state => ({
    restaurant_info: state.items.restaurant_info,
    cartProducts: state.cart.products,
    cartTotal: state.total.data,
});
const mapDispatchToProps = (dispatch, props) => ({
    dispatch: dispatch
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
    //{ placeOrder, getRestaurantInfo }
)(AppInstall);


