import { ORDER_ADDONS_URL } from "../../../../configs/index";

import React, { Component } from "react";
import Ink from "react-ink";
import { connect } from "react-redux";
import BackWithSearch from "../../Elements/BackWithSearch";
import axios from "axios";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUtensils } from '@fortawesome/free-solid-svg-icons';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { faPhoneAlt } from '@fortawesome/free-solid-svg-icons'
import { faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons'

import { PLACE_ORDER } from "../../../../services/checkout/actionTypes";
import { PLACE_ORDER_URL } from "../../../../configs";
import { updateCart } from "../../../../services/total/actions";
import { placeOrder } from "../../../../services/checkout/actions";

import { getRestaurantInfo2 } from "../../../../services/items/actions";



class GuestAddons extends Component {

    state = {
        test: [],
    };

    componentDidMount() {
        this.orderList();
    }
    
    orderList = () => {
        const { id } = this.props;
        axios
            .post(ORDER_ADDONS_URL, {//change this url
                id: id
            })
            .then(response => {
                const test = response.data;
                this.setState({
                    test: [...this.state.test, ...test],
                });
            })
            .catch(function(error) {
                console.log(error);
            });
    }

    render() {
        const { currencyFormat } = this.props;

        return (
            <React.Fragment>
                
                {/* order list addons */}
                {this.state.test.map((row, index) => 

                    <li key={row.id} style={{width:"100%", marginLeft:"20px", fontSize: "0.8rem"}}>
                        {row.addon_name} ({row.addon_price} {currencyFormat})
                    </li>
                )}
                
            </React.Fragment>
        );
    }
}



const mapDispatchToProps = (dispatch, props) => ({
    dispatch: dispatch
})

export default connect(
    mapDispatchToProps
)(GuestAddons);


