import { ORDER_VIEW_URL } from "../../../../configs/index";

import React, { Component } from "react";
import Ink from "react-ink";
import { connect } from "react-redux";
import BackWithSearch from "../../Elements/BackWithSearch";
import axios from "axios";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUtensils } from '@fortawesome/free-solid-svg-icons';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { faPhoneAlt } from '@fortawesome/free-solid-svg-icons'
import { faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons'

import { PLACE_ORDER } from "../../../../services/checkout/actionTypes";
import { PLACE_ORDER_URL } from "../../../../configs";
import { updateCart } from "../../../../services/total/actions";
import { placeOrder } from "../../../../services/checkout/actions";

import { getRestaurantInfo2 } from "../../../../services/items/actions";

import GuestAddons from '../OrderListAddons/index.js';



class GuestCheckout extends Component {

    state = {
        test: [],
    };

    componentDidMount() {
        this.orderList();
    }
    
    orderList = () => {
        const { id } = this.props;
        axios
            .post(ORDER_VIEW_URL, {
                id: id
            })
            .then(response => {
                const test = response.data;
                this.setState({
                    test: [...this.state.test, ...test],
                });
            })
            .catch(function(error) {
                console.log(error);
            });
    }

    render() {
        const { currencyFormat } = this.props;

        return (
            <React.Fragment>
                
                {/* order list */}
                {this.state.test.map((row, index) => 
                    
                    <li key={row.id} style={{width:"100%", overflow:"auto", margin:"5px 0"}}>
                        <span style={{width:"calc(100% - 80px)", float:"left"}}>
                            <span style={{color:"#757575", width:"20px", float:"left"}}>{row.quantity}x</span> 
                            {row.name}
                            <GuestAddons currencyFormat={currencyFormat} id={row.id} />
                        </span>
                        <span style={{padding:"0 4px", width:"80px", float:"right", textAlign:"right"}}>{(row.price*row.quantity).toFixed(2)} {currencyFormat}</span> 
                    </li>
                )}
                
            </React.Fragment>
        );
    }
}



const mapDispatchToProps = (dispatch, props) => ({
    dispatch: dispatch
})

export default connect(
    mapDispatchToProps
)(GuestCheckout);


