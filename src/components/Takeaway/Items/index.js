import React, { Component } from "react";
import { getRestaurantInfo, getRestaurantItems, resetInfo, resetItems } from "../../../services/items/actions";
import axios from "axios";

import FloatCart from "../FloatCart";
import ItemList from "./ItemList";
import Footer from "./Footer";
import Meta from "../../helpers/meta";
import { Redirect } from "react-router";
import RestaurantInfo from "./RestaurantInfo";
import { connect } from "react-redux";
import { getSettings } from "../../../services/settings/actions";
import { clearPages, getPages } from "../../../services/pages/actions";

class Items extends Component {

    state = {
        is_active: 1
    };

    componentDidMount() {
        
        //set country if not set
        if (!localStorage.getItem("country")) {
            this.getCountry();
        }

        //if one config is missing then call the api to fetch settings
        if (!localStorage.getItem("storeColor")) {
            this.props.getSettings();
        }

        //if currentLocation doesnt exists in localstorage then redirect the user to firstscreen
        //else make API calls
        // if (localStorage.getItem("userSetAddress") === null) {
        //     this.props.history.push("/search-location");
        // } else {
            //call to promoSlider API to fetch
            this.props.getRestaurantInfo(this.props.match.params.restaurant);

            console.log("act", this.getUrlParameter('act'));

            if(localStorage.getItem("activeShop") !== this.props.match.params.restaurant){
                this.props.resetItems(); //fix showing items from another shop while loading
                this.props.clearPages(); //fix showing pages from another shop

                setTimeout(() => {
                    if(JSON.parse(localStorage.getItem("state")).items.restaurant_info.id){
                        this.props.getPages(JSON.parse(localStorage.getItem("state")).items.restaurant_info.id);
                    }
                }, 100);
                
            } else if(localStorage.getItem("activeAct") == '' && this.getUrlParameter('act') == undefined){
                //dont reset items
            } else if(localStorage.getItem("activeAct") !== this.getUrlParameter('act') ){
                this.props.resetItems(); //fix showing items from another shop while loading
            }

            if(this.getUrlParameter('act') === 'category'){
                localStorage.setItem("activeAct", "category");
                this.props.getRestaurantItems(this.props.match.params.restaurant, "category", atob(this.getUrlParameter('catType')));
            } 
            else if(this.getUrlParameter('act') === 'sub_category'){
                localStorage.setItem("activeAct", "sub_category");
                this.props.getRestaurantItems(this.props.match.params.restaurant, "sub_category", atob(this.getUrlParameter('catType')));
            } 
            else if(this.getUrlParameter('act') === 'sub_category2'){
                localStorage.setItem("activeAct", "sub_category2");
                this.props.getRestaurantItems(this.props.match.params.restaurant, "sub_category2", atob(this.getUrlParameter('catType')));
            } 
            else {
                localStorage.setItem("activeAct", "");
                this.props.getRestaurantItems(this.props.match.params.restaurant);
            }
        //}

        if (localStorage.getItem("userSelected") === null) {
            localStorage.setItem("userSelected", "DELIVERY");
        }
    }

    componentWillReceiveProps(nextProps) {
        
        if (nextProps.restaurant_info.slug !== undefined) {
            localStorage.setItem("activeShop", nextProps.restaurant_info.slug);   
            if(!localStorage.getItem("activeRestaurant")){
                localStorage.setItem("activeRestaurant", nextProps.restaurant_info.id); 
            }

            if (nextProps.restaurant_info.delivery_type === 2 && localStorage.getItem("userSelected") === "DELIVERY") {
                localStorage.setItem("userSelected", "SELFPICKUP");
            }

            if (nextProps.restaurant_info.delivery_type === 1 && localStorage.getItem("userSelected") === "SELFPICKUP") {
                localStorage.setItem("userSelected", "DELIVERY");
            }

            this.setState({ is_active: nextProps.restaurant_info.is_active });

            if(JSON.parse(localStorage.getItem("state")).items.restaurant_info.id){
                this.props.getPages(JSON.parse(localStorage.getItem("state")).items.restaurant_info.id);
            }
        }
    }

    componentWillUnmount() {
        // this.props.resetItems();
        this.props.resetInfo();
    }

    getUrlParameter(sParam) {
        var sPageURL =decodeURIComponent(window.location.search.substring(1));
        // console.log(sPageURL);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam) {
                return sParameterName[1];
            }
        }
    }

    getDayName = () => {
        switch (new Date().getDay()) {
            case 0:
                return "sonntag"
            case 1:
                return "montag"
            case 2:
                return "dienstag"
            case 3:
                return "mittwoch"
            case 4:
                return "donnerstag"
            case 5:
                return "freitag"
            case 6:
                return "samstag"
        }
    }

    openingHours = (openFrom="14:00", openTo="03:00") => {
        //now date
        var now = new Date(); 

        //given date
        var h = now.getHours();
        var day = now.getDate();
        var month = ('0' + (now.getMonth() + 1)).slice(-2)
        var year = now.getUTCFullYear();

        //function slice() removes last 3 char from openTo to only get h without min
        //check if after midnight because date comparison should not be like 2020/01/09 14:00 - 2020/01/09 03:00
        
        if (h < 8 && openTo.slice(0, -3) < 7){//if openTo is after midnight && current time is after midgnight
            var from = new Date(year + "/" + month + "/" + day + " " + openFrom);
            var from = this.addDays2(from, -1);
            var to = new Date(year + "/" + month + "/" + day + " " + openTo); 
        }else if (openTo.slice(0, -3) < 7){//if openTo is after midnight && current time is NOT after midgnight
            var from = new Date(year + "/" + month + "/" + day + " " + openFrom);
            var to = new Date(year + "/" + month + "/" + day + " " + openTo); 
            var to = this.addDays2(to, 1);
        }else{
            var from = new Date(year+"/"+month+"/"+day+" "+openFrom); //set current date and time from table of restaurant 
            var to   = new Date(year+"/"+month+"/"+day+" "+openTo); //set current date and time from table of restaurant
        }

        var today = now.getFullYear()+'/'+(now.getMonth()+1)+'/'+now.getDate()+' '+now.getHours()+":"+now.getMinutes(); 
        var check = new Date(today); //this must always be current date, never change

        //return from+" - "+to;
        if (check > from && check < to)
            return ""; 
        else 
            return "geschlossen"; 
            
    }

    addDays2 = (date, amount) => {
        var tzOff = date.getTimezoneOffset() * 60 * 1000,
            t = date.getTime(),
            d = new Date(),
            tzOff2;
      
        t += (1000 * 60 * 60 * 24) * amount;
        d.setTime(t);
      
        tzOff2 = d.getTimezoneOffset() * 60 * 1000;
        if (tzOff != tzOff2) {
          var diff = tzOff2 - tzOff;
          t += diff;
          d.setTime(t);
        }
      
        return d;
    }

    //lang and country
    getCountry = () => {
        axios
            .get("https://lieferservice123.com/php/country.php", {
            })
            .then(response => {
                console.log("country", response.data);
                localStorage.setItem("country", response.data);
                if(response.data == "AT"){
                    localStorage.setItem("phoneCountryCode", "43");
                    localStorage.setItem("userPreferedLanguage", "1");
                } 
                else if(response.data == "DE"){
                    localStorage.setItem("phoneCountryCode", "49");
                    localStorage.setItem("userPreferedLanguage", "1");
                } 
                else if(response.data == "CH"){
                    localStorage.setItem("phoneCountryCode", "41");
                    localStorage.setItem("userPreferedLanguage", "1");
                } else {
                    localStorage.setItem("userPreferedLanguage", "2");
                }

                setTimeout(function () {
                    var url = window.location.href;
                    window.location = url;
                }, 100);
            })
            .catch(function(error) {
                console.log(error);
            });
    }

    render() {
        // if (localStorage.getItem("storeColor") === null) {
        //     return <Redirect to={"/"} />;
        // }
        return (
            <React.Fragment>
                <Meta
                    seotitle={`${this.props.restaurant_info.name} | ${localStorage.getItem("seoMetaTitle")}`}
                    seodescription={localStorage.getItem("seoMetaDescription")}
                    ogtype="website"
                    ogtitle={`${this.props.restaurant_info.name} | ${localStorage.getItem("seoOgTitle")}`}
                    ogdescription={localStorage.getItem("seoOgDescription")}
                    ogurl={window.location.href}
                    twittertitle={`${this.props.restaurant_info.name} | ${localStorage.getItem("seoTwitterTitle")}`}
                    twitterdescription={localStorage.getItem("seoTwitterDescription")}
                />
                <div key={this.props.match.params.restaurant}>
                    <RestaurantInfo history={this.props.history} restaurant={this.props.restaurant_info} withLinkToRestaurant={false} />
                    <ItemList data={this.props.restaurant_items} restaurant={this.props.restaurant_info} />
                    <Footer restaurant={this.props.restaurant_info} />
                </div>
                {this.props.restaurant_info.certificate && (
                    <div className="mb-100 text-muted font-w600 text-center">
                        {localStorage.getItem("certificateCodeText")} {this.props.restaurant_info.certificate}
                    </div>
                )}
                <div>
                    {this.state.is_active ? (
                        (this.props.restaurant_info[this.getDayName()]) ? 
                            ((this.props.restaurant_info[this.getDayName()] == "geschlossen" || this.openingHours(
                                this.props.restaurant_info[this.getDayName()].slice(0, 5),//from ex: 10:00
                                this.props.restaurant_info[this.getDayName()].slice(-5)//to ex: 23:00
                            ) == "geschlossen") ? <div className="auth-error no-click"><div className="error-shake">{this.props.restaurant_info.name} ist momentan geschlossen. Bitte versuche es morgen wieder oder wähle einen anderen Anbieter.</div></div> : <FloatCart restaurant={this.props.restaurant_info} />) 
                            : null
                    ) : (
                        <div className="auth-error no-click">
                            <div className="error-shake">{localStorage.getItem("notAcceptingOrdersMsg")}</div>
                        </div>
                    )}
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    restaurant_info: state.items.restaurant_info,
    restaurant_items: state.items.restaurant_items
});

export default connect(
    mapStateToProps,
    {
        getSettings,
        getRestaurantInfo,
        getRestaurantItems,
        resetItems,
        resetInfo,
        clearPages,
        getPages
    }
)(Items);