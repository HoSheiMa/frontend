import { ORDER_LIST_URL } from "../../../../configs/index";
//import { UPDATE_ORDER } from "../../../../configs/index";
import React, { Component } from "react";
import Ink from "react-ink";
import { connect } from "react-redux";
import BackWithSearch from "../../Elements/BackWithSearch";
import axios from "axios";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUtensils } from '@fortawesome/free-solid-svg-icons';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { faPhoneAlt } from '@fortawesome/free-solid-svg-icons'
import { faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons'
import { faDirections} from '@fortawesome/free-solid-svg-icons';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { faCheck, faPrint } from '@fortawesome/free-solid-svg-icons';


import { getRestaurantInfo } from "../../../../services/items/actions";

import Modal from 'react-bootstrap/Modal';
import ReactDOM from 'react-dom';
import { Button } from 'react-bootstrap';

import Nav from "../../Nav";

import { getSingleLanguageData } from "../../../../services/translations/actions";

import ReCAPTCHA from "react-google-recaptcha";
import ReactTooltip from 'react-tooltip'

import PWAPrompt from 'react-ios-pwa-prompt'
import { isChrome, isChromium, isMobileSafari } from 'react-device-detect';

import GuestCheckout from '../OrderListView/index.js';

import QRCode from 'qrcode.react';


declare var INSTALL_APP_POPUP;


class OrderList extends Component {


    constructor(props) {
        super(props);
        this.state = { 
            textIdx: 0,
            test: [],
            isOpen: false,
            verifyModal: false,
            verifyModal2: false,
            acceptModal: false,
            rejectModal: false,
            smsModal: false,
            captchaModal: false,
            bgWidth: window.innerWidth,
            bgHeight: (window.innerHeight / 4.2),
            installModal: false,
            berechtigungenModal: (document.URL.indexOf("?install=1") >= 0 ? true:false),
            activatePremium: false

        };
    }

    static contextTypes = {
        router: () => null
    };

    showModal (id) {
        this.setState({
           isOpen: {
              [id]: true
           }
        });
    }
    hideModal = () => {
        this.setState({
            isOpen: false
        })
    }

    showModalVerify (id) {
        this.hideModal();
        this.setState({
           verifyModal: {
              [id]: true
           }
        });
    }
    hideModalVerify = () => {
        this.setState({
            verifyModal: false
        });
    }

    showModalVerify2 (id) {
        this.hideModal();
        this.setState({
           verifyModal2: {
              [id]: true
           }
        });
    }
    hideModalVerify2 = () => {
        this.setState({
            verifyModal2: false
        });
    }

    showModalAccept (id) {
        this.hideModal();
        this.setState({
            acceptModal: {
              [id]: true
           }
        });
    }
    hideModalAccept = () => {
        this.setState({
            acceptModal: false
        });
    }

    showModalReject (id) {
        this.hideModal();
        this.setState({
            rejectModal: {
              [id]: true
           }
        });
    }
    hideModalReject = () => {
        this.setState({
            rejectModal: false
        });
    }

    showModalSms (id) {
        this.setState({
            smsModal: {
              [id]: true
           }
        });
    }
    hideModalSms = () => {
        this.setState({
            smsModal: false
        });
    }

    showModalCaptcha (id) {
        this.setState({
            captchaModal: {
              [id]: true
           }
        });
    }
    hideModalCaptcha = () => {
        this.setState({
            captchaModal: false
        });
    }


    showModalInstall () {
        this.setState({
           installModal: true
        });
    }
    hideModalInstall = () => {
        this.setState({
            installModal: false
        });
    }

    showModalBerechtigungen () {
        this.setState({
            berechtigungenModal: true
        });
    }
    hideModalBerechtigungen = () => {
        this.setState({
            berechtigungenModal: false
        });
    }

    printDiv() {
        var content = document.getElementById("printDiv");
        var pri = document.getElementById("ifmcontentstoprint").contentWindow;
        pri.document.open();
        pri.document.write(content.innerHTML);
        pri.document.close();
        pri.focus();
        pri.print()
    }

    componentDidMount() {

        // to get the restaurant info using the slug (new freelancer func)
        this.props.getRestaurantInfo(window.location.href.split("/").pop());

        // checking every 100ms if we get data then pull order tables (new freelancer func)
        const tempTimeInstance = setInterval(() => {
            if (this.props.restaurant_info && this.props.restaurant_info.id) {
                this.orderTables(this.props.restaurant_info);
                // clearing the timeout after we got the data
                clearInterval(tempTimeInstance);
            }
        }, 100);

        //lang
        this.props.getSingleLanguageData(localStorage.getItem("userPreferedLanguage")?localStorage.getItem("userPreferedLanguage"):1);

        //sound
        var notification = document.createElement('audio');
        notification.setAttribute('src', '//'+window.location.hostname+'/assets/backend/audio/new-order.mp3');
        notification.setAttribute('type', 'audio/mp3');
        notification.setAttribute('muted', 'muted');

        //check for new order every 10 sec
        this.timeout = setInterval(() => {
            this.refreshNewOrders(notification);
        }, 10000);

        //img width, height
        this.timeout = setInterval(() => {
            this.setState({ 
                bgWidth: window.innerWidth,
                bgHeight: (window.innerHeight / 4.2)
            });
            
        }, 1000);


        //install app for restaurant
        if(document.URL.indexOf("?install=1") >= 0){
            if (window.location.pathname !== '/') {
                document.querySelector("#mainManifest").setAttribute("href", "/php/manifest.php");
            } else {
                document.querySelector("#mainManifest").setAttribute("href", "/manifest.json");
            }

            setTimeout(function () {
                if (INSTALL_APP_POPUP) {//INSTALL_APP_POPUP is declared globally
                    console.log(INSTALL_APP_POPUP); 
                    INSTALL_APP_POPUP.prompt(); //this will show the prompt from chrome
                }
            }, 3000);
        }

        if(document.URL.indexOf("?code=") >= 0 && !localStorage.getItem("code") ){
            console.log("set code");
            localStorage.setItem("code", this.getUrlParameter("code"));
        }
    }

    titleCase(str) {
        var splitStr = str.toLowerCase().split(' ');
        for (var i = 0; i < splitStr.length; i++) {
            // You do not need to check if i is larger than splitStr length, as your for does that for you
            // Assign it back to the array
            splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);     
        }
        // Directly return the joined string
        return splitStr.join(' '); 
    }

    triggerInstallBtn = () => {
        var div = document.getElementById('download');
        div.innerHTML = 'Laden...';
        
        setTimeout(function () {

            if (INSTALL_APP_POPUP) {//INSTALL_APP_POPUP is declared globally
                console.log(INSTALL_APP_POPUP); 
                INSTALL_APP_POPUP.prompt(); //this will show the prompt from chrome
            } 

            div.innerHTML = 'Hinzufügen';
        }, 1500);
    }

    triggerInstallRedirect = () => {
        if(document.URL.indexOf("//app.") >= 0){
            var url = "https://"+window.location.host+window.location.pathname+"?install=1";
            window.location = url;
        } else {
            var url = "https://app."+window.location.host+window.location.pathname+"?install=1";
            window.location = url;
        }
    }

    // componentWillReceiveProps=(props)=>{
        
    // }
    
    orderTables = (restaurant_info) => {
        axios
            .post(ORDER_LIST_URL, {
                id: restaurant_info.id
            })
            .then(response => {
                const test = response.data;
                if (response.data.length) {
                    this.setState({
                        test: [...this.state.test, ...test],
                        no_orders: false
                    });
                } else {
                    this.setState({
                        no_orders: true
                    });
                }
            })
            .catch(function(error) {
                console.log(error);
            });
    }

    refreshNewOrders = (notification) =>{
            const { restaurant_info } = this.props;
            axios
            .post(ORDER_LIST_URL, {
                id: restaurant_info.id
            })
            .then (newArray => {
    			if (newArray.data.length) {
                    if (JSON.stringify(this.state.test) !== JSON.stringify(newArray.data)) {
                        this.setState({
                            test: newArray.data,
                            no_orders: false
                        });

                        notification.play();
                    }
    
                } else {
                    this.setState({
                        no_orders: true
                    });
                }
            })
            .catch(function(error) {
                console.log(error);
            });
    }

    timer = (countDownDate, order_id, orderstatus_id) =>{
        // Set the date we're counting down to
        //var countDownDate = new Date("Feb 7, 2020 01:55:00").getTime();
        var countDownDate = new Date(this.toDate(countDownDate)).getTime();

            // Get today's date and time
            var now = new Date().getTime();
            
            // Find the distance between now and the count down date
            var distance = countDownDate - now;
            
            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);
            
            // If the count down is finished, write some text
            if (distance < 0) {
                // if(orderstatus_id == 1){
                //     //update order set statusid = 8 where id = 'order_id'

                //     axios
                //     .post(UPDATE_ORDER, {
                //         id: order_id
                //     })
                //     .then (newArray => {
                //         console.log(newArray.data);
                //     })
                //     .catch(function(error) {
                //         console.log(error);
                //     });
                // }

                return "expired";
            }else{
                //return days + "d " + hours + "h " + minutes + "m " + seconds + "s ";
                return minutes;
            }
    }


    getAddress = (address, extract, long=true) => {
        if(extract == "str+nr"){
            var res = address;
            var split = res.split(",");
            if(long) var res = split.slice(0, split.length - 1).join(",");
            else var res = split[0];
            return res;
        } else if(extract == "str"){
            var res = address;
            var split = res.split(",");
            var res = split[0];
            return res;
        }  else if(extract == "plz"){
            var res = address;
            var split = res.split(",");
            var res = split[1];
            return res;
        } else {
            var res = address.split(", ");
            return res[0];
        }
    }

    elapsedTime = (time) => {
        switch (typeof time) {
            case 'number':
            break;
            case 'string':
            time = +new Date(time);
            break;
            case 'object':
            if (time.constructor === Date) time = time.getTime();
            break;
            default:
            time = +new Date();
        }
        var time_formats = [
            [60, 's', 1], // 60
            [120, 'vor 1 min', '1 minute from now'], // 60*2
            [3600, 'min', 60], // 60*60, 60
            [7200, 'vor 1 h', '1 hour from now'], // 60*60*2
            [86400, 'h', 3600], // 60*60*24, 60*60
            [172800, 'Gestern', 'Tomorrow'], // 60*60*24*2
            [604800, 'Tagen', 86400], // 60*60*24*7, 60*60*24
            [1209600, 'Letzte Woche', 'Next week'], // 60*60*24*7*4*2
            [2419200, 'Wochen', 604800], // 60*60*24*7*4, 60*60*24*7
            [4838400, 'Letzten Monat', 'Next month'], // 60*60*24*7*4*2
            [29030400, 'Monaten', 2419200], // 60*60*24*7*4*12, 60*60*24*7*4
            [58060800, 'Letztes Jahr', 'Next year'], // 60*60*24*7*4*12*2
            [2903040000, 'Jahren', 29030400], // 60*60*24*7*4*12*100, 60*60*24*7*4*12
        ];
        var seconds = (+new Date() - time) / 1000,
            token = 'vor',
            list_choice = 1;

        if (seconds == 0) {
            return 'Gerade jetzt'
        }
        if (seconds < 0) {
            seconds = Math.abs(seconds);
            token = 'ab jetzt';
            list_choice = 2;
        }
        var i = 0,
            format;
        while (format = time_formats[i++])
            if (seconds < format[0]) {
            if (typeof format[2] == 'string')
                return format[list_choice];
            else
                return token + ' ' + Math.floor(seconds / format[2]) + ' ' + format[1];
            }
        return time;
    }

    toTimestamp = (strDate) =>{
        var datum = Date.parse(strDate);
        return datum/1000;
    }    
    
    toDate = (unix_timestamp) => {
        var d = new Date(unix_timestamp * 1000);
        return d.getFullYear() + '-' + (d.getMonth()+1) + '-' + d.getDate() + ' ' + d.getHours() + ':' + d.getMinutes();
    }

    navigation = (lat, long) =>{
        // If it's an iPhone..
        if( (navigator.platform.indexOf("iPhone") != -1) 
            || (navigator.platform.indexOf("iPod") != -1)
            || (navigator.platform.indexOf("iPad") != -1))
             window.open("maps://maps.google.com/maps?daddr="+lat+","+long+"&amp;ll=");
        else
             window.open("http://maps.google.com/maps?daddr="+lat+","+long+"&amp;ll=");
    }

    sendCode = (id) => {
        console.log("code will be sent");

        fetch('//'+window.location.hostname+'/php/twilio/OrderList_sendCode.php?id='+id, {
            method: 'get',
            // may be some code of fetching comes here
        }).then(function(response) {
                if (response.status >= 200 && response.status < 300) {
                    //return response.text()

                    var div = document.getElementById('result');
                    div.innerHTML = '<span style="color:green">Sie werden jetzt angerufen.</span>';
                }
            })
            .then(function(response) {
                //console.log(response);
            })
    }

    accept = (restaurant_id, code, order_id) => {
        console.log("checking code "+code);

        axios
        .get('//'+window.location.hostname+'/php/twilio/OrderList_checkCode.php?restaurant_id='+restaurant_id+'&code='+code+'&order_id='+order_id+'&action=accept')
        .then(response => {
            //console.log('response', response.data);

            var div = document.getElementById('result');
            
            if(response.data.success == true){
                div.innerHTML = '<span style="color:green">Bestellung wurde bestätigt.</span>';
                localStorage.setItem("code", code);

                setTimeout(function () {
                    window.location.reload(false);
                }, 1500);
            }else{
                div.innerHTML = '<span style="color:red">Bestätigung fehlgeschlagen.</span>';
                //console.log("showModalVerify again");
            }
        })
        .catch(function(error) {
            console.log(error);
        });
    }

    reject = (restaurant_id, code, order_id) => {
        console.log("checking code "+code);

        axios
        .get('//'+window.location.hostname+'/php/twilio/OrderList_checkCode.php?restaurant_id='+restaurant_id+'&code='+code+'&order_id='+order_id+'&action=reject')
        .then(response => {
            //console.log('response', response.data);

            var div = document.getElementById('result');
            
            if(response.data.success == true){
                div.innerHTML = '<span style="color:green">Bestellung wurde abgelehnt.</span>';
                localStorage.setItem("code", code);

                setTimeout(function () {
                    window.location.reload(false);
                }, 1500);
            }else{
                div.innerHTML = '<span style="color:red">Ablehnen fehlgeschlagen.</span>';
                //console.log("showModalVerify again");
            }
        })
        .catch(function(error) {
            console.log(error);
        });
    }

    onCodeChanged = (e) => {
        this.setState({
            verificationCode: e.currentTarget.value
        });
    }

    onTelChanged = (e) => {
        this.setState({
            toTel: e.currentTarget.value
        });
    }

    onCaptchaChanged = (val, id) => {
        console.log(val);
        this.setState({
            captchaCodeNew: val
        });

        if (val){
            this.hideModalCaptcha();
            console.log("captcha solved sucessfully");

            const { restaurant_info } = this.props;
            this.sendCode(restaurant_info.id);
        }
    }

    sendSMS = (tel, body) => {
        var ua = navigator.userAgent.toLowerCase();
        var href;

        var lines = tel.split(/\n/);
        var texts = [];
        for (var i=0; i < lines.length; i++) {
            // only push this line if it contains a non whitespace character.
            if (/\S/.test(lines[i])) {
                texts.push(lines[i]);
            }
        }
        console.log('tel list: '+texts);
        
        ////stackoverflow.com/questions/6480462/how-to-pre-populate-the-sms-body-text-via-an-html-link
        if (ua.indexOf("iphone") > -1 || ua.indexOf("ipad") > -1){
            href = 'sms:/' + tel + '/&body=' + encodeURIComponent(body);
        }else{
            //href = 'sms://' + tel + '?body=' + encodeURIComponent(body);
            href = 'sms://' + texts + '?body=' + encodeURIComponent(body);
        }

        window.open(href);
    }

    showPrompt = () => {
        console.log("button clicked");
        document.getElementById('showPwaprompt').style.display = "block";
    } 


    next = () =>{
        this.hideModalBerechtigungen();
        this.showModalInstall();
    }
  
    openChrome = () =>{
        if(document.URL.indexOf("//app.") >= 0){
            var url = "https://"+window.location.host+window.location.pathname+"?install=1";
        } else {
            var url = "https://app."+window.location.host+window.location.pathname+"?install=1";
        }

        window.open(url);
    }

    getUrlParameter(sParam) {
        var sPageURL =decodeURIComponent(window.location.search.substring(1));
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam) {
                return sParameterName[1];
            }
        }
    }

    getMinOnly = (lat, lng, restaurant_lat, restaurant_lng, delivery_radius)=>{
        var distance = this.getDistance(lat, lng, restaurant_lat, restaurant_lng, delivery_radius);

        var percent = distance * 100 / delivery_radius;

        if(percent >= 90){
            var min = 60;
        } else if(percent >= 80){
            var min = 55;
        } else if(percent >= 70){
            var min = 50;
        } else if(percent >= 60){
            var min = 45;
        } else {
            var min = 40;
        }

        return min;
    }

    //check deliver radius
    degrees_to_radians = (degrees) =>{
        var pi = Math.PI;
        return degrees * (pi/180);
    }
    getDistance = (latitudeFrom, longitudeFrom, latitudeTo, longitudeTo) =>{
        var latFrom = this.degrees_to_radians(latitudeFrom);
        var lonFrom = this.degrees_to_radians(longitudeFrom);
        var latTo = this.degrees_to_radians(latitudeTo);
        var lonTo = this.degrees_to_radians(longitudeTo);

        var latDelta = latTo - latFrom;
        var lonDelta = lonTo - lonFrom;

        var angle = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(latDelta / 2), 2) +
            Math.cos(latFrom) * Math.cos(latTo) * Math.pow(Math.sin(lonDelta / 2), 2)));
        return (angle * 6371);
    }

    capitalizeFLetter = (name) => { 
        var name = name.charAt(0).toUpperCase() + name.slice(1)
        return name;
    } 

    render() {
        const { restaurant_info } = this.props;
        var body = "Hallo, du kannst jetzt auch über lieferservice.vip/"+restaurant_info.slug+" online essen bestellen. Danke für Ihren Besuch bei "+restaurant_info.name;
        const {bgWidth, bgHeight} = this.state;

        const {test} = this.state;

        if(document.URL.indexOf("?install=1") >= 0) {
            if(isChrome || isChromium){
                var install = <div onClick={() => { this.showModalBerechtigungen() }} style={{position:"relative", color:"#fff", boxShadow: "0 1px 2px 0 #ff6a00", background:"linear-gradient(90deg,#fd8d42,#ff6a00)", padding:"8px 10px 4px", borderRadius:".2rem", fontWeight:"600", fontFamily:"'Google Sans', sans-serif", cursor:"pointer", margin: "10px auto", width: "220px", textAlign: "center"}}>Zum Bildschirm Hinzufügen<Ink duration="500" /></div>
            } 
            else if(isMobileSafari){
                var install = <div onClick={() => { this.showPrompt() }} style={{position:"relative", color:"#fff", boxShadow: "0 1px 2px 0 #ff6a00", background:"linear-gradient(90deg,#fd8d42,#ff6a00)", padding:"8px 10px 4px", borderRadius:".2rem", fontWeight:"600", fontFamily:"'Google Sans', sans-serif", cursor:"pointer", margin: "10px auto", width: "220px", textAlign: "center"}}>Zum Bildschirm Hinzufügen<Ink duration="500" /></div>
            }
        } else {
            var install = <div onClick={() => { this.triggerInstallRedirect() }} style={{position:"relative", color:"#fff", boxShadow: "0 1px 2px 0 #ff6a00", background:"linear-gradient(90deg,#fd8d42,#ff6a00)", padding:"8px 10px 4px", borderRadius:".2rem", fontWeight:"600", fontFamily:"'Google Sans', sans-serif", cursor:"pointer", margin: "10px auto", width: "220px", textAlign: "center"}}>Zum Bildschirm Hinzufügen<Ink duration="500" /></div>
        }
        
        //show if main app active 
        if(matchMedia('(display-mode: standalone)').matches == true) {
            var install = <div onClick={() => { this.openChrome() }} style={{position:"relative", color:"#fff", boxShadow: "0 1px 2px 0 #ff6a00", background:"linear-gradient(90deg,#fd8d42,#ff6a00)", padding:"8px 10px 4px", borderRadius:".2rem", fontWeight:"600", fontFamily:"'Google Sans', sans-serif", cursor:"pointer", margin: "10px auto", width: "220px", textAlign: "center"}}>Zum Bildschirm Hinzufügen<Ink duration="500" /></div>
        }

        //hide if demo restaurant active
        if(document.URL.indexOf("?pwa=") >= 0) {
            var install = "";
        }

        // First of all, you shouldn't use javascript innerhtml.
        // react supports great functionalities, element id isn;'t needed.
        // test is changed, - means, status is changed, it render,
        // so I made the elements based on test.
        // newOrdersDataElements  is this.
        // and I draw newOrdersDataElements
        // I think it's not correct.
        var newOrdersDataElements = (orderstatus_id) => {
            const {test} = this.state;

            var isAvailable = false;

            var result = test.map((row, index) => {

                var subTotal = (row.total - row.delivery_charge + parseFloat(row.discount)).toFixed(2);
                var discountPercent = (row.discount * 100 / subTotal).toFixed();

                var stillTime = this.timer(row.added+60*10, row.id, row.orderstatus_id) == "expired" ? false : true;

                if(row.orderstatus_id == 1 || row.orderstatus_id == 7) {
                    var color = "green"; 
                } else if(row.orderstatus_id == 2)  {
                    var color = "rgb(131, 96, 195)"; 
                } else if(row.orderstatus_id == 6 || row.orderstatus_id == 8)  {
                    var color = "red"; 
                } else  {
                    var color = "white";
                } 

                if(orderstatus_id == null || orderstatus_id == undefined)
                    return;
                 
                const idList = (orderstatus_id+"").split(",");//missing data
                if(idList.includes(row.orderstatus_id+"")){
                    isAvailable = true;
                    return (
                        
                        <div key={row.id} style={{margin:"10px auto", maxWidth:"768px", background:"#fff", overflow:"auto", fontSize:"0.98rem", marginBottom:"0.625rem", border: "1px solid rgba(0,0,0,0.15)", borderLeft: "3px solid "+color, boxShadow: "0 2px 3px rgba(0,0,0,0.06)", padding:"8px 12px", borderRadius: "1px"}}>

                        <div style={{width:"100%", float:"left", cursor:"pointer", position:"relative"}} onClick={this.showModal.bind(this, row.id)}>

                            <div style={{float:"right", width:"80px", textAlign:"right", fontWeight:"bold"}}>{this.elapsedTime(this.toDate(row.added))}</div>

                            <div style={{float:"left", width:"calc(100% - 80px)"}}>

                                {/* address */}
                                <div style={{fontSize:"18px"}}>{(row.delivery_type == 2?"Selbstabholung":this.getAddress(row.address, "str+nr", false))}</div>
                                
                                <div style={{fontSize:"12px", color:"rgb(102, 102, 102)", lineHeight:1.5, marginBottom:"4px"}}>
                                    {this.capitalizeFLetter(row.fromName)} {row.vip > 0 ? <img src="/assets/img/vip4.png" style={{width:"26px", height:"26px"}} /> : "" }<br/>
                                    {row.payment_mode === 'Barzahlung'?"":"💳"} {row.payment_mode} ({restaurant_info.currencyFormat} {row.total})
                                    {row.unique_order_id.includes("AOD-") ? <span><span style={{margin:"0 4px"}}>•</span>Automatische Bestellung</span>:""}<br/>
                                </div>

                            </div>

                            {/* total */}
                            {/* <div style={{borderBottom:"1px solid rgb(235, 235, 235)", margin:"10px 0", padding:"10px 0", textAlign:"right", width:"100%"}}>Gesamt: <span style={{color:"rgb(252, 128, 25)", fontWeight:"bold"}}>EUR {row.total}</span></div> */}

                            <Ink duration="500" />
                        </div>

                        {/* rechnung */}
                        <Modal show={this.state.isOpen[row.id]} onHide={this.hideModal} size={"sm"}>
                            
                            <Modal.Header closeButton>
                                <Modal.Title>
                                    <span style={{fontSize:"16px", cursor:"pointer", color:"rgb(26, 115, 232)"}} onClick={() => this.printDiv()}><FontAwesomeIcon icon={faPrint} /> Rechnung Drucken</span>
                                </Modal.Title>
                            </Modal.Header>

                            <Modal.Body style={{padding:"0 25px 10px"}}>
                                <div style={{padding:"10px 0", marginBottom:"10px", borderTop:"1px solid #eaecee", borderBottom:"1px solid #eaecee"}}>

                                    <div style={{float:"right"}}><QRCode value={"https://lieferservice.ag/bestellungen/"+restaurant_info.slug} size="50" /></div>

                                        Datum: {this.toDate(row.added)}<br/>
                                        Bestell Nr: {row.id}<br/>

                                    <br/>

                                    <b>KUNDE</b><br/>
                                    {this.capitalizeFLetter(row.fromName.replace(/.(?=.{4,}$)/g, '*'))} {row.vip > 0 ? <img src="/assets/img/vip4.png" style={{width:"26px", height:"26px"}} /> : "" }<br/>
                                    <span style={{color:"rgb(26, 115, 232)"}} onClick={() => this.navigation(JSON.parse(row.location).lat, JSON.parse(row.location).lng)}>{this.getAddress(row.address, "str+nr")}</span><br/>
                                    TEL {localStorage.getItem("code")?<a style={{color:"rgb(26, 115, 232)"}} href={'tel:' + row.fromTel}>{row.fromTel}</a>:<span>{row.fromTel.replace(/.(?=.{4,}$)/g, '*')}</span>}<br/>

                                    <br/>

                                    <b>WICHTIG</b><br/>
                                    Bezahlen mit {row.payment_mode === 'Barzahlung'?"":"💳"} {row.payment_mode}<br/>
                                    {row.delivery_type == 1 ? "Lieferung":"Abholung"} um: {this.toDate(row.added+60*this.getMinOnly(JSON.parse(row.location).lat, JSON.parse(row.location).lng, restaurant_info.latitude, restaurant_info.longitude, restaurant_info.delivery_radius))}<br/>
                                </div>
                        
                                <ul style={{listStyle:"none", margin:0, marginLeft:"15px", padding:0}}>

                                    <GuestCheckout currencyFormat={restaurant_info.currencyFormat} id={row.id} />

                                    {(row.vip > 0 && subTotal >= 15 && this.state.activatePremium) ? 
                                    <li style={{width:"100%", overflow:"auto", margin:"5px 0"}}>
                                        <span>
                                            <span style={{color:"#757575", width:"20px", float:"left"}}>1x</span> 
                                            Gratisgetränk <FontAwesomeIcon icon={faInfoCircle} data-tip='' data-for='freeDrink' />
                                        </span>
                                        <span style={{padding:"0 4px", width:"80px", float:"right", textAlign:"right"}}>0.00 {restaurant_info.currencyFormat}</span> 
                                    </li> : ""}
                                    <li style={{fontWeight:"bold"}}>
                                        Zwischensumme
                                        <span style={{padding:"0 4px", width:"80px", float:"right", textAlign:"right"}}>{subTotal} {restaurant_info.currencyFormat}</span> 
                                    </li>
                                    <li style={{fontWeight:"bold"}}>
                                        Lieferkosten
                                        <span style={{padding:"0 4px", width:"80px", float:"right", textAlign:"right"}}>{row.delivery_charge} {restaurant_info.currencyFormat}</span> 
                                    </li>
                                    {(row.discount > 0) ? 
                                    <li style={{fontWeight:"bold"}}>
                                        Mengenrabatt ({discountPercent}%)
                                        <span style={{padding:"0 4px", width:"80px", float:"right", textAlign:"right"}}>-{row.discount} {restaurant_info.currencyFormat}</span> 
                                    </li> : ""}
                                    <li style={{fontWeight:"bold"}}>
                                        Gesamt
                                        <span style={{padding:"0 4px", width:"80px", float:"right", textAlign:"right"}}>{row.total} {restaurant_info.currencyFormat}</span> 
                                    </li>

                                </ul>

                                <ReactTooltip id='freeDrink' place="top"><div style={{fontSize:"10px", fontWeight:"300", fontFamily:"open sans"}}>0,3L Cola, Fanta oder Sprite<br />gratis bei jeder Bestellung ab 15 {restaurant_info.currencyFormat}</div></ReactTooltip>

                                {/* print */}
                                <div id="printDiv" style={{display:"none", textAlign:"center"}}>
                                    <div style={{padding:"10px 20px", marginBottom:"10px", textAlign:"center", maxWidth:"272px", margin:"auto"}}>
                                        <QRCode value={"https://lieferservice.ag/bestellungen/"+restaurant_info.slug} size="100" renderAs="svg" />

                                        <br/><br/>
                                        **********************************
                                        <div style={{fontSize:"20pt"}}>{restaurant_info.name}</div>
                                        {this.getAddress(restaurant_info.address, "str")}<br/>
                                        {this.getAddress(restaurant_info.address, "plz")}<br/>
                                        TEL.: {restaurant_info.tel}<br/>
                                        **********************************

                                        <br/><br/>
                                        Bestelldatum: {this.toDate(row.added)}<br/>
                                        Bestell Nr: {row.id}<br/>

                                        <br/>

                                        <b>KUNDE</b><br/>
                                        {this.capitalizeFLetter(row.fromName.replace(/.(?=.{4,}$)/g, '*'))} {row.vip > 0 ? <span>V.I.P</span> : "" }<br/>
                                        {this.getAddress(row.address, "str+nr")}<br/>
                                        TEL {localStorage.getItem("code") ? row.fromTel : row.fromTel.replace(/.(?=.{4,}$)/g, '*')}<br/>

                                        <br/>

                                        <b>WICHTIG</b><br/>
                                        Bezahlen mit {row.payment_mode === 'Barzahlung'?"":"💳"} {row.payment_mode}<br/>
                                        {row.delivery_type == 1 ? "Lieferung":"Abholung"} um: {this.toDate(row.added+60*this.getMinOnly(JSON.parse(row.location).lat, JSON.parse(row.location).lng, restaurant_info.latitude, restaurant_info.longitude, restaurant_info.delivery_radius))}<br/>

                                        **********************************
                                    </div>

                                    <ul style={{listStyle:"none", padding:0, maxWidth:"272px", margin:"auto"}}>

                                        <GuestCheckout currencyFormat={restaurant_info.currencyFormat} id={row.id} />

                                        {(row.vip > 0 && subTotal >= 15 && this.state.activatePremium) ? 
                                        <li style={{width:"100%", overflow:"auto", margin:"5px 0"}}>
                                            <span>
                                                <span style={{color:"#757575", width:"20px", float:"left"}}>1x</span> 
                                                Gratisgetränk
                                            </span>
                                            <span style={{padding:"0 4px", width:"80px", float:"right", textAlign:"right"}}>0.00 {restaurant_info.currencyFormat}</span> 
                                        </li> : ""}
                                        <li style={{fontWeight:"bold"}}>
                                            Zwischensumme
                                            <span style={{padding:"0 4px", width:"80px", float:"right", textAlign:"right"}}>{subTotal} {restaurant_info.currencyFormat}</span> 
                                        </li>
                                        <li style={{fontWeight:"bold"}}>
                                            Lieferkosten
                                            <span style={{padding:"0 4px", width:"80px", float:"right", textAlign:"right"}}>{row.delivery_charge} {restaurant_info.currencyFormat}</span> 
                                        </li>
                                        {(row.discount > 0) ? 
                                        <li style={{fontWeight:"bold"}}>
                                            Mengenrabatt ({discountPercent}%)
                                            <span style={{padding:"0 4px", width:"80px", float:"right", textAlign:"right"}}>-{row.discount} {restaurant_info.currencyFormat}</span> 
                                        </li> : ""}
                                        <li style={{fontWeight:"bold"}}>
                                            Summe
                                            <span style={{padding:"0 4px", width:"80px", float:"right", textAlign:"right"}}>{row.total} {restaurant_info.currencyFormat}</span> 
                                        </li>

                                    </ul>

                                    <iframe id="ifmcontentstoprint" style={{height: "0px", width: "0px", position: "absolute"}}></iframe>
                                </div>

                            </Modal.Body>

                            <Modal.Footer>

                                {row.orderstatus_id == 1 ?
                                    <Button style={{background:"#8360c3", color:"#fff"}} variant="secondary" onClick={(localStorage.getItem("code")?this.showModalAccept.bind(this, row.id):this.showModalVerify.bind(this, row.id))}>  {/* secondary or primary */}
                                        <FontAwesomeIcon icon={faCheck} /> Bestätigen
                                    </Button>
                                :""}

                                {row.orderstatus_id == 1 ?
                                    <Button style={{background:"red", color:"#fff"}} variant="secondary" onClick={(localStorage.getItem("code")?this.showModalReject.bind(this, row.id):this.showModalVerify2.bind(this, row.id))}>  {/* secondary or primary */}
                                        Ablehnen
                                    </Button>
                                :""}
                                
                                {/* <Button style={{background:"#51D36B", color:"#fff"}} variant="secondary" href={'tel:' + row.fromTel}>  
                                    <FontAwesomeIcon icon={faPhoneAlt} /> Anruf
                                </Button> */}

                                {/* <Button variant="primary" onClick={() => this.navigation(JSON.parse(row.location).lat, JSON.parse(row.location).lng)}>
                                    <FontAwesomeIcon icon={faMapMarkerAlt} /> Route
                                </Button> */}

                                {/* <Button variant="secondary" onClick={() => this.printDiv()}>
                                    <FontAwesomeIcon icon={faPrint} />
                                </Button> */}

                            </Modal.Footer>
                        </Modal>

                        {/* status of order */}
                        {/* <div style={{width:"100%", float:"left", color:"#000"}}>
                            {row.orderstatus_id == 1 ?
                            <div>
                                <span style={{fontWeight:"bold", float:"left", width:"calc(100% - 100px)"}}>Warte auf Bestätigung der Bestellung durch das Restaurant</span><span style={{float:"right", color:"#fff", boxShadow: "0 1px 2px 0 #ff6a00", background:"linear-gradient(90deg,#fd8d42,#ff6a00)", padding:"8px 10px 4px", borderRadius:".2rem", fontWeight:"600", fontFamily:"'Google Sans', sans-serif", cursor:"pointer"}} onClick={(localStorage.getItem("code")?this.showModalAccept.bind(this, row.id):this.showModalVerify.bind(this, row.id))}>Bestätigen</span>
                            </div>
                            :""}
                            {(row.orderstatus_id == 2 || row.orderstatus_id == 3 || row.orderstatus_id == 4) ?<span style={{fontWeight:"bold"}}>Unterwegs</span>
                            :""}
                            {row.orderstatus_id == 6 ?<span style={{fontWeight:"bold", color:"red"}}>Storniert</span>
                            :""}
                            {row.orderstatus_id == 7 ?<span style={{fontWeight:"bold"}}>Selbstabholung</span>
                            :""}
                            {row.orderstatus_id == 8 ?<span style={{fontWeight:"bold", color:"red"}}>Bestätigungsfrist abgelaufen</span>
                            :""}
                        </div> */}

                        {/* bestätigungscode anfordern und akzeptieren */}
                        <Modal show={this.state.verifyModal[row.id]} onHide={this.hideModalVerify}>
                            
                            <Modal.Header closeButton>
                                <Modal.Title>Bestätigungscode anfordern</Modal.Title>
                            </Modal.Header>

                            <Modal.Body style={{padding:"0 25px 10px"}}>
                                {stillTime ? 
                                    <div>
                                        <div style={{fontWeight:"bold", marginBottom:"5px"}}>Ist dies Ihr Unternehmen?</div>
                                        
                                        <p>Fordern Sie den 4-stelligen Code an, den wir Ihnen per Anruf an {restaurant_info.tel} senden.</p>

                                        {/* mit captcha */}
                                        {/* <div style={{marginBottom:"5px", border:"1px solid #ccc", borderRadius:"3px"}}><input maxLength="4" type="tel" placeholder="Code eingeben" name="code" onChange={this.onCodeChanged} style={{padding:"8px", maxWidth:"130px", border:"none"}} /><span onClick={this.showModalCaptcha.bind(this, row.id)} style={{padding:"8px", color:"#0872BA", cursor:"pointer", float:"right", fontWeight:"600", fontFamily:"'Google Sans', sans-serif"}}>Code senden</span></div> */}

                                        {/* ohne captcha */}
                                        <div style={{marginBottom:"5px", border:"1px solid #ccc", borderRadius:"3px"}}>
                                            <input maxLength="4" type="tel" placeholder="Code eingeben" name="code" onChange={this.onCodeChanged} style={{padding:"8px", maxWidth:"130px", border:"none"}} />
                                            
                                            <span onClick={() => this.sendCode(restaurant_info.id)} style={{padding:"8px", color:"#0872BA", cursor:"pointer", float:"right", fontWeight:"600", fontFamily:"'Google Sans', sans-serif"}}>
                                                Code senden
                                            </span>
                                        </div>
                                    </div> : <div style={{color:"red"}}>Bestätigungsfrist abgelaufen</div>}

                                    <div id="result"></div>

                                    <div style={{marginTop:"20px", fontSize:"9px", color:"#999"}}>Übernehmen Sie Ihren Lieferservice-Eintrag, um Geschäftsinformationen in Echtzeit zu aktualisieren, auf Bestellungen zu antworten und vieles mehr!</div>
                            </Modal.Body>

                            <Modal.Footer>
                                <Button variant="secondary" onClick={this.hideModalVerify}> 
                                    Schließen
                                </Button> 
                                
                                {stillTime ? 
                                <Button variant="primary" onClick={() => this.accept(restaurant_info.id, this.state.verificationCode, row.id)}>
                                    Bestätigen
                                </Button> : ""}
                            </Modal.Footer>
                        </Modal>

                        {/* bestätigungscode anfordern und ablehnen */}
                        <Modal show={this.state.verifyModal2[row.id]} onHide={this.hideModalVerify2}>
                            
                            <Modal.Header closeButton>
                                <Modal.Title>Bestätigungscode anfordern</Modal.Title>
                            </Modal.Header>

                            <Modal.Body style={{padding:"0 25px 10px"}}>
                                {stillTime ? 
                                    <div>
                                        <div style={{fontWeight:"bold", marginBottom:"5px"}}>Ist dies Ihr Unternehmen?</div>
                                        
                                        <p>Fordern Sie den 4-stelligen Code an, den wir Ihnen per Anruf an {restaurant_info.tel} senden.</p>

                                        {/* mit captcha */}
                                        {/* <div style={{marginBottom:"5px", border:"1px solid #ccc", borderRadius:"3px"}}><input maxLength="4" type="tel" placeholder="Code eingeben" name="code" onChange={this.onCodeChanged} style={{padding:"8px", maxWidth:"130px", border:"none"}} /><span onClick={this.showModalCaptcha.bind(this, row.id)} style={{padding:"8px", color:"#0872BA", cursor:"pointer", float:"right", fontWeight:"600", fontFamily:"'Google Sans', sans-serif"}}>Code senden</span></div> */}

                                        {/* ohne captcha */}
                                        <div style={{marginBottom:"5px", border:"1px solid #ccc", borderRadius:"3px"}}>
                                            <input maxLength="4" type="tel" placeholder="Code eingeben" name="code" onChange={this.onCodeChanged} style={{padding:"8px", maxWidth:"130px", border:"none"}} />
                                            
                                            <span onClick={() => this.sendCode(restaurant_info.id)} style={{padding:"8px", color:"#0872BA", cursor:"pointer", float:"right", fontWeight:"600", fontFamily:"'Google Sans', sans-serif"}}>
                                                Code senden
                                            </span>
                                        </div>
                                    </div> : <div style={{color:"red"}}>Bestätigungsfrist abgelaufen</div>}

                                    <div id="result"></div>

                                    <div style={{marginTop:"20px", fontSize:"9px", color:"#999"}}>Übernehmen Sie Ihren Lieferservice-Eintrag, um Geschäftsinformationen in Echtzeit zu aktualisieren, auf Bestellungen zu antworten und vieles mehr!</div>
                            </Modal.Body>

                            <Modal.Footer>
                                <Button variant="secondary" onClick={this.hideModalVerify2}> 
                                    Schließen
                                </Button> 
                                
                                {stillTime ? 
                                <Button variant="primary" onClick={() => this.reject(restaurant_info.id, this.state.verificationCode, row.id)}>
                                    Ablehnen
                                </Button> : ""}
                            </Modal.Footer>
                        </Modal>

                        {/* captcha lösen */}
                        <Modal show={this.state.captchaModal[row.id]} onHide={this.hideModalCaptcha}>
                            
                            <Modal.Header closeButton>
                                <Modal.Title>Captcha lösen</Modal.Title>
                            </Modal.Header>

                            <Modal.Body style={{padding:"0 25px 10px"}}>
                                <form onSubmit={this.onSubmit} >
                                    <ReCAPTCHA
                                        sitekey="6Lch_dUUAAAAAIcuato7j0ndX7ZlY2NqRraH-gtT"
                                        onChange={this.onCaptchaChanged} 
                                    />
                                </form>
                            </Modal.Body>
                        </Modal>

                        {/* bestellung bestätigen */}
                        <Modal show={this.state.acceptModal[row.id]} onHide={this.hideModalAccept}>
                            
                            <Modal.Header closeButton>
                                <Modal.Title>Bestätigen</Modal.Title>
                            </Modal.Header>

                            <Modal.Body style={{padding:"0 25px 10px"}}>
                                {stillTime ? <p>Sie haben noch {this.timer(row.added+60*10)} Minuten Zeit diese Bestellung zu bestätigen.</p> : <p style={{color:"red"}}>Bestätigungsfrist abgelaufen</p>}
                                
                                <div id="result"></div>

                                <div style={{marginTop:"20px", fontSize:"9px", color:"#999"}}>Mit der Bestätigung dieser Bestellung akzeptieren Sie die Geschäftsbedingungen. </div>
                            </Modal.Body>

                            <Modal.Footer>
                                <Button variant="secondary" onClick={this.hideModalAccept}> 
                                    Schließen
                                </Button> 
                                
                                {stillTime ? 
                                <Button variant="primary" onClick={() => this.accept(restaurant_info.id, localStorage.getItem("code"), row.id)}>
                                    Bestätigen
                                </Button> : ""}
                            </Modal.Footer>
                        </Modal>

                        {/* bestellung ablehnen */}
                        <Modal show={this.state.rejectModal[row.id]} onHide={this.hideModalReject}>
                            
                            <Modal.Header closeButton>
                                <Modal.Title>Ablehnen</Modal.Title>
                            </Modal.Header>

                            <Modal.Body style={{padding:"0 25px 10px"}}>
                                {stillTime ? <p>Möchten Sie diese Bestellung wirklich ablehnen?</p> : <p style={{color:"red"}}>Bestätigungsfrist abgelaufen</p>}
                                
                                <div id="result"></div>

                                {/* <div style={{marginTop:"20px", fontSize:"9px", color:"#999"}}>Mit der Bestätigung dieser Bestellung akzeptieren Sie die Geschäftsbedingungen. </div> */}
                            </Modal.Body>

                            <Modal.Footer>
                                <Button variant="secondary" onClick={this.hideModalReject}> 
                                    Schließen
                                </Button> 
                                
                                {stillTime ? 
                                <Button variant="primary" onClick={() => this.reject(restaurant_info.id, localStorage.getItem("code"), row.id)}>
                                    Ablehnen
                                </Button> : ""}
                            </Modal.Footer>
                        </Modal>

                        <div style={{color:"#202124", fontSize: ".75rem", opacity: ".7"}}>
                            {/* Name: {row.fromName.replace(/.(?=.{4,}$)/g, '*')}<br/>
                            Tel: {row.fromTel.replace(/.(?=.{4,}$)/g, '*')}<br/>*/}
                        </div>
                        </div>
                    );
                } 
            });

            return isAvailable?result:false; //in order to hide title (neu, bestätigt, storniert)
        }

        var cat = restaurant_info.description || '';
        if(cat.indexOf("Handy") >= 0){
            var bgID = "1556656793-08538906a9f8";
        } else {
            var bgID = "1571939634097-f1829662cdfc";
        }


        return (
            <React.Fragment>

                <Nav shop={true} restaurant={restaurant_info} page={"OrderList"} />

                {/* zum bildschirm hinzufügen */}
                <div id="showPwaprompt" style={{display:"none"}}>
                    <PWAPrompt debug={1} 
                    copyTitle={"Add to Home Screen"} 
                    copyBody={"This website has app functionality. Add it to your home screen to use it in fullscreen and while offline."}
                    copyShareButtonLabel={"1) Press the 'Share' button"}
                    copyAddHomeButtonLabel={"2) Press 'Add to Home Screen'"}
                    copyClosePrompt={"Cancel"} 
                    />
                </div>

                {/* app-berechtigungen */}
                <Modal show={this.state.berechtigungenModal} onHide={this.hideModalBerechtigungen}>
                    
                    <Modal.Header closeButton>
                        <Modal.Title>App-Vorteile</Modal.Title>
                    </Modal.Header>

                    <Modal.Body style={{padding:"0 25px 10px"}}>
                        <div style={{marginTop:"12px"}}>
                            <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />{restaurant_info.name} auf Desktop installieren.</div>

                            <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Ohne Internetverbindung bei {restaurant_info.name} bestellen.</div>

                            <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Bis zu 32% Mengenrabatt.</div>

                            <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Lieferung auf der Karte verfolgen.</div>

                            <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Benutzerelebnis durch Schnelles Laden.</div>

                            <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Bessere Kommunikation zwischen Restaurant und Kunde durch Push-Nachrichten.</div>

                            <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Zuverlässig auch bei schlechter Internetverbindung.</div>


                            <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Installation über Google Chrome.</div>
                        </div>

                    </Modal.Body>

                    <Modal.Footer>
                        <Button variant="secondary" onClick={this.hideModalBerechtigungen}> 
                            Schließen
                        </Button> 
                        <Button variant="primary" onClick={() => this.next()}>
                            Weiter
                        </Button>
                    </Modal.Footer>
                </Modal>

                {/* app-installieren */}
                <Modal show={this.state.installModal} onHide={this.hideModalInstall}>
                    
                    <Modal.Header closeButton>
                        <Modal.Title>Installieren</Modal.Title>
                    </Modal.Header>

                    <Modal.Body style={{padding:"0 25px 10px"}}>
                        <div style={{marginTop:"12px"}}>
                            Klicke auf hinzufügen um {restaurant_info.name} auf deinen Bildschirm zu installieren.
                        </div>

                        <div style={{marginTop:"20px", fontSize:"11px", color:"#999"}}>Wenn dir {restaurant_info.name} gefällt, dann installiere bitte die {restaurant_info.name} App.</div>
                    </Modal.Body>

                    <Modal.Footer>
                        <Button variant="secondary" onClick={this.hideModalInstall}> 
                            Schließen
                        </Button> 
                        <Button variant="primary" onClick={() => this.triggerInstallBtn()} id="download">
                            Hinzufügen
                        </Button>
                    </Modal.Footer>
                </Modal>

               

                {/* order list */}
                <div style={{background:"#E8E9EE", overflow:"auto", padding:"0 15px"}} id="newOrdersData">

                    {(isChrome || isChromium || isMobileSafari) ? install : ""}

                    {/* klingelton aktivieren */}
                    {/* <div style={{maxWidth:"768px", margin:"auto", textAlign:"center", fontWeight:"300", fontFamily:"open sans", fontSize:"12px", padding:"10px", color:"#666"}}>Ist dies Ihr Unternehmen? Bitte <a style={{color:"#1a73e8", textDecoration:"underline", cursor:"pointer"}} onClick={() => alert("Klingelton jetzt aktiviert")}>hier klicken</a>, um Klingelton bei Bestellung zu aktivieren. <br/>Verschicke jetzt eine <span style={{color:"#1a73e8", textDecoration:"underline", cursor:"pointer"}} onClick={this.showModalSms.bind(this, restaurant_info.id)}>Massen-SMS</span> an deine Stammkunden. </div> */}
                    {/* <div onClick={() => alert("Klingelton bei Bestellung jetzt aktiviert")} style={{position:"relative", cursor:"pointer", margin:"10px auto", maxWidth:"768px", fontSize:"0.8rem", padding:"0 15px", color:"rgb(26, 115, 232)", textAlign:"center"}}>Klingelton aktivieren<Ink duration="500" /></div> */}

                    {/* massen sms */}
                    {/* <Modal show={this.state.smsModal[restaurant_info.id]} onHide={this.hideModalSms}>
                        
                        <Modal.Header closeButton>
                            <Modal.Title>Gratis Massen-SMS versenden</Modal.Title>
                        </Modal.Header>
    
                        <Modal.Body style={{padding:"0 25px 10px"}}>
    
                            <div style={{marginTop:"12px", color:"#c2c5bc"}}>
                                <label style={{fontWeight:"normal", display:"block"}}>
                                    Tel (pro Zeile eine Telefonnummer eingeben) *
                                    <textarea name="tel" type="tel" style={{padding:"5px", width:"100%", height:"100px"}} onChange={this.onTelChanged} />
                                </label>
    
                                <label style={{fontWeight:"normal", display:"block"}}>
                                    Nachricht *
                                    <textarea style={{padding:"5px", width:"100%", height:"100px"}}
                                    value={body} />
                                </label>
                            </div>
    
                            <div id="result"></div>
    
                            <div style={{marginTop:"20px", fontSize:"10px", color:"#999"}}>
                                - Beste Werbe- und Kommunikationsmethode. <br/>
                                - 94% aller SMS werden innerhalb von 5 Minuten geöffnet <br/>
                                - Erreichen Sie mit einem Schlag eine große Anzahl von Kunden <br/>
                                - Pro Monat 100 Gratis Nachrichten</div>
                        </Modal.Body>
    
                        <Modal.Footer>
                            <Button variant="secondary" onClick={this.hideModalSms}> 
                                Schließen
                            </Button> 
                            
                            <Button variant="primary" onClick={() => this.sendSMS(this.state.toTel, body)}>
                                SMS Generator
                            </Button>
                        </Modal.Footer>
                    </Modal> */}

                    {this.state.no_orders ? <div style={{textAlign:"center", padding:"15px"}}>Noch keine Bestellungen!</div> : null}

                    {newOrdersDataElements("1,7")!==false ? <div><div style={{fontWeight:"300", maxWidth:"768px", margin:"auto", textTransform:"uppercase", fontSize:"1.4rem", fontFamily:"open sans"}}>Neu</div>{newOrdersDataElements("1,7")}</div>:null} 
                    
                    {newOrdersDataElements("2")!==false ? <div><div style={{fontWeight:"300", maxWidth:"768px", margin:"auto", textTransform:"uppercase", fontSize:"1.4rem", fontFamily:"open sans"}}>Bestätigt</div>{newOrdersDataElements("2")}</div>:null} 

                    {newOrdersDataElements("6,8")!==false ? <div><div style={{fontWeight:"300", maxWidth:"768px", margin:"auto", textTransform:"uppercase", fontSize:"1.4rem", fontFamily:"open sans"}}>Abgelehnt oder abgelaufen</div>{newOrdersDataElements("6,8")}</div>:null} 

                </div>
                
            </React.Fragment>
        );
    }
}


const mapStateToProps = state => ({
    restaurant_info: state.items.restaurant_info,
});

export default connect(
    mapStateToProps,
    {
        // to map the getRestaurantInfo to the component props.
        getRestaurantInfo,
        getSingleLanguageData
    }
)(OrderList);


