
import React, { Component } from "react";
import Ink from "react-ink";
import { connect } from "react-redux";
import BackWithSearch from "../../Elements/BackWithSearch";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUtensils } from '@fortawesome/free-solid-svg-icons';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { faPhoneAlt } from '@fortawesome/free-solid-svg-icons'
import { faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons'
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { faWindowClose } from '@fortawesome/free-solid-svg-icons';


import { PLACE_ORDER } from "../../../../services/checkout/actionTypes";
import { PLACE_ORDER_URL } from "../../../../configs";
import { updateCart } from "../../../../services/total/actions";
import { placeOrder } from "../../../../services/checkout/actions";

import { getRestaurantInfo2 } from "../../../../services/items/actions";
import Nav from "../../Nav";

import Modal from 'react-bootstrap/Modal';
import ReactDOM from 'react-dom';
import { Button } from 'react-bootstrap';

import PWAPrompt from 'react-ios-pwa-prompt';
import { isChrome, isChromium, isMobileSafari } from 'react-device-detect';

import Suggestions2 from '../../FirstScreen/suggestions2'


declare var INSTALL_APP_POPUP;

class About extends Component {


    static contextTypes = {
        router: () => null
    };

    componentDidMount() {
        if(this.props.slug){
            getRestaurantInfo2(this.props.slug, this.props.dispatch, res=>{})

        } else {
            getRestaurantInfo2(window.location.href.split("/").pop(), this.props.dispatch, res=>{})
        }
    }

    getDayName = () => {
        switch (new Date().getDay()) {
            case 0:
                return "sonntag"
            case 1:
                return "montag"
            case 2:
                return "dienstag"
            case 3:
                return "mittwoch"
            case 4:
                return "donnerstag"
            case 5:
                return "freitag"
            case 6:
                return "samstag"
        }
    }

    openingHours = (openFrom="14:00", openTo="03:00") => {
        //now date
        var now = new Date(); 

        //given date
        var h = now.getHours();
        var day = now.getDate();
        var month = ('0' + (now.getMonth() + 1)).slice(-2)
        var year = now.getUTCFullYear();

        //function slice() removes last 3 char from openTo to only get h without min
        //check if after midnight because date comparison should not be like 2020/01/09 14:00 - 2020/01/09 03:00
        
        if (h < 8 && openTo.slice(0, -3) < 8){//if openTo is after midnight && current time is after midgnight
            var from = new Date(year + "/" + month + "/" + day + " " + openFrom);
            var from = this.addDays2(from, -1);
            var to = new Date(year + "/" + month + "/" + day + " " + openTo); 
        }else if (openTo.slice(0, -3) < 8){//if openTo is after midnight && current time is NOT after midgnight
            var from = new Date(year + "/" + month + "/" + day + " " + openFrom);
            var to = new Date(year + "/" + month + "/" + day + " " + openTo); 
            var to = this.addDays2(to, 1);
        }else{
            var from = new Date(year+"/"+month+"/"+day+" "+openFrom); //set current date and time from table of restaurant 
            var to   = new Date(year+"/"+month+"/"+day+" "+openTo); //set current date and time from table of restaurant
        }

        var today = now.getFullYear()+'/'+(now.getMonth()+1)+'/'+now.getDate()+' '+now.getHours()+":"+now.getMinutes(); 
        var check = new Date(today); //this must always be current date, never change

        //return from+" - "+to;
        if (check > from && check < to)
            return ""; 
        else 
            return "geschlossen"; 
            
    }

    addDays2 = (date, amount) => {
        var tzOff = date.getTimezoneOffset() * 60 * 1000,
            t = date.getTime(),
            d = new Date(),
            tzOff2;
      
        t += (1000 * 60 * 60 * 24) * amount;
        d.setTime(t);
      
        tzOff2 = d.getTimezoneOffset() * 60 * 1000;
        if (tzOff != tzOff2) {
          var diff = tzOff2 - tzOff;
          t += diff;
          d.setTime(t);
        }
      
        return d;
    }

    getAddress = (address, country=false) => {
        if(country === false){
            var res = address;
            var split = res.split(",");
            var res = split.slice(0, split.length - 1).join(",");
            return res;
        }else{
            var res = address.split(", ");
            return res[0];
        }
    }    
    
    getCityPlz = (address) => {
        var res = address.split(", ");
        return res[1];
    }

    render() {
        const { cartTotal, total, restaurant_info, cartProducts} = this.props;

        var cat = restaurant_info.description || '';
        if(cat.indexOf("Handy") >= 0){
            var bgID = "1556656793-08538906a9f8";
        } else {
            var bgID = "1571939634097-f1829662cdfc";
        }

        return (
            <React.Fragment>

            
                <h2 style={{color:"#393939", padding:"20px 18px", margin:0, textAlign:"center"}}>
                    {restaurant_info.name}
                    <div style={{fontSize:"14px", marginTop:"5px"}}>{restaurant_info.description}</div>
                </h2>

                <div style={{overflow:"auto", background:"rgb(232, 233, 238)"}}>


                    <div style={{background:"#fff", margin:"10px auto", maxWidth:"768px", padding:"16px 24px", lineHeight:1.71, border: "1px solid rgba(0,0,0,0.15)", boxShadow: "0 2px 3px rgba(0,0,0,0.06)"}}>
                        <div style={{fontWeight:"bold"}}>{localStorage.getItem("itemsPageAddress")}</div> {restaurant_info.address}
                        {/* open, closed */}
                        {(restaurant_info[this.getDayName()]) ? 
                            ((restaurant_info[this.getDayName()] == "geschlossen" || this.openingHours(
                                restaurant_info[this.getDayName()].slice(0, 5),//from ex: 10:00
                                restaurant_info[this.getDayName()].slice(-5)//to ex: 23:00
                            ) == "geschlossen") ? <div style={{color:"#ff4246"}}>{localStorage.getItem("homePageClosed")}</div> : <div style={{color:"green"}}>{localStorage.getItem("homePageOpen")}</div>) 
                            : null}
                    </div>
                    
                    {/* delivery time */}
                    <div style={{background:"#fff", margin:"10px auto", maxWidth:"768px", padding:"16px 24px", lineHeight:1.71, border: "1px solid rgba(0,0,0,0.15)", boxShadow: "0 2px 3px rgba(0,0,0,0.06)"}}>
                        <div style={{marginBottom:"5px", fontWeight:"bold"}}>{localStorage.getItem("itemsPageDeliveryTimes")}</div>
                        <div>Montag <span style={{float:"right"}}>{restaurant_info.montag}</span></div>
                        <div>Dienstag <span style={{float:"right"}}>{restaurant_info.dienstag}</span></div>
                        <div>Mittwoch <span style={{float:"right"}}>{restaurant_info.mittwoch}</span></div>
                        <div>Donnerstag <span style={{float:"right"}}>{restaurant_info.donnerstag}</span></div>
                        <div>Freitag <span style={{float:"right"}}>{restaurant_info.freitag}</span></div>
                        <div>Samstag <span style={{float:"right"}}>{restaurant_info.samstag}</span></div>
                        <div>Sonntag <span style={{float:"right"}}>{restaurant_info.sonntag}</span></div>
                    </div>

                    {/* min order, delivery charges */}
                    <div style={{background:"#fff", margin:"10px auto", maxWidth:"768px", padding:"16px 24px", lineHeight:1.71, border: "1px solid rgba(0,0,0,0.15)", boxShadow: "0 2px 3px rgba(0,0,0,0.06)"}}>
                        <div style={{marginBottom:"5px", fontWeight:"bold"}}>{localStorage.getItem("itemsPageDeliveryCosts")}</div>
                        <div>{localStorage.getItem("itemsPageMinOrder")} <span style={{float:"right"}}>{restaurant_info.price_range} {restaurant_info.currencyFormat}</span></div>
                        <div>{localStorage.getItem("itemsPageDeliveryCosts")} <span style={{float:"right"}}>{restaurant_info.delivery_charges} {restaurant_info.currencyFormat}</span></div>
                    </div>

                    {/* rating */}
                    {/* <div style={{background:"#fff", margin:"10px auto", maxWidth:"768px", padding:"16px 24px", lineHeight:1.71, border: "1px solid rgba(0,0,0,0.15)", boxShadow: "0 2px 3px rgba(0,0,0,0.06)"}}>
                        <div style={{marginBottom:"5px", fontWeight:"bold"}}>{localStorage.getItem("itemsPageRatings")}</div>
                        <div>Takeaway <span style={{float:"right"}}><i className="fa fa-star pr-1" style={{ color: localStorage.getItem("storeColor") }} />{" "} {restaurant_info.rating}</span></div>
                        <div>Mjam.at <span style={{float:"right"}}><i className="fa fa-star pr-1" style={{ color: localStorage.getItem("storeColor") }} />{" "} N/A</span></div>
                        <div>google.com <span style={{float:"right"}}><i className="fa fa-star pr-1" style={{ color: localStorage.getItem("storeColor") }} />{" "} N/A</span></div>
                    </div> */}

                    {/* Bezahlmethoden */}
                    <div style={{background:"#fff", margin:"10px auto", maxWidth:"768px", padding:"16px 24px", lineHeight:1.71, border: "1px solid rgba(0,0,0,0.15)", boxShadow: "0 2px 3px rgba(0,0,0,0.06)"}}>
                        <div style={{marginBottom:"10px", fontWeight:"bold"}}>{localStorage.getItem("itemsPagePaymentMethods")}</div>
                        <div>
                            <img src="/assets/img/cash.svg" style={{height: "25px", width: "25px", marginRight:"5px"}} /> 
                            <img src="/assets/img/kreditkarte.svg" style={{height: "25px", width: "25px", marginRight:"5px"}} />
                            <img src="/assets/img/sofort.png" style={{height: "25px", width: "25px", marginRight:"5px"}} />
                            <img src="/assets/img/eps.png" style={{height: "25px", width: "25px", marginRight:"5px"}} />
                        </div>
                    </div>

                    

                    {/* Impressum */}
                    <div style={{background:"#fff", margin:"10px auto", maxWidth:"768px", padding:"16px 24px", lineHeight:1.71, border: "1px solid rgba(0,0,0,0.15)", boxShadow: "0 2px 3px rgba(0,0,0,0.06)"}}>
                        <div style={{marginBottom:"5px", fontWeight:"bold"}}>{localStorage.getItem("footerCompanyDetails")}</div>
                        <div>{restaurant_info.name}</div>
                        <div>{restaurant_info.address}</div>
                        {/* <div>{this.getAddress(restaurant_info.address)}</div>
                        <div>{this.getCityPlz(restaurant_info.address)}</div> */}
                        <div>{restaurant_info.tel}</div>
                    </div>
                </div>

            </React.Fragment>
        );
    }
}


const mapStateToProps = state => ({
    restaurant_info: state.items.restaurant_info,
    cartProducts: state.cart.products,
    cartTotal: state.total.data,
});
const mapDispatchToProps = (dispatch, props) => ({
    dispatch: dispatch
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
    //{ placeOrder, getRestaurantInfo }
)(About);


