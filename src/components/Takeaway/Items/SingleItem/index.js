import React, { Component } from "react";
import { addProduct, removeProduct } from "../../../../services/cart/actions";
import { getRestaurantInfo, getRestaurantItems, getSingleItem, resetInfo, resetItems } from "../../../../services/items/actions";

import Customization from "../Customization";
import Fade from "react-reveal/Fade";
import FloatCart from "../../FloatCart";
import Ink from "react-ink";
import ItemBadge from "../ItemList/ItemBadge";
import LazyLoad from "react-lazyload";
import ProgressiveImage from "react-progressive-image";
import { Redirect } from "react-router";
import RestaurantInfo from "../RestaurantInfo";

import { connect } from "react-redux";

class SingleItem extends Component {
    state = {
        update: true,
        is_active: 1
    };
    forceStateUpdate = () => {
        setTimeout(() => {
            this.forceUpdate();
            if (this.state.update) {
                this.setState({ update: false });
            } else {
                this.setState({ update: true });
            }
        }, 100);
    };

    componentDidMount() {
        this.props.getRestaurantInfo(this.props.match.params.restaurant);
        this.props.getSingleItem(this.props.match.params.id);
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ is_active: nextProps.restaurant_info.is_active });
    }

    render() {
        if (localStorage.getItem("storeColor") === null) {
            return <Redirect to={"/"} />;
        }

        const { addProduct, removeProduct, cartProducts, single_item } = this.props;
        return (
            <React.Fragment>
                <RestaurantInfo history={this.props.history} restaurant={this.props.restaurant_info} withLinkToRestaurant={true} />

                {single_item.id && (
                    <div className="single-item px-15 mt-20">
                        <span className="hidden">{(single_item.quantity = 1)}</span>
                        <div
                            className="category-list-item"
                            style={{
                                display: "flex",
                                justifyContent: "space-between"
                            }}
                        >
                            {single_item.image !== "" && (
                                <LazyLoad>
                                    {/* <ProgressiveImage src={single_item.image} placeholder={single_item.placeholder_image}>
                                        {src => (
                                            <Fade duration={550}>
                                                <img
                                                    src={src}
                                                    alt={single_item.name}
                                                    className="flex-item-image"
                                                    style={{ width: "100%", height: "auto" }}
                                                />
                                            </Fade>
                                        )}
                                    </ProgressiveImage> */}

                                    <React.Fragment>
                                        {cartProducts.find(cp => cp.id === single_item.id) !== undefined && (
                                            <Fade duration={150}>
                                                <div
                                                    className="quantity-badge-list"
                                                    style={{
                                                        backgroundColor: localStorage.getItem("storeColor")
                                                    }}
                                                >
                                                    <span>
                                                        {single_item.addon_categories.length ? (
                                                            <React.Fragment>
                                                                <i
                                                                    className="si si-check"
                                                                    style={{
                                                                        lineHeight: "1.3rem"
                                                                    }}
                                                                ></i>
                                                            </React.Fragment>
                                                        ) : (
                                                            <React.Fragment>
                                                                {cartProducts.find(cp => cp.id === single_item.id).quantity}
                                                            </React.Fragment>
                                                        )}
                                                    </span>
                                                </div>
                                            </Fade>
                                        )}
                                    </React.Fragment>
                                </LazyLoad>
                            )}
                        </div>
                        <div className="single-item-meta">
                            <div className="item-actions pull-right pb-0">
                                <div className="btn-group btn-group-sm" role="group" aria-label="btnGroupIcons1">
                                    {single_item.addon_categories.length ? (
                                        <button
                                            disabled
                                            type="button"
                                            className="btn btn-add-remove"
                                            style={{
                                                color: localStorage.getItem("cartColor-bg")
                                            }}
                                        >
                                            <span className="btn-dec">-</span>
                                            <Ink duration="500" />
                                        </button>
                                    ) : (
                                        <button
                                            type="button"
                                            className="btn btn-add-remove"
                                            style={{
                                                color: localStorage.getItem("cartColor-bg")
                                            }}
                                            onClick={() => {
                                                single_item.quantity = 1;
                                                removeProduct(single_item);
                                                this.forceStateUpdate();
                                            }}
                                        >
                                            <span className="btn-dec">-</span>
                                            <Ink duration="500" />
                                        </button>
                                    )}

                                    {single_item.addon_categories.length ? (
                                        <Customization product={single_item} addProduct={addProduct} forceUpdate={this.forceStateUpdate} />
                                    ) : (
                                        <button
                                            type="button"
                                            className="btn btn-add-remove"
                                            style={{
                                                color: localStorage.getItem("cartColor-bg")
                                            }}
                                            onClick={() => {
                                                addProduct(single_item);
                                                this.forceStateUpdate();
                                            }}
                                        >
                                            <span className="btn-inc">+</span>
                                            <Ink duration="500" />
                                        </button>
                                    )}
                                </div>
                            </div>
                            <div className="item-name font-w600 mt-2">{single_item.name}</div>
                            <div className="item-price">
                                {localStorage.getItem("currencyFormat")} {single_item.price}
                                {single_item.addon_categories.length > 0 && (
                                    <span
                                        className="ml-2 customizable-item-text"
                                        style={{
                                            color: localStorage.getItem("storeColor")
                                        }}
                                    >
                                        {localStorage.getItem("customizableItemText")}
                                    </span>
                                )}
                                <ItemBadge item={single_item} />
                            </div>
                            {single_item.desc !== null ? (
                                <div className="mt-2 mb-100">
                                    <div
                                        dangerouslySetInnerHTML={{
                                            __html: single_item.desc
                                        }}
                                    ></div>
                                </div>
                            ) : (
                                <br />
                            )}
                        </div>
                    </div>
                )}

                {this.state.is_active ? (
                    <FloatCart />
                ) : (
                    <div className="auth-error no-click">
                        <div className="error-shake">{localStorage.getItem("notAcceptingOrdersMsg")}</div>
                    </div>
                )}
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    restaurant_info: state.items.restaurant_info,
    cartProducts: state.cart.products,
    single_item: state.items.single_item
});

export default connect(
    mapStateToProps,
    {
        getRestaurantInfo,
        getRestaurantItems,
        resetItems,
        resetInfo,
        getSingleItem,
        addProduct,
        removeProduct
    }
)(SingleItem);
