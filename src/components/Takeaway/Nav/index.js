import React, { Component } from "react";
import axios from "axios";
import Ink from "react-ink";
import { connect } from "react-redux";

//bootstrap
import ReactDOM from 'react-dom';
import { Modal, Button, Dropdown } from 'react-bootstrap';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons';
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons';
import { faShoppingBag } from '@fortawesome/free-solid-svg-icons';
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import { faStore } from '@fortawesome/free-solid-svg-icons';

//Meine Bestellungen
import { ORDER_LIST_URL } from "../../../configs/index";
import GuestCheckout from '../Items/OrderListView/index.js';

//Meine Daueraufträge
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import setHours from "date-fns/setHours";
import setMinutes from "date-fns/setMinutes";
import PropTypes from 'prop-types';
import checkboxes from '../Checkout/checkboxes';
import Checkbox from '../Checkout/Checkbox';

//Standort festlegen
import Flip from "react-reveal/Flip";
import GooglePlacesAutocomplete from "react-google-places-autocomplete";
import { geocodeByPlaceId } from "react-google-places-autocomplete";
// import { getPopularLocations } from "../../../services/popularLocations/actions";
// import Geocode from "react-geocode";
// import PopularPlaces from "../../Takeaway/Location/PopularPlaces";


// Land/Sprache ändern
import { getSingleLanguageData } from "../../../services/translations/actions";

// other
import ReactTooltip from 'react-tooltip';

import { isChrome, isChromium, isMobileSafari, isMobile, fullBrowserVersion, osName } from 'react-device-detect';

import Shops from "../Shops/RestaurantList";
import UserMenu from "../Account/UserMenu";
import Location from "../Location";
import Info from "../Items/About";
import Cart from "../Cart";
import Categories from '../Items/ItemList/Categories';
import Search from '../Items/ItemList/Search';
import Popup from "reactjs-popup";

declare var INSTALL_APP_POPUP;

class Nav extends Component {
    constructor() {
        super();
        
        this.state = { 

            LoginModal: false,

            // Meine Bestellungen
            lastOrders: [],

            // Meine Daueraufträge
            repeatOrderList: [],
            checkedItems: new Map(),
            cronModal: false,
            invoiceModal2: false,

            // 1-Klick Bestellung
            voiceModal: false,
            berechtigungen2Modal: (document.URL.indexOf("?install-voice=1") >= 0 ? true:false),

            // Standort festlegen
            google_script_loaded: false,

            redirect: '',

            pagesModal: false,
        };

        this.onDayChanged = this.onDayChanged.bind(this);
    }

    static contextTypes = {
        router: () => null
    };

    componentDidMount() {

        // remove firstSelectInput from localstorage in order to show suggestion box if user not using x icon
        // Search
        localStorage.removeItem("firstSelectInput");

        // Land/Sprache ändern
        this.props.getSingleLanguageData(localStorage.getItem("userPreferedLanguage")?localStorage.getItem("userPreferedLanguage"):1);


        // Standort festlegen
        const existingScript = document.getElementById("googleMaps");
        if (!existingScript) {
            const script = document.createElement("script");
            script.src = "https://maps.googleapis.com/maps/api/js?key=" + localStorage.getItem("googleApiKey") + "&libraries=places";
            script.id = "googleMaps";
            document.body.appendChild(script);
            script.onload = () => {
                this.setState({ google_script_loaded: true });
            };
        }

        // Meine Daueraufträge
        if(localStorage.getItem("fromTel")){
            this.lastOrders();
            this.repeatOrderList();
        }

        if(document.URL.indexOf("?showModalShops=1") >= 0){
            this.showModalShops();
        }

        if(document.URL.indexOf("?showModalCart=1") >= 0){
            this.showModalCart();
        }

    }
    
    componentWillUnmount() {// copied from Location/PopularPlaces START
        //remove script when component unmount
        const existingScript = document.getElementById("googleMaps");
        if (existingScript) {
            existingScript.parentNode.removeChild(existingScript);
        }
    }

    //pages
    showModalPages (id) {
        this.setState({
            pagesModal: {
                [id]: true
            }
        });
    }
    hideModalPages = () => {
        this.setState({
            pagesModal: false
        });
    }

    //search
    showModalSearch () {
        this.setState({
            searchModal: true
        });
    }
    hideModalSearch = () => {
        this.setState({
            searchModal: false
        });
    }

    //info
    showModalCat () {
        this.setState({
            catModal: true
        });
    }
    hideModalCat = () => {
        this.setState({
            catModal: false
        });
    }

    //info
    showModalInfo () {
        this.setState({
            infoModal: true
        });
    }
    hideModalInfo = () => {
        this.setState({
            infoModal: false
        });
    }

    //shops
    showModalShops () {
        this.setState({
            shopsModal: true
        });
    }
    hideModalShops = () => {
        this.setState({
            shopsModal: false
        });
    }

    //cart
    showModalCart (redirect) {
        this.setState({
            cartModal: true
        });
        if(redirect){
            this.setState({
                redirect: redirect
            });
        }
    }
    hideModalCart = () => {
        this.setState({
            cartModal: false
        });
    }

    //1-Klick
    showModalVoice (id) {
        this.setState({
            voiceModal: {
                [id]: true
            }
        });
    }
    hideModalVoice = () => {
        this.setState({
            voiceModal: false
        })
    }
    showModalBerechtigungen2 (id) {
        this.setState({
            berechtigungen2Modal: {
                [id]: true
            }
        });
    }
    hideModalBerechtigungen2 = () => {
        this.setState({
            berechtigungen2Modal: false
        })
    }
    showModalInstall2 () {
        this.setState({
            install2Modal: true
        });
    }
    hideModalInstall2 = () => {
        this.setState({
            install2Modal: false
        });
    }
    triggerInstall2Redirect = (id, code, name) => {
        if(document.URL.indexOf("//app.") >= 0){
            var url = "https://"+window.location.host+window.location.pathname+"?install-voice=1&id="+id+"&code="+code+"&name="+name;
        } else {
            var url = "https://app."+window.location.host+window.location.pathname+"?install-voice=1&id="+id+"&code="+code+"&name="+name;
        }

        if(matchMedia('(display-mode: standalone)').matches == true) {
            window.open(url);
        } else {
            window.location = url;
        }
    }
    onAppNameChanged = (e) => {
        this.setState({
            appName: e.currentTarget.value
        });
    }

    // hamburger menu
    showModalAccount () {
        this.setState({
            accountModal: true
        });
    }
    hideModalAccount = () => {
        this.setState({
            accountModal: false
        });
    }

    // Meine Bestellungen
    showModalOrders () {
        this.setState({
            ordersModal: true
        });
    }
    hideModalOrders = () => {
        this.setState({
            ordersModal: false
        });
    }
    lastOrders = () => {
        if(localStorage.getItem("fromTel") && localStorage.getItem("codeCustomer")){
            axios
            .post(ORDER_LIST_URL, {
                FirstScreen: true,
                fromTel: localStorage.getItem("fromTel")
            })
            .then(response => {
                this.setState({
                    lastOrders: [...this.state.lastOrders, ...response.data],
                });
                // console.log("lastOrders", response.data);
            })
            .catch(function(error) {
                console.log(error);
            });
        }
    } 
    elapsedTime = (time) => {
        switch (typeof time) {
            case 'number':
            break;
            case 'string':
            time = +new Date(time);
            break;
            case 'object':
            if (time.constructor === Date) time = time.getTime();
            break;
            default:
            time = +new Date();
        }
        var time_formats = [
            [60, 's', 1], // 60
            [120, 'vor 1 min', '1 minute from now'], // 60*2
            [3600, 'min', 60], // 60*60, 60
            [7200, 'vor 1 h', '1 hour from now'], // 60*60*2
            [86400, 'h', 3600], // 60*60*24, 60*60
            [172800, 'Gestern', 'Tomorrow'], // 60*60*24*2
            [604800, 'Tagen', 86400], // 60*60*24*7, 60*60*24
            [1209600, 'Letzte Woche', 'Next week'], // 60*60*24*7*4*2
            [2419200, 'Wochen', 604800], // 60*60*24*7*4, 60*60*24*7
            [4838400, 'Letzten Monat', 'Next month'], // 60*60*24*7*4*2
            [29030400, 'Monaten', 2419200], // 60*60*24*7*4*12, 60*60*24*7*4
            [58060800, 'Letztes Jahr', 'Next year'], // 60*60*24*7*4*12*2
            [2903040000, 'Jahren', 29030400], // 60*60*24*7*4*12*100, 60*60*24*7*4*12
        ];
        var seconds = (+new Date() - time) / 1000,
            token = 'vor',
            list_choice = 1;

        if (seconds == 0) {
            return 'Gerade jetzt'
        }
        if (seconds < 0) {
            seconds = Math.abs(seconds);
            token = 'ab jetzt';
            list_choice = 2;
        }
        var i = 0,
            format;
        while (format = time_formats[i++])
            if (seconds < format[0]) {
            if (typeof format[2] == 'string')
                return format[list_choice];
            else
                return token + ' ' + Math.floor(seconds / format[2]) + ' ' + format[1];
            }
        return time;
    }  
    toDate = (unix_timestamp) => {
        var d = new Date(unix_timestamp * 1000);
        return d.getFullYear() + '-' + (d.getMonth()+1) + '-' + d.getDate() + ' ' + d.getHours() + ':' + d.getMinutes();
    }
    getAddress = (address, extract, long=true) => {
        if(extract == "str+nr"){
            var res = address;
            var split = res.split(",");
            if(long) var res = split.slice(0, split.length - 1).join(",");
            else var res = split[0];
            return res;
        } else if(extract == "str"){
            var res = address;
            var split = res.split(",");
            var res = split[0];
            return res;
        }  else if(extract == "plz+city"){
            var res = address;
            var split = res.split(",");
            var res = split[1];
            return res;
        } else if(extract == "plz"){
            var res = address;
            var split = res.split(",");
            var res = split[1];

            //only plz without city
            var split = res.split(" ");
            var res = split[1];
            return res;
        } else {
            var res = address.split(", ");
            return res[0];
        }
    }
    navigation = (lat, long) =>{
        // If it's an iPhone..
        if( (navigator.platform.indexOf("iPhone") != -1) 
            || (navigator.platform.indexOf("iPod") != -1)
            || (navigator.platform.indexOf("iPad") != -1))
             window.open("maps://maps.google.com/maps?daddr="+lat+","+long+"&amp;ll=");
        else
             window.open("http://maps.google.com/maps?daddr="+lat+","+long+"&amp;ll=");
    }
    status(orderstatus_id, delivery_type) {
        if(delivery_type == 2){
            var status = "Selbstabholung";
        } else if(orderstatus_id == 1){
            var status = "Offene Bestellung";
        } else if(orderstatus_id == 2){
            var status = "Bestellung bestätigt";
        } else if(orderstatus_id == 3){
            var status = "Wird gekocht";
        } else if(orderstatus_id == 4){
            var status = "Zusteller holt Lieferung ab";
        } else if(orderstatus_id == 5){
            var status = "Zusteller unterwegs zu dir";
        } else if(orderstatus_id == 6){
            var status = "Bestellung abgelehnt";
        } else if(orderstatus_id == 7){
            var status = "Selbstabholung";
        } else if(orderstatus_id == 8){
            var status = "Bestellung abgelaufen";
        }

        return status;
    }
    capitalizeFLetter = (name) => { 
        var name = name.charAt(0).toUpperCase() + name.slice(1)
        return name;
    } 
    getMinOnly = (lat, lng, restaurant_lat, restaurant_lng, delivery_radius)=>{
        var distance = this.getDistance(lat, lng, restaurant_lat, restaurant_lng, delivery_radius);

        var percent = distance * 100 / delivery_radius;

        if(percent >= 90){
            var min = 60;
        } else if(percent >= 80){
            var min = 55;
        } else if(percent >= 70){
            var min = 50;
        } else if(percent >= 60){
            var min = 45;
        } else {
            var min = 40;
        }

        return min;
    }
    getDistance = (latitudeFrom, longitudeFrom, latitudeTo, longitudeTo) =>{
        var latFrom = this.degrees_to_radians(latitudeFrom);
        var lonFrom = this.degrees_to_radians(longitudeFrom);
        var latTo = this.degrees_to_radians(latitudeTo);
        var lonTo = this.degrees_to_radians(longitudeTo);

        var latDelta = latTo - latFrom;
        var lonDelta = lonTo - lonFrom;

        var angle = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(latDelta / 2), 2) +
            Math.cos(latFrom) * Math.cos(latTo) * Math.pow(Math.sin(lonDelta / 2), 2)));
        return (angle * 6371);
    }
    degrees_to_radians = (degrees) =>{
        var pi = Math.PI;
        return degrees * (pi/180);
    }
    olderThen2Hour = (added) => { 
        var ONE_HOUR = 60 * 60 * 1000 * 2; /* ms */    
        if(((Date.now()) - added * 1000) > ONE_HOUR){
            return true;
        } else {
            return false;
        }
    }   

    //Meine Daueraufträge
    showModalInvoice2 (id) {
        this.setState({
            invoiceModal2: {
                [id]: true
            }
        });
    }
    hideModalInvoice2 = () => {
        this.setState({
            invoiceModal2: false
        })
    }
    showModalCron (id) {
        this.setState({
            cronModal: {
                [id]: true
            }
        });
    }
    hideModalCron = () => {
        this.setState({
            cronModal: false
        });
        this.setState({
            checkedItems: new Map()
        });
        this.setState({
            cronDay: null
        });
        this.setState({
            cronTime: null
        });
        this.setState({
            cronAction: null
        });

        // console.log("checkedItems", this.state.checkedItems);
        // setTimeout(function () {
        //     console.log("cronDay", this.state.cronDay);
        //     console.log("cronTime", this.state.cronTime);
        //     console.log("cronAction", this.state.cronAction);
        // }, 1000);
        
    }
    showModalDauerauftag () {
        this.setState({
            dauerauftragModal: true
        });
    }
    hideModalDauerauftrag = () => {
        this.setState({
            dauerauftragModal: false
        });
    }
    repeatOrderList = () => {
        if(localStorage.getItem("fromTel") && localStorage.getItem("codeCustomer")){
            axios
            .post(ORDER_LIST_URL, {
                FirstScreen_repeatOrders: true,
                fromTel: localStorage.getItem("fromTel")
            })
            .then(response => {
                this.setState({
                    repeatOrderList: [...this.state.repeatOrderList, ...response.data],
                });
            })
            .catch(function(error) {
                console.log(error);
            });
        }
    } 
    onDayChanged(e) {
        const item = e.target.name;
        const isChecked = e.target.checked;
        this.setState(
            prevState => ({ 
                checkedItems: prevState.checkedItems.set(item, isChecked)
            })
        );

        setTimeout(() => {
            let checkedValue = this.getCheckedItems(this.state.checkedItems)
            this.setState({
                cronDay: checkedValue.toString()
              });
        }, 1000);
        
    }
    onTimeChanged = date => {
        this.setState({
            cronTime: date
        });
    };
    handleDateChangeRaw = (e) => {//disable user input
        e.preventDefault();
    }
    onActionChanged = (e) => {//this is radio button
        this.setState({
            cronAction: e.currentTarget.value
        });
    }
    onCronNameChanged = (e) => {
        this.setState({
            cronName: e.currentTarget.value
        });
    }
    weekday = (string) =>{
        var string = string.replace(1, "Montag").replace(2, "Dienstag").replace(3, "Mittwoch").replace(4, "Donnerstag").replace(5, "Freitag").replace(6, "Samstag").replace(7, "Sonntag");
        return string;
    }
    getDayName = () => {
        switch (new Date().getDay()) {
            case 0:
                return "sonntag"
            case 1:
                return "montag"
            case 2:
                return "dienstag"
            case 3:
                return "mittwoch"
            case 4:
                return "donnerstag"
            case 5:
                return "freitag"
            case 6:
                return "samstag"
        }
    }
    getCheckedItems = (items) => {
        var result = []
        
        items.forEach((key, value) => {
            if(key){
                result.push(value)
            }
        });

        return result
    }
    showModalCronFunc = (order_id, modal) => {
        this.hideModalInvoice();

        if(modal == "cronModal"){
            this.showModalCron(order_id);
        } else {
            this.showModalVoice(order_id);
        }
    }
    dauerauftragAdd = (order_id, restaurant_id, fromTel, pin, cronDay, cronTime, cronAction) => {

        var div = document.getElementById('dauerauftrag'+order_id);
        div.innerHTML = 'Laden...';

        if(cronDay && cronTime && cronAction){
            var result = document.getElementById('result'+order_id);
            result.innerHTML = '<span style="color:green">Wird gespeichert...</span>';
            
            var url = '//'+window.location.hostname+'/php/twilio/FirstScreen_dauerauftrag.php?order_id='+order_id+'&restaurant_id='+restaurant_id+'&fromTel='+btoa(fromTel)+'&pin='+pin+'&cronDay='+cronDay+'&cronTime='+cronTime+'&cronAction='+cronAction;
            axios
            .get(url)
            .then(response => {
                var div = document.getElementById('result'+order_id);
                div.innerHTML = response.data.text;

                setTimeout(function () {
                    window.location.reload(false);
                }, 3000);
            })
            .catch(function(error) {
                console.log(error);
            });
        } else {
            setTimeout(function () {
                var result = document.getElementById('result'+order_id);
                result.innerHTML = '<span style="color:red">Bitte Wochentage, Zeit und Aktion auswählen.</span>';
                
                var div = document.getElementById('dauerauftrag'+order_id);
                div.innerHTML = 'Speichern';
            }, 1000);
        }
    }
    dauerauftragDel = (order_id, pin) => {

        var div = document.getElementById('dauerauftrag'+order_id);
        div.innerHTML = 'Laden...';

        if(order_id && pin){
            var result = document.getElementById('result'+order_id);
            result.innerHTML = '<span style="color:green">Wird gelöscht...</span>';
            
            var url = '//'+window.location.hostname+'/php/twilio/FirstScreen_dauerauftrag.php?order_id='+order_id+'&pin='+pin+'&del=1';
            axios
            .get(url)
            .then(response => {
                var div = document.getElementById('result'+order_id);
                div.innerHTML = response.data.text;

                setTimeout(function () {
                    window.location.reload(false);
                }, 3000);
            })
            .catch(function(error) {
                console.log(error);
            });
        } else {
            setTimeout(function () {
                var div = document.getElementById('dauerauftrag'+order_id);
                div.innerHTML = 'Dauerauftrag löschen';
            }, 1000);
        }
    }

    //App installieren
    showModalInstall () {
        this.setState({
            installModal: true
        });
    }
    hideModalInstall = () => {
        this.setState({
            installModal: false
        });
    }
    showModalBerechtigungen () {
        this.setState({
            berechtigungenModal: true
        });
    }
    hideModalBerechtigungen = () => {
        this.setState({
            berechtigungenModal: false
        });
    }
    triggerInstallBtn = () => {
        var div = document.getElementById('download');
        div.innerHTML = 'Laden...';
        
        setTimeout(function () {

            if (INSTALL_APP_POPUP) {//INSTALL_APP_POPUP is declared globally
                console.log("INSTALL_APP_POPUP", INSTALL_APP_POPUP); 
                INSTALL_APP_POPUP.prompt(); //this will show the prompt from chrome
            } 

            div.innerHTML = 'Hinzufügen';
        }, 1500);
    }
    showPrompt = () => {
        console.log("button clicked");
        document.getElementById('showPwaprompt').style.display = "block";
    } 
    next = (install, pwaSupported) =>{
        if(pwaSupported == false){
            alert("Deine Browser Version wird nicht unterstützt. Bitte installiere Chrome 70 oder höher.");
        } else if(install == "voice"){
            this.hideModalBerechtigungen2();
            this.showModalInstall2();
        } else {
            this.hideModalBerechtigungen();
            this.showModalInstall();
        }
    }
    fullBrowserVersion = () =>{
        return fullBrowserVersion.replace( /^([^.]*\.)(.*)$/, function ( a, b, c ) { 
            return b + c.replace( /\./g, '' );
        });
    }

    //setup location
    showModalAddress (modal) {
        this.setState({
            AddressModal: true
        });
        if(modal !== undefined){
            this.setState({
                modal: modal
            });
        }
    }
    hideModalAddress = () => {
        this.setState({
            AddressModal: false
        });
    }
    
    convertCodetoLang = (code)=>{
        if(code == "DE"){
            var country = "Deutschland"
        } else if(code == "AT"){
            var country = "Österreich"
        } else if(code == "CH"){
            var country = "Schweiz"
        } 

        return country;
    }
    
    // other
    uppercase(str) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }
    host(){
        // var d= document.domain.split('.');
        // return d[1] || d[0];
        var h = window.location.host.replace("."+this.domain(), "");
        var h = this.uppercase(h);
        return h;
    }
    domain(){
        var d = window.location.host;
        var d = d.split(".").pop();
        return d;
    }
    getUrlParameter(sParam) {
        var sPageURL =decodeURIComponent(window.location.search.substring(1));
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam) {
                return sParameterName[1];
            }
        }
    }

    categories = (cat) => {
        if(cat == "restaurant"){
            try {
                let localState = localStorage.getItem('state');
                if (localState) {
                    localState = JSON.parse(localState).items.restaurant_items.items;

                    return Object.keys(localState).map((category, index) => (
                        <a href={"#"+category} onClick={this.hideModalCat} key={category} style={{display:"block", cursor:"pointer", marginBottom:"20px", fontWeight:600, fontFamily:"open sans", fontSize:"1.15rem", color:"#3d4152"}}>
                            {category}
                        </a>
                    ))
                }
            } catch (err) {
                console.log(err);
            }
        } else {
            return <Categories hideModalCat={this.hideModalCat} shop={JSON.parse(localStorage.getItem("state")).items.restaurant_info.name} slug={JSON.parse(localStorage.getItem("state")).items.restaurant_info.slug} />
        }
    }

    getBgId = (restaurant) => {
        
        // set background cover in header 
        if(restaurant){
            var cat = restaurant.cat || '';
            if(cat == 'grocery'){
                var bgID = "1452948491233-ad8a1ed01085";
            } else if(cat == 'drugStore'){
                var bgID = "1567585902972-53bd0b897bf3";
            } else if(cat == 'restaurant'){
                var bgID = "1571939634097-f1829662cdfc";
            } else if(cat == 'cellPhoneStore'){
                var bgID = "1556656793-08538906a9f8";
            } else if(cat == 'pharmacy'){
                var bgID = "1564202572318-0bda12d302cc";
            } else if(cat == 'bookStore'){
                var bgID = "1507842217343-583bb7270b66";
            }

            return bgID;
        }
    }

    myAddress = (redirectUrl, restaurant, AccLogin=false) => {
        
        //set redirect
        if(document.URL.indexOf("?pwa=") >= 0){
            var redirect = "?pwa="+this.getUrlParameter("pwa");
        } else if(restaurant){
            var slug = window.location.href.replace("?install=1", "").split("/").pop();
            var redirect = "?redirect="+slug;
        } else if(redirectUrl){
            var redirect = "?redirect="+redirectUrl;
        } else {
            var redirect = "?redirect="+window.location.href.split("/").pop();
        }

        //set myAddress
        if(redirectUrl == "shopper"){
            var myAddress = <a className="btn nav-location truncate-text" style={{color:localStorage.getItem("storeColor"), paddingTop:"20px", position: "relative", maxWidth: window.innerWidth - 130 }} href={"#download_app"}>App herunterladen <FontAwesomeIcon icon={faShoppingBag} style={{marginLeft:"5px"}} /><Ink duration="500" /></a>
            var login = <a className="btn nav-location truncate-text" style={{paddingTop:"20px", position: "relative", maxWidth: window.innerWidth - 130 }} href={"/delivery/login"}>Login <FontAwesomeIcon icon={faUser} style={{marginLeft:"5px"}} /><Ink duration="500" /></a>
        } else if(redirectUrl == "store"){
            var myAddress = <a className="btn nav-location truncate-text" style={{color:localStorage.getItem("storeColor"), paddingTop:"20px", position: "relative", maxWidth: window.innerWidth - 130 }} href={"#download_app"}>App herunterladen <FontAwesomeIcon icon={faStore} style={{marginLeft:"5px"}} /><Ink duration="500" /></a>
            var login = <a className="btn nav-location truncate-text" style={{paddingTop:"20px", position: "relative", maxWidth: window.innerWidth - 130 }} href={"https://store.lieferservice123.com"}>Login <FontAwesomeIcon icon={faUser} style={{marginLeft:"5px"}} /><Ink duration="500" /></a>
        } else if(localStorage.getItem("userSetAddress")){
            var myAddress = JSON.parse(localStorage.getItem("userSetAddress")).address;
            var myAddress = <button
                type="submit"
                className="btn nav-location truncate-text"
                style={{ position: "relative", maxWidth: window.innerWidth - 130 }}
                onClick={() => {this.context.router.history.push("/search-location"+redirect)}}
            >
                {myAddress.length > 25 ? `${myAddress.substring(0, 25)}...` : myAddress}
                <i
                    className="si si-arrow-down nav-location-icon ml-1"
                    style={{ color: localStorage.getItem("storeColor") }}
                />
                <Ink duration="500" />
            </button>
        } else {
            var myAddress = localStorage.getItem("firstScreenSetupLocation")?localStorage.getItem("firstScreenSetupLocation"):"Standort festlegen";
            var myAddress = <button
                type="submit"
                className="btn nav-location truncate-text"
                style={{ position: "relative", maxWidth: window.innerWidth - 130 }}
                onClick={() => {this.context.router.history.push("/search-location"+redirect)}}
            >
                {myAddress.length > 25 ? `${myAddress.substring(0, 25)}...` : myAddress}
                <i
                    className="si si-arrow-down nav-location-icon ml-1"
                    style={{ color: localStorage.getItem("storeColor") }}
                />
                <Ink duration="500" />
            </button>
        }

        if(AccLogin) {
            return login;
        } else {
            return myAddress;
        }
    }

    pwaSupported = () => {
        //check if chrome version is supported
        if(isChrome || isChromium){
            if(osName == "Windows" && this.fullBrowserVersion() >= 70){
                var pwaSupported = true;
            } else if(osName == "Android" && this.fullBrowserVersion() >= 31){
                var pwaSupported = true;
            } else if(osName == "Ubuntu" && this.fullBrowserVersion() >= 72){
                var pwaSupported = true;
            } else {
                var pwaSupported = false;
            }
        } else {
            var pwaSupported = false;
        }

        return pwaSupported;
    }

    address = () => {
        //get current address
        if (localStorage.getItem("userSetAddress")){
            var address = JSON.parse(localStorage.getItem("userSetAddress")).address;
            var address = this.getAddress(address, "street+nr");
        } else {
            var address = this.convertCodetoLang(localStorage.getItem("country"));
        }

        return address;
    }

    toogleUserPages () {
        var x = document.getElementById("userPages");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }

    render() {
        const props = this.props;
        const { lastOrders, repeatOrderList } = this.state;
        const { user, cartTotal, cartProducts } = this.props;
        const { pages } = this.props;

        //Meine Daueraufträge
        var cronTime = new Date(this.state.cronTime);
        var cronTime = cronTime.toLocaleTimeString('de').substr(0, 5);
        var datepicker = (restaurant_info) => {
            var fromHour = restaurant_info[this.getDayName()].slice(0, 2);
            var fromMin = restaurant_info[this.getDayName()].slice(3,5);
            if(fromMin == "00") var fromMin = 0;
            var openTo = restaurant_info[this.getDayName()].slice(-5);
            if(openTo.slice(0, -3) < 8) {
                var toHour = 23;
                var toMin = 59;
            } else {
                var toHour = restaurant_info[this.getDayName()].slice(8,-3);
                var toMin = restaurant_info[this.getDayName()].slice(-2);
            }
            if(toMin == "00") var toMin = 0;
            return (
                <DatePicker
                    selected={this.state.cronTime}
                    onChange={this.onTimeChanged}
                    onChangeRaw={this.handleDateChangeRaw}

                    showTimeSelect
                    showTimeSelectOnly
                    timeIntervals={15}
                    timeCaption="Time"
                    dateFormat="HH:mm"
                    timeFormat="HH:mm"
                    minTime={setHours(setMinutes(new Date(), fromMin), fromHour)}
                    maxTime={setHours(setMinutes(new Date(), toMin), toHour)}
                />
            );
        }

        // fix shopMenu position after scrolling
        setTimeout(function () {
            var shopMenu = document.getElementById("shopMenu");
            var backgroundImg = document.getElementById("backgroundImg");
            var restaurant_logo = document.getElementById("restaurant-logo");
            var searchBox2 = document.getElementById("searchBox2");
            var description = document.getElementById("description");
            var header = document.getElementById("header");
            if(pages.length > 0) var userPages = document.getElementById("userPages");
            var desktopSearch = document.getElementById("desktopSearch");
            var shopSearch_a_Name = document.getElementById("shopSearch_a_Name");
            var shopNameDesktop = document.getElementById("shopNameDesktop");
            var storeImg = document.getElementById("storeImg");
            var domainDesktop = document.getElementById("domainDesktop");
            var storeDesktop = document.getElementById("storeDesktop");

            if(shopMenu){
                window.addEventListener("scroll", function(e) {
                
                    if (e.currentTarget.scrollY > 148) {
                        shopMenu.classList.add("fix-shop-menu");
                        backgroundImg.classList.add("fix-backgroundImg");
                        restaurant_logo.classList.add("hideForEver");
                        if(searchBox2) searchBox2.classList.add("hideForEver");
                        description.classList.add("hideForEver");
                        header.style.marginTop = "263px";

                        if(pages.length > 0) userPages.classList.add("fix-userPages");
                        desktopSearch.classList.add("fix-desktopSearch");
                        shopSearch_a_Name.classList.add("fix-shopSearch_a_Name");
                        shopNameDesktop.classList.add("hide-shopNameDesktop");
                        storeImg.classList.add("show-storeImg");
                        domainDesktop.classList.add("addBg-domainDesktop");
                        storeDesktop.classList.add("addBg-storeDesktop");
                    } else {
                        shopMenu.classList.remove("fix-shop-menu");
                        backgroundImg.classList.remove("fix-backgroundImg");
                        restaurant_logo.classList.remove("hideForEver");
                        if(searchBox2) searchBox2.classList.remove("hideForEver");
                        description.classList.remove("hideForEver");
                        header.style.marginTop = "0";

                        if(pages.length > 0) userPages.classList.remove("fix-userPages");
                        desktopSearch.classList.remove("fix-desktopSearch");
                        shopSearch_a_Name.classList.remove("fix-shopSearch_a_Name");
                        shopNameDesktop.classList.remove("hide-shopNameDesktop");
                        storeImg.classList.remove("show-storeImg");
                        domainDesktop.classList.remove("addBg-domainDesktop");
                        storeDesktop.classList.remove("addBg-storeDesktop");
                    }
                
                });
            }
        }, 100);

        return (
            <React.Fragment>

            {props.shop ? (

                <div id="header">
                    <div className="backgroundImg" id="backgroundImg" style={{backgroundImage: "linear-gradient(0deg, rgba(0,0,0,0.2) 0%, rgba(0,0,0,0.8) 80%), url("+(props.restaurant.cover_image ? "https://lieferservice123.com"+props.restaurant.cover_image : "https://images.unsplash.com/photo-"+this.getBgId(props.restaurant)+"?ixlib=rb-1.2.1&auto=format&fit=crop&w="+props.bgWidth+"&h="+props.bgHeight+"&q=80)")}}>

                        <div className="headerMenuLeft">

                            {/* login (mobile) */}
                            <span className="menuHeaderLogin hide3" onClick={() => { this.showModalAccount(); }}>
                                <span className="mr-10 border-0"><i className="si si-user" style={{fontSize:"26px"}}></i></span>
                                <Ink duration="500" />
                            </span>

                            {/* logo */}
                            <a id="domainDesktop" className="domainTopDesktop hide2" href={'/'+props.restaurant.slug+((document.URL && document.URL.indexOf("?pwa=")) >= 0 ? "?pwa="+this.getUrlParameter("pwa"):"")}><span style={{ fontWeight: "700" }}>Lieferservice</span><Ink duration="500" /></a>

                            {/* shops */}
                            <span id="storeDesktop" className="StoreTopDesktop hide2" onClick={() => { localStorage.getItem("userSetAddress") === null ? this.showModalAddress("?showModalShops=1") : this.showModalShops() }}><b style={{ fontWeight: "600", fontFamily:"open sans"}}>Shops <i className="si si-arrow-down nav-location-icon ml-1"></i></b><Ink duration="500" /></span>

                        </div>

                        {/* shopName, description, img */}
                        <div id="shopSearch_a_Name" className="shopSearch_a_Name">

                            {/* shop name mobile */}
                            <div onClick={() => { localStorage.getItem("userSetAddress") === null ? this.showModalAddress("?showModalShops=1") : this.showModalShops() }} className="shopNameMobile hide3">
                                {props.restaurant.name}
                                <i className="si si-arrow-down nav-location-icon ml-10" style={{fontSize:"0.825rem"}}></i>
                                <div style={{fontSize:"11px", fontWeight:500}}>Lieferung in {localStorage.getItem("userSetAddress")?this.getAddress(JSON.parse(localStorage.getItem("userSetAddress")).address, "plz+city"):this.convertCodetoLang(localStorage.getItem("country"))}</div>
                                <Ink duration="500" />
                            </div>

                            {/* shop img */}
                            <div style={{margin:"8px 0 5px"}} className="restaurant-logo" id="restaurant-logo">
                                <div className="restaurant-logo__container" onClick={() => { localStorage.getItem("userSetAddress") === null ? this.showModalAddress("?showModalShops=1") : this.showModalShops() }}>
                                    <div className="restaurant-logo__inner" style={{position:"relative"}}>
                                        <img src={props.restaurant.image} alt={props.restaurant.name} />
                                        <Ink duration="500" />
                                    </div>
                                </div>
                            </div>

                            {/* shop search desktop */}
                            <div id="desktopSearch" style={{width:"100%", display:"none"}}>
                                <input
                                    onChange={this.onSearchChange}
                                    onClick={() => this.showModalSearch()}
                                    ref={input => this.searchAjax = input}
                                    className="form-control search-input search-box"
                                    placeholder={props.restaurant.name+' Suche...'}
                                    style={{fontSize:"16px", marginTop:"-5px", height:"36px", background:"#EFEFEF", border:"1px solid rgba(34, 34, 34, 0.15)", borderRadius:"5px"}}
                                />   
                            </div>

                            {/* shop name desktop */}
                            <div id="shopNameDesktop" style={{marginTop:"10px", fontSize:"28px", fontWeight:"bolder", lineHeight:"38px", textShadow:"1px 1px 1px rgba(0,0,0,0.9)"}} className="hide2">{props.restaurant.name}</div>
                            
                            {/* shop description */}
                            <b id="description" style={{fontSize:"14px", fontWeight:"bold", textShadow:"1px 1px 1px rgba(0,0,0,0.9)"}}>{props.restaurant.description}</b>
                        </div>

                        {/* searchbox */}
                        <div className="searchBox2" id="searchBox2">

                            <div className="input-location-icon-field" style={{ position: "relative" }}>
                                <label style={{display:"inherit"}}>
                                    <input
                                        onChange={this.onSearchChange}
                                        onClick={() => this.showModalSearch()}
                                        ref={input => this.searchAjax = input}
                                        className="form-control search-input search-box"
                                        placeholder={props.restaurant.name+' Suche...'}
                                        style={{fontSize:"16px", height:"40px", background:"#EFEFEF", border:"1px solid rgba(34, 34, 34, 0.15)", borderRadius:"5px"}}
                                    />
                                </label>
                            </div>

                        </div>
                        
                        <div className="headerMenuRight">

                            {/* login (desktop) */}
                            <span className="menuHeaderLogin hide2" onClick={() => { this.showModalAccount(); }} style={{cursor:"pointer", position:"relative", marginRight:"16px"}}>
                                <b style={{ fontWeight: "600", fontFamily:"open sans"}}>Account <i className="si si-arrow-down nav-location-icon ml-1"></i></b>
                                <Ink duration="500" />
                            </span>

                            {/* cart */}
                            <span onClick={() => { localStorage.getItem("userSetAddress") === null ? this.showModalAddress("?showModalCart=1") : this.showModalCart() }} style={{color:"#fff", cursor:"pointer", position:"relative", background:localStorage.getItem("cartColorBg"), borderRadius:"24px", lineHeight:2, padding:"1px 0 1px 8px", display:"inline-block"}} className="menuHeaderCart">
                                <span className="mr-10 border-0">
                                    {/* <i className="si si-basket" style={{fontSize:"26px"}}></i> */}
                                    <FontAwesomeIcon icon={faShoppingCart} style={{fontSize:"15px"}} />
                                    <span style={{paddingLeft: "9px", fontWeight: "bold", fontSize: "15px", textAlign:"center"}}>{cartTotal.productQuantity}</span>
                                </span>
                                <Ink duration="500" />
                            </span>
                        </div>

                    </div>

                    {/* speisekarte, bestellungen, info, kategorien */}
                    <div id="shopMenu" style={{overflow:"auto", fontFamily:"open sans", fontWeight:600, fontSize:"1.125rem", borderBottom:"1px solid #ebebeb", color:"#5f6368"}}>

                        <a id="storeImg" href={'/'+props.restaurant.slug+((document.URL && document.URL.indexOf("?pwa=")) >= 0 ? "?pwa="+this.getUrlParameter("pwa"):"")} className="tabStyle" style={{display:"none"}}>
                            <img src={'https://lieferservice123.com/'+props.restaurant.image} style={{position:"absolute", top:"6px", height: "40px", maxWidth:"50px"}} /> 
                            <span style={{marginLeft:"60px"}}>{props.restaurant.name}</span>
                            <Ink duration="500" />
                        </a>

                        <a href={'/'+props.restaurant.slug+((document.URL && document.URL.indexOf("?pwa=")) >= 0 ? "?pwa="+this.getUrlParameter("pwa"):"")} className={props.page=="RestaurantInfo"?"tabStyleClicked":"tabStyle"}>Home<Ink duration="500" /></a>

                        {/* {props.restaurant.cat == 'grocery' && <a href={'/kategorien/'+props.restaurant.slug+(document.URL.indexOf("?pwa=") >= 0 ? "?pwa="+this.getUrlParameter("pwa"):"")} style={props.page=="ShopCat"?tabStyleClicked:tabStyle}>Kategorien<Ink duration="500" /></a>} */}

                        {/* <a href={'/bestellungen/'+props.restaurant.slug+(document.URL.indexOf("?pwa=") >= 0 ? "?pwa="+this.getUrlParameter("pwa"):"")} style={props.page=="OrderList"?tabStyleClicked:tabStyle}>{localStorage.getItem("itemsPageOrders")}<Ink duration="500" /></a> */}

                        <a onClick={() => this.showModalSearch()} className="tabStyle search">Suche<Ink duration="500" /></a>

                        {(window.location.href.indexOf("/checkout") == -1 || JSON.parse(localStorage.getItem("state")).items.restaurant_info.cat == "grocery") && <a onClick={() => this.showModalCat()} className="tabStyle">Kategorien<Ink duration="500" /></a>}

                        <a onClick={() => this.showModalInfo()} className="tabStyle">Info<Ink duration="500" /></a>

                        <a id="mehr" onClick={() => this.toogleUserPages()} className="tabStyle" style={{display:pages.length > 0 ? "inherit" : "none"}}>Mehr <i className="si si-arrow-down nav-location-icon ml-10" style={{fontSize:"12px"}}></i><Ink duration="500" /></a>

                        <div id="userPages" style={{display:"none"}}>

                            {pages.map(page => (
                                <div key={page.id} style={{cursor:"pointer", padding:".5rem 1rem", marginTop:"1px"}}>
                                    <div 
                                        // onClick={() => this.showModalPages(page.id)}
                                        onClick={() => window.location = "/pages/"+JSON.parse(localStorage.getItem("state")).items.restaurant_info.name+"/"+page.slug}
                                    >
                                        {page.name}
                                    </div>
                                    <Modal show={this.state.pagesModal[page.id]} onHide={this.hideModalPages} size="lg">
                                        <Modal.Header closeButton style={{padding:"16px 18px 0"}}>
                                        </Modal.Header>
                                        <Modal.Body style={{padding:"0 30px 30px"}}>
                                            <div dangerouslySetInnerHTML={{ __html: page.body }} />
                                        </Modal.Body>
                                    </Modal>
                                </div>
                            ))}

                        </div>

                        {/* liefern nach (desktop) */}
                        <span className="hide2" onClick={() => { this.showModalAddress(); }} style={{float:"right", cursor:"pointer", position: "relative", padding:"13px 16px"}}>
                            Liefern nach <span style={{color:"rgb(252, 128, 25)"}}>
                                {this.address()}
                                <i className="si si-arrow-down nav-location-icon ml-10" style={{fontSize:"12px"}}></i>
                            </span>
                            <Ink duration="500" />
                        </span>
                    </div>

                    {/* liefern nach (mobile) */}
                    {props.shop && <div className="deliveryToMobile hide3">
                        <div onClick={() => { this.showModalAddress(); }}>
                            {/* <FontAwesomeIcon style={{float:"left", marginRight:"5px", marginTop: "5px"}} icon={faMapMarkerAlt} /> */}
                            Liefern nach <span style={{color:"rgb(252, 128, 25)"}}>
                                {this.address()}
                                <i className="si si-arrow-down nav-location-icon ml-10"></i>
                            </span>
                            <Ink duration="500" />
                        </div>
                    </div>}

                </div>

            ) : (

                <div>
                    <div className="col-12 p-0 sticky-top" style={{zIndex:0}}>
                        <div className="block m-0">
                            <div className="block-content p-0">
                                <div className="input-group search-box">
                                    
                                    <div className="input-group-prepend" onClick={this.context.router.history.goBack}>
                                        <button type="button" className="btn search-navs-btns" style={{ position: "relative" }}>
                                            <i className="si si-arrow-left" />
                                            <Ink duration="500" />
                                        </button>
                                    </div>

                                    <div className="form-control search-input" style={{lineHeight: "40px"}}>
                                        {/* {this.props.logo && <img src="/assets/img/logos/logo.png" alt={localStorage.getItem("storeName")} width="120" />} */}
                                        <span className="domainTop" style={{position:"relative", cursor:"pointer"}} onClick={() => this.context.router.history.push("/")}><span style={{ fontWeight: "700" }}>{this.host()}</span>.{this.domain()}<Ink duration="500" /></span>
                                    </div>

                                    <div className="input-group-append">
                                        {this.myAddress(props.redirectUrl, props.restaurant)}
                                        {window.innerWidth > 468 && this.myAddress(props.redirectUrl, props.restaurant, true)}
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>     
                </div>

            )}

            {/* shops */}
            <Modal show={this.state.shopsModal} onHide={this.hideModalShops}>
                <Modal.Header closeButton style={{background:"rgb(232, 233, 238)"}}>
                </Modal.Header>
                <Modal.Body style={{padding:0, background:"rgb(232, 233, 238)"}}>
                    <Shops />
                </Modal.Body>
            </Modal>

            {/* account */}
            <Modal show={this.state.accountModal} onHide={this.hideModalAccount} size="sm">
                <Modal.Header closeButton style={{padding:"16px 20px 0"}}>
                    <Modal.Title>Hi, {this.props.user.success ? this.capitalizeFLetter(this.props.user.data.name) : "Gast"}</Modal.Title>
                </Modal.Header>

                <Modal.Body style={{padding:"0 0 10px"}}>

                    <hr style={{borderColor:"#e5e5e5"}} />
                    
                    <UserMenu />

                </Modal.Body>
            </Modal>

            {/* cart */}
            <Modal show={this.state.cartModal} onHide={this.hideModalCart}>
                <Modal.Header closeButton className="bg-grey"></Modal.Header>
                <Modal.Body style={{padding:0}} className="bg-grey">
                    <Cart />
                </Modal.Body>
            </Modal>

            {/* departments */}
            <Modal show={this.state.catModal} onHide={this.hideModalCat} size="sm">
                <Modal.Header closeButton>
                </Modal.Header>
                <Modal.Body style={{padding:"32px"}}>
                    {JSON.parse(localStorage.getItem("state")).items.restaurant_info.name && this.categories(JSON.parse(localStorage.getItem("state")).items.restaurant_info.cat)}
                </Modal.Body>
            </Modal>

            {/* search */}
            <Modal show={this.state.searchModal} onHide={this.hideModalSearch}>
                {/* <Modal.Header closeButton>
                </Modal.Header> */}
                <Modal.Body style={{padding:0}}>
                    <Search restaurant={JSON.parse(localStorage.getItem("state")).items.restaurant_info} cartProducts={cartProducts} />
                </Modal.Body>
            </Modal>
       
            {/* info */}
            <Modal show={this.state.infoModal} onHide={this.hideModalInfo}>
                <Modal.Header closeButton style={{background:"rgb(232, 233, 238)"}}></Modal.Header>
                <Modal.Body style={{padding:"0 10px 0", background:"rgb(232, 233, 238)"}}>
                    {JSON.parse(localStorage.getItem("state")).items.restaurant_info.name && <Info slug={JSON.parse(localStorage.getItem("state")).items.restaurant_info.slug} />}
                </Modal.Body>
            </Modal>

            {/* deliver to */}
            <Modal show={this.state.AddressModal} onHide={this.hideModalAddress} size="sm">
                <Modal.Header closeButton className="bg-grey"><Modal.Title>Standort festlegen</Modal.Title></Modal.Header>
                <Modal.Body style={{padding:"0 25px 25px"}} className="bg-grey">
                    <div style={{fontSize: "12px", lineHeight: 1.5, marginBottom:"20px", color:"#555"}}> Lieferoptionen und -geschwindigkeit können, abhängig von der Lieferadresse, variieren</div>
                    <Location 
                        modal={this.state.modal || ''} 
                        redirect={window.location.href.replace("?install=1", "").split("/").pop()}
                    />
                </Modal.Body>
            </Modal>

            {/* my orders */}
            <Modal show={this.state.ordersModal} onHide={this.hideModalOrders}>
                <Modal.Header closeButton>
                    <Modal.Title>Meine Bestellungen</Modal.Title>
                </Modal.Header>
                <Modal.Body style={{padding:"0 0 10px"}}>

                    {lastOrders.length ? <div>
                        <div style={{maxWidth:"381px", margin:"auto", textShadow: "0 1px 4px rgba(0,0,0,0.1)"}}>
                            <div style={{marginTop:"15px"}}>
                                {lastOrders.map((row, index) => {

                                    return (
                            
                                        <div key={row.id} style={{maxWidth:"768px", overflow:"auto", fontSize:"0.98rem", padding:"8px 12px"}}>

                                            <div style={{width:"100%", float:"left", cursor:"pointer", position:"relative"}} onClick={() => window.location = '/running-order-s/'+row.id}>

                                                <div style={{float:"right", width:"80px", textAlign:"right", fontWeight:"bold"}}>-{row.currencyFormat} {(parseFloat(row.subtotal) + parseFloat(row.delivery_charge) + parseFloat(row.subtotal*(row.delivery_tip/100)) + parseFloat(row.subtotal*(localStorage.getItem("serviceFee")/100))).toFixed(2)}</div>

                                                <div style={{float:"left", width:"calc(100% - 80px)"}}>

                                                    <div style={{float:"left", height:"60px", marginRight:"15px"}}><img src={'https://lieferservice123.com'+row.image} style={{width:"50px"}} /></div>

                                                    <div style={{color:"#666", fontSize:"12px"}}>{this.elapsedTime(this.toDate(row.added))}</div>

                                                    <div style={{fontSize:"16px"}}>{row.name}</div>

                                                </div>

                                                <Ink duration="500" />

                                            </div>

                                            {/* Dauerauftrag Einstellen */}
                                            <Modal show={this.state.cronModal[row.id]} onHide={this.hideModalCron}>
                                                <Modal.Header closeButton>
                                                    <Modal.Title>Dauerauftrags Bestellung</Modal.Title>
                                                </Modal.Header>
                                                <Modal.Body style={{padding:"0 25px 10px"}}>

                                                    <hr/>

                                                    {/* day picker */}
                                                    <div style={{fontWeight: 300, fontFamily: "open sans", marginBottom:"10px"}}>Wochentage *</div>  
                                                    {checkboxes.map(item => (
                                                        <label key={item.key} style={{margin:"5px 15px 5px 0", fontWeight:"normal"}}>
                                                            <Checkbox name={item.key} checked={this.state.checkedItems.get(item.key)} onChange={this.onDayChanged} /> {item.name}
                                                        </label>
                                                    ))}

                                                    <hr/>

                                                    {/* time picker */}
                                                    {this.state.cronDay ? <div><div style={{fontWeight: 300, fontFamily: "open sans", marginBottom:"10px"}}>Zeit *</div>{datepicker(row)}<hr/></div>:null}
                                                    
                                                    {/* action */}
                                                    {this.state.cronDay && this.state.cronTime ? <div><div style={{fontWeight: 300, fontFamily: "open sans", marginBottom:"10px"}}>Aktion *</div>
                                                    <label className="payment_func" style={{display:"inline-block", border:"none", marginBottom:0}}>
                                                        Zahlungspflichtig Bestellen 
                                                        {this.state.cronAction == 0 ? <input type="radio" name="cronAction" value="0" onChange={this.onActionChanged} defaultChecked />  : <input type="radio" name="cronAction" value="0" onChange={this.onActionChanged} /> }
                                                        <span className="checkmark"></span>
                                                        <Ink duration="500" />
                                                    </label>
                                                    <FontAwesomeIcon data-for='action0' data-tip='' data-place='left' style={{marginLeft:"10px", fontSize:"16px"}} icon={faInfoCircle} />
                                                    <ReactTooltip id='action0'><div style={{fontSize:"10px", fontWeight:"300", fontFamily:"open sans"}}>Wir senden zum vereinbarten<br/>Zeitpunkt eine verbindliche Bestellung<br/>an den Anbieter.</div></ReactTooltip>
                                                    <br/>
                                                    <label className="payment_func" style={{display:"inline-block", border:"none", marginBottom:0}}>
                                                        Bestätigungs SMS senden 
                                                        {this.state.cronAction == 1 ? <input type="radio" name="cronAction" value="1" onChange={this.onActionChanged} defaultChecked />  : <input type="radio" name="cronAction" value="1" onChange={this.onActionChanged} /> }
                                                        <span className="checkmark"></span>
                                                        <Ink duration="500" />
                                                    </label>
                                                    <FontAwesomeIcon data-for='action1' data-tip='' data-place='left' style={{marginLeft:"10px", fontSize:"16px"}} icon={faInfoCircle} />
                                                    <ReactTooltip id='action1'><div style={{fontSize:"10px", fontWeight:"300", fontFamily:"open sans"}}>Wir senden nur dann eine verbindliche<br/>Bestellung an den Anbieter, wenn Sie<br/>den Link klicken, denn wir Ihnen<br/>eine Stunde vor dem vereinbarten<br/>Zeitpunkt, per SMS zusenden.</div></ReactTooltip>

                                                    <hr/></div> : null}

                                                    <div id={'result'+row.id} style={{marginTop:"10px"}}></div>

                                                    <div style={{marginTop:"20px", fontSize:"12px", color:"#999"}}>
                                                        Einmalig einstellen und Dein Essen wird täglich zum vereinbarten Zeitpunkt zugestellt.
                                                    </div>
                                                </Modal.Body>
                                                <Modal.Footer>
                                                    <Button variant="secondary" onClick={this.hideModalCron}> 
                                                        Schließen
                                                    </Button> 
                                                    
                                                    <Button variant="primary" onClick={() => { this.dauerauftragAdd(row.id, row.restaurant_id, row.fromTel, row.code, this.state.cronDay, cronTime, this.state.cronAction); }} id={'dauerauftrag'+row.id}> 
                                                        Speichern 
                                                    </Button>
                                                </Modal.Footer>
                                            </Modal>

                                            {/* 1-Klick Einstellen */}
                                            <Modal show={this.state.voiceModal[row.id]} onHide={this.hideModalVoice} size="sm">
                                                <Modal.Header closeButton>
                                                    <Modal.Title>1-Klick Bestellung</Modal.Title>
                                                </Modal.Header>
                                                <Modal.Body style={{padding:"0 25px 10px"}}>

                                                    <label style={{fontWeight:"normal", display:"block", marginTop:"10px"}}>
                                                        speichern als... *
                                                        <input 
                                                        name="appName" 
                                                        style={{padding:"5px", width:"100%"}} 
                                                        defaultValue={this.state.appName}
                                                        onChange={this.onAppNameChanged} 
                                                        />
                                                    </label>
                                                    
                                                    <div id={'result'+row.id} style={{marginTop:"10px"}}></div>

                                                    <div style={{marginTop:"20px", fontSize:"12px", color:"#999"}}>
                                                        Einmal eingestellt und du kannst diese Bestellung mit nur einem Klick (vom Startbildschirm aus) aufgeben.
                                                    </div>
                                                </Modal.Body>
                                                <Modal.Footer>
                                                    <Button variant="secondary" onClick={this.hideModalVoice}> 
                                                        Schließen
                                                    </Button> 
                                                    
                                                    <Button variant="primary" onClick={() => { this.triggerInstall2Redirect(row.id, row.code, this.state.appName) }}> 
                                                        Weiter 
                                                    </Button>
                                                </Modal.Footer>
                                            </Modal>
                                            
                                        </div>
                                    )
                                    
                                })}
                                
                            </div> 
                        </div>
                    </div> : <div style={{padding:"25px", textAlign:"center"}}>
                        <div style={{fontWeight:"bold", marginBottom:"10px"}}>Noch keine Bestellungen</div>
                        <div style={{fontSize:"12px"}}>Du hast noch keine Bestellung getätigt.</div>
                    </div>}

                </Modal.Body>
            </Modal>

            {/* my repeat orders */}
            <Modal show={this.state.dauerauftragModal} onHide={this.hideModalDauerauftrag}>
                <Modal.Header closeButton>
                    <Modal.Title>Meine Daueraufträge</Modal.Title>
                </Modal.Header>
                <Modal.Body style={{padding:"0 0 10px"}}>

                    {repeatOrderList.length ? <div>
                        <div style={{maxWidth:"381px", margin:"auto", textShadow: "0 1px 4px rgba(0,0,0,0.1)"}}>
                            <div style={{marginTop:"15px"}}>
                                {repeatOrderList.map((row, index) => {

                                    return (
                            
                                        <div key={row.id} style={{maxWidth:"768px", background:"#fff", overflow:"auto", fontSize:"0.98rem", padding:"8px 12px"}}>

                                            <div style={{width:"100%", float:"left", cursor:"pointer", position:"relative"}} onClick={this.showModalInvoice2.bind(this, row.id)}>

                                                <div style={{float:"right", width:"80px", textAlign:"right", fontWeight:"bold"}}>-{row.currencyFormat} {row.total}</div>

                                                <div style={{float:"left", width:"calc(100% - 80px)"}}>

                                                    <div style={{float:"left", height:"80px", marginRight:"15px"}}><img src={'https://lieferservice123.com'+row.image} style={{width:"50px"}} /></div>

                                                    {/* anbieter */}
                                                    <div style={{fontSize:"16px"}}>{row.name}</div>

                                                    {/* address */}
                                                    {/* <div style={{fontSize:"18px"}}>{(row.delivery_type == 2?"Selbstabholung":this.getAddress(row.address, "", false))}</div> */}
                                                    
                                                    {/* status */}
                                                    <div style={{fontSize:"12px", color:"rgb(102, 102, 102)", lineHeight:1.5, marginBottom:"4px"}}>
                                                        {row.confirm_required == 1 ? "Bestätigungs SMS senden":"Zahlungspflichtig bestellen"} am {this.weekday(row.day)} um {row.time.slice(0, -3)}
                                                    </div>

                                                </div>

                                                <Ink duration="500" />

                                            </div>

                                            {/* rechnung, dauerauftrag löschen, dauerauftrag status */}
                                            <Modal show={this.state.invoiceModal2[row.id]} onHide={this.hideModalInvoice2}>
                                                
                                                <Modal.Header closeButton>
                                                    <Modal.Title>Dauerauftrag - {row.name}</Modal.Title>
                                                </Modal.Header>

                                                <Modal.Body style={{padding:"0 25px 10px"}}>
                                                    <div style={{padding:"10px 0", marginBottom:"10px", borderTop:"1px solid #eaecee", borderBottom:"1px solid #eaecee"}}>
                                                        {this.capitalizeFLetter(row.fromName)} {row.vip > 0 ? <img src="/assets/img/vip4.png" style={{width:"26px", height:"26px"}} /> : "" }<br/>
                                                        <span style={{color:"rgb(26, 115, 232)"}} onClick={() => this.navigation(JSON.parse(row.location).lat, JSON.parse(row.location).lng)}>{this.getAddress(row.address, "str+nr")}</span><br/>
                                                        TEL <a style={{color:"rgb(26, 115, 232)"}} href={'tel:' + row.fromTel}>{row.fromTel}</a><br/>

                                                        <br/>

                                                        <b>WICHTIG</b><br/>
                                                        Bezahlen mit {row.payment_mode === 'Barzahlung'?"":"💳"} {row.payment_mode}<br/>
                                                        {/* {row.delivery_type == 1 ? "Lieferung":"Abholung"} um: {this.toDate(row.added+3600)}<br/> */}

                                                        <br/>

                                                        <b>INTERVALL</b><br/>
                                                        {row.confirm_required == 1 ? "Bestätigungs SMS senden":"Zahlungspflichtig bestellen"} am {this.weekday(row.day)} um {row.time.slice(0, -3)}
                                                        
                                                    </div>

                                                    <ul style={{listStyle:"none", margin:0, marginLeft:"15px", padding:0}}>

                                                        <GuestCheckout currencyFormat={row.currencyFormat} id={row.id} />

                                                        {(row.vip && this.state.activatePremium > 0 && (row.total - row.delivery_charge + parseFloat(row.discount)).toFixed(2) >= 15) ? 
                                                        <li style={{width:"100%", overflow:"auto", margin:"5px 0"}}>
                                                            <span>
                                                                <span style={{color:"#757575", width:"20px", float:"left"}}>1x</span> 
                                                                Gratisgetränk <FontAwesomeIcon icon={faInfoCircle} data-tip='' data-for='freeDrink' />
                                                            </span>
                                                            <span style={{padding:"0 4px", width:"80px", float:"right", textAlign:"right"}}>0.00 {row.currencyFormat}</span> 
                                                        </li> : ""}
                                                        <li style={{fontWeight:"bold"}}>
                                                            Zwischensumme
                                                            <span style={{padding:"0 4px", width:"80px", float:"right", textAlign:"right"}}>{(row.total - row.delivery_charge + parseFloat(row.discount)).toFixed(2)} {row.currencyFormat}</span> 
                                                        </li>
                                                        <li style={{fontWeight:"bold"}}>
                                                            Lieferkosten
                                                            <span style={{padding:"0 4px", width:"80px", float:"right", textAlign:"right"}}>{row.delivery_charge} {row.currencyFormat}</span> 
                                                        </li>
                                                        {(row.discount > 0) ? 
                                                        <li style={{fontWeight:"bold"}}>
                                                            Mengenrabatt ({(row.discount * 100 / (row.total - row.delivery_charge + parseFloat(row.discount)).toFixed(2)).toFixed()}%)
                                                            <span style={{padding:"0 4px", width:"80px", float:"right", textAlign:"right"}}>-{row.discount} {row.currencyFormat}</span> 
                                                        </li> : ""}
                                                        <li style={{fontWeight:"bold"}}>
                                                            Gesamt
                                                            <span style={{padding:"0 4px", width:"80px", float:"right", textAlign:"right"}}>{row.total} {row.currencyFormat}</span> 
                                                        </li>

                                                    </ul>

                                                    <ReactTooltip id='freeDrink' place="top"><div style={{fontSize:"10px", fontWeight:"300", fontFamily:"open sans"}}>0,3L Cola, Fanta oder Sprite<br />gratis bei jeder Bestellung ab 15 {row.currencyFormat}</div></ReactTooltip>

                                                    <div id={'result'+row.id} style={{marginTop:"10px"}}></div>
                                                </Modal.Body>

                                                <Modal.Footer>
                                                    <Button variant="secondary" onClick={this.hideModalInvoice2}> 
                                                        Schließen
                                                    </Button> 

                                                    <Button variant="danger" onClick={() => { this.dauerauftragDel(row.id, row.code); }} id={'dauerauftrag'+row.id}> 
                                                        Löschen 
                                                    </Button>
                                                </Modal.Footer>
                                            </Modal>


                                        </div>
                                    )
                                    
                                })}
                                
                            </div> 
                        </div>
                    </div> : <div style={{padding:"25px", textAlign:"center"}}>
                        <div style={{fontWeight:"bold", marginBottom:"10px"}}>Noch keine Daueraufträge</div>
                        <div style={{fontSize:"12px"}}>Du hast noch keine Daueraufträge erstellt.</div>
                    </div>}

                </Modal.Body>
            </Modal>

            {/* app advantages */}
            <Modal show={this.state.berechtigungenModal} onHide={this.hideModalBerechtigungen}>
                
                <Modal.Header closeButton>
                    <Modal.Title>App-Vorteile</Modal.Title>
                </Modal.Header>

                <Modal.Body style={{padding:"0 25px 10px"}}>
                    <div style={{marginTop:"12px"}}>
                        <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Lieferservice123 auf Desktop installieren.</div>

                        {/* <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Ohne Internetverbindung bei Lieferservice123 bestellen.</div> */}

                        {/* <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Bis zu 32% Mengenrabatt.</div> */}

                        <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Lieferung auf der Karte verfolgen.</div>

                        <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Benutzerelebnis durch Schnelles Laden.</div>

                        <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Bessere Kommunikation zwischen Shopper und Kunde durch Push-Nachrichten.</div>

                        <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Zuverlässig auch bei schlechter Internetverbindung.</div>

                        <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Installation über Google Chrome.</div>
                    </div>

                </Modal.Body>

                <Modal.Footer>
                    <Button variant="secondary" onClick={this.hideModalBerechtigungen}> 
                        Schließen
                    </Button> 
                    <Button variant="primary" onClick={() => this.next("", this.pwaSupported())}>
                        Weiter
                    </Button>
                </Modal.Footer>
            </Modal>

            {/* app install */}
            <Modal show={this.state.installModal} onHide={this.hideModalInstall}>
                
                <Modal.Header closeButton>
                    <Modal.Title>Installieren</Modal.Title>
                </Modal.Header>

                <Modal.Body style={{padding:"0 25px 10px"}}>
                    <div style={{marginTop:"12px"}}>
                        Klicke auf hinzufügen um Lieferservice123 auf deinen Bildschirm zu installieren.
                    </div>

                    <div style={{marginTop:"20px", fontSize:"11px", color:"#999"}}>Wenn dir Lieferservice123 gefällt, dann installiere bitte die  App.</div>
                </Modal.Body>

                <Modal.Footer>
                    <Button variant="secondary" onClick={this.hideModalInstall}> 
                        Schließen
                    </Button> 
                    <Button variant="primary" onClick={() => this.triggerInstallBtn()} id="download">
                        Hinzufügen
                    </Button>
                </Modal.Footer>
            </Modal>
 
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    language: state.languages.language,
    cartTotal: state.total.data,
    cartProducts: state.cart.products,
    user: state.user.user,
    pages: state.pages.pages,
});

export default connect(
    mapStateToProps,
    { getSingleLanguageData }
)(Nav);
