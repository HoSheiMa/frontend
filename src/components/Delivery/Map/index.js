import React, { Component } from "react";
import ReactMapGL, { Marker } from "react-map-gl";

import ContentLoader from "react-content-loader";
import { connect } from "react-redux";
import { sendDeliveryGuyGpsLocation } from "../../../services/Delivery/gpslocation/actions";
import { geolocated, isGeolocationEnabled } from "react-geolocated";

class Map extends Component {
    state = {
        viewport: {
            width: window.innerWidth,
            height: 400,
            zoom: 15,
            latitude: null,
            longitude: null
        },
        gps_latitude: null,
        gps_longitude: null,
        loading: true,
        showMap: false
    };

    componentDidMount() {}
    componentWillReceiveProps(nextProps) {
        const { single_delivery_order } = nextProps;
        if (nextProps.single_delivery_order.restaurant) {
            try {
                this.setState({
                    viewport: {
                        ...this.state.viewport,
                        latitude: parseFloat(single_delivery_order.restaurant.latitude),
                        longitude: parseFloat(single_delivery_order.restaurant.longitude)
                    },
                    gps_latitude: parseFloat(JSON.parse(single_delivery_order.location).lat),
                    gps_longitude: parseFloat(JSON.parse(single_delivery_order.location).lng)
                });
                console.log("HERE");
                this.setState({ loading: false, showMap: true });
            } catch {
                console.log("SECOND");

                //handling old addresses without lat lng
                this.setState({
                    viewport: {
                        ...this.state.viewport,
                        latitude: single_delivery_order.restaurant.latitude,
                        longitude: single_delivery_order.restaurant.longitude
                    },
                    loading: false,
                    showMap: false
                });
            }
        }

        if (this.props.startSendingDeliveryLocation !== nextProps.startSendingDeliveryLocation || this.props.startSendingDeliveryLocation) {
            if (this.props.coords === nextProps.coords) {
                if (this.state.viewport.latitude !== null) {
                    this.__sendGpsLocation();
                }
            }
        }

        // if (nextProps.isGeolocationEnabled) {
        //     this.setState({ showLocationError: false });
        //     if (!this.state.showMap) {
        //         this.setState({ showMap: false });
        //     }
        // } else
        if (!nextProps.isGeolocationEnabled && !this.state.showLocationError) {
            this.setState({ showLocationError: true, loading: false });
            console.log("Please allow location access for live tracking");
        }
    }

    sendingGpsLocationInterval = 0;
    __sendGpsLocation = () => {
        this.sendingGpsLocationInterval = setInterval(() => {
            this.props.sendDeliveryGuyGpsLocation(
                this.props.delivery_user.data.auth_token,
                this.props.order_id,
                this.state.gps_latitude,
                this.state.gps_longitude
            );
        }, 15 * 1000);
    };

    componentWillUnmount() {
        clearInterval(this.sendingGpsLocationInterval);
    }

    render() {
        return (
            <React.Fragment>
                <div className="pt-50">
                    {this.state.showLocationError && (
                        <div className="auth-error location-error">
                            <div className="error-shake">{localStorage.getItem("allowLocationAccessMessage")}</div>
                        </div>
                    )}
                    {this.state.loading ? (
                        <ContentLoader height={400} width={window.innerWidth} speed={1.2} primaryColor="#f3f3f3" secondaryColor="#ecebeb">
                            <rect x="0" y="0" rx="0" ry="0" width={window.innerWidth} height="400" />
                        </ContentLoader>
                    ) : (
                        <React.Fragment>
                            {this.state.showMap && (
                                <ReactMapGL
                                    {...this.state.viewport}
                                    mapboxApiAccessToken={localStorage.getItem("mapApiKey")}
                                    onViewportChange={viewport => this.setState({ viewport })}
                                >
                                    <Marker latitude={this.state.gps_latitude} longitude={this.state.gps_longitude} offsetLeft={-30} offsetTop={-35}>
                                        <div>
                                            <img src="/assets/img/various/selected-location-marker.png" alt="Delivery Location" />
                                        </div>
                                    </Marker>
                                    <Marker
                                        latitude={parseFloat(this.props.restaurant_latitude)}
                                        longitude={parseFloat(this.props.restaurant_longitude)}
                                        offsetLeft={-30}
                                        offsetTop={-35}
                                    >
                                        <div>
                                            <img
                                                src="/assets/img/various/restaurant-map-marker.png"
                                                alt="Restaurant Location"
                                                width="35"
                                                height="47"
                                            />
                                        </div>
                                    </Marker>
                                </ReactMapGL>
                            )}
                        </React.Fragment>
                    )}
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    delivery_user: state.delivery_user.delivery_user
});

export default connect(
    mapStateToProps,
    { sendDeliveryGuyGpsLocation }
)(geolocated({ isOptimisticGeolocationEnabled: false }, isGeolocationEnabled)(Map));
