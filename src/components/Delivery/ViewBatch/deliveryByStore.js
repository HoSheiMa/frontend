import React, { Component } from 'react'
import Ink from "react-ink";
import ReactDOM from 'react-dom';
import axios from "axios";
import { getSettings } from "../../../services/settings/actions";
import { connect } from "react-redux";

import { Modal, Button } from 'react-bootstrap';

import { SUPERMARKET_ORDER_URL } from "../../../configs/index";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCoins } from '@fortawesome/free-solid-svg-icons';
import { faShoppingBag } from '@fortawesome/free-solid-svg-icons';
import { faCar } from '@fortawesome/free-solid-svg-icons';
import { faDirections } from '@fortawesome/free-solid-svg-icons';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { faPhoneAlt } from '@fortawesome/free-solid-svg-icons';
import { faComment } from '@fortawesome/fontawesome-free-regular'
import { faComments } from '@fortawesome/fontawesome-free-regular';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';

import Itemsfromsuper from '../../Takeaway/RunningOrder/RunOrd/items.js';
import ItemsPic from '../../Takeaway/RunningOrder/RunOrd/itemsPic.js';
import ItemsPicBig from './itemsPic.js';

import Map from "../../Takeaway/RunningOrder/Map";

import Webcam from "react-webcam";
// import Camera, { FACING_MODES, IMAGE_TYPES } from 'react-html5-camera-photo';
// import 'react-html5-camera-photo/build/css/index.css';

import style from "./style.css";
import { Redirect } from "react-router";

import { deliverOrderByStore, createRefund } from "../../../services/Delivery/deliveryprogress/actions";


class ViewBatch2 extends Component {

    constructor(props) {
        super(props);

        this.leftAtDoorRef = React.createRef();

        this.state = { 
            row: [],
            google_script_loaded: false,
        };
    }

    // hamburger menu
    showModalMenu () {
        this.setState({
            MenuModal: true
        });
    }
    hideModalMenu = () => {
        this.setState({
            MenuModal: false
        });
    }

    showModalContact () {
        this.setState({
            ContactModal: true
        });
    }
    hideModalContact = () => {
        this.setState({
            ContactModal: false
        });
    }

    //delivery
    showModalDelivery (id) {
        this.setState({
            DeliveryModal: true
        });
    }
    hideModalDelivery = () => {
        this.setState({
            DeliveryModal: false
        });
    }
    showModalConfirmDeliveryMethod () {
        this.setState({
            ConfirmDeliveryMethodModal: true
        });
    }
    hideModalConfirmDeliveryMethod = () => {
        this.setState({
            ConfirmDeliveryMethodModal: false
        });
    }
    showModalCompleteDelivery () {
        this.setState({
            CompleteDeliveryModal: true
        });
    }
    hideModalCompleteDelivery = () => {
        this.setState({
            CompleteDeliveryModal: false
        });
    }
    showModalLeftAtDoorPhoto () {
        this.setState({
            LeftAtDoorPhotoModal: true
        });
    }
    hideModalLeftAtDoorPhoto = () => {
        this.setState({
            LeftAtDoorPhotoModal: false
        });
    }
    showModalUploadLeftAtDoorPhoto () {
        this.setState({
            UploadLeftAtDoorPhotoModal: true
        });
    }
    hideModalUploadLeftAtDoorPhoto = () => {
        this.setState({
            UploadLeftAtDoorPhotoModal: false
        });
    }

    componentDidMount() {
        this.supermarketOrders();

        this.props.getSettings();


        const existingScript = document.getElementById("googleMaps");
        if (!existingScript) {
            const script = document.createElement("script");
            script.src = "https://maps.googleapis.com/maps/api/js?key=" + localStorage.getItem("googleApiKey") + "&libraries=places";
            script.id = "googleMaps";
            document.body.appendChild(script);
            script.onload = () => {
                this.setState({ google_script_loaded: true });
            };
        }
    }

    supermarketOrders = () =>{
        axios
        .post(SUPERMARKET_ORDER_URL, {
            unique_order_id: window.location.pathname.split("/").pop()
        })
        .then (newArray => {
            // console.log("row", newArray.data);
            this.setState({
                row: newArray.data
            });
        })
        .catch(function(error) {
            console.log(error);
        });
    }

    itemsPic = (row, style="small") =>{
        if(style == "small"){
            if(row.items == null || row.items.length === 0) return null;        
            return <ItemsPic supermarket_orders={row} />
        } else {
            if(row.items == null || row.items.length === 0) return null;        
            return <ItemsPicBig supermarket_orders={row} />
        }
    }

    //check deliver radius
    degrees_to_radians = (degrees) =>{
        var pi = Math.PI;
        return degrees * (pi/180);
    }
    getDistance = (latitudeFrom, longitudeFrom, latitudeTo, longitudeTo) =>{
        var latFrom = this.degrees_to_radians(latitudeFrom);
        var lonFrom = this.degrees_to_radians(longitudeFrom);
        var latTo = this.degrees_to_radians(latitudeTo);
        var lonTo = this.degrees_to_radians(longitudeTo);

        var latDelta = latTo - latFrom;
        var lonDelta = lonTo - lonFrom;

        var angle = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(latDelta / 2), 2) +
            Math.cos(latFrom) * Math.cos(latTo) * Math.pow(Math.sin(lonDelta / 2), 2)));
        return (angle * 6371);
    }

    getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1));
        // console.log(sPageURL);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam) {
                return sParameterName[1];
            }
        }
    }

    navigation = (lat, long) =>{
        // If it's an iPhone..
        if( (navigator.platform.indexOf("iPhone") != -1) 
            || (navigator.platform.indexOf("iPod") != -1)
            || (navigator.platform.indexOf("iPad") != -1))
             window.open("maps://maps.google.com/maps?daddr="+lat+","+long+"&amp;ll=");
        else
             window.open("http://maps.google.com/maps?daddr="+lat+","+long+"&amp;ll=");
    }
    
    getAddress = (address, extract, long=true) => {
        if(extract == "str+nr"){
            var res = address;
            var split = res.split(",");
            if(long) var res = split.slice(0, split.length - 1).join(",");
            else var res = split[0];
            return res;
        } else if(extract == "str"){
            var res = address;
            var split = res.split(",");
            var res = split[0];
            return res;
        }  else if(extract == "plz"){
            var res = address;
            var split = res.split(",");
            var res = split[1];

            //only plz without city
            var split = res.split(" ");
            var res = split[1];
            return res;
        } else {
            var res = address.split(", ");
            return res[0];
        }
    }

    capitalizeFirstLetter = (string) => {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    sendSMS = (tel) => {
        var ua = navigator.userAgent.toLowerCase();
        var href;

        ////stackoverflow.com/questions/6480462/how-to-pre-populate-the-sms-body-text-via-an-html-link
        // if (ua.indexOf("iphone") > -1 || ua.indexOf("ipad") > -1){
        //     href = 'sms:/' + tel;
        // } else{
        //     href = 'sms://' + tel;
        // }

        href = 'sms:' + tel;

        window.open(href);
    }

    acceptBatch = (unique_order_id, cat) =>{

        var div = document.getElementById('laden');
        div.innerHTML = '<span>Laden...</span>';

        var url = '//'+window.location.hostname+'/php/twilio/Delivery_acceptOrder.php?unique_order_id='+unique_order_id;
        axios
        .get(url)
        .then(response => {
            if(response.data.success){
                window.location = '/viewBatch2/'+unique_order_id+'?act=startShopping&tab=todo'
            } else {
                alert("Sorry! Auftrag wurde bereits von einem anderen Zusteller übernommen.");
            }   
        })
        .catch(function(error) {
            console.log(error);
        });
    }

    makeTotalUnits = (data = []) => {
        let sum = 0;
        data.forEach((row) => {
            sum = sum + parseFloat(row.quantity)
        });
        return sum;
    }

    request = (id, unique_order_id, act) => {

        if(act == "startShopping"){
            var url = '//'+window.location.hostname+'/php/shopperPwa.php?unique_order_id='+unique_order_id+'&act=startShopping&deliveryByStore=1';
            var redirect = '/viewBatch2/'+unique_order_id+'?act=startShopping&tab=todo';
        } 
        else if(act == "startNavigate"){
            var url = '//'+window.location.hostname+'/php/shopperPwa.php?unique_order_id='+unique_order_id+'&act=startNavigate&deliveryByStore=1';
        }  
        else if(act == "markAsDelivered"){
            var redirect = '/viewBatch2/'+unique_order_id+'?act=deliveryDone';
            this.__acceptToDeliver(id, unique_order_id);
        }   
        if(url){
            axios
            .get(url)
            .then(response => {
                
            })
            .catch(function(error) {
                console.log(error);
            });
        }

        setTimeout(function () {
            if(redirect){
                window.location = redirect;
            }
        }, 100);
    }

    __acceptToDeliver = (id, unique_order_id) => {
        this.props.deliverOrderByStore(id, unique_order_id);
        this.setState({ loading: true });
    };


    makeTotalQuantity = (data = [], tab) => {
        let sum = 0;
        data.forEach((row) => {

            if(tab == "todo" && row.found == null){
                sum = sum + 1
            } 
            else if(tab == "review" && row.found == 0){
                sum = sum + 1
            } 
            else if(tab == "replacements" && row.found == 0 && row.replacement){
                sum = sum + 1
            } 
            else if(tab == "refunds" && row.found == 0 && row.replacement == null){
                sum = sum + 1
            } 
            else if(tab == "done" && row.found > 0){
                sum = sum + 1
            }
        });
        return sum;
    }

    showTime = (unix_timestamp) => {
        var d = new Date(unix_timestamp * 1000);
        return  d.getHours() + ':' + d.getMinutes();
    }

    uploadLeftAtDoor = (id) => {

        // var url = '//'+window.location.hostname+'/php/shopperPwa_uploadLeftAtDoor.php';
        // var fd = new FormData();
        // fd.append('user_id', delivery_user.data.id);
        // fd.append('auth_token', btoa(delivery_user.data.auth_token));
        // fd.append('id', id);
        // fd.append('leftAtDoorSrc', this.state.leftAtDoorSrc);
        // axios
        // .post(url, fd)
        // .then(res => {
        //     if(res.data.error === false){
        //         //window.location = '/viewBatch/'+id+'?act=navigateCustomer'
        //     } else {
        //         alert("Ein Fehler ist aufgetreten.");
        //     }   
        // })
        // .catch(function(error) {
        //     console.log(error);
        // });
    }

    handedToCustomer = () => {
        this.hideModalConfirmDeliveryMethod();
        document.getElementById('confirmDeliveryMethod').style.display = "none";
        document.getElementById('markAsDelivered').style.display = "block";
    }

    captureLeftAtDoor() {
        const src = this.leftAtDoorRef.current.getScreenshot();
        // console.log("leftAtDoorSrc", src);

        this.setState({
            leftAtDoorSrc: src
        });

        this.showModalUploadLeftAtDoorPhoto();
    }

    makeTotalRefunds = (data = [], subtotal) => {
        let sum = 0;
        data.forEach((row) => {

            if(row.found == 0 && row.replacement){
                sum = sum + (row.replacement_price * row.found_replacement)
            } 
            else if(row.found > 0){
                sum = sum + (row.price * row.found)
            }
        });
        return subtotal - sum;
    }

    refund = (id, unique_order_id, refunds) => {
        if(refunds){
            console.log(refunds+' will be refunded to user');

            this.props.createRefund(
                id,
                unique_order_id, 
                refunds,
                ( res ) => {
                    console.log("createRefund", res.data);
                }
            )

            setTimeout(function () {
                // if(redirect){
                    window.location = "/viewBatch2/"+unique_order_id+"?act=deliveryCanceled";
                // }
            }, 2000);
        }
    }

    heading = (type, fromName) => {
        if(this.getUrlParameter('act') == 'startShopping'){
            var headingTxt = <span style={{fontSize:"14px"}}>{fromName}'s Bestellung</span>
            var headerStyle = {
                background:"#fff", width:"100%", float:"left", paddingTop: "10px"
            };
        } else if(this.getUrlParameter('act') == 'reviewChanges'){
            var headingTxt = 'Änderungen Prüfen'
            var headerStyle = {
                background:"#D83584", width:"100%", float:"left", padding: "10px 0", color:"#fff"
            };
        } else if(this.getUrlParameter('act') == 'receipt'){
            var headingTxt = "Quittungsprüfung"
            var headerStyle = {
                background:"#DEA0E2", width:"100%", float:"left", padding: "10px 0", color:"#fff"
            };
        } else if(this.getUrlParameter('act') == 'navigateCustomer'){
            var headingTxt = "Lieferung"
            var headerStyle = {
                background:"#EC837D", width:"100%", float:"left", padding: "10px 0", color:"#fff"
            };
        } else if(this.getUrlParameter('act') == 'deliveryDone'){
            var headingTxt = "Auftrag fertig"
            var headerStyle = {
                background:localStorage.getItem("storeColor"), width:"100%", float:"left", padding: "10px 0", color:"#fff"
            };
        } else if(this.getUrlParameter('act') == 'deliveryCanceled'){
            var headingTxt = "Auftrag storniert"
            var headerStyle = {
                background:localStorage.getItem("storeColor"), width:"100%", float:"left", padding: "10px 0", color:"#fff"
            };
        } 

        if(type == "headingTxt") return headingTxt;
        else if(type == "headerStyle") return headerStyle;
    }

    getUrlParameter(sParam) {
        var sPageURL =decodeURIComponent(window.location.search.substring(1));
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam) {
                return sParameterName[1];
            }
        }
    }

    makeTotalCheckout = (data = []) => {
        let sum = 0;
        data.forEach((row) => {

            if(row.found == 0 && row.replacement){
                sum = sum + (row.replacement_price * row.found_replacement)
            } 
            else if(row.found > 0){
                sum = sum + (row.price * row.found)
            }
        });
        return sum - (sum*0.3);
    }

    render() {

        const { row } = this.state;

        // if (!delivery_user.success) {
        //     return (
        //         //redirect to account page
        //         <Redirect to={"/delivery/login"} />
        //     );
        // }

        // delivery done
        if (row.markDeliveredDate && this.getUrlParameter("act") !== "deliveryDone") {
            return (
                <Redirect to={"/viewBatch2/"+row.unique_order_id+"?act=deliveryDone"} />
            );
        } 

        // delivery canceled
        if (row.orderstatus_id == 6 && this.getUrlParameter("act") == undefined) {
            return (
                <Redirect to={"/viewBatch2/"+row.unique_order_id+"?act=deliveryCanceled"} />
            );
        } 

        // navigate to customer
        if (row.startNavigateDate && this.getUrlParameter("act") == "startShopping") {
            return (
                <Redirect to={"/viewBatch2/"+row.unique_order_id+"?act=navigateCustomer"} />
            );
        } 

        // start shopping
        if (row.acceptedDate && this.getUrlParameter("act") == undefined) {
            return (
                <Redirect to={"/viewBatch2/"+row.unique_order_id+"?act=startShopping&tab=todo"} />
            );
        } 

        var tabStyle = {
            float:"left", padding:"10px 10px 5px", width:"33%"
        };

        var tabStyleHover = {
            float:"left", padding:"10px 10px 5px", width:"33%", color:localStorage.getItem("storeColor"), borderBottom:"2px solid rgb(252, 128, 25)"
        };
        const screenHeight = window.innerHeight;
        const videoConstraints = {
            facingMode: { exact: "environment" }
        };

        const confirmDeliveryMethodModal = {
            position: "absolute",
            bottom: 0,
            width: "95%"
        };

        return (
   
            <React.Fragment>

            {/* header */}
            {this.heading("headingTxt", row.fromName) && <div style={this.heading("headerStyle")}>
                <FontAwesomeIcon onClick={() => { this.showModalMenu(); }} style={{fontSize: "26px", marginTop:"8px", marginLeft:"15px", marginRight:"5px", float:"left", cursor:"pointer"}} className="infobaars" icon={faBars} />

                <span style={{fontSize:"24px", fontFamily:"open sans", fontWeight:600, marginLeft:"5px", marginTop:"2px", float:"left"}}>{this.heading("headingTxt", row.fromName)}</span>

                <span onClick={() => this.showModalContact()} style={{float:"right", padding:"10px 10px 0"}}><FontAwesomeIcon icon={faComments} style={{fontSize:"16px"}} /></span>
            </div>}

            {row.length == null ? (

                this.getUrlParameter('act') == 'startShopping' ? (//Customer's order

                    <div style={{padding:"0 18px 18px", width:"100%", float:"left"}}>

                        <div style={{textAlign:"center", fontSize:"13px", fontFamily:"open sans", fontWeight:400, color:"#999"}}>
                            
                            <span onClick={() =>  { window.location = '/viewBatch2/'+row.unique_order_id+'?act=startShopping&tab=todo' }} style={this.getUrlParameter('tab')=="todo"?tabStyleHover:tabStyle}>{this.makeTotalQuantity(row.items, "todo")} To-Do</span>
                            
                            <span onClick={() =>  { window.location = '/viewBatch2/'+row.unique_order_id+'?act=startShopping&tab=in_review' }} style={this.getUrlParameter('tab')=="in_review"?tabStyleHover:tabStyle}>{this.makeTotalQuantity(row.items, "review")} In Review</span>
                            
                            <span onClick={() =>  { window.location = '/viewBatch2/'+row.unique_order_id+'?act=startShopping&tab=done' }} style={this.getUrlParameter('tab')=="done"?tabStyleHover:tabStyle}>{this.makeTotalQuantity(row.items, "done")} Fertig</span>
                        
                        </div>

                        <div style={{marginTop:"15px", float:"left", width:"100%"}}>{this.itemsPic(row, "big")}</div>

                    </div>
                
                ) : this.getUrlParameter('act') == 'reviewChanges' ? (//Review Changes
                    
                    <div style={{padding:"0 18px 18px", width:"100%", float:"left"}}>
                        <div style={{float:"left", width:"100%", padding: "20px 0", background:"#fff", maxWidth: "600px", borderBottom:"1px solid rgb(241, 237, 242)"}}>
                            <div style={{fontWeight:"bold"}}>{row.fromName} zur Bestätigung anrufen</div>
                            <div style={{fontSize:"12px"}}>{this.makeTotalQuantity(row.items, "replacements")} Ersatz, {this.makeTotalQuantity(row.items, "refunds")} Rückerstattung</div>
                        </div>

                        <div style={{float:"left", width:"100%"}}>{this.itemsPic(row, "big")}</div>

                        <div style={{float:"left", width:"100%", margin:"10px 0"}}><a href={'tel:'+row.fromTel} style={{position:"relative", display:"block", color:"#fff", background:"#41B129", borderRadius:"2px", padding:"10px", borderRadius:"3px", cursor:"pointer", textAlign:"center", fontWeight:600, fontFamily:"open sans", cursor:"pointer"}}><FontAwesomeIcon icon={faPhoneAlt} style={{marginRight:"5px"}} /> {row.fromName} anrufen<Ink duration="500" /></a></div>

                        {this.makeTotalCheckout(row.items).toFixed(2) == "0.00" ? (
                            <React.Fragment>
                                <button
                                onClick={() => this.refund(row.id, row.unique_order_id, this.makeTotalRefunds(row.items, row.total) > 0 && this.makeTotalRefunds(row.items, row.total).toFixed(2)) }
                                type="button"
                                style={{position:"relative", fontFamily:"open sans", fontWeight:600, padding:"10px", borderRadius:"3px", border:"none", width:"100%", color:"#fff", textTransform:"unset", backgroundColor: localStorage.getItem("storeColor")}}
                                >
                                Rückerstattung senden
                                <Ink duration="500" />
                                </button>

                                <div style={{margin:"10px", fontFamily:"open sans", fontWeight:300, textAlign:"center"}}>Der Kunde möchte eine Rückerstattung, da keine der oben genannte Artikel verfügbar ist.</div>
                            </React.Fragment>
                        ) : (
                            <React.Fragment>
                                {row.partner == 1 ? (
                                    <button
                                        onClick={() => window.location = '/viewBatch2/'+row.unique_order_id+'?act=receipt' }
                                        type="button"
                                        style={{position:"relative", fontFamily:"open sans", fontWeight:600, padding:"10px", borderRadius:"3px", border:"none", width:"100%", color:"#fff", textTransform:"unset", backgroundColor: localStorage.getItem("storeColor")}}
                                    >
                                        Weiter
                                        <Ink duration="500" />
                                    </button>
                                ) : (
                                    <button
                                        onClick={() => window.location = '/viewBatch2/'+row.unique_order_id+'?act=checkout' }
                                        type="button"
                                        style={{position:"relative", fontFamily:"open sans", fontWeight:600, padding:"10px", borderRadius:"3px", border:"none", width:"100%", color:"#fff", textTransform:"unset", backgroundColor: localStorage.getItem("storeColor")}}
                                    >
                                        Zur Kasse gehen
                                        <Ink duration="500" />
                                    </button>
                                )}

                                {this.makeTotalRefunds(row.items, row.subtotal).toFixed(1)*(-1) !== 0.0 && (
                                    this.makeTotalRefunds(row.items, row.subtotal) < 0 ? (
                                        <div style={{margin:"10px", fontFamily:"open sans", fontWeight:300, textAlign:"center"}}>Dem Kunden werden €{this.makeTotalRefunds(row.items, row.subtotal).toFixed(2)*(-1)} zusätzlich vom Konto abgezogen.</div>
                                    ) : (
                                        <div style={{margin:"10px", fontFamily:"open sans", fontWeight:300, textAlign:"center"}}>Dem Kunden werden €{this.makeTotalRefunds(row.items, row.subtotal).toFixed(2)} zurückerstattet.</div>
                                    )
                                )}
                            </React.Fragment>
                        )}

                    </div>    
                    
                ) : this.getUrlParameter('act') == 'receipt' ? (//Receipt Check

                    <div style={{width:"100%", float:"left", margin:"auto"}}>
                        <div style={{width:"100%", padding: "20px", background:"#fff", maxWidth: "468px", margin:"20px auto", textAlign:"center"}}>
                            <img src={'https://lieferservice123.com/assets/img/receipt.png'} style={{width:"60px", marginBottom:"10px"}} />
                            <div style={{fontWeight:"bold"}}>Rechnung behalten</div>
                            <div style={{color:"#999", marginTop:"10px"}}>
                                Kunden erhalten von uns eine elektronische Rechnung. Bitte nicht in die Taschen für den Kunden legen.
                            </div>
                            <a href={'/viewBatch2/'+row.unique_order_id+'?act=navigateCustomer'} style={{position:"relative", display:"block", textTransform:"uppercase", color:"#fff", background:"rgb(252, 128, 25)", borderRadius:"2px", padding:"10px", marginTop:"10px", borderRadius:"3px", cursor:"pointer", textAlign:"center", fontWeight:600, fontFamily:"open sans", cursor:"pointer"}}>Weiter<Ink duration="500" /></a>
                        </div>  
                    </div>    

                ) : this.getUrlParameter('act') == 'navigateCustomer' ? (//Deliver order to customer

                    <div style={{width:"100%", float:"left", margin:"auto"}}>
                        <div style={{width:"100%", padding: "20px", background:"#fff", maxWidth:"468px", margin:"20px auto", textAlign:"center"}}>
                            <FontAwesomeIcon icon={faCar} style={{fontSize:"60px", marginBottom:"10px"}} />
                            <div style={{fontWeight:"bold"}}>Bestellung ausliefern</div>
                            {/* <div style={{color:"#999", marginTop:"10px"}}>Fällig: so schnell wie möglich</div> */}
                            <div style={{color:"#999", marginTop:"10px"}}>Fällig um {this.showTime(row.added+3600)} Uhr oder früher</div>
                            <a onClick={() => { this.showModalDelivery(); this.request(row.id, row.unique_order_id, "startNavigate"); }} 
                            style={{position:"relative", display:"block", textTransform:"uppercase", color:"#fff", background:"rgb(252, 128, 25)", borderRadius:"2px", padding:"10px", marginTop:"10px", borderRadius:"3px", cursor:"pointer", textAlign:"center", fontWeight:600, fontFamily:"open sans", cursor:"pointer"}}>Lieferung starten<Ink duration="500" /></a>
                        </div>
                    </div>   

                ) : this.getUrlParameter('act') == 'deliveryDone' ? (//show success msg

                    <div style={{width:"100%", float:"left", margin:"auto"}}>
                        <div style={{float:"left", width:"100%", padding: "20px", background:"#fff", margin:"20px auto", textAlign:"center"}}>
                            <img src={'https://lieferservice123.com/assets/img/order-placed.gif'} style={{width:"60px", marginBottom:"10px"}} />
                            <div style={{fontWeight:"bold"}}>Lieferung abgeschlossen</div>
                            <div style={{color:"#999", marginTop:"10px"}}>
                                Bestellung: {row.unique_order_id}<br/>
                                Geliefert um {this.showTime(row.markDeliveredDate)} Uhr
                            </div>
                        </div>  
                    </div>    

                ) : this.getUrlParameter('act') == 'deliveryCanceled' ? (//delivery canceled

                    <div style={{width:"100%", float:"left", margin:"auto"}}>
                        <div style={{float:"left", width:"100%", padding: "20px", background:"#fff", margin:"20px auto", textAlign:"center"}}>
                            <img src={'https://lieferservice123.com/assets/img/order-canceled.png'} style={{width:"60px", marginBottom:"10px"}} />
                            <div style={{fontWeight:"bold"}}>Bestellung abgebrochen</div>
                            <div style={{color:"#999", marginTop:"10px"}}>
                                Bestellung: {row.unique_order_id}<br/>
                                Der Kunde erhält eine Rückerstattung.
                            </div>
                        </div>  
                    </div>    

                ) : (//View batch
                    
                    <div style={{color:"#888"}}>

                        <Map 
                            show_delivery_gps={(localStorage.getItem("userSetAddress") ? true : false)}
                            delivery_gps_location={(localStorage.getItem("userSetAddress")?'{"lat":"'+JSON.parse(localStorage.getItem("userSetAddress")).lat+'","lng":"'+JSON.parse(localStorage.getItem("userSetAddress")).lng+'","address":"'+JSON.parse(localStorage.getItem("userSetAddress")).address+'","house":null,"tag":null}':"")}

                            restaurant_latitude={row.shopLat}
                            restaurant_longitude={row.shopLng}
                            
                            deliveryLocation={'{"lat":"'+row.fromLat+'","lng":"'+row.fromLng+'","address":"'+row.fromAddress+'","house":null,"tag":null}'}

                            order_id={row.id}
                            orderstatus_id={row.orderstatus_id}
                        />

                        <div style={{width:"100%", position:"absolute", top: "320px", padding:"18px"}}>

                            <FontAwesomeIcon icon={faShoppingBag} style={{marginRight:"10px"}} /> {row.items.length} Produkte ({this.makeTotalUnits(row.items)} Einheiten)<br/>

                            {/* <FontAwesomeIcon icon={faCoins} style={{marginRight:"10px"}} /> Kunde bezahlt mit Bargeld<br/> */}

                            <FontAwesomeIcon icon={faCar} style={{marginRight:"10px"}} /> {this.getDistance(row.fromLat, row.fromLng, row.shopLat, row.shopLng).toFixed(2)} km<br/>

                            <br/>

                            <div style={{marginBottom:"15px", float:"left", widht:"100%"}}>{this.itemsPic(row)}</div>

                            <div style={{float:"left", width:"100%"}}>
                                <button
                                    onClick={() => this.acceptBatch(row.unique_order_id, row.cat)}
                                    type="button"
                                    className="btn-save-address"
                                    style={{textTransform:"unset", backgroundColor: localStorage.getItem("storeColor")}}
                                    id="laden"
                                >
                                    Auftrag annehmen
                                    <Ink duration="500" />
                                </button>
                            </div>


                        </div>

                        
                    </div> 

                )
            ) : (
                <h2 style={{textAlign:"center", fontFamily:"open sans", fontWeight:300}}>Bestätigung wird geladen...</h2>
            )}

            {/* hamburger menu */}
            <Modal show={this.state.MenuModal} onHide={this.hideModalMenu} size="sm">
                <Modal.Header closeButton style={{background:"#fff"}}>
                    <Modal.Title style={{lineHeight:"1", color:"#000"}}>
                        <div style={{fontSize:"30px", fontFamily:"open sans", fontWeight:300}}>Lieferservice123</div>
                        <div style={{fontSize:"12px", fontWeight:600, fontFamily:"open sans"}}>shopper app v{localStorage.getItem("version")}</div>
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body style={{padding:"20px 25px 10px", background:"rgb(13, 54, 64)", color:"white", textTransform:"uppercase", fontWeight:"600", fontFamily:"open sans"}}>

                    <div className="category-list-item" onClick={() => { window.location = '/search-location?redirect=shopperPwa' }} style={{cursor:"pointer", position:"relative"}}>
                    {/* <div className="category-list-item" onClick={() => { this.context.router.history.push("/search-location"+backToRes) }} style={{cursor:"pointer", position:"relative"}}> */}
                        <div className="display-flex py-2">
                            <div className="mr-10 border-0">
                                <i className="si si-location-pin" />
                            </div>
                            <div className="flex-auto border-0">{localStorage.getItem("firstScreenSetupLocation")}</div>
                            <div className="flex-auto text-right">
                                <i className="si si-arrow-right" />
                            </div>
                        </div>
                        <Ink duration="500" />
                    </div>

                    <div className="category-list-item" onClick={() => { this.hideModalMenu(); this.showModalLang(); }} style={{cursor:"pointer", position:"relative"}}>
                        <div className="display-flex py-2">
                            <div className="mr-10 border-0">
                                <i className="si si-settings" />
                            </div>
                            <div className="flex-auto border-0">Land/Sprache ändern</div>
                            <div className="flex-auto text-right">
                                <i className="si si-arrow-right" />
                            </div>
                        </div>
                        <Ink duration="500" />
                    </div>
                
                </Modal.Body>
            </Modal>

            {/* contact customer */}
            <Modal show={this.state.ContactModal} onHide={this.hideModalContact} size="sm" style={{background:"#333"}}>
                <Modal.Header closeButton style={{background:"#fff"}}>
                    <Modal.Title style={{lineHeight:"1", color:"#000"}}>
                        <div style={{fontSize:"30px", fontFamily:"open sans", fontWeight:300}}>{row.fromName}</div>
                        <div style={{fontSize:"12px", fontFamily:"open sans", fontWeight:500, marginTop:"5px"}}>{row.fromTel}</div>
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body style={{padding:"25px"}}>

                    <div style={{background:"#fff", maxWidth: "600px", textAlign:"center"}}>
                        {/* <div style={{fontSize:"12px", color:"#999", marginTop:"10px", fontFamily:"open sans", fontWeight:300}}>
                            Bitte nur anrufen, wenn Sie die Wohnung des Kunden nicht finden.
                        </div> */}

                        {/* <div style={{fontSize:"12px", color:"#999", marginTop:"10px", fontFamily:"open sans", fontWeight:300}}>
                            Für Fragen wegen replacements, können Sie den Kunden eine Nachricht senden.
                        </div> */}

                        <div style={{marginTop:"40px"}}>
                            <a onClick={() => this.sendSMS(row.fromTel)} style={{display:"block", textTransform:"uppercase", color:"#fff", background:"rgb(252, 128, 25)", borderRadius:"2px", padding:"10px", marginTop:"10px", borderRadius:"3px", cursor:"pointer", textAlign:"center", fontWeight:600, fontFamily:"open sans", cursor:"pointer"}}><FontAwesomeIcon icon={faComment} style={{marginRight:"5px"}} /> Nachricht</a>
                            
                            <a href={'tel:'+row.fromTel} style={{display:"block", textTransform:"uppercase", color:"#fff", background:"#24C644", borderRadius:"2px", padding:"10px", marginTop:"10px", borderRadius:"3px", cursor:"pointer", textAlign:"center", fontWeight:600, fontFamily:"open sans", cursor:"pointer"}}><FontAwesomeIcon icon={faPhoneAlt} style={{marginRight:"5px"}} /> Anrufen</a>
                        </div>

                    </div>

                
                </Modal.Body>
            </Modal>

            {/* delivery to customer */}
            <Modal show={this.state.DeliveryModal} onHide={this.hideModalDelivery} size="sm">
                <Modal.Header closeButton>
                    <Modal.Title style={{color:"#000"}}>
                        Lieferung
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body style={{padding:"0"}}>

                    <div style={{maxWidth: "600px"}}>

                        <div style={{padding:"25px"}}>
                            <div style={{color:"#333", fontFamily:"open sans", fontWeight:"bold", fontSize:"12px"}}>Adresse</div>
                            <div style={{color:"#333", fontFamily:"open sans", fontWeight:300, fontSize:"12px"}}>{row.fromName}<br/>{row.fromAddress}</div>
                            <hr/>
                            <div style={{color:localStorage.getItem("storeColor"), fontFamily:"open sans", fontWeight:400}} onClick={() => this.navigation(row.fromLat, row.fromLng)}>Navigieren</div>
                        </div>

                        <div style={{background:"#F9F7FB", height:"40px"}}></div>

                        <div style={{padding:"25px"}}>
                            <div style={{color:"#333", fontFamily:"open sans", fontWeight:"bold", fontSize:"12px"}}>Versuche den Kunden zu kontaktieren</div>
                            <div style={{color:"#333", fontFamily:"open sans", fontWeight:300, fontSize:"12px"}}>Lassen Sie die Lieferung an der Tür, wenn der Kunde nicht antwortet.</div>
                            <hr/>
                            <div style={{color:localStorage.getItem("storeColor"), fontFamily:"open sans", fontWeight:400}} onClick={() => this.showModalContact()}>Kontakt</div>
                        </div>

                        <div style={{background:"#F9F7FB", height:"40px"}}></div>

                        <div id="confirmDeliveryMethod" style={{background:"#F9F7FB", padding:"25px"}}><a onClick={() => this.showModalConfirmDeliveryMethod()} style={{display:"block", color:"#333", background:"#ccc", borderRadius:"2px", padding:"10px", borderRadius:"3px", cursor:"pointer", textAlign:"center", fontWeight:600, fontFamily:"open sans", cursor:"pointer"}}>Zustellungsmethode bestätigen</a></div>

                        <div id="markAsDelivered" style={{display:"none", background:"#F9F7FB", padding:"25px"}}><a onClick={() => this.showModalCompleteDelivery()} style={{display:"block", color:"#fff", background:"rgb(252, 128, 25)", borderRadius:"2px", padding:"10px", borderRadius:"3px", cursor:"pointer", textAlign:"center", fontWeight:600, fontFamily:"open sans", cursor:"pointer"}}>Lieferung abschließen</a></div>

                    </div>
                </Modal.Body>
            </Modal>

            {/* confirm delivery method */}
            <Modal show={this.state.ConfirmDeliveryMethodModal} onHide={this.hideModalConfirmDeliveryMethod} size="sm" dialogClassName="confirmDeliveryMethodModal" style={{background:"rgba(0, 0, 0, 0.7)"}}>
                
                <Modal.Body style={{padding:"0"}}>
                    <div style={{color:"#8D8B8F", padding:"18px", fontWeight:300, fontFamily:"open sans"}}>Zustellungsmethode bestätigen</div>
                    <div style={{color:localStorage.getItem("storeColor"), fontFamily:"open sans", fontWeight:600, fontSize:"15px", cursor:"pointer"}}>
                        <div style={{borderTop:"1px solid #C4C1C3", padding:"18px"}} onClick={() => this.handedToCustomer()}>Dem Kunden übergeben</div>
                        <div style={{borderTop:"1px solid #C4C1C3", padding:"18px"}} onClick={() => this.showModalLeftAtDoorPhoto()}>An der Tür gelassen</div>
                        <div style={{borderTop:"1px solid #C4C1C3", padding:"18px"}} onClick={() => this.hideModalConfirmDeliveryMethod()}>Schließen</div>
                    </div>
                </Modal.Body>
            </Modal>

            {/* left at door photo */}
            <Modal show={this.state.LeftAtDoorPhotoModal} onHide={this.hideModalLeftAtDoorPhoto} size="sm" style={{background:"#333"}}>
                {/* <Modal.Header closeButton style={{paddingBottom:0}}>
                    <Modal.Title><div style={{fontFamily:"open sans", fontWeight:300, fontSize:"22px", height: "50px"}}>Foto aufnehmen</div></Modal.Title>
                </Modal.Header> */}
                <Modal.Body style={{padding:"0"}}>

                    <div onClick={() => this.hideModalLeftAtDoorPhoto()} style={{zIndex:1, position:"fixed", top:10, right:10, width:"40px", height:"40px", background:"#000", color:"#fff", textAlign:"center", padding:"8px 15px", borderRadius: "24px", boxShadow: "0 1px 2px #8d8d8d", color: "#fff", fontSize: "16px", fontWeight: "bold"}}>x</div>

                    <Webcam
                        audio={false}
                        height={screenHeight-77} 
                        //width={"100%"}            
                        ref={this.leftAtDoorRef}
                        screenshotFormat="image/jpeg"
                        videoConstraints={videoConstraints}
                        screenshotQuality={1}
                        minScreenshotHeight={1080}
                        minScreehshotWidth={1920}
                    />

                    <div style={{float:"left", width:"100%"}}>
                        <button
                            onClick={() => this.captureLeftAtDoor()}
                            type="button"
                            className="btn-save-address"
                            style={{textTransform:"unset", backgroundColor: localStorage.getItem("storeColor")}}
                        >
                            Foto aufnehmen
                            <Ink duration="500" />
                        </button>
                    </div>

                </Modal.Body>
            </Modal>

            {/* upload left at door photo */}
            <Modal show={this.state.UploadLeftAtDoorPhotoModal} onHide={this.hideModalUploadLeftAtDoorPhoto} size="sm" style={{background:"#333"}}>
                {/* <Modal.Header closeButton style={{paddingBottom:0}}>
                    <Modal.Title><div style={{fontFamily:"open sans", fontWeight:300, fontSize:"22px", height: "50px"}}>Foto hochladen</div></Modal.Title>
                </Modal.Header> */}
                <Modal.Body style={{padding:"0"}}>

                    <div onClick={() => this.hideModalUploadLeftAtDoorPhoto()} style={{zIndex:1, position:"fixed", top:10, right:10, width:"40px", height:"40px", background:"#000", color:"#fff", textAlign:"center", padding:"8px 15px", borderRadius: "24px", boxShadow: "0 1px 2px #8d8d8d", color: "#fff", fontSize: "16px", fontWeight: "bold"}}>x</div>

                    <div style={{width:"100%"}}>
                        <img src={this.state.leftAtDoorSrc} style={{width:"100%", height:screenHeight-70}} />
                    </div>

                    <div style={{float:"left", width:"100%"}}>
                        <button
                            // onClick={() => this.capture()}
                            onClick={() => {
                                document.getElementById('confirmDeliveryMethod').style.display = "none";
                                document.getElementById('markAsDelivered').style.display = "block";
                                this.hideModalConfirmDeliveryMethod();
                                this.hideModalLeftAtDoorPhoto(); 
                                this.hideModalUploadLeftAtDoorPhoto(); 
                                this.uploadLeftAtDoor(row.id);
                            }}
                            type="button"
                            className="btn-save-address"
                            style={{textTransform:"unset", backgroundColor: localStorage.getItem("storeColor")}}
                        >
                            Foto hochladen
                            <Ink duration="500" />
                        </button>
                    </div>

                </Modal.Body>
            </Modal>

            {/* complete delivery */}
            <Modal show={this.state.CompleteDeliveryModal} onHide={this.hideModalCompleteDelivery} size="sm" style={{background:"#333"}}>
                <Modal.Header closeButton style={{paddingBottom:0}}>
                    <Modal.Title><div style={{fontFamily:"open sans", fontWeight:300, fontSize:"22px"}}>Möchten Sie die Bestellung wirklich als geliefert markieren?</div></Modal.Title>
                </Modal.Header>
                <Modal.Body style={{padding:"0"}}>
                    <div style={{padding:"15px 25px 25px"}}>
                        <a onClick={() => {
                            this.request(row.id, row.unique_order_id, "markAsDelivered"); 
                            this.refund(row.id, row.unique_order_id, this.makeTotalRefunds(row.items, row.subtotal) > 0 && this.makeTotalRefunds(row.items, row.subtotal).toFixed(2)); 
                        }} 
                        style={{display:"block", color:"#fff", background:"rgb(252, 128, 25)", borderRadius:"2px", padding:"10px", borderRadius:"3px", cursor:"pointer", textAlign:"center", fontWeight:600, fontFamily:"open sans", cursor:"pointer"}}>Als geliefert markieren</a>
                        {/* <div style={{color:"#333", fontFamily:"open sans", fontWeight:300, fontSize:"12px"}}>You're not at the customer's location.</div> */}
                    </div>
                </Modal.Body>
            </Modal>

            </React.Fragment>
   
        )

    }
}


const mapStateToProps = state => ({
    settings: state.settings.settings
});
export default connect(
    mapStateToProps,
    { getSettings, deliverOrderByStore, createRefund }
)(ViewBatch2);
