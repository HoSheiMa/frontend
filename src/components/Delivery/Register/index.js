import React, { Component } from "react";
import { loginUser, registerUser, sendOtp, verifyOtp } from "../../../services/user/actions";

import BackButton from "../../Takeaway/Elements/BackButton";
import ContentLoader from "react-content-loader";
import { NavLink } from "react-router-dom";
import { Redirect } from "react-router";
import SimpleReactValidator from "simple-react-validator";
import SocialButton from "../../Takeaway/Auth/SocialButton";
import { connect } from "react-redux";
import Nav from "../../Takeaway/Nav";
import Ink from "react-ink";
import axios from "axios";

// fontawesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';
import { faShoppingBag } from '@fortawesome/free-solid-svg-icons';
import { faCar } from '@fortawesome/free-solid-svg-icons';
import { faCoins } from '@fortawesome/free-solid-svg-icons';
import { faCheck } from '@fortawesome/free-solid-svg-icons';

//bootstrap
import ReactDOM from 'react-dom';
import { Modal, Button } from 'react-bootstrap';

//browser check
import PWAPrompt from 'react-ios-pwa-prompt'
import { isChrome, isChromium, isMobileSafari, isMobile, fullBrowserVersion, osName } from 'react-device-detect';

declare var INSTALL_APP_POPUP;
class Register extends Component {
    constructor() {
        super();
        this.validator = new SimpleReactValidator({
            autoForceUpdate: this,
            messages: {
                required: localStorage.getItem("fieldValidationMsg"),
                string: localStorage.getItem("nameValidationMsg"),
                email: localStorage.getItem("emailValidationMsg"),
                regex: localStorage.getItem("phoneValidationMsg"),
                min: localStorage.getItem("minimumLengthValidationMessage")
            }
        });
    }

    state = {
        loading: false,
        name: "",
        email: "",
        phone: "",
        password: "",
        otp: "",
        accessToken: "",
        provider: "",
        error: false,
        email_phone_already_used: false,
        invalid_otp: false,
        showResendOtp: false,
        countdownStart: false,
        countDownSeconds: 15
    };

    static contextTypes = {
        router: () => null
    };

    componentDidMount() {
        if (localStorage.getItem("enableFacebookLogin") === "true" || localStorage.getItem("enableGoogleLogin") === "true") {
            setTimeout(() => {
                if (this.refs.socialLogin) {
                    this.refs.socialLogin.classList.remove("hidden");
                }
                if (this.refs.socialLoginLoader) {
                    this.refs.socialLoginLoader.classList.add("hidden");
                }
            }, 0.5 * 1000);
        }

        // const { user } = this.props;
        // if (user.success) {
        //     this.setDeliveryGuyRole(user.data.id, user.data.auth_token);
        // }
    }

    handleInputChange = event => {
        this.setState({ [event.target.name]: event.target.value });
    };

    handleRegister = event => {
        event.preventDefault();

        if (
            this.validator.fieldValid("name") &&
            this.validator.fieldValid("email") &&
            this.validator.fieldValid("phone") &&
            this.validator.fieldValid("password")
        ) {
            this.setState({ loading: true });
            if (localStorage.getItem("enSOV") === "true") {
                //sending email and phone, first verify if not exists, then send OTP from the server
                this.props.sendOtp(this.state.email, this.state.phone, null);
            } else {
                this.props.registerUser(
                    this.state.name,
                    this.state.email,
                    this.state.phone,
                    this.state.password,
                    JSON.parse(localStorage.getItem("userSetAddress"))
                );
            }
        } else {
            console.log("Validation Failed");
            this.validator.showMessages();
        }
    };

    handleRegisterAfterSocialLogin = event => {
        event.preventDefault();
        this.setState({ loading: true });
        if (this.validator.fieldValid("phone")) {
            if (localStorage.getItem("enSOV") === "true") {
                //sending email and phone, first verify if not exists, then send OTP from the server
                this.props.sendOtp(this.state.email, this.state.phone, null);
            } else {
                this.props.loginUser(
                    this.state.name,
                    this.state.email,
                    null,
                    this.state.accessToken,
                    this.state.phone,
                    this.state.provider,
                    JSON.parse(localStorage.getItem("userSetAddress"))
                );
            }
        } else {
            this.setState({ loading: false });
            console.log("Validation Failed");
            this.validator.showMessages();
        }
    };

    resendOtp = () => {
        if (this.validator.fieldValid("phone")) {
            this.setState({ countDownSeconds: 15, showResendOtp: false });
            this.props.sendOtp(this.state.email, this.state.phone, null);
        }
    };

    handleVerifyOtp = event => {
        event.preventDefault();
        console.log("verify otp clicked");
        if (this.validator.fieldValid("otp")) {
            this.setState({ loading: true });
            this.props.verifyOtp(this.state.phone, this.state.otp);
        }
    };

    componentWillReceiveProps(newProps) {
        const { user } = this.props;

        if (user !== newProps.user) {
            this.setState({ loading: false });
        }

        if (newProps.user.success) {
            if (newProps.user.data.default_address !== null) {
                const userSetAddress = {
                    lat: newProps.user.data.default_address.latitude,
                    lng: newProps.user.data.default_address.longitude,
                    address: newProps.user.data.default_address.address,
                    house: newProps.user.data.default_address.house,
                    tag: newProps.user.data.default_address.tag
                };
                localStorage.setItem("userSetAddress", JSON.stringify(userSetAddress));
            }
            // this.context.router.history.goBack();
            //after registration set delivery guy role instead of going back to last page
            this.setDeliveryGuyRole(newProps.user.data.id, newProps.user.data.auth_token);
        }

        if (newProps.user.email_phone_already_used) {
            this.setState({ email_phone_already_used: true });
        }
        if (newProps.user.otp) {
            this.setState({ email_phone_already_used: false, error: false });
            //otp sent, hide reg form and show otp form
            document.getElementById("registerForm").classList.add("hidden");
            document.getElementById("socialLoginDiv").classList.add("hidden");
            document.getElementById("phoneFormAfterSocialLogin").classList.add("hidden");
            document.getElementById("otpForm").classList.remove("hidden");

            //start countdown
            this.setState({ countdownStart: true });
            this.handleCountDown();
            this.validator.hideMessages();
        }

        if (newProps.user.valid_otp) {
            this.setState({ invalid_otp: false, error: false, loading: true });
            // register user
            if (this.state.social_login) {
                this.props.loginUser(
                    this.state.name,
                    this.state.email,
                    null,
                    this.state.accessToken,
                    this.state.phone,
                    this.state.provider,
                    JSON.parse(localStorage.getItem("userSetAddress"))
                );
            } else {
                this.props.registerUser(
                    this.state.name,
                    this.state.email,
                    this.state.phone,
                    this.state.password,
                    JSON.parse(localStorage.getItem("userSetAddress"))
                );
            }

            console.log("VALID OTP, REG USER NOW");
            this.setState({ loading: false });
        }

        if (newProps.user.valid_otp === false) {
            console.log("Invalid OTP");
            this.setState({ invalid_otp: true });
        }

        if (!newProps.user) {
            this.setState({ error: true });
        }

        //old user, proceed to login after social login
        if (newProps.user.proceed_login) {
            console.log("From Social : user already exists");
            this.props.loginUser(
                this.state.name,
                this.state.email,
                null,
                this.state.accessToken,
                null,
                this.state.provider,
                JSON.parse(localStorage.getItem("userSetAddress"))
            );
        }

        if (newProps.user.enter_phone_after_social_login) {
            this.validator.hideMessages();
            document.getElementById("registerForm").classList.add("hidden");
            document.getElementById("socialLoginDiv").classList.add("hidden");
            document.getElementById("phoneFormAfterSocialLogin").classList.remove("hidden");
            // populate name & email
            console.log("ask to fill the phone number and send otp process...");
        }
    }

    handleSocialLogin = user => {
        //if otp verification is enabled
        if (localStorage.getItem("enSOV") === "true") {
            //save user data in state
            this.setState({
                name: user._profile.name,
                email: user._profile.email,
                accessToken: user._token.accessToken,
                provider: user._provider,
                social_login: true
            });
            //request for OTP, send accessToken, if email exists in db, user will login
            this.props.sendOtp(user._profile.email, null, user._token.accessToken, user._provider);
        } else {
            //call to new api to check if phone number present

            //if record phone number present, then login,

            //else show enter phone number
            this.setState({
                name: user._profile.name,
                email: user._profile.email,
                accessToken: user._token.accessToken,
                provider: user._provider,
                social_login: true
            });
            this.props.loginUser(
                user._profile.name,
                user._profile.email,
                null,
                user._token.accessToken,
                null,
                user._provider,
                JSON.parse(localStorage.getItem("userSetAddress"))
            );
        }
    };

    handleSocialLoginFailure = err => {
        this.setState({ error: true });
    };

    handleCountDown = () => {
        setTimeout(() => {
            this.setState({ showResendOtp: true });
            clearInterval(this.intervalID);
        }, 15000 + 1000);
        this.intervalID = setInterval(() => {
            console.log("interval going on");
            this.setState({ countDownSeconds: this.state.countDownSeconds - 1 });
        }, 1000);
    };

    componentWillUnmount() {
        //clear countdown
        console.log("Countdown cleared");
        clearInterval(this.intervalID);
    }

    //footer
    showModalAGB () {
        this.setState({
            AGBModal: true
        });
    }
    hideModalAGB = () => {
        this.setState({
            AGBModal: false
        });
    }

    //install app
    showModalInstall () {
        this.setState({
            installModal: true
        });
    }
    hideModalInstall = () => {
        this.setState({
            installModal: false
        });
    }
    showModalBerechtigungen () {
        this.setState({
            berechtigungenModal: true
        });
    }
    hideModalBerechtigungen = () => {
        this.setState({
            berechtigungenModal: false
        });
    }
    triggerInstallBtn = () => {
        var div = document.getElementById('download');
        div.innerHTML = 'Laden...';
        
        setTimeout(function () {

            if (INSTALL_APP_POPUP) {//INSTALL_APP_POPUP is declared globally
                console.log("INSTALL_APP_POPUP", INSTALL_APP_POPUP); 
                INSTALL_APP_POPUP.prompt(); //this will show the prompt from chrome
            } 

            div.innerHTML = 'Hinzufügen';
        }, 1500);
    }
    showPrompt = () => {
        console.log("button clicked");
        document.getElementById('showPwaprompt').style.display = "block";
    } 
    next = (install, pwaSupported) =>{
        if(pwaSupported == false){
            alert("Deine Browser Version wird nicht unterstützt. Bitte installiere Chrome 70 oder höher.");
        } else if(install == "voice"){
            this.hideModalBerechtigungen2();
            this.showModalInstall2();
        } else {
            this.hideModalBerechtigungen();
            this.showModalInstall();
        }
    }
    fullBrowserVersion = () =>{
        return fullBrowserVersion.replace( /^([^.]*\.)(.*)$/, function ( a, b, c ) { 
            return b + c.replace( /\./g, '' );
        });
    }

    //work model
    showModalSVS () {
        this.setState({
            SVSModal: true
        });
    }
    hideModalSVS = () => {
        this.setState({
            SVSModal: false
        });
    }
    showModalASVG () {
        this.setState({
            ASVGModal: true
        });
    }
    hideModalASVG = () => {
        this.setState({
            ASVGModal: false
        });
    }

    // When the user clicks on the button, scroll to the top of the document
    topFunction = () => {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }

    //add delivery guy role after registration
    setDeliveryGuyRole = (user_id, auth_token) => {
        axios
        .get('//'+window.location.hostname+'/php/shopperPwa_isDeliveryGuy.php?user_id='+user_id+'&auth_token='+btoa(auth_token))
        .then(response => {
            console.log("setDeliveryGuyRole", response.data);
            window.location = "/delivery/login";
        })
        .catch(function(error) {
            console.log(error);
        });
    } 

    render() {
        if (localStorage.getItem("storeColor") === null) {
            return <Redirect to={"/"} />;
        }

        const { user, delivery_user, history } = this.props;
        if(delivery_user.success) {
            return (
                window.location = "/delivery/dashboard"
            );
        }

        //check if chrome version is supported
        if(isChrome || isChromium){
            if(osName == "Windows" && this.fullBrowserVersion() >= 70){
                var pwaSupported = true;
            } else if(osName == "Android" && this.fullBrowserVersion() >= 31){
                var pwaSupported = true;
            } else if(osName == "Ubuntu" && this.fullBrowserVersion() >= 72){
                var pwaSupported = true;
            } else {
                var pwaSupported = false;
            }
        } else {
            var pwaSupported = false;
        }

        return (
            <React.Fragment>

                {/* PreLoading the loading gif */}
                <img src="/assets/img/loading-food.gif" className="hidden" alt="prefetching" />

                {this.state.error && (
                    <div className="auth-error">
                        <div className="error-shake">{localStorage.getItem("registerErrorMessage")}</div>
                    </div>
                )}
                {this.state.email_phone_already_used && (
                    <div className="auth-error">
                        <div className="error-shake">{localStorage.getItem("emailPhoneAlreadyRegistered")}</div>
                    </div>
                )}
                {this.state.invalid_otp && (
                    <div className="auth-error">
                        <div className="error-shake">{localStorage.getItem("invalidOtpMsg")}</div>
                    </div>
                )}
                {this.state.loading && (
                    <div className="height-100 overlay-loading">
                        <div>
                            <img src="/assets/img/loading-food.gif" alt={localStorage.getItem("pleaseWaitText")} />
                        </div>
                    </div>
                )}

                <div style={{ backgroundColor: "#f2f4f9" }}>
                    <div style={{ backgroundColor: "#f2f4f9" }}>
                        {/* {window.innerWidth < 768 ? (
                            <div className="input-group">
                                <div className="input-group-prepend">
                                    <BackButton history={this.props.history} />
                                </div>
                            </div>
                        ) : (
                            <Nav redirectUrl="shopper" logo={true} active_nearme={true} disable_back_button={true} history={history} loggedin={user.success} />
                        )} */}
                        <Nav redirectUrl="shopper" logo={true} active_nearme={true} disable_back_button={true} history={history} loggedin={user.success} />
                    </div>
                </div>

                <div className="bg-white">

                    <div style={{background:"linear-gradient(318.68deg, rgb(15, 73, 92) 0%, rgb(13, 54, 64) 49.72%, rgb(8, 20, 31) 100%)"}}>

                        <div  style={{maxWidth:"400px", margin:"auto", padding:"30px 18px 20px"}}>

                            <div style={{fontSize:"34px", fontFamily:"open sans", fontWeight:600, color:"#fff"}}>Werde Shopper und verdiene Geld</div>

                            <form onSubmit={this.handleRegister} id="registerForm">
                                <div className="form-group pt-30">

                                    {/* name */}
                                    {this.validator.message("name", this.state.name, "required|string")}
                                    <div style={{marginBottom:"10px"}}>
                                        <input placeholder={localStorage.getItem("loginLoginNameLabel")} type="text" name="name" onChange={this.handleInputChange} style={{padding: "5px", width:"100%"}} />
                                    </div>

                                    {/* email */}
                                    {this.validator.message("email", this.state.email, "required|email")}
                                    <div style={{marginBottom:"10px"}}>
                                        <input placeholder={localStorage.getItem("loginLoginEmailLabel")} type="text" name="email" onChange={this.handleInputChange} style={{padding: "5px", width:"100%"}} />
                                    </div>

                                    {/* phone */}
                                    {this.validator.message("phone", this.state.phone, ["required", { regex: ["^\\+[1-9]\\d{1,14}$"] }, { min: ["8"] }])}
                                    <div style={{marginBottom:"10px"}}>
                                        <input
                                            placeholder={localStorage.getItem("loginLoginPhoneLabel")+" "+(localStorage.getItem("phoneCountryCode") === null ? "" : "("+localStorage.getItem("phoneCountryCode")+")")}
                                            // defaultValue={localStorage.getItem("phoneCountryCode") === null ? "" : localStorage.getItem("phoneCountryCode")}
                                            name="phone"
                                            type="tel"
                                            onChange={this.handleInputChange}
                                            style={{padding: "5px", width:"100%"}}
                                        />
                                    </div>

                                    {/* password */}
                                    {this.validator.message("password", this.state.password, "required|min:8")}
                                    <div style={{marginBottom:"10px"}}>
                                        <input
                                            placeholder={localStorage.getItem("loginLoginPasswordLabel")}
                                            type="password"
                                            name="password"
                                            onChange={this.handleInputChange}
                                            style={{padding: "5px", width:"100%"}}
                                        />
                                    </div>

                                </div>

                                <div className="mt-20 pt-15 button-block">
                                    <button type="submit" className="btn btn-main" style={{ backgroundColor: localStorage.getItem("storeColor") }}>
                                        {localStorage.getItem("registerRegisterTitle")}
                                    </button>
                                </div>
                                
                            </form>

                            <form onSubmit={this.handleVerifyOtp} id="otpForm" className="hidden">
                                <div className="form-group px-15 pt-30">
                                    <label className="col-12 edit-address-input-label">
                                        {localStorage.getItem("otpSentMsg")} {this.state.phone}
                                        {this.validator.message("otp", this.state.otp, "required|numeric|min:4|max:5")}
                                    </label>
                                    <div className="col-md-9">
                                        <input name="otp" type="tel" onChange={this.handleInputChange} className="form-control edit-address-input" required />
                                    </div>

                                    <button type="submit" className="btn btn-main" style={{ backgroundColor: localStorage.getItem("storeColor") }}>
                                        {localStorage.getItem("verifyOtpBtnText")}
                                    </button>

                                    <div className="mt-30 mb-10">
                                        {this.state.showResendOtp && (
                                            <div className="resend-otp" onClick={this.resendOtp}>
                                                {localStorage.getItem("resendOtpMsg")} {this.state.phone}
                                            </div>
                                        )}

                                        {this.state.countDownSeconds > 0 && (
                                            <div className="resend-otp countdown">
                                                {localStorage.getItem("resendOtpCountdownMsg")} {this.state.countDownSeconds}
                                            </div>
                                        )}
                                    </div>
                                </div>
                            </form>

                            {/* <form onSubmit={this.handleRegisterAfterSocialLogin} id="phoneFormAfterSocialLogin" className="hidden">
                                <div className="form-group px-15 pt-30">
                                    <label className="col-12 edit-address-input-label">
                                        {localStorage.getItem("socialWelcomeText")} {this.state.name},
                                    </label>
                                    <label className="col-12 edit-address-input-label">
                                        {localStorage.getItem("enterPhoneToVerify")}{" "}
                                        {this.validator.message("phone", this.state.phone, ["required", { regex: ["^\\+[1-9]\\d{1,14}$"] }, { min: ["8"] }])}
                                    </label>
                                    <div className="col-md-9 pb-5">
                                        <input
                                            defaultValue={localStorage.getItem("phoneCountryCode") === null ? "" : localStorage.getItem("phoneCountryCode")}
                                            name="phone"
                                            type="tel"
                                            onChange={this.handleInputChange}
                                            className="form-control edit-address-input"
                                        />
                                    </div>
                                    <button type="submit" className="btn btn-main" style={{ backgroundColor: localStorage.getItem("storeColor") }}>
                                        {localStorage.getItem("registerRegisterTitle")}
                                    </button>
                                </div>
                            </form>

                            <div className="text-center mt-3 mb-20" id="socialLoginDiv">
                                <p className="login-or mt-2">OR</p>
                                <div ref="socialLoginLoader">
                                    <ContentLoader height={60} width={400} speed={1.2} primaryColor="#f3f3f3" secondaryColor="#ecebeb">
                                        <rect x="28" y="0" rx="0" ry="0" width="165" height="45" />
                                        <rect x="210" y="0" rx="0" ry="0" width="165" height="45" />
                                    </ContentLoader>
                                </div>
                                <div ref="socialLogin" className="hidden">
                                    {localStorage.getItem("enableFacebookLogin") === "true" && (
                                        <SocialButton
                                            provider="facebook"
                                            appId={localStorage.getItem("facebookAppId")}
                                            onLoginSuccess={this.handleSocialLogin}
                                            onLoginFailure={() => console.log("Failed didn't get time to init or login failed")}
                                            className="facebook-login-button mr-2"
                                        >
                                            {localStorage.getItem("facebookLoginButtonText")}
                                        </SocialButton>
                                    )}
                                    {localStorage.getItem("enableGoogleLogin") === "true" && (
                                        <SocialButton
                                            provider="google"
                                            appId={localStorage.getItem("googleAppId")}
                                            onLoginSuccess={this.handleSocialLogin}
                                            onLoginFailure={() => console.log("Failed didn't get time to init or login failed")}
                                            className="google-login-button"
                                        >
                                            {localStorage.getItem("googleLoginButtonText")}
                                        </SocialButton>
                                    )}
                                </div>
                            </div> */}

                            <div id="riderFormular">

                                <div style={{marginTop:"20px", fontSize:"12px", color:"#999"}}>
                                    <p style={{marginTop:"5px", fontSize:"11px", fontFamily:"open sans", fontWeight:400}}>
                                    Sie müssen mindestens 18 Jahre alt sein, um sich anzumelden. Mit Ihrer Anmeldung stimmen Sie den <span style={{textDecoration: "underline", cursor: "pointer"}} onClick={() => { this.showModalAGB(); }}>AGB</span> von Lieferservice123 zu und bestätigen, dass Sie die Datenschutzbestimmungen gelesen haben. Durch die Angabe Ihrer Telefonnummer stimmen Sie dem Empfang von Textnachrichten von Lieferservice123 zu.
                                    </p>
                                </div>

                                <div style={{marginTop:"20px", fontSize:"12px", color:"#fff"}}>
                                    <p style={{marginTop:"5px"}}>
                                        {/* Sie haben bereits ein Konto? <a href={'/shopperPwa?act=dashboard'} style={{textDecoration:"underline"}}>Jetzt anmelden</a> */}
                                        Sie haben bereits ein Konto? <a href={'/delivery/login'} style={{textDecoration:"underline"}}>Jetzt anmelden</a>
                                    </p>
                                </div>

                                {/* <div style={{padding: "20px 16px 16px", fontSize: "15px", maxWidth:"1020px", margin:"auto"}}>
                                    <FontAwesomeIcon icon={faCheck} style={{marginRight:"15px", color:"green"}} />Online Trinkgeld erhalten<br/>
                                    <FontAwesomeIcon icon={faCheck} style={{marginRight:"15px", color:"green"}} />Sofortauszahlung nach Auftrag<br/>
                                    <FontAwesomeIcon icon={faCheck} style={{marginRight:"15px", color:"green"}} />Shoppe via Prepaid Card<br/>
                                </div> */}

                            </div>  

                        </div>

                    </div>  

                    {/* add to homescreen */}
                    {matchMedia('(display-mode: standalone)').matches == false ? <div id="download_app" style={{width:"100%", float:"left", textAlign:"center", padding: "60px 18px 50px"}}>
                        <div style={{maxWidth:"768px", margin:"auto", textShadow: "0 1px 4px rgba(0,0,0,0.1)"}}>

                            <div style={{fontSize: "24px", fontWeight: 400, fontFamily: "open sans", marginBottom: "15px"}}>{localStorage.getItem("firstScreenDownloadApp")}</div>

                            <div>
                                {/* {localStorage.getItem("firstScreenDownloadAppSub")}  */}
                                <div style={{fontSize:"10px", margin:"10px 0"}}>{localStorage.getItem("firstScreenDownloadAppWin")}</div>
                            </div>
    
                            <div style={{marginBottom:"5px"}}><a onClick={() => { this.showModalBerechtigungen(); }} style={{cursor:"pointer"}}><img width="156px" alt="Google Play" src="//lieferservice123.com/assets/img/playstore.png" /></a></div>
                        
                            <div><a onClick={() => { this.showPrompt() }} style={{cursor:"pointer"}}><img width="156px" alt="App Store" src="//lieferservice123.com/assets/img/appstore.png" /></a></div>

                            <div id="showPwaprompt" style={{display:"none"}}>
                                <PWAPrompt debug={1} 
                                copyTitle={"Add to Home Screen"} 
                                copyBody={"This website has app functionality. Add it to your home screen to use it in fullscreen and while offline."}
                                copyShareButtonLabel={"1) Press the 'Share' button"}
                                copyAddHomeButtonLabel={"2) Press 'Add to Home Screen'"}
                                copyClosePrompt={"Cancel"} 
                                />
                            </div>
                        </div>
                    </div>
                    : null}

                    {/* advantages */}
                    <div style={{background:"linear-gradient(318.68deg, rgb(15, 73, 92) 0%, rgb(13, 54, 64) 49.72%, rgb(8, 20, 31) 100%)", overflow:"auto", padding: "60px 18px 16px", textAlign:"center", lineHeight: "24px"}}>
                        
                        <div style={{fontSize: "15px", maxWidth:"1020px", margin:"auto"}}>

                            <div className="advantagesShopper">
                                <div>Beim Einkaufen Geld verdienen</div>
                                <p>Als Shopper übernehmen Sie Einkäufe und werden dafür pro Auftrag bezahlt.</p>
                            </div>

                            <div className="advantagesShopper">
                                <div>Einkommenschancen</div>
                                <p>Verdiene bis zu 30€ pro Auftrag!</p>
                            </div>

                            <div className="advantagesShopper">
                                <div>Nutze die Zeit, wie es dir gefällt.</div>
                                <p>Öffne die App und shoppe, wann immer es für dich passt.</p>
                            </div>

                        </div>
                    </div>

                    {/* tel verification */}
                    <Modal show={this.state.VerifyRegistrationModal} onHide={this.hideModalVerifyRegistration} size="sm">
                        <Modal.Header closeButton>
                            <Modal.Title>Bestätigen</Modal.Title>
                        </Modal.Header>
                        <Modal.Body style={{padding:"0 25px 10px"}}>

                            <div style={{color:"#999", fontFamily:"open sans", fontWeight:"300"}}>Aktion</div>
                            <div style={{marginBottom:"15px"}}>Telefonnummer verifizieren</div>

                            {/* <div style={{color:"#999", fontFamily:"open sans", fontWeight:"300"}}>Restaurant</div>
                            <div style={{marginBottom:"15px"}}>{this.props.restaurant_info.name}<br/>{this.props.restaurant_info.address}</div> */}

                            <div style={{color:"#999", fontFamily:"open sans", fontWeight:"300", marginBottom:"15px"}}>Klicke auf <span onClick={() => this.sendCode(this.state.fromTel, "", "", true)} style={{color:"#0872BA", cursor:"pointer"}}>Code senden</span> um den Bestätigungscode erneut an {this.state.fromTel} zu senden.</div>

                            <input maxLength="4" type="tel" placeholder="Code eingeben" name="code" onChange={this.onCodeChanged} style={{padding:"8px", marginBottom:"5px", border:"1px solid #ccc"}} ref={this.innerRef} />

                            <div id="result"></div>

                            <div style={{marginTop:"20px", fontSize:"9px", color:"#999"}}>
                                Bitte überprüfen Sie die SMS-Daten und geben Sie mit dem Code Ihr Go.
                            </div>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button variant="secondary" onClick={this.hideModalVerifyRegistration}> 
                                Schließen
                            </Button> 
                            <Button id="confirm" variant="primary" onClick={() => this.acceptRegistration(this.state.fromName, this.state.fromTel, this.state.fromAddress, this.state.verificationCode, this.state.total, "", "", this.state.trinkgeld, this.state.fromLat, this.state.fromLng)}> 
                                Bestätigen
                            </Button> 
                        </Modal.Footer>
                    </Modal>

                    {/* agb */}
                    <Modal show={this.state.AGBModal} onHide={this.hideModalAGB} size="lg">
                        <Modal.Header closeButton>
                            <Modal.Title>AGB</Modal.Title>
                        </Modal.Header>
                        <Modal.Body style={{padding:"0 25px 10px"}}>
                            <iframe src={'//'+window.location.hostname+'/php/footer/agb.php'} style={{width:"100%", height:(window.innerHeight / 1.5)}} frameBorder="0" />
                        </Modal.Body>
                        <Modal.Footer>
                            <Button variant="secondary" onClick={this.hideModalAGB}> 
                                Schließen
                            </Button> 
                        </Modal.Footer>
                    </Modal>

                    {/* work as */}
                    <div style={{margin: "60px 16px 16px", float:"left", width:"100%", fontSize:"30px", fontFamily:"open sans", fontWeight:400, textAlign:"center"}}>Was passt zu Dir?</div>

                    <div style={{margin:"18px 18px 40px", float:"left", width:"100%"}}><div style={{maxWidth:"700px", margin:"auto"}}>

                        {/* Neuer Selbstständiger */}
                        <div className="workModel">
                            <div style={{textAlign:"center", fontWeight:"bold", fontSize:"20px"}}>Selbstständig</div>
                            <div style={{textAlign:"center", marginBottom:"5px", fontFamily:"open sans", fontWeight:300}}>Werksvertrag</div>

                            <div style={{maxWidth:"300px", margin:"auto"}}>
                                <li>Verdiene bis €{15*10*30} monatlich</li>
                                <li>Online Trinkgeld Funktion</li>

                                <li>Eigenes Fahrzeug notwendig</li>
                                <li>Arbeite, wann du willst</li>
                                
                                <div style={{textTransform:"uppercase", color:"rgb(252, 128, 25)", border:"1px solid rgb(252, 128, 25)", padding:"10px", marginTop:"10px", borderRadius:"3px", cursor:"pointer", textAlign:"center"}} onClick={() => { this.showModalSVS(); }}>Mehr anzeigen</div>
                            </div>
                        </div>

                        {/* Freier Dienstnehmer */}
                        <div className="workModel">
                            <div style={{textAlign:"center", fontWeight:"bold", fontSize:"20px"}}>Geringfügig</div>
                            <div style={{textAlign:"center", marginBottom:"5px", fontFamily:"open sans", fontWeight:300}}>Freier Dienstvertrag</div>

                            <div style={{maxWidth:"300px", margin:"auto"}}>

                                <li>Verdiene bis €460 monatlich</li>
                                <li>Unfallversichert</li>

                                <li>Eigenes Fahrzeug notwendig</li>
                                <li>Flexibler Zeitplan</li>

                                <div style={{textTransform:"uppercase", color:"rgb(252, 128, 25)", border:"1px solid rgb(252, 128, 25)", padding:"10px", marginTop:"10px", borderRadius:"3px", cursor:"pointer", textAlign:"center"}} onClick={() => { this.showModalASVG(); }}>Mehr anzeigen</div>
                            </div>
                        </div>

                    </div></div>

                    {/* become shopper */}
                    <div className="shopper2" style={{padding:"20px 0"}} onClick={() => this.topFunction()}>
                        <div style={{color:"#fff", fontFamily:"open sans", fontWeight:"bold"}}>Werde jetzt ein Teil unserer Shopper-Community!</div>
                        <div style={{margin:"20px 0 15px"}}><span style={{border:"2px solid #fff", borderRadius:"4px", padding:"10px 20px"}}>Jetzt anmelden<Ink duration="500" /></span></div>
                    </div>

                    {/* independent contractor modal */}
                    <Modal show={this.state.SVSModal} onHide={this.hideModalSVS} size="lg">
                        <Modal.Header closeButton>
                            <Modal.Title>Selbstständige Shopper</Modal.Title>
                        </Modal.Header>
                        <Modal.Body style={{padding:"0 25px 10px"}}>
                        <div style={{margin:"10px auto", fontSize:"16px"}}>

                            <b style={{marginBottom:"10px", float: "left", width: "100%"}}>Warum sollte ich Lieferservice123 verwenden?</b>
                            <p>
                            Wir bemühen uns, die Lieferung von Lebensmitteln mühelos, erschwinglich und für jedermann zugänglich zu machen. Unsere Technologie ermöglicht Kunden den Online-Zugriff auf ihre Lieblingsgeschäfte, sodass sie von überall aus Lebensmittel bestellen können. Selbstständige Shopper erhalten diese Bestellungen über eine App auf ihrem Smartphone, kaufen dann ein und liefern Lebensmittel an die Tür des Kunden.
                            </p>
                            <ul>
                                <li>Verdienen Sie schnelles Geld</li>
                                <li>Bestimme die Tage und Arbeitsstunden</li>
                                <li>Shoppen und liefern Sie die Bestellung des Kunden</li>
                                <li>Lernen Sie Ihre Nachbarschaft kennen</li>
                                <li>Online Trinkgeld Funktion</li>
                                <li>Sofortauszahlung nach Auftrag</li>
                            </ul>
                            
                            <b style={{marginBottom:"10px", float: "left", width: "100%"}}>Grundvoraussetzungen:</b>
                            <ul>
                                <li>Mindestens 18 Jahre alt</li>
                                <li>Berechtigt, in Österreich bzw. EU zu arbeiten</li>
                                <li>Zugang zu einem Fahrzeug</li>
                                <li>Zugriff auf ein aktuelles Smartphone (iPhone 5 / Android 4.4 oder neuer)</li>
                            </ul>

                            <b style={{marginBottom:"10px", float: "left", width: "100%"}}>Weitere Informationen:</b>
                            <p>Wenn Sie bereits als Personal Shopper Dienstleistungen erbringen oder in der Lebensmittel- oder Transportbranche tätig sind (z. B. Kurier, Fahrer, LKW-Fahrer, Berufskraftfahrer, Taxifahrer, Fahrer für die Zustellung von Lebensmitteln), sollten Sie in Betracht ziehen, bei Lieferservice123 zu arbeiten. Wir begrüßen interessierte Personen aus verschiedenen Branchen und mit unterschiedlichem Hintergrund. Das Arbeiten auf der Lieferservice123-Plattform ist ideal für alle, die nach flexiblen, saisonalen, regionalen, Einstiegs-, Wochenend-, Wochentags-, außerschulischen oder vorübergehenden Möglichkeiten suchen.
                            </p>
                            <p>Lieferservice123 setzt sich für Vielfalt ein und bietet unabhängigen Auftragnehmern bzw. Selbstständigen Chancengleichheit. Lieferservice123 berücksichtigt qualifizierte Personen ohne Rücksicht auf Geschlecht, sexuelle Orientierung, Rasse, Behinderungsstatus oder andere durch geltendes Recht geschützte Kategorien.
                            </p>

                            <div style={{color:"blue", textDecoration:"underline", cursor:"pointer"}} onClick={() => window.open("/pages/werksvertrag", "_blank")}>Lesen Sie hier die Vereinbarung mit Selbstständigen</div>
                        </div>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button variant="secondary" onClick={this.hideModalSVS}> 
                                Schließen
                            </Button> 
                        </Modal.Footer>
                    </Modal>

                    {/* geringfügig modal */}
                    <Modal show={this.state.ASVGModal} onHide={this.hideModalASVG} size="lg">
                        <Modal.Header closeButton>
                            <Modal.Title>Geringfügige Shopper</Modal.Title>
                        </Modal.Header>
                        <Modal.Body style={{padding:"0 25px 10px"}}>
                        <div style={{margin:"10px auto", fontSize:"16px"}}>

                            <b style={{marginBottom:"10px", float: "left", width: "100%"}}>Warum sollte ich Lieferservice123 verwenden?</b>
                            <p>
                            Wir bemühen uns, die Lieferung von Lebensmitteln mühelos, erschwinglich und für jedermann zugänglich zu machen. Unsere Technologie ermöglicht Kunden den Online-Zugriff auf ihre Lieblingsgeschäfte, sodass sie von überall aus Lebensmittel bestellen können. Geringfügige Shopper erhalten diese Bestellungen über eine App auf ihrem Smartphone, kaufen dann ein und liefern Lebensmittel an die Tür des Kunden.
                            </p>
                            <ul>
                                <li>Verdienen Sie schnelles Geld</li>
                                <li>Bestimme die Tage und Arbeitsstunden, zu denen Du arbeiten möchtest!</li>
                                <li>Shoppen und liefern Sie die Bestellung des Kunden</li>
                                <li>Lernen Sie Ihre Nachbarschaft kennen</li>
                                <li>Einen Stundenlohn und einen freien Dienstvertrag</li>
                                <li>Eine echte Unfallversicherung, wir halten Dir den Rücken frei</li>
                            </ul>
                            
                            <b style={{marginBottom:"10px", float: "left", width: "100%"}}>Grundvoraussetzungen:</b>
                            <ul>
                                <li>Mindestens 18 Jahre alt</li>
                                <li>Berechtigt, in Österreich bzw. EU zu arbeiten</li>
                                <li>Zugang zu einem Fahrzeug</li>
                                <li>Zugriff auf ein aktuelles Smartphone (iPhone 5 / Android 4.4 oder neuer)</li>
                            </ul>

                            <b style={{marginBottom:"10px", float: "left", width: "100%"}}>Weitere Informationen:</b>
                            <p>Wenn Sie Erfahrung in der Lebensmittelbranche haben, sollten Sie sich dem Lieferservice123-Team anschließen. Wir begrüßen auch interessierte Personen aus anderen Branchen und mit unterschiedlichem Hintergrund. Das Arbeiten mit Lieferservice123 ist ideal für alle, die nach flexiblen, saisonalen, Einstiegs-, Wochenend-, Wochentags-, außerschulischen, vorübergehenden oder Teilzeitmöglichkeiten suchen.
                            </p>

                            <p>Lieferservice123 setzt sich für Vielfalt ein und ist ein Arbeitgeber für Chancengleichheit. Lieferservice123 berücksichtigt qualifizierte Personen ohne Rücksicht auf Geschlecht, sexuelle Orientierung, Rasse, Behinderungsstatus oder andere durch geltendes Recht geschützte Kategorien.
                            </p>

                            <div style={{color:"blue", textDecoration:"underline", cursor:"pointer"}} onClick={() => window.open("/pages/freier_dienstvertrag", "_blank")}>Lesen Sie hier die Vereinbarung mit freien Dienstnehmern</div>

                        </div>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button variant="secondary" onClick={this.hideModalASVG}> 
                                Schließen
                            </Button> 
                        </Modal.Footer>
                    </Modal>

                    {/* app-vorteile */}
                    <Modal show={this.state.berechtigungenModal} onHide={this.hideModalBerechtigungen}>
                        
                        <Modal.Header closeButton>
                            <Modal.Title>Shopper App</Modal.Title>
                        </Modal.Header>

                        <Modal.Body style={{padding:"0 25px 10px"}}>
                            <div style={{marginTop:"12px"}}>
                                <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Shopper App auf Desktop installieren.</div>

                                <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Verdiene bis zu 30€ pro Auftrag.</div>

                                <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Push-Nachrichten erhalten.</div>

                                <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Installation über Google Chrome.</div>
                            </div>

                        </Modal.Body>

                        <Modal.Footer>
                            <Button variant="secondary" onClick={this.hideModalBerechtigungen}> 
                                Schließen
                            </Button> 
                            <Button variant="primary" onClick={() => this.next("", pwaSupported)}>
                                Weiter
                            </Button>
                        </Modal.Footer>
                    </Modal>

                    {/* app-installieren */}
                    <Modal show={this.state.installModal} onHide={this.hideModalInstall}>
                        
                        <Modal.Header closeButton>
                            <Modal.Title>Installieren</Modal.Title>
                        </Modal.Header>

                        <Modal.Body style={{padding:"0 25px 10px"}}>
                            <div style={{marginTop:"12px"}}>
                                Klicke auf hinzufügen um die Shopper App auf deinen Bildschirm zu installieren.
                            </div>

                            <div style={{marginTop:"20px", fontSize:"11px", color:"#999"}}>Wenn dir Lieferservice123 gefällt, dann installiere bitte die  App.</div>
                        </Modal.Body>

                        <Modal.Footer>
                            <Button variant="secondary" onClick={this.hideModalInstall}> 
                                Schließen
                            </Button> 
                            <Button variant="primary" onClick={() => this.triggerInstallBtn()} id="download">
                                Hinzufügen
                            </Button>
                        </Modal.Footer>
                    </Modal>

                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user.user,
    delivery_user: state.delivery_user.delivery_user,

});

export default connect(
    mapStateToProps,
    { registerUser, loginUser, sendOtp, verifyOtp }
)(Register);
