import React, { Component } from "react";
import axios from "axios";

import Ink from "react-ink";
import Meta from "../../helpers/meta";
import { NavLink } from "react-router-dom";
import Nav from "../../Takeaway/Nav";
import { connect } from "react-redux";
import { getSettings } from "../../../services/settings/actions";
import { getSingleLanguageData } from "../../../services/translations/actions";

// fontawesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';
import { faShoppingBag } from '@fortawesome/free-solid-svg-icons';
import { faCar } from '@fortawesome/free-solid-svg-icons';
import { faCoins } from '@fortawesome/free-solid-svg-icons';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { faCreditCard } from '@fortawesome/free-solid-svg-icons';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import { faIdCard } from '@fortawesome/free-solid-svg-icons';
import { faMobile } from '@fortawesome/free-solid-svg-icons';
import { faListAlt } from '@fortawesome/free-solid-svg-icons';
import { faUpload } from '@fortawesome/free-solid-svg-icons';
import { faBolt } from '@fortawesome/free-solid-svg-icons';
import { faHome } from '@fortawesome/free-solid-svg-icons';
import { faHeartbeat } from '@fortawesome/free-solid-svg-icons';
import { faQuestionCircle } from '@fortawesome/free-solid-svg-icons';
import { faKey } from '@fortawesome/free-solid-svg-icons';
import { faStore } from '@fortawesome/free-solid-svg-icons';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

//bootstrap
import ReactDOM from 'react-dom';
import { Modal, Button, Dropdown } from 'react-bootstrap';
import ReactTooltip from 'react-tooltip';

//supermarket
import { SUPERMARKET_ORDER_ALL_ACCEPTED_URL } from "../../../configs/index";
import { TRANSACTION_HISTORY_URL } from "../../../configs/index";

import Location from "../../Takeaway/Location/";

import { updateDeliveryUserInfo } from "../../../services/Delivery/user/actions";

import CountTo from "react-count-to";
import Moment from "react-moment";

import ViewOrder from "./viewOrder.js";
import ShopperMenu from "./shopperMenu.js";
import { walletTransaction } from "../../../services/Delivery/deliveryprogress/actions";


declare var INSTALL_APP_POPUP;


class Earnings extends Component {

    constructor() {
        super();
        
        this.state = { 
            invoiceSupermarketModal: false,
            supermarketOrdersDelivered: [],//earnings
            transactionHistory: [],//letzte auszahlungen
        };
    }

    static contextTypes = {
        router: () => null
    };

    componentDidMount() {
        const { settings } = this.props;

        //set country if not set
        if (!localStorage.getItem("country")) {
            this.getCountry();
        }
        
        //if one config is missing then call the api to fetch settings
        //if (!localStorage.getItem("storeColor")) {
            this.props.getSettings();
        //}

        this.props.getSingleLanguageData(localStorage.getItem("userPreferedLanguage")?localStorage.getItem("userPreferedLanguage"):1);

        const { delivery_user } = this.props;
        //update delivery guy info
        if(delivery_user.success){
            this.props.updateDeliveryUserInfo(delivery_user.data.id, delivery_user.data.auth_token);
        }

        if(localStorage.getItem("country") == "AT"){
            this.supermarketOrdersDelivered();
            this.transactionHistory();
            this.refreshSetInterval = setInterval(() => {
                this.__refreshOrderStatus();
            }, 30 * 1000);
        }
    }

    //view batch
    showModalInvoiceSupermarket (id) {
        this.setState({
            invoiceSupermarketModal: {
              [id]: true
           }
        });
    }
    hideModalInvoiceSupermarket = () => {
        this.setState({
            invoiceSupermarketModal: false
        })
    }

    //lang
    showModalLang () {
        this.setState({
            LangModal: true
        });
    }
    hideModalLang = () => {
        this.setState({
            LangModal: false
        });
    }

    //cashout
    showModalStripeAgreement () {
        this.setState({
            StripeAgreementModal: true
        });
    }
    hideModalStripeAgreement = () => {
        this.setState({
            StripeAgreementModal: false
        });
    }
    showModalCashout () {
        this.setState({
            CashoutModal: true
        });
    }
    hideModalCashout = () => {
        this.setState({
            CashoutModal: false
        });
    }
    showModalPaymentMethods () {
        this.setState({
            PaymentMethodsModal: true
        });
    }
    hideModalPaymentMethods = () => {
        this.setState({
            PaymentMethodsModal: false
        });
    }
    showModalCashoutConfirmed (amount, payment_method) {
        this.setState({
            CashoutConfirmedModal: true
        });

        this.__cashout(payment_method);
    }
    hideModalCashoutConfirmed = () => {
        this.setState({
            CashoutConfirmedModal: false
        });

        setTimeout(() => {
            window.location = window.location;
        }, 2 * 1000);
    }
    __cashout = (payment_method) => {
        const { delivery_user } = this.props;

        this.props.walletTransaction(
            delivery_user.data.auth_token,
            delivery_user.data.id,
            '',
            'withdraw',
            'Cashout earnings',
            payment_method,
            this.state.paypal,
            this.state.iban,
            this.state.receivername
        );
    };

    __refreshOrderStatus = () => {
        this.supermarketOrdersDelivered();
        this.transactionHistory();
        if (this.refs.refreshButton) {
            var div = this.refs.refreshButton;
            div.innerHTML = 'Laden...';
        }
        
        setTimeout(() => {
            if (this.refs.refreshButton) {
                var div = this.refs.refreshButton;
                div.innerHTML = 'Datenaktualisierung in 30 Sekunden';
            }
        }, 2 * 1000);
    };

    //lang and country
    getCountry = () => {
        axios
            .get("https://lieferservice123.com/php/country.php", {
            })
            .then(response => {
                localStorage.setItem("country", response.data);
                if(response.data == "AT"){
                    localStorage.setItem("phoneCountryCode", "43");
                } 
                else if(response.data == "DE"){
                    localStorage.setItem("phoneCountryCode", "49");
                } 
                else if(response.data == "CH"){
                    localStorage.setItem("phoneCountryCode", "41");
                } 
            })
            .catch(function(error) {
                console.log(error);
            });
    }

    //earnings
    supermarketOrdersDelivered = () => {
        const { delivery_user } = this.props;
    
        axios
        .post(SUPERMARKET_ORDER_ALL_ACCEPTED_URL, {
            limit: 50,
            fromTel: delivery_user.data.phone,
            orderstatus_id: 3
        })
        .then(response => {


            // console.log("response", response);
            
            if (response.data.length) {
                if (JSON.stringify(this.state.supermarketOrdersDelivered) !== JSON.stringify(response.data)) {
                    this.setState({
                        supermarketOrdersDelivered: response.data,
                        
                        no_ordersDelivered: false
                    });
                }

            } else {
                this.setState({
                    no_ordersDelivered: true
                });
            }
        })
        .catch(function(error) {
            console.log(error);
        });
    } 
    transactionHistory = () => {
        const { delivery_user } = this.props;
    
        axios
        .post(TRANSACTION_HISTORY_URL, {
            limit: 3,
            fromTel: delivery_user.data.phone
        })
        .then(response => {

            // console.log("response", response);
            
            if (response.data.length) {
                if (JSON.stringify(this.state.transactionHistory) !== JSON.stringify(response.data)) {
                    this.setState({
                        transactionHistory: response.data,
                        no_transactionHistory: false
                    });
                }

            } else {
                this.setState({
                    no_transactionHistory: true
                });
            }
        })
        .catch(function(error) {
            console.log(error);
        });
    } 

    //time
    elapsedTime = (time) => {
        switch (typeof time) {
            case 'number':
            break;
            case 'string':
            time = +new Date(time);
            break;
            case 'object':
            if (time.constructor === Date) time = time.getTime();
            break;
            default:
            time = +new Date();
        }
        var time_formats = [
            [60, 's', 1], // 60
            [120, 'vor 1 min', '1 minute from now'], // 60*2
            [3600, 'min', 60], // 60*60, 60
            [7200, 'vor 1 h', '1 hour from now'], // 60*60*2
            [86400, 'h', 3600], // 60*60*24, 60*60
            [172800, 'Gestern', 'Tomorrow'], // 60*60*24*2
            [604800, 'Tagen', 86400], // 60*60*24*7, 60*60*24
            [1209600, 'Letzte Woche', 'Next week'], // 60*60*24*7*4*2
            [2419200, 'Wochen', 604800], // 60*60*24*7*4, 60*60*24*7
            [4838400, 'Letzten Monat', 'Next month'], // 60*60*24*7*4*2
            [29030400, 'Monaten', 2419200], // 60*60*24*7*4*12, 60*60*24*7*4
            [58060800, 'Letztes Jahr', 'Next year'], // 60*60*24*7*4*12*2
            [2903040000, 'Jahren', 29030400], // 60*60*24*7*4*12*100, 60*60*24*7*4*12
        ];
        var seconds = (+new Date() - time) / 1000,
            token = 'vor',
            list_choice = 1;

        if (seconds == 0) {
            return 'Gerade jetzt'
        }
        if (seconds < 0) {
            seconds = Math.abs(seconds);
            token = 'ab jetzt';
            list_choice = 2;
        }
        var i = 0,
            format;
        while (format = time_formats[i++])
            if (seconds < format[0]) {
            if (typeof format[2] == 'string')
                return format[list_choice];
            else
                return token + ' ' + Math.floor(seconds / format[2]) + ' ' + format[1];
            }
        return time;
    }  
    toDate = (unix_timestamp) => {
        var d = new Date(unix_timestamp * 1000);
        return d.getFullYear() + '-' + (d.getMonth()+1) + '-' + d.getDate() + ' ' + d.getHours() + ':' + d.getMinutes();
    }
    showDate = (unix_timestamp) => {
        var days = ['So','Mo','Di','Mi','Do','Fr','Sa'];
        const monthNames = ["Jän", "Feb", "März", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dez"];
        var d = new Date(unix_timestamp * 1000);
        return  days[d.getDay()]+', '+d.getDate()+ '. ' + (monthNames[d.getMonth()]);
    }

    // hamburger menu
    showModalMenu () {
        this.setState({
            MenuModal: true
        });
    }
    hideModalMenu = () => {
        this.setState({
            MenuModal: false
        });
    }
    
    //einnahmen
    makeTotalTips = (data = []) => {
        let sum = 0;
        data.forEach((row) => {
            if(row.subtotal == this.newTotal(row.items)){
                sum = sum + parseFloat(( parseFloat(row.subtotal * row.delivery_tip/100) ).toFixed(2))
            } else {
                sum = sum + parseFloat(( parseFloat(this.newTotal(row.items) * row.delivery_tip/100) ).toFixed(2))
            }   
        });
        return sum.toFixed(2);
    }
    makeTotalDeliveryEarnings = (data = []) => {
        let sum = 0;
        data.forEach((row) => {
            if(row.subtotal == this.newTotal(row.items)){
                sum = sum + parseFloat(( parseFloat(row.delivery_charge) + parseFloat(row.subtotal * row.shopper_reward/100) ).toFixed(2))
            } else {
                sum = sum + parseFloat(( parseFloat(row.delivery_charge) + parseFloat(this.newTotal(row.items) * row.shopper_reward/100) ).toFixed(2))
            }   
        });
        return sum.toFixed(2);
    }

    //cashout
    handleChangePaymentMethods = event => {
        const value = event.target.innerHTML;
        console.log("payment method", value);
      
        this.setState({
            payment_method: value
        });

        if(value == "PayPal"){
            document.getElementById('Soldo').style.display = "none";
            document.getElementById('Banküberweisung').style.display = "none";
            document.getElementById('PayPal').style.display = "block";
        } else if(value == "Banküberweisung"){
            document.getElementById('Soldo').style.display = "none";
            document.getElementById('PayPal').style.display = "none";
            document.getElementById('Banküberweisung').style.display = "block";
        } else if(value == "Instant Auszahlung"){
            document.getElementById('PayPal').style.display = "none";
            document.getElementById('Banküberweisung').style.display = "none";
            document.getElementById('Soldo').style.display = "block";
        }
    }
    onPaypalChanged = (e) => {
        this.setState({
            paypal: e.currentTarget.value
        });
    }
    onIbanChanged = (e) => {
        this.setState({
            iban: e.currentTarget.value
        });
    }
    onReceivernameChanged = (e) => {
        this.setState({
            receivername: e.currentTarget.value
        });
    }

    // new total price after refund and replacements
    newTotal = (data = []) => {
        let sum = 0;
        data.forEach((row) => {
            if(row.found > 0 && row.found_replacement == null){
                // console.log("NORMAL ("+row.found+' * '+row.price+')', row.id+'. '+(row.found * row.price));
                // console.log("("+row.found+" * "+row.price+") + ");
                sum = sum + (row.found * row.price)
            } 
            else if(row.found == 0 && row.found_replacement > 0){
                // console.log("REPLACEMENT ", row.replacement_id+'. '+(row.found_replacement * row.replacement_price));
                // console.log("("+row.found_replacement+" * "+row.replacement_price+") + ");
                sum = sum + (row.found_replacement * row.replacement_price)
            } 
        });
        return sum.toFixed(2);
    }

    getSettings = (data, keyName)=>{
        var filteredData = data.filter(x=>x.key == keyName);
        var focusData = undefined;
        if(filteredData.length > 0){
            focusData = filteredData[0].value;
        }
        return focusData;
    }

    render() {
        const { delivery_user } = this.props;
        const { supermarketOrdersDelivered, transactionHistory } = this.state;

        if (!delivery_user.success) {
            return (
                //redirect to account page
                window.location = "/delivery/login"
            );
        }

        var langStyle = {
            marginBottom:"15px", cursor:"pointer", color:"#666"
        };

        var langStyleBold = {
            marginBottom:"15px", cursor:"pointer", fontWeight:"bold", color:"#666"
        };

        return (
            <React.Fragment>
                <Meta
                    seotitle={localStorage.getItem("seoMetaTitle")}
                    seodescription={localStorage.getItem("seoMetaDescription")}
                    ogtype="website"
                    ogtitle={localStorage.getItem("seoOgTitle")}
                    ogdescription={localStorage.getItem("seoOgDescription")}
                    ogurl={window.location.href}
                    twittertitle={localStorage.getItem("seoTwitterTitle")}
                    twitterdescription={localStorage.getItem("seoTwitterDescription")}
                />

                {/* <Nav redirectUrl={"shopper"} logo={true} active_nearme={true} disable_back_button={true} loggedin={delivery_user.success} /> */}

                {/* header */}
                <div style={{width:"100%", padding: "10px 0", float:"left"}}><div style={{maxWidth: "468px", margin: "auto"}}>

                    {/* hamburger icon */}
                    <FontAwesomeIcon onClick={() => { this.showModalMenu(); }} style={{fontSize: "26px", marginTop:"8px", marginLeft:"15px", marginRight:"5px", float:"left", cursor:"pointer"}} className="infobaars" icon={faBars} />

                    <span style={{fontSize:"24px", fontFamily:"open sans", fontWeight:600, marginLeft:"5px", marginTop:"2px", float:"left"}}>Einnahmen</span>
                </div></div>

                {/* earnings */}
                <div style={{float:"left", width:"100%", background:"#F7F7F7"}}><div className="mb-200" style={{maxWidth: "468px", margin: "auto", background:"#F7F7F7"}}>

                    {(supermarketOrdersDelivered.length || delivery_user.data.wallet_balance) ? (
                        <div style={{margin:"auto", textShadow: "0 1px 4px rgba(0,0,0,0.1)"}}>

                            {/* current balance, geld auszahlen btn */}
                            <div style={{padding:"18px"}}>
                                <div style={{marginBottom:"5px", fontFamily:"open sans", fontWeight:300, textAlign:"center", color:"#8D8B8D"}}><b>Verfügbar</b></div>

                                <div style={{marginBottom:"5px", fontFamily:"open sans", fontWeight:300, textAlign:"center", color:"#333", fontSize:"30px"}}><b>
                                    €<CountTo
                                        to={delivery_user.data.wallet_balance}
                                        speed={1000}
                                        easing={t => {
                                            return t < 0.5 ? 16 * t * t * t * t * t : 1 + 16 * --t * t * t * t * t;
                                        }}
                                        digits={2}
                                    />
                                </b></div>

                                {delivery_user.data.wallet_balance > 10 ? (
                                    <div style={{width:"100%", margin: "auto", textAlign:"center"}}><div onClick={() => { this.showModalCashout(); }} style={{fontFamily:"open sans", fontWeight:300, color:"#fff", fontWeight:"bold", background:"#369423", display:"inline-block", marginBottom:"5px", borderRadius: "3px", padding: "5px 10px", cursor:"pointer"}}><FontAwesomeIcon icon={faBolt} /> Geld auszahlen</div></div>
                                ) : (
                                    <div style={{width:"100%", margin: "auto", textAlign:"center"}}><div style={{fontFamily:"open sans", fontWeight:300, color:"#fff", fontWeight:"bold", background:"#E0DFE1", display:"inline-block", marginBottom:"5px", borderRadius: "3px", padding: "5px 10px"}}><FontAwesomeIcon icon={faBolt} /> Geld auszahlen</div></div>
                                )}

                                {/* cashout */}
                                <Modal show={this.state.CashoutModal} onHide={this.hideModalCashout} size="sm">
                                    <Modal.Header closeButton>
                                        {/* <Modal.Title>Geld auszahlen</Modal.Title> */}
                                    </Modal.Header>
                                    <Modal.Body style={{paddingTop:0}}>

                                        <div style={{textAlign:"center", marginBottom:"30px"}}>
                                            <FontAwesomeIcon icon={faBolt} style={{color:"#29B36A", fontSize:"20px"}} /> <br/><br/>
                                            <div style={{fontWeight:400, fontFamily:"open sans", color:"#696969"}}><b>Geld Auszahlung</b></div>
                                            <div style={{fontWeight:300, fontFamily:"open sans", fontSize:"40px"}}><b>€{delivery_user.data.wallet_balance.toFixed(2)}</b></div>
                                        </div>

                                        Verfügbarer Betrag <span style={{float:"right"}}>€{delivery_user.data.wallet_balance.toFixed(2)}</span><br/>
                                        Transaktionsgebühren <span style={{float:"right"}}>- €0,50</span><hr/>
                                        Überweisungsbetrag <span style={{float:"right"}}><b>€{parseFloat(delivery_user.data.wallet_balance-0.50).toFixed(2)}</b></span>

                                        <div style={{marginTop:"30px", fontSize: "10px", lineHeight: 1.5, color:"#999"}}> Überweisungen dauern in der Regel nur wenige Minuten, können jedoch je nach Bank bis zu 24 Stunden dauern.</div>

                                        
                                    </Modal.Body>
                                    <Modal.Footer>
                                        <Button variant="primary" onClick={() => { this.hideModalCashout(); this.showModalPaymentMethods(); }} style={{width:"100%", background:"#0AAD0A"}}> 
                                            Weiter
                                        </Button> 
                                    </Modal.Footer>
                                </Modal>

                                {/* payment methods */}
                                <Modal show={this.state.PaymentMethodsModal} onHide={this.hideModalPaymentMethods} size="sm">
                                    <Modal.Header closeButton>
                                        {/* <Modal.Title>Geld Auszahlung</Modal.Title> */}
                                    </Modal.Header>
                                    <Modal.Body style={{paddingTop:0}}>

                                        <div style={{marginBottom:"30px"}}>
                                            <div style={{fontWeight:300, fontFamily:"open sans", fontSize:"40px", lineHeight:1}}><b>€{parseFloat(delivery_user.data.wallet_balance-0.50).toFixed(2)}</b></div>
                                            <div style={{fontWeight:300, fontFamily:"open sans", fontSize:"10px", textTransform:"uppercase"}}>Angeforderter Betrag</div>
                                        </div>

                                        <Dropdown>
                                            <Dropdown.Toggle variant="secondary" id="dropdown-basic" style={{fontSize:"12px"}}>
                                                Zahlungsmethode auswählen
                                            </Dropdown.Toggle>

                                            <Dropdown.Menu>
                                                <Dropdown.Item onClick={this.handleChangePaymentMethods}>Instant Auszahlung</Dropdown.Item>
                                                {/* <Dropdown.Item onClick={this.handleChangePaymentMethods}>PayPal</Dropdown.Item> */}
                                                <Dropdown.Item onClick={this.handleChangePaymentMethods}>Banküberweisung</Dropdown.Item>
                                            </Dropdown.Menu>
                                        </Dropdown>

                                        <br/>

                                        <div id="PayPal" style={{display:"none"}}>
                                            <input 
                                            style={{border:"1px solid #333", padding:"5px", margin:"5px 0", width:"100%"}} 
                                            placeholder={"PayPal Email"}
                                            onChange={this.onPaypalChanged}
                                            />

                                            <div style={{marginTop:"30px", fontSize: "10px", lineHeight: 1.5, color:"#999"}}>Mit dem Absenden dieses Auszahlungsantrags bestätige ich, dass ich die Widerrufsbelehrung und die Gebühren von Lieferservice123 gelesen und verstanden habe.</div>
                                        </div>

                                        <div id="Banküberweisung" style={{display:"none"}}>
                                            <input 
                                            style={{border:"1px solid #333", padding:"5px", margin:"5px 0", width:"100%"}} 
                                            placeholder={"Empfängername"}
                                            onChange={this.onReceivernameChanged} 
                                            />
                                            <input 
                                            style={{border:"1px solid #333", padding:"5px", margin:"5px 0", width:"100%"}} 
                                            placeholder={"IBAN"}
                                            onChange={this.onIbanChanged} 
                                            />

                                            <div style={{marginTop:"30px", fontSize: "10px", lineHeight: 1.5, color:"#999"}}>Mit dem Absenden dieses Auszahlungsantrags bestätige ich, dass ich die Widerrufsbelehrung, die Gebühren von Lieferservice123, den Servicevertrag und dem <span onClick={() => this.showModalStripeAgreement()} style={{color:"#5469D4"}}>Stripe Connected Account-Vertrag</span> gelesen und verstanden habe.</div>
                                        </div>

                                        <div id="Soldo" style={{display:"none"}}>
                                            <p><b>Was ist Instant Auszahlung?</b> Sie können sofort bezahlt werden, ohne auf Ihre wöchentliche Auszahlung zu warten. </p>
                                            <p><b>Wo kann ich die Auszahlung abheben?</b> Mit Ihrer Soldo Prepaid Karte können Sie den Auszahlungsbetrag bei jedem Bankomaten abheben. </p>
                                            <div style={{marginTop:"30px", fontSize: "10px", lineHeight: 1.5, color:"#999"}}>Mit dem Absenden dieses Auszahlungsantrags bestätige ich, dass ich die Widerrufsbelehrung und die Gebühren von Lieferservice123 gelesen und verstanden habe.</div>
                                        </div>

                                    </Modal.Body>
                                    {(this.state.payment_method && this.state.iban && this.state.receivername 
                                        || this.state.payment_method && this.state.paypal 
                                        || this.state.payment_method == "Instant Auszahlung") && (
                                        <Modal.Footer>
                                            <Button variant="primary" onClick={() => { this.hideModalPaymentMethods(); this.showModalCashoutConfirmed(delivery_user.data.wallet_balance.toFixed(2), this.state.payment_method); }} style={{width:"100%", background:"#0AAD0A"}}> 
                                                Auszahlung Bestätigen
                                            </Button> 
                                        </Modal.Footer>
                                    )}
                                </Modal>

                                {/* stripe agreement */}
                                <Modal show={this.state.StripeAgreementModal} onHide={this.hideModalStripeAgreement}>
                                    <Modal.Header closeButton>
                                        <Modal.Title>Stripe Vereinbarung</Modal.Title>
                                    </Modal.Header>
                                    <Modal.Body style={{paddingTop:0}}>
                                        Zahlungsdienstleistungen für Shopper auf Lieferservice123 werden von Stripe erbracht und unterliegen der Stripe Connected Account Vereinbarung (<a href={'https://stripe.com/connect-account/legal/full'} target='_blank' style={{color:"#5469D4"}}>Stripe Connected Account Agreement</a>), welche die Stripe Nutzungsbedingungen (Stripe Terms of Service) beinhaltet (zusammengefasst unter dem Sammelbegriff “<a href={'https://stripe.com/de/legal'} target='_blank' style={{color:"#5469D4"}}>Stripe Services Agreement</a>”). Durch die Zustimmung zu den vorliegenden [Nutzungsbedingungen, Bedingungen, ...] oder das weitere agieren als Shopper auf Lieferservice123, akzeptieren Sie die Bedingungen der Vereinbarung “Stripe Services Agreement”, welche von Stripe von Zeit zu Zeit angepasst werden darf. Als Voraussetzung, dass Lieferservice123 die Zahlungsdienstleistungen von Stripe in Anspruch nehmen kann, stimmen Sie zu, vollständige und komplette Informationen über sich und ihr Unternehmen für Lieferservice123 bereitzustellen, und sie autorisieren Lieferservice123, diese Informationen und Transaktionsinformationen, die im Zusammenhang mit ihrer Nutzung der von Stripe offerierten Zahlungsdienstleistungen stehen, weiterzugeben.
                                    </Modal.Body>
                                    <Modal.Footer>
                                        <Button variant="primary" onClick={() => this.hideModalStripeAgreement()} style={{width:"100%"}}> 
                                            Schließen
                                        </Button> 
                                    </Modal.Footer>
                                </Modal>

                                {/* cashout confirmed */}
                                <Modal show={this.state.CashoutConfirmedModal} onHide={this.hideModalCashoutConfirmed} size="sm">
                                    <Modal.Header closeButton>
                                        {/* <Modal.Title>Geld Auszahlung</Modal.Title> */}
                                    </Modal.Header>
                                    <Modal.Body style={{paddingTop:0}}>

                                        <div style={{marginBottom:"30px", textAlign:"center"}}>
                                            <FontAwesomeIcon icon={faCheck} style={{color:"#29B36A", fontSize:"40px"}} /> <br/><br/>
                                            <div style={{fontSize:"12px"}}>Vielen Dank, Ihre Auszahlungsanfrage wurde eingereicht. Falls keine weiteren Informationen erforderlich sind, wird Ihre Anfrage innerhalb eines Werktages bearbeitet.</div>
                                        </div>

                                        <div style={{fontWeight:300, fontFamily:"open sans", fontSize:"40px", lineHeight:1}}><b>€{parseFloat(delivery_user.data.wallet_balance-0.50).toFixed(2)}</b></div>
                                        <div style={{fontWeight:300, fontFamily:"open sans", fontSize:"10px", textTransform:"uppercase"}}>Auszahalungsbetrag</div>

                                    </Modal.Body>
                                    <Modal.Footer>
                                        <Button variant="primary" onClick={() => this.hideModalCashoutConfirmed()} style={{width:"100%"}}> 
                                            Schließen
                                        </Button> 
                                    </Modal.Footer>
                                </Modal>
                            </div>

                            {/* summary */}
                            <div style={{padding:"15px 18px", fontFamily:"open sans", fontWeight:600, fontSize:"12px", color:"#8D8B8D", textTransform:"uppercase"}}>Übersicht</div>
                            <div style={{background:"#fff"}}>
                                <div style={{padding:"15px 18px", borderBottom:"1px solid #eee"}}>Aufträge angenommen: <span style={{fontWeight:"bold", float:"right"}}>{supermarketOrdersDelivered.length}</span></div>

                                <div style={{padding:"15px 18px", borderBottom:"1px solid #eee"}}>Trinkgelder: <span style={{fontWeight:"bold", float:"right"}}>€{this.makeTotalTips(supermarketOrdersDelivered)}</span></div>

                                <div style={{padding:"15px 18px", borderBottom:"1px solid #eee"}}>Liefereinnahmen: <span style={{fontWeight:"bold", float:"right"}}>€{this.makeTotalDeliveryEarnings(supermarketOrdersDelivered)}</span></div>
                            </div>

                            {/* transaction history */}
                            {transactionHistory.length ? 
                                <div><div style={{padding:"15px 18px", fontFamily:"open sans", fontWeight:600, fontSize:"12px", color:"#8D8B8D", textTransform:"uppercase"}}>Letzte Auszahlungen</div>
                                {transactionHistory.map((row, index) => {
                                    return (<div style={{padding:"15px 18px", borderBottom:"1px solid #eee", background:"#fff", overflow:"auto"}}>
                                        <div key={row.id}>

                                            <div style={{float:"left", width:"50%"}}>
                                                {this.showDate(row.added)}
                                                <div style={{fontSize:"12px", fontWeight:300, fontFamily:"open sans"}}>{row.payment_method}</div>
                                            </div>

                                            <div style={{float:"right", width:"50%", fontWeight:"bold", textAlign:"right"}}>{localStorage.getItem("currencyFormat")} {parseFloat(row.amount).toFixed(2)}</div>

                                        </div>
                                    </div>)
                                })}
                            </div> : null}

                            {/* daily earnings */}
                            <div style={{padding:"15px 18px", fontFamily:"open sans", fontWeight:600, fontSize:"12px", color:"#8D8B8D", textTransform:"uppercase"}}>Tägliche Einnahmen</div>
                            {delivery_user.data.earnings && delivery_user.data.earnings.map((row, index) => {
                                // console.log("1. row.subtotal", row.subtotal);
                                // console.log("2. this.newTotal(row.items)", this.newTotal(row.items));

                                return (

                                    <React.Fragment>

                                        <div key={row.id} style={{cursor:"pointer", position:"relative", padding:"15px 18px", borderBottom:"1px solid #eee", background:"#fff", overflow:"auto"}} onClick={() => this.showModalInvoiceSupermarket(row.id)}>

                                            <div style={{float:"left", width:"50%"}} title={row.created_at}>
                                                {/* {this.showDate(row.created_at)} */}
                                                <Moment fromNow>{row.created_at}</Moment>
                                                <div style={{fontSize:"12px", fontWeight:300, fontFamily:"open sans"}}>{row.meta["description"]}</div>
                                            </div>

                                            <div style={{float:"right", width:"50%", fontWeight:"bold", textAlign:"right"}}>
                                                {localStorage.getItem("currencyFormat")} {(row.amount / 100).toFixed(2)}     
                                            </div>

                                            <Ink duration="300" />
                                        </div>

                                        {/* batch summary */}
                                        <Modal show={this.state.invoiceSupermarketModal[row.id]} onHide={this.hideModalInvoiceSupermarket}>
                                            
                                            <Modal.Header closeButton>
                                                {/* <Modal.Title style={{textAlign:"center"}}>Auftrags-Übersicht</Modal.Title> */}
                                            </Modal.Header>

                                            <Modal.Body style={{padding:"0 25px 25px"}}>
                                                {row.meta["description"].indexOf("Payment for order: ") >= 0 ? (
                                                    <ViewOrder id={row.meta["description"].replace("Payment for order: ", "")} />
                                                ) : (
                                                    <React.Fragment>

                                                        <div style={{margin:"5px 0"}}><b>Beschreibung:</b> {row.meta["description"]}</div>
                                                        <div style={{margin:"5px 0"}}><b>Betrag:</b> {localStorage.getItem("currencyFormat")} {(row.amount / 100).toFixed(2)}</div>
                                                        <div style={{margin:"5px 0"}}><b>Datum:</b> {row.created_at}</div>

                                                        <div style={{margin:"5px 0"}}>
                                                            <b>Typ:</b> {row.type === "deposit" && (
                                                                localStorage.getItem("walletDepositText")
                                                            )}
                                                            {row.type === "withdraw" && (
                                                                localStorage.getItem("walletWithdrawText")
                                                            )}
                                                        </div>
                                                        
                                                    </React.Fragment>
                                                )}
                                                
                                            </Modal.Body>
                                        </Modal>

                                    </React.Fragment>

                                )
                            })}
                            
                        </div>
                    ) : this.state.no_ordersDelivered ? (
                        <h2 style={{padding:"50px 15px", textAlign:"center", fontFamily:"open sans", fontWeight:300, float:"left", width:"100%"}}>Sie haben noch keine Einnahmen generiert.</h2>
                    ) : (
                        <h2 style={{padding:"50px 15px", textAlign:"center", fontFamily:"open sans", fontWeight:300, float:"left", width:"100%"}}>Laden...</h2>
                    )}

                </div></div>

                {/* shopper menu */}
                <Modal show={this.state.MenuModal} onHide={this.hideModalMenu} size="sm">
                    <Modal.Header closeButton style={{background:"#fff"}}>
                        <Modal.Title style={{lineHeight:"1", color:"#000"}}>
                            <div style={{fontSize:"30px", fontFamily:"open sans", fontWeight:300}}>Lieferservice123</div>
                            <div style={{fontSize:"12px", fontWeight:600, fontFamily:"open sans"}}>shopper app v{localStorage.getItem("version")}</div>
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body style={{padding:"20px 25px 10px", background:"rgb(13, 54, 64)", color:"white", textTransform:"uppercase", fontWeight:"600", fontFamily:"open sans"}}>

                        <ShopperMenu />
                    
                    </Modal.Body>
                </Modal>

            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    language: state.languages.language,
    settings: state.settings.settings,
    delivery_user: state.delivery_user.delivery_user,

});

export default connect(
    mapStateToProps,
    { getSettings, getSingleLanguageData, updateDeliveryUserInfo, walletTransaction }
)(Earnings);