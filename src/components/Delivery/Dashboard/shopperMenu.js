import * as firebase from "firebase/app";
import messaging from "../../../init-fcm";
import { saveNotificationToken } from "../../../services/notification/actions";

import React, { Component } from "react";
import axios from "axios";

import Ink from "react-ink";
import Meta from "../../helpers/meta";
import { NavLink } from "react-router-dom";
import Nav from "../../Takeaway/Nav";
import { connect } from "react-redux";
import { getSettings } from "../../../services/settings/actions";
import { getSingleLanguageData } from "../../../services/translations/actions";

// fontawesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';
import { faShoppingBag } from '@fortawesome/free-solid-svg-icons';
import { faCar } from '@fortawesome/free-solid-svg-icons';
import { faCoins } from '@fortawesome/free-solid-svg-icons';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { faCreditCard } from '@fortawesome/free-solid-svg-icons';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import { faIdCard } from '@fortawesome/free-solid-svg-icons';
import { faMobile } from '@fortawesome/free-solid-svg-icons';
import { faListAlt } from '@fortawesome/free-solid-svg-icons';
import { faUpload } from '@fortawesome/free-solid-svg-icons';
import { faBolt } from '@fortawesome/free-solid-svg-icons';
import { faHome } from '@fortawesome/free-solid-svg-icons';
import { faHeartbeat } from '@fortawesome/free-solid-svg-icons';
import { faQuestionCircle } from '@fortawesome/free-solid-svg-icons';
import { faKey } from '@fortawesome/free-solid-svg-icons';
import { faStore } from '@fortawesome/free-solid-svg-icons';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

//bootstrap
import ReactDOM from 'react-dom';
import { Modal, Button, Dropdown } from 'react-bootstrap';
import ReactTooltip from 'react-tooltip';

//browser check
import PWAPrompt from 'react-ios-pwa-prompt'
import { isChrome, isChromium, isMobileSafari, isMobile, fullBrowserVersion, osName } from 'react-device-detect';

import Location from "../../Takeaway/Location/";

import { updateDeliveryUserInfo, logoutDeliveryUser } from "../../../services/Delivery/user/actions";

declare var INSTALL_APP_POPUP;


class ShopperMenu extends Component {

    constructor() {
        super();
        
        this.state = { 
            cards: [],
        };
    }

    static contextTypes = {
        router: () => null
    };

    componentDidMount() {
        const { settings } = this.props;
        
        //if one config is missing then call the api to fetch settings
        //if (!localStorage.getItem("storeColor")) {
            this.props.getSettings();
        //}

        this.props.getSingleLanguageData(localStorage.getItem("userPreferedLanguage")?localStorage.getItem("userPreferedLanguage"):1);

        const { delivery_user } = this.props;
        //update delivery guy info
        if(delivery_user.success){
            this.props.updateDeliveryUserInfo(delivery_user.data.id, delivery_user.data.auth_token);
        }
    }

    // shopper menu
    showModalMenu () {
        this.setState({
            MenuModal: true
        });
    }
    hideModalMenu = () => {
        this.setState({
            MenuModal: false
        });
    }

    //setup location
    showModalAddress () {
        this.setState({
            AddressModal: true
        });
    }
    hideModalAddress = () => {
        this.setState({
            AddressModal: false
        });
    }

    //lang
    showModalLang () {
        this.setState({
            LangModal: true
        });
    }
    hideModalLang = () => {
        this.setState({
            LangModal: false
        });
    }

    //change country / lang
    changeCountry = (country) => {
        if(country !== localStorage.getItem("country")){
            localStorage.removeItem("userSetAddress");
        }

        localStorage.setItem("country", country);
        if(country == "AT"){
            localStorage.setItem("phoneCountryCode", "43");
            localStorage.setItem("shop", "Spar");
        } 
        else if(country == "DE"){
            localStorage.setItem("phoneCountryCode", "49");
            localStorage.setItem("shop", "REWE");
        } 
        else if(country == "CH"){
            localStorage.setItem("phoneCountryCode", "41");
        } 
        var url = window.location.href;
        window.location = url;
    }
    changeLang = (lang) => {
        // console.log("lang", lang);
        this.props.getSingleLanguageData(lang);
        localStorage.setItem("userPreferedLanguage", lang);
        var url = window.location.href;
        window.location = url;
    }

    //install app
    showModalInstall () {
        this.setState({
            installModal: true
        });
    }
    hideModalInstall = () => {
        this.setState({
            installModal: false
        });
    }
    showModalBerechtigungen () {
        this.setState({
            berechtigungenModal: true
        });
    }
    hideModalBerechtigungen = () => {
        this.setState({
            berechtigungenModal: false
        });
    }
    triggerInstallBtn = () => {
        var div = document.getElementById('download');
        div.innerHTML = 'Laden...';
        
        setTimeout(function () {

            if (INSTALL_APP_POPUP) {//INSTALL_APP_POPUP is declared globally
                console.log("INSTALL_APP_POPUP", INSTALL_APP_POPUP); 
                INSTALL_APP_POPUP.prompt(); //this will show the prompt from chrome
            } 

            div.innerHTML = 'Hinzufügen';
        }, 1500);
    }
    showPrompt = () => {
        console.log("button clicked");
        document.getElementById('showPwaprompt').style.display = "block";
    } 
    next = (install, pwaSupported) =>{
        if(pwaSupported == false){
            alert("Deine Browser Version wird nicht unterstützt. Bitte installiere Chrome 70 oder höher.");
        } else if(install == "voice"){
            this.hideModalBerechtigungen2();
            this.showModalInstall2();
        } else {
            this.hideModalBerechtigungen();
            this.showModalInstall();
        }
    }
    fullBrowserVersion = () =>{
        return fullBrowserVersion.replace( /^([^.]*\.)(.*)$/, function ( a, b, c ) { 
            return b + c.replace( /\./g, '' );
        });
    }

    render() {
        const { user, delivery_user, logoutDeliveryUser } = this.props;
        const { cards } = this.state;

        if (!delivery_user.success) {
            return (
                //redirect to account page
                window.location = "/delivery/login"
            );
        }

        var langStyle = {
            marginBottom:"15px", cursor:"pointer", color:"#666"
        };

        var langStyleBold = {
            marginBottom:"15px", cursor:"pointer", fontWeight:"bold", color:"#666"
        };

        //check if chrome version is supported
        if(isChrome || isChromium){
            if(osName == "Windows" && this.fullBrowserVersion() >= 70){
                var pwaSupported = true;
            } else if(osName == "Android" && this.fullBrowserVersion() >= 31){
                var pwaSupported = true;
            } else if(osName == "Ubuntu" && this.fullBrowserVersion() >= 72){
                var pwaSupported = true;
            } else {
                var pwaSupported = false;
            }
        } else {
            var pwaSupported = false;
        }

        if(matchMedia('(display-mode: standalone)').matches == true) {
            var install = false
        } else if(isMobileSafari){
            // var install = <span onClick={() => { this.showPrompt(); }} style={{borderRadius:"0.2rem", cursor:"pointer", position:"relative", boxShadow:"rgb(255, 106, 0) 0px 1px 2px 0px", background:"linear-gradient(90deg, rgb(253, 141, 66), rgb(255, 106, 0))", color:"#fff", padding:"12px 20px 10px", fontWeight:600}}>{localStorage.getItem("firstScreenInstall")}<Ink duration="500" /></span>
            var install = false;
        } else {
            var install = true;
        }

        return (
            <React.Fragment>

                {/* language */}
                <Modal show={this.state.LangModal} onHide={this.hideModalLang} style={{background:"rgba(51,51,51, 0.85)"}}>
                    <Modal.Header closeButton>
                        {/* <Modal.Title>Einstellungen</Modal.Title> */}
                    </Modal.Header>
                    <Modal.Body style={{padding:"0 25px 10px"}}>

                        <div style={{float:"left", width:"188px", borderRight:"1px solid #f8f5f2", margin:"0 12px"}}>
                            <div style={{fontSize:"1.8rem", marginBottom:"15px", fontWeight:"bold"}}>Land</div>
                            <div style={localStorage.getItem("country")=="DE"?langStyleBold:langStyle}><a onClick={() => { this.changeCountry("DE"); }}>Deutschland</a></div>
                            <div style={localStorage.getItem("country")=="AT"?langStyleBold:langStyle}><a onClick={() => { this.changeCountry("AT"); }}>Österreich</a></div>
                            <div style={localStorage.getItem("country")=="CH"?langStyleBold:langStyle}><a onClick={() => { this.changeCountry("CH"); }}>Schweiz</a></div>
                        </div>

                        <div style={{float:"left", width:"188px", margin:"0 12px"}}>
                            <div style={{fontSize:"1.8rem", marginBottom:"15px", fontWeight:"bold"}}>{localStorage.getItem("footerLanguage")}</div>

                            <div style={localStorage.getItem("userPreferedLanguage")==1?langStyleBold:langStyle}><div className="flag-container"><span className="locale-flag locale-flag-de"></span></div> <a onClick={() => { this.changeLang("1"); }} style={{marginLeft:"10px"}}>Deutsch</a></div>

                            <div style={localStorage.getItem("userPreferedLanguage")==2?langStyleBold:langStyle}><div className="flag-container"><span className="locale-flag locale-flag-en"></span></div> <a onClick={() => { this.changeLang("2"); }} style={{marginLeft:"10px"}}>English</a></div>
                        </div>

                    </Modal.Body>
                </Modal>

                {/* Standort festlegen */}
                <Modal show={this.state.AddressModal} onHide={this.hideModalAddress} style={{background:"rgba(51,51,51, 0.85)"}}>
                    <Modal.Header closeButton><Modal.Title>Standort festlegen</Modal.Title></Modal.Header>
                    <Modal.Body style={{padding:"0 25px 10px"}}>
                        <div style={{fontSize: "12px", lineHeight: 1.5, marginBottom:"20px", color:"#555"}}> Lieferoptionen und -geschwindigkeit können, abhängig von der Lieferadresse, variieren</div>
                        <Location redirect={window.location.pathname.replace("?install=1", "")} />
                    </Modal.Body>
                </Modal>

                {/* app-vorteile */}
                <Modal show={this.state.berechtigungenModal} onHide={this.hideModalBerechtigungen} style={{background:"rgba(51,51,51, 0.85)"}}>
                    
                    <Modal.Header closeButton>
                        <Modal.Title>App-Vorteile</Modal.Title>
                    </Modal.Header>

                    <Modal.Body style={{padding:"0 25px 10px"}}>
                        <div style={{marginTop:"12px"}}>
                            <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Shopper App auf Desktop installieren.</div>

                            {/* <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Ohne Internetverbindung bei Lieferservice123 bestellen.</div> */}

                            {/* <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Bis zu 32% Mengenrabatt.</div> */}

                            <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Lieferung auf der Karte verfolgen.</div>

                            <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Benutzerelebnis durch Schnelles Laden.</div>

                            <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Bessere Kommunikation zwischen Shopper und Kunde durch Push-Nachrichten.</div>

                            <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Zuverlässig auch bei schlechter Internetverbindung.</div>

                            <div style={{marginBottom:"5px"}}><FontAwesomeIcon icon={faCheck} style={{color:"green", marginRight:"5px"}} />Installation über Google Chrome.</div>
                        </div>

                    </Modal.Body>

                    <Modal.Footer>
                        <Button variant="secondary" onClick={this.hideModalBerechtigungen}> 
                            Schließen
                        </Button> 
                        <Button variant="primary" onClick={() => this.next("", pwaSupported)}>
                            Weiter
                        </Button>
                    </Modal.Footer>
                </Modal>

                {/* app-installieren */}
                <Modal show={this.state.installModal} onHide={this.hideModalInstall} style={{background:"rgba(51,51,51, 0.85)"}}>
                    
                    <Modal.Header closeButton>
                        <Modal.Title>Installieren</Modal.Title>
                    </Modal.Header>

                    <Modal.Body style={{padding:"0 25px 10px"}}>
                        <div style={{marginTop:"12px"}}>
                            Klicke auf hinzufügen um die Shopper App auf deinen Bildschirm zu installieren.
                        </div>

                        <div style={{marginTop:"20px", fontSize:"11px", color:"#999"}}>Wenn dir Lieferservice123 gefällt, dann installiere bitte die  App.</div>
                    </Modal.Body>

                    <Modal.Footer>
                        <Button variant="secondary" onClick={this.hideModalInstall}> 
                            Schließen
                        </Button> 
                        <Button variant="primary" onClick={() => this.triggerInstallBtn()} id="download">
                            Hinzufügen
                        </Button>
                    </Modal.Footer>
                </Modal>

                <div className="category-list-item" onClick={() => { window.location = '/delivery/dashboard' }} style={{cursor:"pointer", position:"relative"}}>
                    <div className="display-flex py-2">
                        <div className="mr-10 border-0">
                            <i className="si si-users" />
                        </div>
                        <div className="flex-auto border-0">Dashboard</div>
                        <div className="flex-auto text-right">
                            <i className="si si-arrow-right" />
                        </div>
                    </div>
                    <Ink duration="500" />
                </div>

                <div className="category-list-item" onClick={() => { window.location = '/delivery/batches' }} style={{cursor:"pointer", position:"relative"}}>
                    <div className="display-flex py-2">
                        <div className="mr-10 border-0">
                            <i className="si si-basket-loaded" />
                        </div>
                        <div className="flex-auto border-0">Aufträge</div>
                        <div className="flex-auto text-right">
                            <i className="si si-arrow-right" />
                        </div>
                    </div>
                    <Ink duration="500" />
                </div>

                {/* <div className="category-list-item" onClick={() => { window.location = '/shopperPwa?act=hours' }} style={{cursor:"pointer", position:"relative"}}>
                    <div className="display-flex py-2">
                        <div className="mr-10 border-0">
                            <i className="si si-calendar" />
                        </div>
                        <div className="flex-auto border-0">Stunden</div>
                        <div className="flex-auto text-right">
                            <i className="si si-arrow-right" />
                        </div>
                    </div>
                    <Ink duration="500" />
                </div> */}

                <div className="category-list-item" onClick={() =>  { window.location = '/delivery/earnings' }} style={{cursor:"pointer", position:"relative"}}>
                    <div className="display-flex py-2">
                        <div className="mr-10 border-0">
                            <i className="si si-wallet" />
                        </div>
                        <div className="flex-auto border-0">Einnahmen</div>
                        <div className="flex-auto text-right">
                            <i className="si si-arrow-right" />
                        </div>
                    </div>
                    <Ink duration="500" />
                </div>

                <div className="category-list-item" onClick={() =>  { window.location = '/delivery/payment-card' }} style={{cursor:"pointer", position:"relative"}}>
                    <div className="display-flex py-2">
                        <div className="mr-10 border-0">
                            <i className="si si-credit-card" />
                        </div>
                        <div className="flex-auto border-0">Meine Zahlungskarte</div>
                        <div className="flex-auto text-right">
                            <i className="si si-arrow-right" />
                        </div>
                    </div>
                    <Ink duration="500" />
                </div>

                <hr/>

                {/* <div className="category-list-item" onClick={() =>  { window.location = '/shopperPwa' }} style={{cursor:"pointer", position:"relative"}}>
                    <div className="display-flex py-2">
                        <div className="mr-10 border-0">
                            <i className="si si-user" />
                        </div>
                        <div className="flex-auto border-0">Profil</div>
                        <div className="flex-auto text-right">
                            <i className="si si-arrow-right" />
                        </div>
                    </div>
                    <Ink duration="500" />
                </div>

                <div className="category-list-item" onClick={() =>  { window.location = '/shopperPwa' }} style={{cursor:"pointer", position:"relative"}}>
                    <div className="display-flex py-2">
                        <div className="mr-10 border-0">
                            <i className="si si-settings" />
                        </div>
                        <div className="flex-auto border-0">Einstellungen</div>
                        <div className="flex-auto text-right">
                            <i className="si si-arrow-right" />
                        </div>
                    </div>
                    <Ink duration="500" />
                </div>

                <hr/> */}

                {install && (
                    <div className="category-list-item" onClick={() => this.showModalBerechtigungen()} style={{cursor:"pointer", position:"relative"}}>
                        <div className="display-flex py-2">
                            <div className="mr-10 border-0">
                                <i className="si si-cloud-download" />
                            </div>
                            <div className="flex-auto border-0">Shopper App installieren</div>
                            <div className="flex-auto text-right">
                                <i className="si si-arrow-right" />
                            </div>
                        </div>
                        <Ink duration="500" />
                    </div>
                )}

                <div className="category-list-item" onClick={() => this.showModalAddress()} style={{cursor:"pointer", position:"relative"}}>
                {/* <div className="category-list-item" onClick={() => { this.context.router.history.push("/search-location"+backToRes) }} style={{cursor:"pointer", position:"relative"}}> */}
                    <div className="display-flex py-2">
                        <div className="mr-10 border-0">
                            <i className="si si-location-pin" />
                        </div>
                        <div className="flex-auto border-0">{localStorage.getItem("firstScreenSetupLocation")}</div>
                        <div className="flex-auto text-right">
                            <i className="si si-arrow-right" />
                        </div>
                    </div>
                    <Ink duration="500" />
                </div>

                <div className="category-list-item" onClick={() => { this.hideModalMenu(); this.showModalLang(); }} style={{cursor:"pointer", position:"relative"}}>
                    <div className="display-flex py-2">
                        <div className="mr-10 border-0">
                            <i className="si si-settings" />
                        </div>
                        <div className="flex-auto border-0">Land/Sprache ändern</div>
                        <div className="flex-auto text-right">
                            <i className="si si-arrow-right" />
                        </div>
                    </div>
                    <Ink duration="500" />
                </div>

                <hr/>

                <div className="category-list-item" onClick={() => logoutDeliveryUser(delivery_user)} style={{cursor:"pointer", position:"relative"}}>
                    <div className="display-flex py-2">
                        <div className="mr-10 border-0">
                            <i className="si si-power logout-icon" />
                        </div>
                        <div className="flex-auto border-0">Logout</div>
                        <div className="flex-auto text-right">
                            <i className="si si-arrow-right" />
                        </div>
                    </div>
                    <Ink duration="500" />
                </div>

            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    language: state.languages.language,
    settings: state.settings.settings,
    delivery_user: state.delivery_user.delivery_user,

});

export default connect(
    mapStateToProps,
    { getSettings, saveNotificationToken, getSingleLanguageData, updateDeliveryUserInfo, logoutDeliveryUser }
)(ShopperMenu);