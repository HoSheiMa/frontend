import { BrowserRouter, Route, Switch } from "react-router-dom";

import App from "./components/App";
import Loadable from "react-loadable";
import Loading from "./components/helpers/loading";
import React from "react";
import ReactDOM from "react-dom";
import Root from "./Root";
import withTracker from "./withTracker";
import OrderList from "./components/Takeaway/Items/OrderList";
import About from "./components/Takeaway/Items/About";
import OrderListView from "./components/Takeaway/Items/OrderListView";
import AppInstall from "./components/Takeaway/Items/AppInstall";
import RunOrd from "./components/Takeaway/RunningOrder/RunOrd";
import Shopper from "./components/Delivery/Register";
import ShopperPwa from "./components/Delivery/Dashboard";
import ViewBatch from "./components/Delivery/ViewBatch";
import ViewBatch2 from "./components/Delivery/ViewBatch/deliveryByStore.js";


// import Shops from "./components/Takeaway/Shops";

// import { Redirect } from "react-router";

// const App = Loadable({
//     loader: () => import("./components/App"),
//     loading: () => <Loading />
// });

const Home = Loadable({
    loader: () => import("./components/Home/FirstScreen"),
    loading: () => <Loading />
});

const HomeDownload = Loadable({
    loader: () => import("./components/Home/Download"),
    loading: () => <Loading />
});

// login
const HomeLogin = Loadable({
    loader: () => import("./components/Home/Account/Login"),
    loading: () => <Loading />
});
const HomePhoneVerify = Loadable({
    loader: () => import("./components/Home/Account/Login/phoneVerify.js"),
    loading: () => <Loading />
});
const HomePhoneLogin = Loadable({
    loader: () => import("./components/Home/Account/Login/phoneLogin.js"),
    loading: () => <Loading />
});

// friends
const HomeFriends = Loadable({
    loader: () => import("./components/Home/Friends"),
    loading: () => <Loading />
});

// acount
const HomeAccount = Loadable({
    loader: () => import("./components/Home/Account"),
    loading: () => <Loading />
});
const HomeAdvantages = Loadable({
    loader: () => import("./components/Home/Account/advantages.js"),
    loading: () => <Loading />
});
const HomeIdentityVerification = Loadable({
    loader: () => import("./components/Home/Account/identityVerification.js"),
    loading: () => <Loading />
});
const HomeIdentityVerificationForm = Loadable({
    loader: () => import("./components/Home/Account/identityVerificationForm.js"),
    loading: () => <Loading />
});
const HomeAddBankAcc = Loadable({
    loader: () => import("./components/Home/Account/addBankAcc.js"),
    loading: () => <Loading />
});

// scan
const HomeScan = Loadable({
    loader: () => import("./components/Home/Scan"),
    loading: () => <Loading />
});
const HomeScanVendor = Loadable({
    loader: () => import("./components/Home/Scan/vendor.js"),
    loading: () => <Loading />
});
const HomeScanIndividual = Loadable({
    loader: () => import("./components/Home/Scan/individual.js"),
    loading: () => <Loading />
});

// pay
const HomePay = Loadable({
    loader: () => import("./components/Home/Pay"),
    loading: () => <Loading />
});
const HomePayActivate = Loadable({
    loader: () => import("./components/Home/Pay/activate.js"),
    loading: () => <Loading />
});

// collect
const HomeRequest = Loadable({
    loader: () => import("./components/Home/Request"),
    loading: () => <Loading />
});

// pocket, wallet
const HomeWallet = Loadable({
    loader: () => import("./components/Home/Wallet"),
    loading: () => <Loading />
});
const HomeWalletDeposit = Loadable({
    loader: () => import("./components/Home/Wallet/deposit.js"),
    loading: () => <Loading />
});

// settings
const HomeSettings = Loadable({
    loader: () => import("./components/Home/Account/Settings"),
    loading: () => <Loading />
});
const HomeSettingsSecurity = Loadable({
    loader: () => import("./components/Home/Account/Settings/security.js"),
    loading: () => <Loading />
});
const HomeSettingsPayment = Loadable({
    loader: () => import("./components/Home/Account/Settings/payment.js"),
    loading: () => <Loading />
});
const HomeSettingsNotifications = Loadable({
    loader: () => import("./components/Home/Account/Settings/notifications.js"),
    loading: () => <Loading />
});
const HomeSettingsFunctionalManagement = Loadable({
    loader: () => import("./components/Home/Account/Settings/functionalManagement.js"),
    loading: () => <Loading />
});

// profile
const HomeProfile = Loadable({
    loader: () => import("./components/Home/Account/Profile"),
    loading: () => <Loading />
});
const HomeProfileHomepage = Loadable({
    loader: () => import("./components/Home/Account/Profile/Homepage"),
    loading: () => <Loading />
});
const HomeProfilePhoneNumber = Loadable({
    loader: () => import("./components/Home/Account/Profile/PhoneNumber"),
    loading: () => <Loading />
});
const HomeProfileQrcode = Loadable({
    loader: () => import("./components/Home/Account/Profile/QrCode"),
    loading: () => <Loading />
});
const HomeProfileAddress = Loadable({
    loader: () => import("./components/Home/Account/Profile/Address"),
    loading: () => <Loading />
});

// mini apps
const MiniApps = Loadable({
    loader: () => import("./components/MiniApps/FirstScreen"),
    loading: () => <Loading />
});

// transfer app
const Transfer = Loadable({
    loader: () => import("./components/Transfer/FirstScreen"),
    loading: () => <Loading />
});

const Restaurants = Loadable({
    loader: () => import("./components/Takeaway/FirstScreen"),
    loading: () => <Loading />
});

const Shops = Loadable({
    loader: () => import("./components/Takeaway/Shops"),
    loading: () => <Loading />
});

// import NotFound from "./components/NotFound";
const NotFound = Loadable({
    loader: () => import("./components/NotFound"),
    loading: () => <Loading />
});

// import Location from "./components/Takeaway/Location";
const Location = Loadable({
    loader: () => import("./components/Takeaway/Location"),
    loading: () => <Loading />
});

// import Items from "./components/Takeaway/Items";
const Items = Loadable({
    loader: () => import("./components/Takeaway/Items"),
    loading: () => <Loading />
});

// import Login from "./components/Takeaway/Auth/Login";
const Login = Loadable({
    loader: () => import("./components/Takeaway/Auth/Login"),
    loading: () => <Loading />
});

// import Register from "./components/Takeaway/Auth/Register";
const Register = Loadable({
    loader: () => import("./components/Takeaway/Auth/Register"),
    loading: () => <Loading />
});

// import CartPage from "./components/Takeaway/Cart";
const CartPage = Loadable({
    loader: () => import("./components/Takeaway/Cart"),
    loading: () => <Loading />
});

// import Account from "./components/Takeaway/Account";
const Account = Loadable({
    loader: () => import("./components/Takeaway/Account"),
    loading: () => <Loading />
});

// import Explore from "./components/Takeaway/Explore";
const Explore = Loadable({
    loader: () => import("./components/Takeaway/Explore"),
    loading: () => <Loading />
});

// import Addresses from "./components/Takeaway/Account/Addresses";
const Addresses = Loadable({
    loader: () => import("./components/Takeaway/Account/Addresses"),
    loading: () => <Loading />
});

// import Checkout from "./components/Takeaway/Checkout";
const Checkout = Loadable({
    loader: () => import("./components/Takeaway/Checkout"),
    loading: () => <Loading />
});

// import RunningOrder from "./components/Takeaway/RunningOrder";
const RunningOrder = Loadable({
    loader: () => import("./components/Takeaway/RunningOrder"),
    loading: () => <Loading />
});

// import Orders from "./components/Takeaway/Account/Orders";
const Orders = Loadable({
    loader: () => import("./components/Takeaway/Account/Orders"),
    loading: () => <Loading />
});

// import WalletPage from "./components/Takeaway/Account/Wallet";
const WalletPage = Loadable({
    loader: () => import("./components/Takeaway/Account/Wallet"),
    loading: () => <Loading />
});

/* Delivery */
// import Delivery from "./components/Delivery";
const Delivery = Loadable({
    loader: () => import("./components/Delivery"),
    loading: () => <Loading />
});

// import DeliveryLogin from "./components/Delivery/Login";
const DeliveryLogin = Loadable({
    loader: () => import("./components/Delivery/Login"),
    loading: () => <Loading />
});

// import DeliveryOrders from "./components/Delivery/Orders";
const DeliveryOrders = Loadable({
    loader: () => import("./components/Delivery/Orders"),
    loading: () => <Loading />
});

const DeliveryDashboard = Loadable({
    loader: () => import("./components/Delivery/Dashboard"),
    loading: () => <Loading />
});

const DeliveryEarnings = Loadable({
    loader: () => import("./components/Delivery/Dashboard/earnings.js"),
    loading: () => <Loading />
});

const DeliveryBatches = Loadable({
    loader: () => import("./components/Delivery/Dashboard/batches.js"),
    loading: () => <Loading />
});

const DeliveryPaymentCard = Loadable({
    loader: () => import("./components/Delivery/Dashboard/paymentCard.js"),
    loading: () => <Loading />
});

// import ViewOrder from "./components/Delivery/ViewOrder";
const ViewOrder = Loadable({
    loader: () => import("./components/Delivery/ViewOrder"),
    loading: () => <Loading />
});

// import GeoLocationPage from "./components/Takeaway/GeoLocationPage";
const GeoLocationPage = Loadable({
    loader: () => import("./components/Takeaway/GeoLocationPage"),
    loading: () => <Loading />
});

// import SingleItem from "./components/Takeaway/Items/SingleItem";
const SingleItem = Loadable({
    loader: () => import("./components/Takeaway/Items/SingleItem"),
    loading: () => <Loading />
});

const SinglePage = Loadable({
    loader: () => import("./components/SinglePage"),
    loading: () => <Loading />
});

const ScrollToTop = () => {
    window.scrollTo(0, 0);
    return null;
};

/* Store */
const Store = Loadable({
    loader: () => import("./components/Store/Register"),
    loading: () => <Loading />
});

ReactDOM.render(
    <Root>
        <BrowserRouter>
            <React.Fragment>
                <Route component={ScrollToTop} />
                <Switch>
                    {/* <Route exact strict path="/:url*" render={props => <Redirect to={`${props.location.pathname}/`} />} /> */}
                    <Route path={"/"} exact component={withTracker(App)} />
                    <Route path={"/home/download"} exact component={withTracker(HomeDownload)} />

                    {/* scan */}
                    <Route path={"/home/scan"} exact component={withTracker(HomeScan)} />
                    <Route path={"/home/scan/vendor"} exact component={withTracker(HomeScanVendor)} />
                    <Route path={"/home/scan/individual"} exact component={withTracker(HomeScanIndividual)} />
                    
                    {/* pay */}
                    <Route path={"/home/pay"} exact component={withTracker(HomePay)} />
                    <Route path={"/home/pay/activate"} exact component={withTracker(HomePayActivate)} />
                    
                    {/* collect */}
                    <Route path={"/home/request"} exact component={withTracker(HomeRequest)} />
                    
                    {/* wallet */}
                    <Route path={"/home/wallet"} exact component={withTracker(HomeWallet)} />
                    <Route path={"/home/wallet/deposit"} exact component={withTracker(HomeWalletDeposit)} />
                    
                    {/* friends, message */}
                    <Route path={"/home/friends"} exact component={withTracker(HomeFriends)} />

                    {/* profile */}
                    <Route path={"/home/account/profile"} exact component={withTracker(HomeProfile)} />
                    <Route path={"/home/account/profile/homepage"} exact component={withTracker(HomeProfileHomepage)} />
                    <Route path={"/home/account/profile/phoneNumber"} exact component={withTracker(HomeProfilePhoneNumber)} />
                    <Route path={"/home/account/profile/qrCode"} exact component={withTracker(HomeProfileQrcode)} />
                    <Route path={"/home/account/profile/address"} exact component={withTracker(HomeProfileAddress)} />

                    {/* account */}
                    <Route path={"/home/account"} exact component={withTracker(HomeAccount)} />
                    <Route path={"/home/account/identityVerification"} exact component={withTracker(HomeIdentityVerification)} />
                    <Route path={"/home/account/identityVerificationForm"} exact component={withTracker(HomeIdentityVerificationForm)} />
                    <Route path={"/home/account/advantages"} exact component={withTracker(HomeAdvantages)} />
                    <Route path={"/home/account/addBankAcc"} exact component={withTracker(HomeAddBankAcc)} />
                    <Route path={"/home/account/settings"} exact component={withTracker(HomeSettings)} />
                    <Route path={"/home/account/settings/security"} exact component={withTracker(HomeSettingsSecurity)} />
                    <Route path={"/home/account/settings/payment"} exact component={withTracker(HomeSettingsPayment)} />
                    <Route path={"/home/account/settings/notifications"} exact component={withTracker(HomeSettingsNotifications)} />
                    <Route path={"/home/account/settings/functionalManagement"} exact component={withTracker(HomeSettingsFunctionalManagement)} />

                    {/* login */}
                    <Route path={"/home/account/login"} exact component={withTracker(HomeLogin)} />
                    <Route path={"/home/account/login/phoneVerify"} exact component={withTracker(HomePhoneVerify)} />
                    <Route path={"/home/account/login/phoneLogin"} exact component={withTracker(HomePhoneLogin)} />
                    
                    <Route path={"/apps"} exact component={withTracker(MiniApps)} />

                    <Route path={"/transfer"} exact component={withTracker(Transfer)} />

                    <Route path={"/takeaway"} exact component={withTracker(Restaurants)} />
                    <Route path={"/search-location"} exact component={withTracker(Location)} />
                    <Route path={"/my-location"} exact component={withTracker(GeoLocationPage)} />
                    <Route path={"/pages/:slug"} exact component={withTracker(SinglePage)} />
                    <Route path={"/pages/:restaurant/:slug"} exact component={withTracker(SinglePage)} />

                    <Route path={"/login"} exact component={withTracker(Login)} />
                    <Route path={"/register"} exact component={withTracker(Register)} />

                    <Route path={"/my-account"} exact component={withTracker(Account)} />
                    <Route path={"/my-addresses"} exact component={withTracker(Addresses)} />
                    <Route path={"/my-wallet"} exact component={withTracker(WalletPage)} />
                    <Route path={"/my-orders"} exact component={withTracker(Orders)} />

                    <Route path={"/shops"} exact component={withTracker(Shops)} />
                    {/* <Route path={"/restaurants/:restaurant"} exact component={withTracker(Items)} /> */}
                    <Route path={"/restaurants/:restaurant/:id"} exact component={withTracker(SingleItem)} />
                    <Route path={"/explore"} exact component={withTracker(Explore)} />
                    <Route path={"/about"} component={About} />

                    <Route path={"/cart"} exact component={withTracker(CartPage)} />
                    <Route path={"/checkout"} exact component={withTracker(Checkout)} />
                    <Route path={"/running-order/:unique_order_id"} exact component={withTracker(RunningOrder)} />
                    <Route path={"/running-order-s/:unique_order_id"} component={RunOrd} />

                    {/* Delivery Routes */}
                    <Route path={"/delivery"} exact component={Delivery} />
                    <Route path={"/delivery/login"} exact component={DeliveryLogin} />
                    <Route path={"/delivery/orders"} exact component={DeliveryOrders} />
                    <Route path={"/delivery/dashboard"} exact component={DeliveryDashboard} />
                    <Route path={"/delivery/earnings"} exact component={DeliveryEarnings} />
                    <Route path={"/delivery/batches"} exact component={DeliveryBatches} />
                    <Route path={"/delivery/payment-card"} exact component={DeliveryPaymentCard} />
                    <Route path={"/delivery/orders/:unique_order_id"} exact component={ViewOrder} />
                    <Route path={"/delivery/completed-orders"} exact component={Delivery} />
                    <Route path={"/shopper"} exact component={withTracker(Shopper)} />
                    <Route path={"/shopperPwa"} exact component={withTracker(ShopperPwa)} />
                    <Route path={"/viewBatch/:unique_order_id"} exact component={withTracker(ViewBatch)} />
                    <Route path={"/viewBatch2/:unique_order_id"} exact component={withTracker(ViewBatch2)} />
                    
                    {/* Store Routes */}
                    <Route path={"/store"} component={Store} />
                    <Route path={"/store/bestellungen"} component={OrderList} />
                    <Route path={"/store/bestellung"} component={OrderListView} />

                    {/* this must always be the last route */}
                    <Route path={"/:restaurant"} exact component={withTracker(Items)} />

                    {/* Common Routes */}
                    <Route component={NotFound} />
                </Switch>
            </React.Fragment>
        </BrowserRouter>
    </Root>,
    document.getElementById("root")
);