import { CHECK_RESTAURANT_OPERATION_SERVICE } from "./actionTypes";
import { CHECK_RESTAURANT_OPERATION_SERVICE_URL } from "../../configs";
import Axios from "axios";

export const checkRestaurantOperationService = (restaurant_id, latitude, longitude) => dispatch => {
    Axios.post(CHECK_RESTAURANT_OPERATION_SERVICE_URL, {
        restaurant_id: restaurant_id,
        latitude: latitude,
        longitude: longitude
    })
        .then(response => {
            const coupon = response.data;
            return dispatch({ type: CHECK_RESTAURANT_OPERATION_SERVICE, payload: coupon });
        })
        .catch(function(error) {
            console.log(error);
        });
};
