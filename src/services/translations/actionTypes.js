export const GET_ALL_LANGUAGES = "GET_ALL_LANGUAGES";
export const GET_SINGLE_LANGUAGE_DATA = "GET_SINGLE_LANGUAGE_DATA";
export const GET_TRANSLATION = "GET_TRANSLATION";
