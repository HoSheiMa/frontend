import { PLACE_ORDER, PLACE_ORDER_V2, CART_DISCOUNT } from "./actionTypes";
import { PLACE_ORDER_URL } from "../../configs";
import { PLACE_ORDER_V2_URL } from "../../configs";
import Axios from "axios";
import { updateCart } from "../total/actions";
import { loadStripe } from '@stripe/stripe-js';
import { RESTAURANT_URL } from "../../configs/website";

export const cartDiscount = (discount) => {//by freelancer on 13 Feb
    return { type: CART_DISCOUNT, payload: discount }
} 

const stripePromise = loadStripe('pk_live_6SY1GFAnsvJEYaGtOCMZhbrf006iB8fEHP');

export const placeOrderV2 = (order, coupon, location, order_comment, payment_token, delivery_type, partial_wallet, cronDay, cronTime, cronAction, user, cat, subtotal, subtotalStore, total, fromName, fromTel, country, fromAddress, fromLat, fromLng, items, delivery_charge, delivery_tip, shopper_reward, service_fee, payment_method, delivery_instructions, leaveAtDoorChecked) => (
    dispatch,
    getState
) => {
    let sessionId;
    let sofortUrl;
    let id;
    // Axios.post(PLACE_ORDER_V2_URL, {
    Axios.post(PLACE_ORDER_URL, {
        //old data
        order: order,
        coupon: coupon, 
        location: location, 
        order_comment: order_comment, 
        payment_token: payment_token, 
        delivery_type: delivery_type,
        partial_wallet: partial_wallet, 
        cronDay: cronDay, 
        cronTime: cronTime, 
        cronAction: cronAction,
        
        //new data
        user: user,
        cat: cat,
        subtotal: subtotal,
        subtotalStore: subtotalStore,
        total: total,
        fromName: fromName,
        fromTel: fromTel,
        country: country,
        fromAddress: fromAddress,
        fromLat: fromLat,
        fromLng: fromLng,
        items: items,
        delivery_charge: delivery_charge,
        delivery_tip: delivery_tip,
        shopper_reward: shopper_reward,
        service_fee: service_fee,
        payment_method: payment_method,
        delivery_instructions: delivery_instructions,
        leaveAtDoorChecked: leaveAtDoorChecked
    })
    .then(async (response) => {
        const checkout = response.data;

        if (checkout.success) {
            dispatch({ type: PLACE_ORDER, payload: checkout });

            const state = getState();
            // console.log(state);
            const cartProducts = state.cart.products;
            // const user = state.user.user;
            localStorage.removeItem("orderComment");

            for (let i = cartProducts.length - 1; i >= 0; i--) {
                // remove all items from cart
                cartProducts.splice(i, 1);
            }

            dispatch(updateCart(cartProducts));
        }

        console.log("1. step", response);
        //clear items in cart
        localStorage.removeItem("orderComment");
        localStorage.removeItem("state");
        id = response.data.id;

        // console.log("id", id);
        
        if(payment_method == "cards"){
            return Axios.get(RESTAURANT_URL+'/php/stripe/acceptCard.php?total='+total+'&redirectUrl='+window.location.origin+'/running-order-s/'+response.data.id);
        } else if(payment_method == "sofort"){
            return Axios.get(RESTAURANT_URL+'/php/stripe/acceptSofort.php?total='+total+'&redirectUrl='+window.location.origin+'/running-order-s/'+response.data.id);
        } else if(payment_method == "eps"){
            return Axios.get(RESTAURANT_URL+'/php/stripe/acceptEps.php?total='+total+'&redirectUrl='+window.location.origin+'/running-order-s/'+response.data.id+'&name='+fromName);
        }
    })
    .then((response) => {
        console.log("2. step", response);
        if(payment_method == "cards"){
            sessionId = response.data.sessionId;
            //update payment_session
            Axios.get(RESTAURANT_URL+'/php/stripe/updatePaymentSession.php?session_id='+response.data.sessionId+'&id='+id+'&fromTel='+btoa(fromTel)+'&code='+localStorage.getItem("codeCustomer"));
            // When the customer clicks on the button, redirect them to Checkout.
            return stripePromise;
        } else if(payment_method == "sofort" || payment_method == "eps"){
            sofortUrl = response.data.redirect.url;
            //update payment_source
            // console.log("source_id", response.data.id);
            return Axios.get(RESTAURANT_URL+'/php/stripe/updatePaymentSource.php?source_id='+response.data.id+'&id='+id+'&fromTel='+btoa(fromTel)+'&code='+localStorage.getItem("codeCustomer"));
        }                      
    })
    .then((stripe) => {
        console.log("3. step", stripe);
        //redirect to payment processor
        if(payment_method == "cards"){
            return stripe.redirectToCheckout({
                sessionId,
            });
        } else if(payment_method == "sofort" || payment_method == "eps"){
            window.location = sofortUrl;
        } else if(payment_method == "wallet"){
            window.location = "/running-order-s/"+id;
        }
    })
    .catch(function(error) {//this is why we get errror
        console.log("stripe error", error);
    });
};



export const placeOrder = (user, order, coupon, location, order_comment, total, method, payment_token, delivery_type, partial_wallet, fromName, fromTel, code, discount, cronDay, cronTime, cronAction) => (
    dispatch,
    getState
) => {
    Axios.post(PLACE_ORDER_URL, {
        //token: user.data.auth_token,
        user: user,
        order: order,
        coupon: coupon,
        location: location,
        order_comment: order_comment,
        total: total,
        method: method,
        payment_token: payment_token,
        delivery_type: delivery_type,
        partial_wallet: partial_wallet,
        fromName: fromName,
        fromTel: fromTel,
        code: code,
        discount: discount,
        cronDay: cronDay,
        cronTime: cronTime,
        cronAction: cronAction
    })
        .then(response => {
            const checkout = response.data;

            if (checkout.success) {
                dispatch({ type: PLACE_ORDER, payload: checkout });

                const state = getState();
                // console.log(state);
                const cartProducts = state.cart.products;
                // const user = state.user.user;
                localStorage.removeItem("orderComment");

                for (let i = cartProducts.length - 1; i >= 0; i--) {
                    // remove all items from cart
                    cartProducts.splice(i, 1);
                }

                dispatch(updateCart(cartProducts));
            }
        })
        .catch(function(error) {
            console.log(error);
        });
};
