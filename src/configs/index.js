import { RESTAURANT_URL } from "./website";
import { WEBSITE_URL } from "./website";

export const GET_SETTINGS_URL = RESTAURANT_URL + "/public/api/get-settings";
export const SEARCH_LOCATIONS_URL = RESTAURANT_URL + "/public/api/search-location";
export const GET_POPULAR_LOCATIONS_URL = RESTAURANT_URL + "/public/api/popular-geo-locations";
export const GET_PROMO_SLIDER_URL = RESTAURANT_URL + "/public/api/promo-slider";
export const GET_DELIVERY_RESTAURANTS_URL = RESTAURANT_URL + "/public/api/get-delivery-restaurants";
export const GET_SELFPICKUP_RESTAURANTS_URL = RESTAURANT_URL + "/public/api/get-selfpickup-restaurants";
export const GET_RESTAURANT_INFO_URL = RESTAURANT_URL + "/public/api/get-restaurant-info";
export const GET_RESTAURANT_INFO_BY_ID_URL = RESTAURANT_URL + "/public/api/get-restaurant-info-by-id";
export const GET_RESTAURANT_ITEMS_URL = RESTAURANT_URL + "/public/api/get-restaurant-items";
export const APPLY_COUPON_URL = RESTAURANT_URL + "/public/api/apply-coupon";
export const LOGIN_USER_URL = RESTAURANT_URL + "/public/api/login";
export const REGISTER_USER_URL = RESTAURANT_URL + "/public/api/register";
export const GET_PAGES_URL = RESTAURANT_URL + "/public/api/get-pages";
export const GET_SINGLE_PAGE_URL = RESTAURANT_URL + "/public/api/get-single-page";
export const SEARCH_RESTAURANTS_URL = RESTAURANT_URL + "/public/api/search-restaurants";
export const GET_ADDRESSES_URL = RESTAURANT_URL + "/public/api/get-addresses";
export const SAVE_ADDRESS_URL = RESTAURANT_URL + "/public/api/save-address";
export const DELETE_ADDRESS_URL = RESTAURANT_URL + "/public/api/delete-address";
export const UPDATE_USER_INFO_URL = RESTAURANT_URL + "/public/api/update-user-info";
export const PLACE_ORDER_URL = RESTAURANT_URL + "/public/api/place-order";
export const SET_DEFAULT_URL = RESTAURANT_URL + "/public/api/set-default-address";
export const GET_ORDERS_URL = RESTAURANT_URL + "/public/api/get-orders";
export const GET_PAYMENT_GATEWAYS_URL = RESTAURANT_URL + "/public/api/get-payment-gateways";
export const NOTIFICATION_TOKEN_URL = RESTAURANT_URL + "/public/api/save-notification-token";
export const SEND_OTP_URL = RESTAURANT_URL + "/public/api/send-otp";
export const VERIFY_OTP_URL = RESTAURANT_URL + "/public/api/verify-otp";
export const RAZORPAY_PAYMENT_URL = RESTAURANT_URL + "/public/api/payment/process-razor-pay/";
export const CHECK_USER_RUNNING_ORDER_URL = RESTAURANT_URL + "/public/api/check-running-order";
export const GET_ORDER_CANCEL_URL = RESTAURANT_URL + "/public/api/cancel-order";
export const GET_WALLET_TRANSACTIONS_URL = RESTAURANT_URL + "/public/api/get-wallet-transactions";
export const CHECK_RESTAURANT_OPERATION_SERVICE_URL = RESTAURANT_URL + "/public/api/check-restaurant-operation-service";
export const GET_SINGLE_ITEM_URL = RESTAURANT_URL + "/public/api/get-single-item";
export const GET_ALL_LANGUAGES_URL = RESTAURANT_URL + "/public/api/get-all-languages";
export const GET_SINGLE_LANGUAGE_DATA_URL = RESTAURANT_URL + "/public/api/get-single-language";

/* Delivery URLs */
export const LOGIN_DELIVERY_USER_URL = RESTAURANT_URL + "/public/api/delivery/login";
export const UPDATE_DELIVERY_USER_INFO_URL = RESTAURANT_URL + "/public/api/delivery/update-user-info";
export const GET_DELIVERY_ORDERS_URL = RESTAURANT_URL + "/public/api/delivery/get-delivery-orders";
export const GET_SINGLE_DELIVERY_ORDER_URL = RESTAURANT_URL + "/public/api/delivery/get-single-delivery-order";
export const SEND_DELIVERY_GUY_GPS_LOCATION_URL = RESTAURANT_URL + "/public/api/delivery/set-delivery-guy-gps-location";
export const GET_DELIVERY_GUY_GPS_LOCATION_URL = RESTAURANT_URL + "/public/api/delivery/get-delivery-guy-gps-location";
export const ACCEPT_TO_DELIVER_URL = RESTAURANT_URL + "/public/api/delivery/accept-to-deliver";
export const PICKEDUP_ORDER_URL = RESTAURANT_URL + "/public/api/delivery/pickedup-order";
export const DELIVER_ORDER_URL = RESTAURANT_URL + "/public/api/delivery/deliver-order";
export const CREATE_REFUND_URL = RESTAURANT_URL + "/public/api/delivery/refund-order";
export const DELIVER_ORDER_BY_STORE_URL = RESTAURANT_URL + "/public/api/delivery/deliver-order-by-store";
export const WALLET_TRANSACTION_URL = RESTAURANT_URL + "/public/api/delivery/wallet-transaction";

export const TEST_URL = RESTAURANT_URL + "/public/api/test";
export const ORDER_LIST_URL = RESTAURANT_URL + "/public/api/bestellungen";
export const ORDER_VIEW_URL = RESTAURANT_URL + "/public/api/bestellung";
export const ORDER_ADDONS_URL = RESTAURANT_URL + "/public/api/bestellung-addons";
export const UPDATE_ORDER = RESTAURANT_URL + "/public/api/orders/expired-order";
export const SUPERMARKET_ORDER_URL = RESTAURANT_URL + "/public/api/supermarket-order";
export const SUPERMARKET_ITEMS_URL = RESTAURANT_URL + "/public/api/supermarket-items";
export const SUPERMARKET_ORDER_ALL_URL = RESTAURANT_URL + "/public/api/supermarket-order-all";
export const SUPERMARKET_ORDER_ALL_ACCEPTED_URL = RESTAURANT_URL + "/public/api/supermarket-order-accepted-all";
export const TRANSACTION_HISTORY_URL = RESTAURANT_URL + "/public/api/transaction-history";
export const PLACE_ORDER_V2_URL = RESTAURANT_URL + "/public/api/place-order-v2";
export const SEARCH_PRODUCT_URL = RESTAURANT_URL + "/public/api/search-restaurant-items";

// paydizer
export const SEND_SMS_CODE_URL = WEBSITE_URL + "/public/api/send-sms-code";
export const GET_TRANSLATION_URL = WEBSITE_URL + "/public/api/get-translation";
